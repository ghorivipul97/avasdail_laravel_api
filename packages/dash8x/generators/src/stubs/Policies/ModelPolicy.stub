<?php

namespace App\Policies;

use App\User;
use App\{{singularClass}};
use Illuminate\Auth\Access\HandlesAuthorization;

class {{singularClass}}Policy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the {{singularSnake}}.
     *
     * @param  App\User  $user
     * @param  App\{{singularClass}}  ${{singularSnake}}
     * @return mixed
     */
    public function view(User $user, {{singularClass}} ${{singularSnake}})
    {
        return $this->update($user, ${{singularSnake}});
    }

    /**
     * Determine whether the user can create {{pluralSnake}}.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_{{pluralSnake}}');
    }

    /**
     * Determine whether the user can update the {{singularSnake}}.
     *
     * @param  App\User  $user
     * @param  App\{{singularClass}}  ${{singularSnake}}
     * @return mixed
     */
    public function update(User $user, {{singularClass}} ${{singularSnake}})
    {
        //edit_resources permission required
        if ( $user->can('edit_{{pluralSnake}}') ) {
            //edit_others_{{pluralSnake}} permission required to edit others
            return ${{singularSnake}}->user_id == $user->id || $this->editOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the {{singularSnake}}.
     *
     * @param  App\User  $user
     * @param  App\{{singularClass}}  ${{singularSnake}}
     * @return mixed
     */
    public function delete(User $user, {{singularClass}} ${{singularSnake}})
    {
        //delete_resources permission required
        if ( $user->can('delete_{{pluralSnake}}') ) {
            //delete_others_{{pluralSnake}} permission required to delete others
            return ${{singularSnake}}->user_id == $user->id || $this->deleteOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can see all {{pluralSnake}}
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('view_{{pluralSnake}}');
    }

    /**
     * Determine whether the user can edit others {{pluralSnake}}
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_others_{{pluralSnake}}') && $user->can('edit_{{pluralSnake}}');
    }

    /**
     * Determine whether the user can delete others {{pluralSnake}}
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_others_{{pluralSnake}}') && $user->can('delete_{{pluralSnake}}');
    }

    /**
     * Determine whether the user can delete own {{pluralSnake}}
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_{{pluralSnake}}');
    }

}
