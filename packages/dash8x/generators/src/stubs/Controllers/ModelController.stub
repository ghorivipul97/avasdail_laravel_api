<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\{{pluralClass}}Request;
use App\{{singularClass}};

use Illuminate\Http\Request;

class {{pluralClass}}Controller extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource({{singularClass}}::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', {{singularClass}}::class);

        $title = __('All {{pluralClass}}');
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'name';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        ${{pluralSnake}} = {{singularClass}}::orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            ${{pluralSnake}} = ${{pluralSnake}}->search($search);
            $title = __('{{pluralClass}} matching \':search\'', ['search' => $search]);
        }

        ${{pluralSnake}} = ${{pluralSnake}}->paginate(20)->appends($request->except('page'));

        return view('admin.{{pluralSlug}}.index', compact('{{pluralSnake}}', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.{{pluralSlug}}.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param {{pluralClass}}Request $request
     * @return \Illuminate\Http\Response
     */
    public function store({{pluralClass}}Request $request)
    {
        ${{singularSnake}} = new {{singularClass}}($request->all());

        ${{singularSnake}}->save();

        return redirect()->action('Admin\\{{pluralClass}}Controller@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  {{singularClass}} ${{singularSnake}}
     * @return \Illuminate\Http\Response
     */
    public function show({{singularClass}} ${{singularSnake}})
    {
        return redirect()->action('Admin\\{{pluralClass}}Controller@edit', ${{singularSnake}});
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  {{singularClass}} ${{singularSnake}}
     * @return \Illuminate\Http\Response
     */
    public function edit({{singularClass}} ${{singularSnake}})
    {
        return view('admin.{{pluralSlug}}.edit', compact('{{singularSnake}}'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param {{pluralClass}}Request $request
     * @param  {{singularClass}} ${{singularSnake}}
     * @return \Illuminate\Http\Response
     */
    public function update({{pluralClass}}Request $request, {{singularClass}} ${{singularSnake}})
    {
        ${{singularSnake}}->update($request->all());

        ${{singularSnake}}->save();

        return redirect()->action('Admin\\{{pluralClass}}Controller@edit', ${{singularSnake}});
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param {{singularClass}} ${{singularSnake}}
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy({{singularClass}} ${{singularSnake}}, Request $request)
    {
        if ( !${{singularSnake}}->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\{{pluralClass}}Controller@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('index', {{singularClass}}::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,reject,publish',
            '{{pluralSnake}}' => 'required|array',
            '{{pluralSnake}}.*' => 'exists:{{pluralSnake}},id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('{{pluralSnake}}', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', {{singularClass}}::class);

                $items = {{singularClass}}::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', {{singularClass}}::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;
        }

        return redirect()->action('Admin\\{{pluralClass}}Controller@index');
    }
}
