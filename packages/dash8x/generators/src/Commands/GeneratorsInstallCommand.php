<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\InstallAndReplaceCommand;
use Illuminate\Support\Facades\Artisan;
use SplFileInfo;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsInstallCommand extends InstallAndReplaceCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Views, Models, Controllers, etc into Laravel 5.4 project';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        if($this->option('force')) {
            $name = $this->getParsedNameInput();

            if(!$this->option('model')) {
                Artisan::call('generators:model', [
                    'name' => $name,
                    '--force' => true
                ]);

                $this->installMigration();
            }

            if(!$this->option('policy')) {
                Artisan::call('generators:policy', [
                    'name' => $name,
                    '--force' => true
                ]);
            }
            
            if(!$this->option('request')) {
                Artisan::call('generators:request', [
                    'name' => $name,
                    '--force' => true
                ]);
            }

            if(!$this->option('controller')) {
                Artisan::call('generators:controller', [
                    'name' => $name,
                    '--force' => true
                ]);
            }

            if(!$this->option('views')) {
                Artisan::call('generators:views', [
                    'name' => $name,
                    '--force' => true
                ]);
            }

            if(!$this->option('routes')) {
                Artisan::call('generators:routes', [
                    'name' => $name,
                    '--force' => true
                ]);
            }

            /*if(!$this->option('lang')) {
                Artisan::call('generators:lang', [
                    'name' => $name,
                    '--force' => true
                ]);
            }*/

            $this->info(ucfirst($name) . ' files successfully installed.');

            return true;
        }

        $this->info('Use `-f` flag first.');

        return true;
    }

    /**
     * Install Migration.
     *
     * @return bool
     */
    public function installMigration()
    {
        $name = $this->getParsedNameInput();

        $migrationDir = base_path() . '/database/migrations/';
        $migrationName = 'create_' . str_plural(snake_case($name)) .'_table.php';
        $migrationStub = new SplFileInfo(__DIR__ . '/../stubs/Model/migration.stub');

        $files = $this->files->allFiles($migrationDir);

        foreach ($files as $file) {
            if(str_contains($file->getFilename(), $migrationName)) {
                $this->putFile($file->getPathname(), $migrationStub);

                return true;
            }
        }

        $path = $migrationDir . date('Y_m_d_His') . '_' . $migrationName;
        $this->putFile($path, $migrationStub);

        return true;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force override existing files'],
            ['policy', null, InputOption::VALUE_NONE, 'Exclude policy'],
            ['controller', null, InputOption::VALUE_NONE, 'Exclude controller'],
            ['request', null, InputOption::VALUE_NONE, 'Exclude request'],
            ['model', null, InputOption::VALUE_NONE, 'Exclude model and migration'],
            ['views', null, InputOption::VALUE_NONE, 'Exclude views'],
            ['routes', null, InputOption::VALUE_NONE, 'Exclude routes'],
            ['lang', null, InputOption::VALUE_NONE, 'Exclude language files'],
        ];
    }
}
