<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\InstallFilesCommand;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsRequestInstallCommand extends InstallFilesCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Request';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getFiles()
    {
        $name = $this->getPluralClassNameInput();

        return [
            'model' => [
                'path' => '/app/Http/Requests/' . $name .'Request.php',
                'stub' => __DIR__ . '/../stubs/Requests/ModelRequest.stub',
            ],
        ];
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getPluralClassNameInput()
    {
        return ucfirst(camel_case(str_plural($this->getNameInput())));
    }
}
