<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\InstallAndReplaceCommand;
use Symfony\Component\Console\Input\InputOption;
use SplFileInfo;


class GeneratorsViewsInstallCommand extends InstallAndReplaceCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:views';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install generators views';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $this->installViews();
    }

    /**
     * Install Web Routes.
     *
     * @return bool
     */
    public function installViews()
    {
        $name = $this->getPluralSlugNameInput();

        $path = '/resources/views/admin/' . $name . '/';
        $views = __DIR__ . '/../stubs/views/model';

        if($this->installFiles($path, $this->files->allFiles($views))) {
            $this->info('Copied: ' . $path);
        }

        //add model base view
        $view_stub = __DIR__ . '/../stubs/views/model.blade.stub';
        $full_view_path = base_path() . $path . $name . '.blade.php';
        $stub_file_object = new SplFileInfo($view_stub);

        if($this->putFile($full_view_path, $stub_file_object)) {
            $this->getInfoMessage($full_view_path);
        }
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getPluralSlugNameInput()
    {
        return str_slug(str_plural($this->getNameInput()));
    }

    /**
     * Get file extension.
     *
     * @param $file
     * @return bool
     */
    protected function getExtension($file)
    {
        return 'php';
    }
}
