<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\AppendContentCommand;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsRoutesInstallCommand extends AppendContentCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:routes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Routes';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getSettings()
    {
        return [
            'admin_routes' => [
                'path' => '/routes/admin.php',
                'search' => 'Route::resource(\'users\', \'UsersController\');'."\n\n",
                'stub' => __DIR__ . '/../stubs/routes/admin.stub',
                'prefix' => false,
            ],
            'admin_sidebar_links' => [
                'path' => '/resources/views/admin/partials/sidebar.blade.php',
                'search' => '$menu_items = [',
                'stub' => __DIR__ . '/../stubs/views/sidebar-links.blade.stub',
                'prefix' => false,
            ],
        ];
    }
}
