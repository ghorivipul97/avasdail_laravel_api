<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\InstallFilesCommand;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsControllerInstallCommand extends InstallFilesCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Controller';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getFiles()
    {
        $name = $this->getPluralClassNameInput();

        return [
            'model' => [
                'path' => '/app/Http/Controllers/Admin/' . $name .'Controller.php',
                'stub' => __DIR__ . '/../stubs/Controllers/ModelController.stub',
            ],
        ];
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getPluralClassNameInput()
    {
        return ucfirst(camel_case(str_plural($this->getNameInput())));
    }
}
