<?php

namespace Dash8x\Generators\Commands;

use Symfony\Component\Console\Input\InputOption;


class GeneratorsPolicyInstallCommand extends InstallFilesAndAppendContentCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:policy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Policy';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getSettings()
    {
        return [
            'add_permissions' => [
                'path' => '/database/seeds/PermissionsSeeder.php',
                'search' => 'protected $data = ['."\n",
                'stub' => __DIR__ . '/../stubs/seeds/add-permissions.stub',
                'prefix' => false,
            ],
            'register_permissions_to_roles' => [
                'path' => '/database/seeds/RolesSeeder.php',
                'search' => '\'admin\' => [\'description\' => \'Administrator\', \'permissions\' => ['."\n",
                'stub' => __DIR__ . '/../stubs/seeds/register-permissions-to-roles.stub',
                'prefix' => false,
            ],
            'register_policy' => [
                'path' => '/app/Providers/AuthServiceProvider.php',
                'search' => 'protected $policies = ['."\n",
                'stub' => __DIR__ . '/../stubs/Providers/register-policy.stub',
                'prefix' => false,
            ],
            'register_policy_use' => [
                'path' => '/app/Providers/AuthServiceProvider.php',
                'search' => 'use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;'."\n",
                'stub' => __DIR__ . '/../stubs/Providers/register-policy-use.stub',
                'prefix' => false,
            ],
        ];
    }

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getFiles()
    {
        $name = $this->getSingularClassNameInput();

        return [
            'model' => [
                'path' => '/app/Policies/' . $name .'Policy.php',
                'stub' => __DIR__ . '/../stubs/Policies/ModelPolicy.stub',
            ],
        ];
    }
}
