<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\AppendContentCommand;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsLangInstallCommand extends AppendContentOnceCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:lang';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Translations';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getSettings()
    {
        return [
            'dv' => [
                'path' => '/resources/lang/dv.json',
                'search' => '{',
                'stub' => __DIR__ . '/../stubs/lang/dv.stub',
                'prefix' => false,
            ],
        ];
    }
}
