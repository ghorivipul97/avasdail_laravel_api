<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\InstallFilesCommand;
use Symfony\Component\Console\Input\InputOption;


class GeneratorsModelInstallCommand extends InstallFilesAndAppendContentCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generators:model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Model';

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getFiles()
    {
        $name = $this->getSingularClassNameInput();

        return [
            'model' => [
                'path' => '/app/' . $name .'.php',
                'stub' => __DIR__ . '/../stubs/Model/Model.stub',
            ],
        ];
    }

    /**
     * Get the destination path.
     *
     * @return string
     */
    public function getSettings()
    {
        return [
            'register_morph_map' => [
                'path' => '/app/Providers/AppServiceProvider.php',
                'search' => 'Relation::morphMap(['."\n",
                'stub' => __DIR__ . '/../stubs/Providers/register-morph-map.stub',
                'prefix' => false,
            ],
        ];
    }
}
