<?php

namespace Dash8x\Generators\Commands;

use Hesto\Core\Commands\AppendContentCommand;
use Illuminate\Filesystem\Filesystem;
use SplFileInfo;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


abstract class InstallFilesAndAppendContentCommand extends AppendContentCommand
{

    /**
     * Get the destination path.
     *
     * @return string
     */
    abstract function getFiles();

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $files = $this->getFiles();

        foreach ($files as $file) {
            $path = $file['path'];
            $fullPath = base_path() . $path;

            $fileObject = new SplFileInfo($file['stub']);

            if($this->putFile($fullPath, $fileObject)) {
                $this->getInfoMessage($fullPath);
            }
        }

        $settings = $this->getSettings();

        foreach ($settings as $setting) {
            $path = $setting['path'];
            $fullPath = base_path() . $path;

            if ($this->putContent($fullPath, $this->compileContent($fullPath, $setting))) {
                $this->getInfoMessage($fullPath);
            }

        }

        return true;
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getPluralClassNameInput()
    {
        return ucfirst(camel_case(str_plural($this->getNameInput())));
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getSingularClassNameInput()
    {
        return ucfirst(camel_case(str_singular($this->getNameInput())));
    }
}
