<?php

namespace Tests;

use App\Customer;
use App\Helpers\Enums\CustomerStatuses;
use App\Helpers\Enums\UserStatuses;
use App\Store;
use App\User;
use Illuminate\Foundation\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;
use ReflectionObject;
use Spatie\Permission\PermissionRegistrar;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Initialize the db
     */
    protected function seedDatabase()
    {
        $this->seed(\PermissionsSeeder::class);
        $this->seed(\RolesSeeder::class);
        $this->seed(\DefaultUsersSeeder::class);
    }

    /**
     * Reloaded the permissions
     * @return mixed
     */
    protected function reloadPermissions()
    {
        return app(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Acting as a specific user
     * @param $username
     * @param null $guard
     */
    protected function actingAsUser($username, $guard = null)
    {
        $this->seedDatabase();
        $this->reloadPermissions();

        //find the user
        $user = User::whereUsername($username)->first();

        //if missing, create
        if ( !$user ) {
            $user = factory(User::class)->create([
                'username' => $username,
                'status' => UserStatuses::ACTIVE,
            ]);
        }

        $this->actingAs($user, $guard);
    }

    /**
     * Acting as admin
     */
    protected function actingAsAdmin()
    {
        $this->actingAsUser('admin_user');
    }

    /**
     * Acting as a specific API user
     * @param $username
     * @param array $scopes
     */
    protected function actingAsApiUser($username, $scopes = [])
    {
        $this->seedDatabase();
        $this->reloadPermissions();

        //find the user
        $user = User::whereUsername($username)->first();

        //if missing, create
        if ( !$user ) {
            $user = factory(User::class)->create([
                'username' => $username,
                'status' => UserStatuses::ACTIVE,
            ]);
        }

        Passport::actingAs($user, $scopes);
    }

    /**
     * Acting as admin
     * @param array $scopes
     */
    protected function actingAsApiAdmin($scopes = ['read', 'write'])
    {
        $this->actingAsApiUser('admin_user', $scopes);
    }
    
}
