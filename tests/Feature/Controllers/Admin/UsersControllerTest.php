<?php

namespace Tests\Feature\Controllers\Admin;

use App\Helpers\Enums\UserStatuses;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UsersControllerTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /** @test */
    public function it_can_list_users()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->create();

        $response = $this->get('/admin/users');

        $response->assertSee($user->name);
    }

    /** @test */
    public function it_can_create_a_new_user()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->make();
        $password = '123456';

        $response = $this->post('/admin/users', [
            'name' => $user->name,
            'email' => $user->email,
            'status' => UserStatuses::ACTIVE,
            'password' => $password,
            'password_confirmation' => $password,
        ]);

        $response->assertSessionMissing('errors');

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
            'status' => UserStatuses::ACTIVE,
        ]);
    }

    /** @test */
    public function it_can_update_a_user()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->create([
            'name' => 'Foo'
        ]);

        $response = $this->patch("/admin/users/{$user->id}", [
            'name' => 'Bar',
        ]);

        $response->assertSessionMissing('errors');

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => 'Bar',
        ]);
    }

    /** @test */
    public function it_can_show_a_user()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->create();

        $response = $this->get("/admin/users/{$user->id}")
                    ->assertRedirect("/admin/users/{$user->id}/edit");

        $response = $this->get($response->headers->get('Location'));

        $response->assertSee($user->name);
    }

    /** @test */
    public function it_cannot_add_an_unauthorized_user()
    {
        $this->actingAsStoreStaff();

        $user = factory(User::class)->make();

        $response = $this->patch('/admin/users', [
            'name' => $user->name,
        ]);

        $response->assertStatus(403);

        $this->assertDatabaseMissing('users', [
            'name' => $user->name,
        ]);
    }

    /** @test */
    public function it_cannot_update_an_unauthorized_user()
    {
        $this->actingAsStoreOwner();

        $user = factory(User::class)->create([
            'name' => 'Foo'
        ]);

        $response = $this->patch("/admin/users/{$user->id}", [
            'name' => 'Bar',
        ]);

        $response->assertStatus(403);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'name' => 'Bar',
        ]);
    }

    /** @test */
    public function it_can_delete_a_user()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->create();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'deleted_at' => null,
        ]);

        $response = $this->delete("/admin/users/{$user->id}");

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'deleted_at' => null,
        ]);
    }

    /** @test */
    public function it_can_bulk_delete_users()
    {
        $this->actingAsAdmin();

        $users = factory(User::class, 3)->create();

        $users = $users->map(function ($user) {
            return collect($user->toArray())
                ->only('id')
                ->all();
        });

        $users->each(function ($user) {
            $this->assertDatabaseHas('users', $user);
        });

        $users_to_delete = $users->slice(1);

        $response = $this->patch('/admin/users', [
            'users' => $users_to_delete->pluck('id')->all(),
            'action' => 'delete',
        ]);

        $response->assertSessionMissing('errors');

        $users_to_delete->each(function ($user) {
            //$this->assertTrue( User::find($user['id']) );
            $this->assertDatabaseMissing('users', array_merge($user, ['deleted_at' => null]));
        });
    }

    /** @test */
    public function it_can_validate_inputs()
    {
        $this->actingAsAdmin();

        $user = factory(User::class)->make();

        $response = $this->post('/admin/users', [
            'name' => '',
        ]);

        $response->assertSessionHasErrors(['name']);

        $this->assertDatabaseMissing('users', [
            'name' => $user->name,
        ]);
    }

    /** @test */
    public function it_can_validate_notification_preferences()
    {
        $this->actingAsAdmin();

        $user = Auth::user();

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => '',
        ]);

        $response->assertSessionHasErrors(['preferences']);

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => [
                ['type' => 'abcd']
            ],
        ]);

        $response->assertSessionHasErrors(['preferences.0.type']);

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => [
                ['type' => 'new_merchant_ticket', 'notify' => 'x'],
                ['type' => 'abcd']
            ],
        ]);

        $response->assertSessionHasErrors([
            'preferences.0.notify',
            'preferences.1.type'
        ]);

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => [
                ['type' => 'new_merchant_ticket']
            ],
        ]);

        $response->assertSessionMissing('errors');
    }

    /** @test */
    public function it_can_add_notification_preferences_to_users()
    {
        $this->actingAsAdmin();

        $user = Auth::user();

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => [
                ['type' => 'new_merchant_ticket', 'notify' => true, 'mail' => true]
            ],
            'role' => 'admin',
        ]);

        $response->assertSessionMissing('errors');

        $this->assertDatabaseHas('notification_preferences', [
            'notifiable_id' => $user->id,
            'notifiable_type' => 'user',
            'type' => 'new_merchant_ticket',
            'mail' => true,
            'sms' => false,
        ]);

        $response = $this->patch("/admin/users/{$user->id}", [
            'preferences' => [
                ['type' => 'new_merchant_ticket', 'notify' => false, 'mail' => true]
            ],
        ]);

        $response->assertSessionMissing('errors');

        $this->assertDatabaseMissing('notification_preferences', [
            'notifiable_id' => $user->id,
            'notifiable_type' => 'user',
            'type' => 'new_merchant_ticket',
        ]);
    }
}