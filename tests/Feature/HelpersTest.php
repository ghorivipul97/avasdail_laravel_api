<?php

namespace Tests\Feature;

use App\Customer;
use App\Helpers\Address\Address;
use App\Helpers\Address\Country;
use App\Helpers\Address\State;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelpersTest extends TestCase
{

    use DatabaseMigrations, DatabaseTransactions;

    /** @test */
    public function it_can_set_and_get_the_location_from_the_session()
    {
        $country = factory(Country::class)->create();

        session_location($country);

        $response = $this->get('/');
        $response->assertSessionHas('location', [
            'id' => $country->id,
            'type' => 'country',
        ]);

        $location = session_location();

        $this->assertEquals($country->id, $location->id, 'Wrong location id');
        $this->assertEquals('country', $location->getMorphClass(), 'Wrong location type');
    }

    /** @test */
    public function it_can_get_the_location_from_logged_in_customer()
    {
        $this->actingAsCustomer();
        $customer = Auth::user();

        $other_country = factory(Country::class)->create();
        $country = factory(Country::class)->create();
        $address = factory(Address::class)->make([
            'country_id' => $country->id,
            'state_id' => null,
            'city_id' => null,
        ]);

        $customer->addresses()->save($address);

        $response = $this->get('/');
        $this->assertLocation($country->id, 'country', $response);
    }
}
