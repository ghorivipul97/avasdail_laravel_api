<?php

namespace Tests\Feature\Models;

use App\Store;
use App\User;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /**
     * Initialize the db
     */
    protected function seedDatabase()
    {
        parent::seedDatabase();

        $this->seed(\DefaultStoresSeeder::class);
    }

    /** @test */
    public function it_has_a_name()
    {
        $user = factory(User::class)->create(['name' => 'John Doe']);

        $this->assertEquals('John Doe', $user->name);
    }

    /** @test */
    public function admin_can_add_a_user_to_a_store()
    {
        $this->actingAsAdmin();

        $store = factory(Store::class)->create();
        $user = factory(User::class)->create();

        $user->updateStore($store->id);
        $user->save();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'store_id' => $store->id,
        ]);
    }

    /** @test */
    public function staff_can_add_a_user_to_a_store()
    {
        $this->actingAsStaff();

        $store = factory(Store::class)->create();
        $user = factory(User::class)->create();

        $user->updateStore($store->id);
        $user->save();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'store_id' => $store->id,
        ]);
    }

    /** @test */
    public function store_owner_can_add_a_user_to_a_store()
    {
        $this->actingAsStoreOwner();

        $store = Auth::user()->ownStore;
        $user = factory(User::class)->create();

        $user->updateStore();
        $user->save();

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'store_id' => $store->id,
        ]);

        $user->updateStore(null);
        $user->save();

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'store_id' => null,
        ]);
    }

    /** @test */
    public function store_owner_cannot_add_a_user_to_another_store()
    {
        $this->actingAsStoreOwner();

        $store = factory(Store::class)->create();
        $user = factory(User::class)->create();

        $user->updateStore($store->id);
        $user->save();

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'store_id' => $store->id,
        ]);
    }

    /** @test */
    public function store_staff_cannot_add_a_user_to_a_store()
    {
        $this->actingAsStoreStaff();

        $store = factory(Store::class)->create();
        $user = factory(User::class)->create();

        $user->updateStore($store->id);
        $user->save();

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'store_id' => $store->id,
        ]);
    }
}
