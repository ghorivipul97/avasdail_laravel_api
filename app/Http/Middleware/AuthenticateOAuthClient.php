<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Exceptions\MissingScopeException;
use Laravel\Passport\Http\Middleware\CheckClientCredentials;

class AuthenticateOAuthClient
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$scopes
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$scopes)
    {
        try {

            //first try normal authentication
            return app(Authenticate::class)->handle($request, function ($request) use ($next, $scopes) {

                //check for scopes
                foreach ($scopes as $scope) {
                    if (! $request->user()->tokenCan($scope)) {
                        throw new MissingScopeException($scope);
                    }
                }

                return $next($request);

            }, 'api');

        } catch (AuthenticationException $e) {

            //authentication failed, try client auth
            return app(CheckClientCredentials::class)->handle($request, $next, ...$scopes);

        }
    }
}
