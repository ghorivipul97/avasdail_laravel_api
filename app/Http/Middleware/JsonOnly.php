<?php namespace App\Http\Middleware;

use App\Exceptions\JsonOnlyException;
use Closure;

class JsonOnly {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if ($request->method() != 'GET' && !$request->isJson())
        {
            throw new JsonOnlyException('Only JSON requests allowed');
        }

		return $next($request);
	}

}
