<?php

namespace App\Http\Requests;

use App\Category;
use App\Helpers\Enums\QuestionTypes;
use Illuminate\Foundation\Http\FormRequest;

class QuestionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:255',
            'type' => 'required|integer|in:' . implode(QuestionTypes::getKeys(), ','),
            'required' => 'boolean',
            'options' => 'array|required_if:type,'. implode(QuestionTypes::mcqTypes(), ','),
            'options.*' => 'required|string|max:255',
            'menu_order' => 'integer|min:0|max:9999999',
            'allow_other' => 'boolean',
        ];

        return $rules;
    }
}
