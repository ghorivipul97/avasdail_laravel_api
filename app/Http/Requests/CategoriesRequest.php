<?php

namespace App\Http\Requests;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'parent_id' => 'exists:categories,id',
            'category_image' => 'mimes:jpeg,jpg,png|max:'.get_setting('max_upload_file_size'),

        ];

        if ( $category = $this->route('category') ) {
            //prevent circular parent selection
            $not_in = Category::descendantsOf($category)->pluck('id')->all();
            $not_in[] = $category->id;
            $not_in = implode(',', $not_in );
            $rules['parent_id'] .= '|not_in:'.$not_in;
        }

        return $rules;
    }
}
