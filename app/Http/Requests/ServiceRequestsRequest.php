<?php

namespace App\Http\Requests;

use App\Category;
use App\Helpers\Enums\BusinessStatuses;
use App\Helpers\Enums\ServiceRequestStatuses;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class ServiceRequestsRequest extends FormRequest
{

    protected $category, $questions;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the category
     */
    public function getCategory()
    {
        if ( empty($this->category) ) {
            if ( $category_id = $this->input('category') ) {
                $this->category = Category::find($category_id);
            } else {
                $this->category = null;
            }
        }

        return $this->category;
    }

    /**
     * Get the questions
     */
    public function getQuestions()
    {
        if ( empty($this->questions) ) {
            if ( $category = $this->getCategory() ) {
                $this->questions = $category->allQuestions()->get();
            }
        }

        return $this->questions;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category' => 'required|exists:categories,id',
            'user' => 'exists:users,id',
            'business' => 'exists:businesses,id',
            'businesses' => 'required|array',
            'businesses.*' => 'exists:businesses,id,status,'.BusinessStatuses::APPROVED,
            'accepted_at' => 'date',
            'note' => 'string',
            'answers' => 'array',
            'status' => 'integer|in:'.implode(',', ServiceRequestStatuses::getKeys()),
            'skip_questions' => 'boolean',
        ];

        $required = !$this->has('skip_questions');

        if ( $questions = $this->getQuestions() ) {
            foreach ( $questions as $question ) {
                $rules['answers.'.$question->id] = $question->validationRules($required);
            }
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [];

        if ( $questions = $this->getQuestions() ) {
            foreach ( $questions as $question ) {
                $attributes['answers.'.$question->id] = '"'.$question->title.'"';
            }
        }

        return $attributes;
    }
}
