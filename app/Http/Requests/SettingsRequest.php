<?php

namespace App\Http\Requests;

use App\Http\Controllers\Admin\SettingsController;
use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'app_name' => 'required|string|max:255',
            'max_upload_file_size' => 'required|integer|min:1',
        ];

        foreach (SettingsController::$files as $file) {
            $rules[$file] = 'mimes:jpeg,jpg,png|max:'.get_setting('max_upload_file_size');
        }

        return $rules;
    }
}
