<?php

namespace App\Http\Requests;

use App\Helpers\Enums\BusinessStatuses;
use Illuminate\Foundation\Http\FormRequest;

class BusinessesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'sometimes|required|string|max:255',
            'address' => 'string',
            'city' => 'exists:cities,id',
            'user' => 'exists:users,id',
            'website' => 'url',
            'email' => 'email',
            'status' => 'integer|in:'.implode(',', BusinessStatuses::getKeys()),
            'logo' => 'mimes:jpeg,jpg,png|max:'.get_setting('max_upload_file_size'),
            'approve' => 'boolean',
            'categories' => 'array|ids_exist:categories,id',
        ];

        return $rules;
    }
}
