<?php

namespace App\Http\Requests;

use App\Helpers\Enums\ReviewStatuses;
use Illuminate\Foundation\Http\FormRequest;

class ReviewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'content' => 'string',
            'rating' => 'required|integer|min:0|max:5',
            'business' => 'required|exists:businesses,id',
            'user' => 'exists:users,id',
            'status' => 'integer|in:'.implode(',', ReviewStatuses::getKeys()),
            'approve' => 'boolean',
        ];

        if ( $review = $this->route('review') ) {
            $rules['business'] .= '|sometimes';
            $rules['rating'] .= '|sometimes';
        }

        return $rules;
    }
}
