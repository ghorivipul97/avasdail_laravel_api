<?php

namespace App\Http\Requests;

use App\Helpers\Enums\BusinessStatuses;
use App\Helpers\Enums\OAuthClientStatuses;
use Illuminate\Foundation\Http\FormRequest;

class OAuthClientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'user' => 'exists:users,id',
            'redirect' => 'url',
            'email' => 'email',
            'status' => 'boolean|in:'.implode(',', OAuthClientStatuses::getKeys()),
            'revoke' => 'boolean',
            'regenerate' => 'boolean',
            'personal_access_client' => 'boolean',
            'password_client' => 'boolean',
        ];

        return $rules;
    }
}
