<?php

namespace App\Http\Requests;

use App\Helpers\Enums\UserStatuses;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'username' => 'string|min:4|max:255|required|slug:_|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'name' => 'required|max:255|string',
            'role' => 'exists:roles,name',
            'password' => 'min:6|confirmed',
            'status' => 'in:' . implode(UserStatuses::getKeys(), ','),
            'avatar' => 'mimes:jpeg,jpg,png|max:'.get_setting('max_upload_file_size'),
            'phone_id' => [
                Rule::exists('mobile_numbers', 'id')->where(function ($query) {
                    $query->whereNull('user_id');
                }),
            ],
            'mobile_number' => [
                'mobile',
                Rule::unique('mobile_numbers', 'number')->where(function ($query) {
                    $query->whereNotNull('user_id');
                }),
            ],
        ];

        if ( $this->route()->getName() == 'api.users.store' ) {
            $rules['code'] = 'required';
            $rules['phone_id'][] = 'required';
        }

        if ( ($user = $this->route('user')) || $this->route()->getName() == 'api.users.update' ) {

            if ( empty($user) ) {
                $user = $this->user();
            }

            $rules['name'] .= '|sometimes';
            $rules['username'] .= ',username,' . $user->id . '|sometimes';
            $rules['email'] .= ',email,' . $user->id.'|sometimes';
            $rules['current_password'] = 'required_with:password|passcheck:users,password,id,' . $user->id;

            $rules['phone_id'] = [
                Rule::exists('mobile_numbers', 'id')->where(function ($query) use ($user) {
                    $query->whereNull('user_id')
                        ->orWhere('user_id', '=', $user->id);
                }),
            ];

            $rules['mobile_number'] = [
                'mobile',
                Rule::unique('mobile_numbers', 'number')->where(function ($query) use($user) {
                    $query->whereNotNull('user_id')
                        ->where('user_id', '!=', $user->id);
                }),
            ];
        } else {
            $rules['password'] .= '|required';
        }

        return $rules;
    }
}
