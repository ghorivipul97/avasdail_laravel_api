<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'max:255|required|slug:_|unique:roles',
            'description' => 'max:255|required',
            'permissions' => 'array|ids_exist:permissions'
        ];

        if ( $role = $this->route('role') ) {
            $rules['name'] .= ',name,' . $role->id;
        }

        return $rules;
    }
}
