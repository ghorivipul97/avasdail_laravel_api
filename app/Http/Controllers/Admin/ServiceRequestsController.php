<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Category;
use App\Helpers\AllowedOrdersTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequestsRequest;
use App\ServiceRequest;

use Illuminate\Http\Request;

class ServiceRequestsController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        //$this->authorizeResource(ServiceRequest::class);
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'created_at' => __('Created At'),
            'updated_at' => __('Updated At'),
            'accepted_at' => __('Accepted At'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', ServiceRequest::class);

        $title = __('All ServiceRequests');
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'created_at';
        $order = $request->order == 'ASC' ? $request->order : 'DESC';

        $service_requests = ServiceRequest::orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $service_requests = $service_requests->search($search);
            $title = __('ServiceRequests matching \':search\'', ['search' => $search]);
        }

        $service_requests = $service_requests->paginate(20)->appends($request->except('page'));

        return view('admin.service-requests.index', compact('service_requests', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ServiceRequest::class);

        $this->validate($request, [
            'category' => 'sometimes|required|exists:categories,id',
        ]);

        $category = null;
        $questions = null;
        if ( $category_id = $request->input('category') ) {
            $category = Category::find($category_id);
            if ( $category ) {
                $questions = $category->allQuestions()->get();
            }
        }

        return view('admin.service-requests.create', compact('category', 'questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ServiceRequestsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequestsRequest $request)
    {
        $this->authorize('create', ServiceRequest::class);

        $service_request = new ServiceRequest($request->all());

        $user = $request->input('user');
        $category = $request->input('category');

        $service_request->updateUser($user);
        $service_request->category()->associate($category);
        $service_request->skip_questions = $request->has('skip_questions');

        $service_request->save();

        //save all the answers
        if ( !$service_request->skip_questions ) {
            $questions = $service_request->questions;
            foreach ( $questions as $question ) {
                //first set the question, then the content
                $answer = new Answer();
                $answer->question()->associate($question);

                $content = $request->input('answers.'.$question->id, '');
                $answer->content = $content;

                $service_request->answers()->save($answer);

                //save file
                if ( $question->is_file ) {
                    $answer->updateSingleMedia('default', $request, 'answers.'.$question->id);
                }
            }
        }

        return redirect()->action('Admin\\ServiceRequestsController@edit', $service_request);
    }

    /**
     * Display the specified resource.
     *
     * @param  ServiceRequest $service_request
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceRequest $service_request)
    {
        $this->authorize('view', $service_request);

        return redirect()->action('Admin\\ServiceRequestsController@edit', $service_request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ServiceRequest $service_request
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceRequest $service_request)
    {
        $this->authorize('update', $service_request);

        $category = $service_request->category;
        $questions = $category->allQuestions()->get();

        return view('admin.service-requests.edit', compact('service_request', 'category', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequestsRequest $request
     * @param  ServiceRequest $service_request
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequestsRequest $request, ServiceRequest $service_request)
    {
        $this->authorize('update', $service_request);

        $service_request->update($request->all());

        $user = $request->input('user');

        $service_request->updateUser($user);
        $service_request->skip_questions = $request->has('skip_questions');

        //save all the answers
        if ( !$service_request->skip_questions ) {
            $questions = $service_request->questions;
            foreach ( $questions as $question ) {
                //find the answer
                $answer = $service_request->getAnswer($question->id);

                $content = $request->input('answers.'.$question->id, '');
                $answer->update(compact('content'));

                //save file
                if ( $question->is_file ) {
                    $answer->updateSingleMedia('default', $request, 'answers.'.$question->id);
                }
            }
        }

        $service_request->save();

        return redirect()->action('Admin\\ServiceRequestsController@edit', $service_request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ServiceRequest $service_request
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceRequest $service_request, Request $request)
    {
        $this->authorize('delete', $service_request);

        if ( !$service_request->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\ServiceRequestsController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('index', ServiceRequest::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,reject,publish',
            'service_requests' => 'required|array',
            'service_requests.*' => 'exists:service_requests,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('service_requests', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', ServiceRequest::class);

                $items = ServiceRequest::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', ServiceRequest::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;
        }

        return redirect()->action('Admin\\ServiceRequestsController@index');
    }
}
