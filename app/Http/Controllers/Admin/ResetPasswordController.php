<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function broker()
    {
        return Password::broker('admins');
    }

    protected function resetPassword($user, $password)
    {
        $user->fill(['password' => bcrypt($password)])->save();
    }

    public function redirectTo()
    {
        return route('admin.login');
    }
}
