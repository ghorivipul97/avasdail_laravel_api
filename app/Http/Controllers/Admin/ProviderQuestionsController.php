<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Requests\QuoteQuestionRequest;
use App\Http\Controllers\Controller;
use App\Models\ServiceProviderQuestion;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ProviderQuestionsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(ServiceProviderQuestion::all())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/provider-questions/'.$modal->getKey().'/edit'),
                        'deleteUrl' => url('/admin/provider-questions/'.$modal->getKey())
                    ])->render();
                })
                ->editColumn('is_required', function($model) {
                    return $model->is_required ? 'YES' : 'NO';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.provider-questions.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.provider-questions.create');
    }

    /**
     * @param QuoteQuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(QuoteQuestionRequest $request)
    {
        ServiceProviderQuestion::create($request->persist());
        session()->flash('success', 'Question created successfully');

        return redirect()->route('admin.provider-questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $question = ServiceProviderQuestion::findorfail($id);

        return view('admin.provider-questions.edit', compact('question'));
    }

    /**
     * @param QuoteQuestionRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(QuoteQuestionRequest $request, $id)
    {
        $question = ServiceProviderQuestion::findorfail($id);
        $question->fill($request->persist())->save();
        session()->flash('success', 'Question updated successfully.');

        return redirect()->route('admin.provider-questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $question = ServiceProviderQuestion::findorfail($id);
        $question->delete();
        session()->flash('success', 'Question deleted successfully.');

        return redirect()->back();
    }
}
