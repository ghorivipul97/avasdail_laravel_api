<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReportData;
use Yajra\Datatables\Facades\Datatables;

class ReportsController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(ReportData::all())
                ->addIndexColumn()
                ->addColumn('show', function ($modal) {
                    return view('admin.reports._actions', [
                        'showUrl' => url('/admin/report/'.$modal->getKey().'/show'),
                    ])->render();
                })
                ->rawColumns(['show'])
                ->make(true);
        }

        return view('admin.reports.index');
    }

    public function show($id)
    {
        $report = ReportData::findorfail($id);

        return view('admin.reports.show', compact('report'));
    }
}
