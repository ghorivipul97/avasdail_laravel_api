<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SignupQuestion;
use Yajra\Datatables\Facades\Datatables;
use App\Domain\Admin\Requests\SignupQuestionRequest;

class SignupQuestionsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(SignupQuestion::all())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/signup_questions/' . $modal->getKey() . '/edit'),
                        'deleteUrl' => url('/admin/signup_questions/'.$modal->getKey()),
                    ])->render();
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.signupquestions.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.signupquestions.create');
    }

    /**
     * @param SignupQuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SignupQuestionRequest $request)
    {
        SignupQuestion::create($request->persist());
        session()->flash('success', 'Question created successfully');

        return redirect()->route('admin.signup_questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $question = SignupQuestion::findorfail($id);

        return view('admin.signupquestions.edit', compact('question'));
    }

    /**
     * @param SignupQuestionRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SignupQuestionRequest $request, $id)
    {
        $question = SignupQuestion::findorfail($id);
        $question->fill($request->persist())->save();
        session()->flash('success', 'Question updated successfully.');

        return redirect()->route('admin.signup_questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $question = SignupQuestion::findorfail($id);
        $question->delete();
        session()->flash('success', 'Question deleted successfully.');

        return redirect()->back();
    }
}
