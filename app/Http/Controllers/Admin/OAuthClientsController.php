<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Http\Controllers\Controller;

use App\Http\Requests\OAuthClientsRequest;
use Illuminate\Http\Request;

use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;

class OAuthClientsController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * The client repository instance.
     *
     * @var ClientRepository
     */
    protected $clients;

    /**
     * Create a client controller instance.
     *
     * @param  ClientRepository  $clients
     * @return void
     */
    public function __construct(ClientRepository $clients)
    {
        $this->clients = $clients;
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'created_at' => __('Created At'),
            'updated_at' => __('Updated At'),
            'name' => __('Name'),
            'id' => __('ID'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Client::class);

        $title = 'All OAuth Clients';
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'name';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        $oauth_clients = Client::orderBy($orderby, $order);
        if ( !$request->user()->can('editOthers', Client::class) ) {
            $oauth_clients->whereUserId( $request->user()->getKey() );
        }
            
        $search = null;
        if ( $search = $request->input('search') ) {
            $oauth_clients->where('name', 'like', '%'.$search.'%');
            $title = __('OAuth Clients matching \':search\'', ['search' => $search]);
        }

        if ( $request->has('status') ) {
            $status = $request->input('status');
            $oauth_clients->whereRevoked($status);
        }

        if ( $user = $request->input('user') ) {
            $oauth_clients->whereUserId($user);
        }

        if (  $request->has('personal_access')  ) {
            $personal_access = $request->input('personal_access');
            $oauth_clients->wherePersonalAccessClient($personal_access);
        }

        if (  $request->has('password')  ) {
            $password = $request->input('password');
            $oauth_clients->wherePasswordClient($password);
        }

        $oauth_clients = $oauth_clients->paginate(20)
            ->appends($request->except('page'));

        return view('admin.oauth-clients.index', compact('oauth_clients', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Client::class);

        return view('admin.oauth-clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OAuthClientsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OAuthClientsRequest $request)
    {
        $this->authorize('create', Client::class);

        $user = $request->input('user', null);
        if ( !$request->user()->can('editOthers', Client::class) ||
            (empty($user) && $request->has('personal_access_client')) ) {
            $user = $request->user()->getKey();
        }
        $user = $user ? $user : null;

        $oauth_client = $this->clients->create(
            $user, $request->name, $request->input('redirect', ''),
            $request->has('personal_access_client'), $request->has('password_client')
        );

        return redirect()->action('Admin\\OAuthClientsController@edit', $oauth_client);
    }

    /**
     * Display the specified resource.
     *
     * @param  Client $oauth_client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $oauth_client)
    {
        $this->authorize('view', $oauth_client);

        return redirect()->action('Admin\\OAuthClientsController@edit', $oauth_client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Client $oauth_client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $oauth_client)
    {
        $this->authorize('update', $oauth_client);

        return view('admin.oauth-clients.edit', compact('oauth_client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OAuthClientsRequest $request
     * @param  Client $oauth_client
     * @return \Illuminate\Http\Response
     */
    public function update(OAuthClientsRequest $request, Client $oauth_client)
    {
        $this->authorize('update', $oauth_client);

        $oauth_client = $this->clients->update(
            $oauth_client, $request->name, $request->redirect
        );

        if ( $request->has('revoke') ) {
            $this->clients->delete($oauth_client);
        }

        if ( $request->has('regenerate') ) {
            $oauth_client = $this->clients->regenerateSecret($oauth_client);
        }

        return redirect()->action('Admin\\OAuthClientsController@edit', $oauth_client);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $oauth_client
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $oauth_client, Request $request)
    {
        $this->authorize('delete', $oauth_client);

        if ( !$oauth_client->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\OAuthClientsController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('create', Client::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,revoke,approve',
            'oauth_clients' => 'required|array',
            'oauth-clients.*' => 'exists:oauth_clients,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('oauth_clients', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', Client::class);

                $items = Client::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', Client::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;

            case 'approve':
                $items = Client::whereIn('id', $ids);
                if ( !$user->can('editOthers', Client::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(['revoked' => false]);
                break;

            case 'revoke':
                $items = Client::whereIn('id', $ids);
                if ( !$user->can('editOthers', Client::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(['revoked' => true]);
                break;
        }

        return redirect()->action('Admin\\OAuthClientsController@index');
    }
}
