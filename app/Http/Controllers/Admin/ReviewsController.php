<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Helpers\Enums\ReviewStatuses;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewsRequest;
use App\Review;

use Illuminate\Http\Request;

use App\Http\Requests;

class ReviewsController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Review::class);
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'created_at' => __('Created At'),
            'updated_at' => __('Updated At'),
            'rating' => __('Rating'),
            'id' => __('ID'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Review::class);

        $title = 'All Reviews';
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'created_at';
        $order = $request->order == 'ASC' ? $request->order : 'DESC';

        $reviews = Review::userVisible()->orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $reviews->search($search);
            $title = __('Reviews matching \':search\'', ['search' => $search]);
        }

        if ( $status = $request->input('status') ) {
            $reviews->whereStatus($status);
        }

        if ( $user = $request->input('user') ) {
            $reviews->whereUserId($user);
        }

        if ( $business = $request->input('business') ) {
            $reviews->whereBusinessId($business);
        }

        $reviews = $reviews->with('user', 'business')
            ->paginate(20)
            ->appends($request->except('page'));

        return view('admin.reviews.index', compact('reviews', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReviewsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewsRequest $request)
    {
        $status = $request->input('status');
        $approve = $request->has('approve');
        $business = $request->input('business');
        $user_id = $request->input('user');
        if ( empty($user_id) || !$request->user()->can('editOthers', Review::class) ) {
            //default to current user
            $user_id = $request->user()->id;
        }

        //check if a review exists
        $review = Review::whereUserId($user_id)
                    ->whereBusinessId($business)
                    ->first();

        //create new if doesn't exist
        if ( !$review ) {
            $review = new Review($request->all());
            $review->updateBusiness($business);
            $review->updateUser($user_id);
        } else {
            $review->update($request->all());
        }

        $review->updateStatus($status, $approve);

        $review->save();

        return redirect()->action('Admin\\ReviewsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        return redirect()->action('Admin\\ReviewsController@edit', $review);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReviewsRequest $request
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewsRequest $request, Review $review)
    {
        $review->update($request->all());

        $status = $request->input('status');
        $approve = $request->has('approve');
        $review->updateStatus($status, $approve);

        $review->save();

        return redirect()->action('Admin\\ReviewsController@edit', $review);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Review $review
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review, Request $request)
    {
        if ( !$review->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\ReviewsController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('create', Review::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,reject,approve',
            'reviews' => 'required|array',
            'reviews.*' => 'exists:reviews,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('reviews', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', Review::class);

                $items = Review::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', Review::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;

            case 'approve':
                $status = $user->can('approve', Review::class) ?
                    ReviewStatuses::APPROVED : ReviewStatuses::PENDING;

                $items = Review::whereIn('id', $ids);
                if ( !$user->can('editOthers', Review::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(compact('status'));
                break;

            case 'reject':
                //make sure allowed to approve
                $status = $user->can('approve', Review::class) ?
                    ReviewStatuses::REJECTED : ReviewStatuses::DRAFT;

                $items = Review::whereIn('id', $ids);
                if ( !$user->can('editOthers', Review::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(compact('status'));
                break;
        }

        return redirect()->action('Admin\\ReviewsController@index');
    }
}
