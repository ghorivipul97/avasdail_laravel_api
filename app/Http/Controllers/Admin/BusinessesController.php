<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Helpers\Dropzone\DropzoneUploadTrait;
use App\Helpers\Enums\BusinessStatuses;
use App\Http\Controllers\Controller;
use App\Http\Requests\BusinessesRequest;
use App\Business;

use Illuminate\Http\Request;

use App\Http\Requests;

class BusinessesController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Business::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Business::class);

        $title = 'All Businesses';
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'name';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        $businesses = Business::userVisible()->orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $businesses->search($search);
            $title = __('Businesses matching \':search\'', ['search' => $search]);
        }

        if ( $status = $request->input('status') ) {
            $businesses->whereStatus($status);
        }

        if ( $user = $request->input('user') ) {
            $businesses->whereUserId($user);
        }

        if ( $city = $request->input('city') ) {
            $businesses->whereCityId($city);
        }

        if ( $state = $request->input('state') ) {
            $businesses->hasState($state);
        }

        if ( $category = $request->input('category') ) {
            $businesses->belongsToCategory($category);
        }

        $businesses = $businesses->with('user', 'city', 'city.state', 'media')
            ->paginate(20)
            ->appends($request->except('page'));

        return view('admin.businesses.index', compact('businesses', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.businesses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BusinessesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusinessesRequest $request)
    {
        $business = new Business($request->all());
        $status = $request->input('status');
        $approve = $request->has('approve');
        $user = $request->input('user');

        $business->city_id = $request->input('city', null);
        $business->slug = $request->input('slug');
        $business->updateUser($user);
        $business->updateStatus($status, $approve);

        $business->save();

        //set categories
        $categories = $request->input('categories', []);
        $business->categories()->sync($categories);

        $business->updateSingleMedia('logo', $request);

        return redirect()->action('Admin\\BusinessesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Business $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        return redirect()->action('Admin\\BusinessesController@edit', $business);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Business $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        return view('admin.businesses.edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BusinessesRequest $request
     * @param  Business $business
     * @return \Illuminate\Http\Response
     */
    public function update(BusinessesRequest $request, Business $business)
    {
        $business->update($request->all());

        $status = $request->input('status');
        $approve = $request->has('approve');
        $user = $request->input('user');

        $business->city_id = $request->input('city', null);
        $business->updateUser($user);
        $business->updateStatus($status, $approve);

        if ( $request->user()->can('editOthers', Business::class) ) {
            if ( $slug = $request->input('slug') ) {
                $business->slug = $slug;
            }
        }

        //set categories
        $categories = $request->input('categories', []);
        $business->categories()->sync($categories);

        $business->updateSingleMedia('logo', $request);

        $business->save();

        return redirect()->action('Admin\\BusinessesController@edit', $business);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Business $business
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Business $business, Request $request)
    {
        if ( !$business->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\BusinessesController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('create', Business::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,reject,approve',
            'businesses' => 'required|array',
            'businesses.*' => 'exists:businesses,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('businesses', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', Business::class);

                $items = Business::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', Business::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;

            case 'approve':
                $status = $user->can('approve', Business::class) ?
                    BusinessStatuses::APPROVED : BusinessStatuses::PENDING;

                $items = Business::whereIn('id', $ids);
                if ( !$user->can('editOthers', Business::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(compact('status'));
                break;

            case 'reject':
                //make sure allowed to approve
                $status = $user->can('approve', Business::class) ?
                    BusinessStatuses::REJECTED : BusinessStatuses::DEACTIVATED;

                $items = Business::whereIn('id', $ids);
                if ( !$user->can('editOthers', Business::class) ) {
                    //not allowed to publish others
                    $items = $items->whereUserId($user->id);
                }
                $items->update(compact('status'));
                break;
        }

        return redirect()->action('Admin\\BusinessesController@index');
    }
}
