<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Domain\Admin\Requests\SettingRequest;
class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::first();

        return view('admin.settings.index', compact('setting'));
    }

    public function update(SettingRequest $request)
    {
        $setting = Setting::first();
        $setting->fill($request->persist())->save();
        session()->flash('success', 'Setting updated Successfully.');

        return redirect()->to('admin/settings');
    }
}
