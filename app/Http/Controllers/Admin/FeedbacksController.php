<?php

namespace App\Http\Controllers\Admin;

use App\Models\FeedbackData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

class FeedbacksController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(FeedbackData::all())
                ->addIndexColumn()
                ->addColumn('show', function ($modal) {
                    return view('admin.feedback._actions', [
                        'showUrl' => url('/admin/feedback/'.$modal->getKey().'/show'),
                    ])->render();
                })
                ->rawColumns(['show'])
                ->make(true);
        }
        
        return view('admin.feedback.index');
    }

    public function show($id)
    {
        $feedback = FeedbackData::find($id);

        return view('admin.feedback.show', compact('feedback'));
    }
}
