<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    /**
     * Dynamic CSS
     */
    public function css()
    {
        return response()
            ->view('admin.dynamic-css')
            ->header('Content-Type', 'text/css');
    }
}
