<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Domain\Admin\Requests\LoginRequest;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function redirectTo()
    {
        return redirect()->to('admin/home');
    }

    public function login(LoginRequest $request)
    {
        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'username';

        $request->merge([
            $login_type => $request->input('login')
        ]);

        if (auth()->guard('admin')->attempt($request->only($login_type, 'password'))) {
            return redirect()->to('admin/home');
        }

        session()->flash('error', 'These credentials do not match our records.');

        return redirect()->route('admin.login');
    }

    public function adminLogout()
    {
        auth('admin')->logout();

        return redirect()->route('admin.login');
    }
}
