<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Helpers\Dropzone\DropzoneUploadTrait;
use App\Helpers\Enums\QuestionTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionsRequest;
use App\Category;
use App\Question;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionsController extends Controller
{

    use AllowedOrdersTrait;
    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        //$this->authorizeResource(Question::class);
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'menu_order' => __('Menu Order'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Category $category
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category, Request $request)
    {
        $this->authorize('update', $category);

        $title = __('All Category Questions');
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'menu_order';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        $questions = $category->questions()->orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $questions = $questions->search($search);
            $title = __('Questions matching \':search\'', ['search' => $search]);
        }

        if ( $type = $request->input('type') ) {
            $questions = $questions->whereType($type);
        }

        $questions = $questions->paginate(20)
            ->appends($request->except('page'));

        return view('admin.questions.index', compact('category', 'questions', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Category $category
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category, Request $request)
    {
        $this->authorize('update', $category);

        return view('admin.questions.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionsRequest $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionsRequest $request, Category $category)
    {
        $this->authorize('update', $category);

        $question = new Question($request->all());
        $question->required = $request->has('required');
        $question->allow_other = $request->has('allow_other');
        $question->type = $request->input('type', QuestionTypes::TEXT);

        $category->questions()->save($question);

        return redirect()->action('Admin\\CategoriesController@edit', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @param  Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Question $question)
    {
        $this->authorize('update', $category);

        return redirect()->to( $question->url('edit') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @param  Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, Question $question)
    {
        $this->authorize('update', $category);

        return view('admin.questions.edit', compact('category', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param QuestionsRequest $request
     * @param Category $category
     * @param  Question $question
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionsRequest $request, Category $category, Question $question)
    {
        $this->authorize('update', $category);

        $question->update($request->all());

        $question->required = $request->has('required');
        $question->allow_other = $request->has('allow_other');
        $question->type = $request->input('type', QuestionTypes::TEXT);

        $question->save();

        return redirect()->action('Admin\\CategoriesController@edit', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @param Question $question
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, Question $question, Request $request)
    {
        $this->authorize('update', $category);

        if ( !$question->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\QuestionsController@index', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Category $category
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Category $category, Request $request)
    {
        $this->authorize('update', $category);

        $this->validate($request, [
            'action' => 'required|in:delete',
            'questions' => 'required|array',
            'questions.*' => 'exists:questions,id,category_id,'.$category->id,
        ]);

        $action = $request->input('action');
        $ids = $request->input('questions', []);

        switch ($action) {
            case 'delete':
                $category->questions()
                    ->whereIn('id', $ids)
                    ->delete();
                break;
        }

        return redirect()->action('Admin\\QuestionsController@index', $category);
    }

    /**
     * Sorts the resource
     *
     * @param Category $category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function sort(Category $category, Request $request)
    {
        //validate
        $this->validate($request, [
            'questions' => 'required|array',
            'questions.*' => 'exists:questions,id,category_id,'.$category->id,
        ]);

        $ids = $request->input('questions', []);
        $questions = Question::whereIn('id', $ids)
            ->orderByRaw('FIELD(id, '.trim(str_repeat('?,', count($ids)), ',').')', $ids)
            ->get();

        $resp = [];

        foreach ( $questions as $order => $question ) {
            $question->menu_order = $order;
            $question->save();
            $resp[$order] = $question->id;
        }

        if ($request->expectsJson()) {
            return response()->json($resp);
        }

        return redirect()->action('Admin\\QuestionsController@index', $category);
    }

}
