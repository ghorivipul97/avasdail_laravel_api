<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\RolesRequest;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Role::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Role::class);

        $title = __('All Roles');
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'name';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        $roles = Role::orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $roles = $roles->where('name', 'like', '%'.$search.'%')
                ->orWhere('description', 'like', '%'.$search.'%');
            $title = "Roles matching '$search'";
        }

        $roles = $roles->paginate(20)->appends($request->except('page'));

        return view('admin.roles.index', compact('roles', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RolesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesRequest $request)
    {
        $role = Role::create($request->only(['name', 'description']));

        //add the permissions
        foreach ( $request->input('permissions') as $perm_id ) {
            if ( $permission = Permission::whereId($perm_id)->first() ) {
                $role->givePermissionTo($permission);
            }
        }

        $role->save();

        return redirect()->action('Admin\\RolesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return redirect()->action('Admin\\RolesController@edit', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all();

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RolesRequest $request
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(RolesRequest $request, Role $role)
    {
        $role->update($request->only(['name', 'description']));

        //sync ids
        $perm_ids = $request->input('permissions', []);
        $permissions = Permission::whereIn('id', $perm_ids)->get();
        $role->syncPermissions($permissions);

        $role->save();

        if ( $request->user()->hasRole($role) && !$role->hasPermissionTo('edit_roles') ) {
            return redirect()->to( url('/') );
        } else {
            return redirect()->action('Admin\\RolesController@edit', $role);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, Request $request)
    {
        if ( !$role->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\RolesController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('create', Role::class);

        $this->validate($request, [
            'action' => 'required|in:delete',
            'roles' => 'required|array',
            'roles.*' => 'exists:roles,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('roles', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('delete_roles');

                Role::whereIn('id', $ids)->delete();
                break;
        }

        return redirect()->action('Admin\\RolesController@index');
    }
}
