<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\QuoteQuestion;
use Yajra\Datatables\Facades\Datatables;
use App\Domain\Admin\Requests\QuoteQuestionRequest;

class QuoteQuestionsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(QuoteQuestion::all())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/quote_questions/'.$modal->getKey().'/edit'),
                        'deleteUrl' => url('/admin/quote_questions/'.$modal->getKey())
                    ])->render();
                })
                ->editColumn('is_required', function($model) {
                    return $model->is_required ? 'YES' : 'NO';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.quotequestions.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.quotequestions.create');
    }

    /**
     * @param QuoteQuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(QuoteQuestionRequest $request)
    {
        QuoteQuestion::create($request->persist());
        session()->flash('success', 'Question created successfully');

        return redirect()->route('admin.quote_questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $question = QuoteQuestion::findorfail($id);

        return view('admin.quotequestions.edit', compact('question'));
    }

    /**
     * @param QuoteQuestionRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(QuoteQuestionRequest $request, $id)
    {
        $question = QuoteQuestion::findorfail($id);
        $question->fill($request->persist())->save();
        session()->flash('success', 'Question updated successfully.');

        return redirect()->route('admin.quote_questions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $question = QuoteQuestion::findorfail($id);
        $question->delete();
        session()->flash('success', 'Question deleted successfully.');

        return redirect()->back();
    }
}
