<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Contracts\View\Factory;
use Yajra\Datatables\Facades\Datatables;
use App\Models\State;
use App\Domain\Admin\Requests\CitiesRequest;

class CitiesController extends Controller
{
    /**
     * @return Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(City::with('state')->get())
                ->addIndexColumn()
                ->addColumn('state_name', function ($model) {
                    return $model->state->name;
                })
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/cities/' . $modal->getKey() . '/edit'),
                        'deleteUrl' => url('/admin/cities', $modal->getKey())
                    ])->render();
                })
                ->rawColumns(['actions', 'state_name'])
                ->make(true);
        }

        return view('admin.cities.index');
    }

    /**
     * @return Factory|\Illuminate\View\View
     */
    public function create()
    {
        $states = State::pluck('name', 'id')->toArray();

        return view('admin.cities.create', compact('states'));
    }

    /**
     * @param CitiesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CitiesRequest $request)
    {
        City::create($request->persist());
        session()->flash('success', 'City created successfully');

        return redirect()->to('admin/cities');
    }

    /**
     * @param $id
     * @return Factory|\Illuminate\View\View
     * @internal param $id1
     */
    public function edit($id)
    {
        $city = City::findorfail($id);
        $states = State::pluck('name', 'id')->toArray();

        return view('admin.cities.edit', compact('city', 'states'));
    }

    /**
     * @param CitiesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CitiesRequest $request, $id)
    {
        $city = City::findorfail($id);
        $city->fill($request->persist())->save();
        session()->flash('success', 'City updated successfully.');

        return redirect()->to('admin/cities');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $city = City::findorfail($id);
        $city->delete();
        session()->flash('success', 'City deleted successfully.');

        return redirect()->back();
    }
}
