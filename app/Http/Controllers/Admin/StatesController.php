<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\State;
use Yajra\Datatables\Facades\Datatables;
use App\Domain\Admin\Requests\StatesRequest;

class StatesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(State::all())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/states/'.$modal->getKey().'/edit'),
                        'deleteUrl' => url('admin/states/' . $modal->getKey())
                    ])->render();
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.states.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.states.create');
    }

    /**
     * @param StatesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StatesRequest $request)
    {
        State::create($request->persist());
        session()->flash('success', 'State created successfully');

        return redirect()->route('admin.states.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $state = State::findorfail($id);

        return view('admin.states.edit', compact('state'));
    }

    /**
     * @param StatesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StatesRequest $request, $id)
    {
        $state = State::findorfail($id);
        $state->fill($request->persist())->save();
        session()->flash('success', 'State updated successfully.');

        return redirect()->route('admin.states.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $state = State::findorfail($id);
        $state->delete();
        session()->flash('success', 'State deleted successfully.');

        return redirect()->back();
    }
}
