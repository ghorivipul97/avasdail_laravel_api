<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\CategoryPageImage;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class SliderImagesController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $query = CategoryPageImage::all();

            return Datatables::of($query)
                ->addIndexColumn()
                ->editColumn('image', function($model) {
                    return '<img src="' . asset('uploads/slider-images/' . $model->name) . '" width="200px;">';
                })
                ->addColumn('actions', function ($model) {
                    return view('admin.slider-images._actions', [
                        'deleteUrl' => url('admin/category-slider-images/' . $model->getKey())
                    ])->render();
                })
                ->rawColumns(['actions', 'image'])
                ->make(true);
        }

        return view('admin.slider-images.index');
    }

    public function create()
    {
        return view('admin.slider-images.create');
    }

    public function store(ImageRequest $request)
    {
        CategoryPageImage::create([
            'name' => $request->uploadImage()
        ]);
        session()->flash('success', 'Image added successfully');

        return redirect()->route('admin.category-slider-images.index');
    }

    public function destroy($id)
    {
        $image = CategoryPageImage::findorfail($id);
        $image->delete();
        session()->flash('success', 'Image deleted successfully');

        return redirect()->route('admin.category-slider-images.index');
    }
}
