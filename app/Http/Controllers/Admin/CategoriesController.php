<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Domain\Admin\Requests\CategoriesRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $query = Category::withCount('subcategories')->whereNull('parent_id')->get();

            return Datatables::of($query)
                ->addIndexColumn()
                ->addColumn('actions', function ($model) {
                    return view('admin.categories._actions', [
                        'editUrl' => url('/admin/categories/'.$model->getKey().'/edit'),
                        'deleteUrl' => $model->getKey(),
                        'subcategoryUrl' => $model->subcategories_count
                    ])->render();
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.categories.index');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subCategory(Request $request, $id)
    {
        if ($request->ajax())
        {
            return Datatables::of(Category::where('parent_id', $id)->get())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.categories._actions', [
                        'editUrl' => url('/admin/categories/'.$modal->getKey().'/edit'),
                        'deleteUrl' => $modal->getKey()
                    ])->render();
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.categories.subcategories', compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roots = Category::whereNull('parent_id')->pluck('name', 'id');

        return view('admin.categories.create', compact('roots'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoriesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
        Category::create($request->persist());
        session()->flash('success', 'Category created successfully');

        return redirect()->route('admin.categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findorfail($id);
        $roots = Category::whereNull('parent_id')->pluck('name', 'id');

        return view('admin.categories.edit', compact('category', 'roots'));
    }

    /**
     * @param CategoriesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoriesRequest $request, $id)
    {
        $category = Category::findorfail($id);
        $category->fill($request->updatePersist($category))->save();
        session()->flash('success', 'Category updated successfully.');

        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findorfail($id);
        $category->delete();
        session()->flash('success', 'Category deleted successfully.');

        return redirect()->back();
    }
}
