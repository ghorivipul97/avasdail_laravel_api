<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Yajra\Datatables\Facades\Datatables;
use App\Domain\Admin\Requests\TagRequest;

class TagsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return Datatables::of(Tag::all())
                ->addIndexColumn()
                ->addColumn('actions', function ($modal) {
                    return view('admin.shared.actions', [
                        'editUrl' => url('/admin/tags/'.$modal->getKey().'/edit'),
                        'deleteUrl' => url('/admin/tags/'.$modal->getKey())
                    ])->render();
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.tags.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * @param TagRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TagRequest $request)
    {
        Tag::create($request->persist());
        session()->flash('success', 'Tag created successfully');

        return redirect()->route('admin.tags.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('admin.tags.edit', compact('tag'));
    }

    /**
     * @param TagRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TagRequest $request, $id)
    {
        $tag = Tag::findorfail($id);
        $tag->fill($request->persist())->save();
        session()->flash('success', 'Tag updated successfully.');

        return redirect()->route('admin.tags.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $tag = Tag::findorfail($id);
        $tag->delete();
        session()->flash('success', 'Question deleted successfully.');

        return redirect()->back();
    }
}
