<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AllowedOrdersTrait;
use App\Helpers\Enums\UserStatuses;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest;
use App\MobileNumber;
use App\User;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

    use AllowedOrdersTrait;

    /**
     * Create a new  controller instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'created_at' => __('Created At'),
            'updated_at' => __('Updated At'),
            'name' => __('Name'),
            'username' => __('Username'),
            'email' => __('Email'),
            'id' => __('ID'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', User::class);

        $title = __('All Users');
        $orderby = array_key_exists($request->orderby, self::getOrderbys()) ? $request->orderby : 'name';
        $order = $request->order == 'DESC' ? $request->order : 'ASC';

        $users = User::orderBy($orderby, $order);

        $search = null;
        if ( $search = $request->input('search') ) {
            $users->search($search);
            $title = __('Users matching \':search\'', ['search' => $search]);
        }

        if ( $status = $request->input('status') ) {
           $users->whereStatus($status);
        }
        
        if ( $role = $request->input('role') ) {
            $users->whereHas('roles', function ($query) use ($role) {
                $query->whereId($role);
            });
        }

        if ( $phone = $request->input('phone') ) {
            $users->hasPhone($phone);
        }

        $users = $users->with('phone', 'media', 'roles')
            ->paginate(20)
            ->appends($request->except('page'));

        return view('admin.users.index', compact('users', 'title', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = Role::pluck('description', 'name')->all();
        $selected_role = $request->user()->can('create', User::class) ? old('role') : Role::whereName('subscriber')->value('name');

        $user_statuses = UserStatuses::getLabels();
        $selected_user_status = $request->user()->can('create', User::class) ? old('status') : UserStatuses::PENDING;

        return view('admin.users.create', compact('roles', 'selected_role', 'user_statuses', 'selected_user_status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $user = new User($request->all());
        $user->email = $request->input('email');
        $user->username = $request->input('username');
        $user->password = $request->input('password');

        if ( $request->user()->can('create', User::class) ) {
            $user->status = $request->input('status');
        } else {
            $user->status = UserStatuses::PENDING;
        }

        $user->save();

        //add the role
        $role_name = $request->user()->can('create', User::class) ? $request->input('role') : 'subscriber';
        if ( $role = Role::whereName($role_name)->first() ) {
            $user->assignRole($role);
        }

        //update avatar
        $user->updateSingleMedia('avatar', $request);

        if ( $request->user()->can('assignMobileNumbers', User::class) ) {
            //save mobile number
            if ($mobile_number = $request->input('mobile_number')) {
                //create the number
                $phone = MobileNumber::firstOrCreate([
                    'country_code' => MobileNumber::DEFAULT_COUNTRY_CODE,
                    'number' => $mobile_number,
                ]);

                $user->updatePhone($phone);
            }
        }


        return redirect()->action('Admin\\UsersController@edit', $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect()->action('Admin\\UsersController@edit', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('description', 'name')->all();
        $selected_role = $user->roles()->value('name');

        $user_statuses = UserStatuses::getLabels();
        $selected_user_status = $user->status;

        return view('admin.users.edit', compact('user', 'roles', 'selected_role', 'user_statuses', 'selected_user_status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersRequest $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UsersRequest $request, User $user)
    {
        $user->update($request->all());

        //update password
        if ( $password = $request->input('password') ) {
            $user->password = $password;

            $request->session()->flash('alerts', [[
                'text' => __('Password changed successfully'),
                'type' => 'success',
                'title' => __('Success!'),
            ]]);
        }

        //update email
        if ( $email = $request->input('email') ) {
            $user->email = $email;
        }

        //update avatar
        $user->updateSingleMedia('avatar', $request);

        //restricted update
        if ( $request->user()->can('create', User::class) ) {
            //sync roles
            if ( $role = $request->input('role') ) {
                if (!is_array($role)) {
                    $role = [$role];
                }
                $user->syncRoles($role);
            }

            //update status
            if ( $status = $request->input('status') ) {
                $user->status = $status;
            }
        }

        $user->save();

        if ( $request->user()->can('assignMobileNumbers', User::class) ) {
            //save mobile number
            if ($mobile_number = $request->input('mobile_number')) {
                //create the number
                $phone = MobileNumber::firstOrCreate([
                    'country_code' => MobileNumber::DEFAULT_COUNTRY_CODE,
                    'number' => $mobile_number,
                ]);

                $user->updatePhone($phone);
            } else {
                $user->clearPhone();
            }
        }

        //check if current user
        if ( $user->id == $request->user()->id ) {
            return redirect()->action('Admin\\UsersController@profile');
        } else {
            return redirect()->action('Admin\\UsersController@edit', $user);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Request $request)
    {
        if ( !$user->delete() ) {
            if ($request->expectsJson()) {
                return response()->json(false, 500);
            }
            App::abort(500);
        }

        if ($request->expectsJson()) {
            return response()->json(true);
        }

        return redirect()->action('Admin\\UsersController@index');
    }

    /**
     * Shows the current user profile
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        return $this->edit($request->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Inquiry $inquiry
     */
    public function bulk(Request $request)
    {
        $this->authorize('create', User::class);

        $user = $request->user();

        $this->validate($request, [
            'action' => 'required|in:delete,activate,ban',
            'users' => 'required|array',
            'users.*' => 'exists:users,id',
        ]);

        $action = $request->input('action');
        $ids = $request->input('users', []);

        switch ($action) {
            case 'delete':
                //make sure allowed to delete
                $this->authorize('deleteOwn', User::class);

                $items = User::whereIn('id', $ids);
                if ( !$user->can('deleteOthers', User::class) ) {
                    //not allowed to delete others
                    $items = $items->whereUserId($user->id);
                }
                $items->delete();
                break;

            case 'ban':
            case 'activate':
                //make sure can edit others
                $this->authorize('editOthers', User::class);

                $status = $action == 'activate' ? UserStatuses::ACTIVE : UserStatuses::BANNED;

                User::whereIn('id', $ids)->update([
                    'status' => $status
                ]);
                break;
        }

        return redirect()->action('Admin\\UsersController@index');
    }
}
