<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\DiscoverServiceProviderRequest;
use App\Domain\Api\Request\FilterProvderRequest;
use App\Domain\Api\Request\UpdateProviderAnswersRequest;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use App\Models\UserSignupAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceProviderController extends Controller
{
    public function discoverServiceProviders(DiscoverServiceProviderRequest $request)
    {
        $users = User::with('reviews')
            ->select(DB::raw('users.*,
            ( 3959 * acos( cos( radians(' . $request->get('latitude') . ') ) * cos( radians( latitude ) ) *
            cos( radians( longitude ) - radians(' . $request->get('longitude') . ') ) + sin( radians(' . $request->get('latitude') . ') ) *
            sin( radians( latitude ) ) ) ) AS distance'))
            ->having('distance', '<', $request->get('distance'))
            ->join('user_role', 'users.id', '=', 'user_role.user_id')
            ->join('roles', 'user_role.role_id', '=', 'roles.id')
            ->where('roles.name', \App\Models\Role::SERVICE_PROVIDER)
            ->get();

        $usersData = [];
        foreach ($users as $user) {
            $data = $user->toArray();
            unset($data['reviews']);

            $usersData[] = array_merge(
                $data,
                [
                    'reviewsCount' => $user->reviews()->count(),
                    'reviewsAverage' => $user->reviews()->count() == 0 ? 0 : $user->reviews()->sum('ratings') / $user->reviews()->count()
                ]
            );
        }

        return response()->json([
            'success' => true,
            'service_providers' => $usersData
        ]);
    }

    public function filterProviders(FilterProvderRequest $request)
    {
        if ($request->get('main_category_id')) {
            $mainCategory = Category::with('subcategories')->find($request->get('main_category_id'));
            $categories = $mainCategory->subcategories()->pluck('id')->toArray();
        } else if ($request->get('sub_category_id')) {
            $categories = [$request->get('sub_category_id')];
        } else {
            $categories = null;
        }

        if (!$request->get('keyword')) {
            $users = User::with('reviews')
                ->select('users.*')
                ->join('user_role', 'users.id', '=', 'user_role.user_id')
                ->join('roles', 'user_role.role_id', '=', 'roles.id')
                ->where('roles.name', \App\Models\Role::SERVICE_PROVIDER);
            if ($categories) {
                $users->join('user_categories', 'user_categories.user_id', '=', 'users.id')
                ->whereIn('user_categories.category_id', $categories);
            }
            $users->paginate(10);
        } else {
            $users = User::with('reviews')
                ->select('users.*')
                ->join('user_role', 'users.id', '=', 'user_role.user_id')
                ->join('roles', 'user_role.role_id', '=', 'roles.id')
                ->leftJoin('user_tags', 'users.id', '=', 'user_tags.user_id')
                ->leftJoin('tags', 'user_tags.tag_id', '=', 'tags.id')
                ->where('roles.name', \App\Models\Role::SERVICE_PROVIDER)
                ->where(function($query) use ($request) {
                    $query->where('users.name', 'like', '%' . $request->get('keyword') . '%');
                });

            if ($categories) {
                $users->join('user_categories', 'user_categories.user_id', '=', 'users.id')
                    ->whereIn('user_categories.category_id', $categories);
            }
            $users->distinct()->paginate(10);
        }

        $usersData = [];
        foreach ($users->get() as $user) {
            $data = $user->toArray();
            unset($data['reviews']);

            $usersData[] = array_merge(
                $data,
                [
                    'reviewsCount' => $user->reviews()->count(),
                    'reviewsAverage' => $user->reviews()->count() == 0 ? 0 : $user->reviews()->sum('ratings') / $user->reviews()->count()
                ]
            );
        }

        return response()->json([
            'success' => true,
            'service_providers' => $usersData
        ]);
    }

    public function updateProviderAnswers(UpdateProviderAnswersRequest $request)
    {
        $user = User::find($request->get('user_id'));
        UserSignupAnswer::whereUserId($user->id)->delete();

        $answersData = [];
        foreach (json_decode($request->get('answers'), true) as $answer) {
            $answersData[] = [
                'user_id' => $user->id,
                'signup_question_id' => $answer['question_id'],
                'answer' => $answer['answer']
            ];
        }
        UserSignupAnswer::insert($answersData);

        $answers = UserSignupAnswer::with('question')
            ->where('user_id', $user->id)
            ->get();

        $answers = array_map(function($answer) {
            return [
                'question_id' => $answer['signup_question_id'],
                'question' => $answer['question']['question'],
                'answer' => $answer['answer']
            ];
        }, $answers->toArray());

        return response()->json([
            'success' => true,
            'answers' => $answers
        ]);
    }
}
