<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\ServiceProviderAnswerRequest;
use App\Domain\Api\Service\NotificationService;
use App\Domain\Utility\PushNotificationUtil;
use App\Models\Answer;
use App\Domain\Api\Request\AnswerRequest;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\QuoteAnswerRequest;
use App\Models\Category;
use App\Models\Notification;
use App\Models\QuoteAnswer;
use App\Models\QuoteQuestion;
use App\Models\QuoteServiceProvider;
use App\Models\Role;
use App\Models\ServiceProviderAnswer;
use App\Models\ServiceProviderQuestion;
use App\Models\User;
use App\Models\UserQuote;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function postQuoteAnswer(QuoteAnswerRequest $request)
    {
        $category = Category::find($request->get('category_id'));
        $answers = json_decode($request->get('answers'), true);

        $compulsoryQueArray = QuoteQuestion::whereIsRequired(true)->pluck('id')->toArray();
        foreach ($compulsoryQueArray as $id) {
            if (array_search($id, array_column($answers, 'question_id')) != false && array_search($id, array_column($answers, 'question_id')) == false) {
                return [
                    'success' => false,
                    'message' => 'Please add all required answers.'
                ];
            }
        }

        $descKey = array_search(0, array_column($answers, 'question_id'));
        if ($descKey != 0 && $descKey == false) {
            return [
                'success' => false,
                'message' => 'Please add all required answers.'
            ];
        }
        $descriptionData = $answers[$descKey];

        $userQuote = UserQuote::create(array_merge(
            $request->only('user_id', 'category_id'),
            [
                'name' => $category->name . '_' . rand(100000000, 999999999),
                'description' => $descriptionData['answer'],
                'expired_at' => Carbon::now()->addDays(2),
                'status' => UserQuote::OPEN,
                'is_all_providers' => $request->has('is_all_providers')
            ]
        ));

        $answersArray = [];
        foreach ($answers as $index => $answer) {
            if ($answer['question_id'] != 0) {
                {
                    $answersArray[] = [
                        'user_quote_id' => $userQuote->id,
                        'quote_question_id' => $answer['question_id'],
                        'answer' => $answer['answer']
                    ];
                }
            }
        }
        QuoteAnswer::insert($answersArray);

        $message = 'New job ' . $userQuote->name . ' created by user - ' . $userQuote->user->name;
        if ($request->has('is_all_providers') && $request->get('is_all_providers') == 1) {
            $providers = User::select('users.is_login', 'users.player_id')
                ->join('user_role', 'user_role.user_id', '=', 'users.id')
                ->join('roles', 'user_role.role_id', '=', 'roles.id')
                ->where('roles.name', Role::SERVICE_PROVIDER)
                ->get();

            foreach ($providers as $provider) {
                if ($provider->is_login) {
                    try {
                        PushNotificationUtil::sendPush($provider->player_id, $message, null, ['type' => Notification::REQUEST_QUOTE]);
                    } catch (\Exception $e) { }
                }
            }

            return response()->json([
                'success' => true
            ]);
        }

        QuoteServiceProvider::create([
            'user_quote_id' => $userQuote->id,
            'service_provider_id' => $request->get('service_provider_id'),
            'status' => QuoteServiceProvider::NOT_QUOTED
        ]);

        $provider = User::find($request->get('service_provider_id'));
        if ($provider->is_login) {
            try {
                PushNotificationUtil::sendPush($provider->player_id, $message, null, ['type' => Notification::REQUEST_QUOTE]);
            } catch (\Exception $e) { }
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function postServiceProviderAnswers(ServiceProviderAnswerRequest $request)
    {
        $userQuote = UserQuote::with('user')->find($request->get('job_id'));

        $quoteServiceProvider = QuoteServiceProvider::whereUserQuoteId($request->get('job_id'))
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->first();

        $answers = json_decode($request->get('answers'), true);
        $compulsoryQueArray = ServiceProviderQuestion::whereIsRequired(true)->pluck('id')->toArray();
        foreach ($compulsoryQueArray as $id) {
            if (array_search($id, array_column($answers, 'question_id')) != false && array_search($id, array_column($answers, 'question_id')) == false) {
                return [
                    'success' => false,
                    'message' => 'Please add all required answers.'
                ];
            }
        }

        $amountKey = array_search(0, array_column($answers, 'question_id'));
        if ($amountKey != 0 && $amountKey == false) {
            return [
                'success' => false,
                'message' => 'Please add all required answers.'
            ];
        }
        $amountData = $answers[$amountKey];

        if (!$quoteServiceProvider) {
            if (!$userQuote->is_all_providers) {
                return response()->json([
                    'success' => false,
                    'message' => 'Job not found.'
                ]);
            }
            $quoteServiceProvider = QuoteServiceProvider::create([
                'user_quote_id' => $request->get('job_id'),
                'service_provider_id' => $request->get('service_provider_id'),
                'status' => QuoteServiceProvider::NOT_QUOTED,
                'amount' => $amountData['answer'],
                'status' => QuoteServiceProvider::QUOTED
            ]);
        } else {
            $quoteServiceProvider->fill([
                'amount' => $amountData['answer'],
                'status' => QuoteServiceProvider::QUOTED
            ])->save();
        }

        foreach ($answers as $index => $answer) {
            if ($answer['question_id'] != 0) {
                ServiceProviderAnswer::create([
                    'quote_service_provider_id' => $quoteServiceProvider->id,
                    'service_provider_question_id' => $answer['question_id'],
                    'answer' => $answer['answer']
                ]);
            }
        }

        $message = 'Provider - ' . $quoteServiceProvider->serviceProvider->name . ' sent quote for work - ' . $userQuote->name . '.';
        $this->notificationService->addNotification(
            $request->get('service_provider_id'),
            $userQuote->user_id,
            $message,
            Notification::PROVIDER_SEND_QUOTE,
            $userQuote->id
        );

        if ($userQuote->user->is_login) {
            try {
                PushNotificationUtil::sendPush($userQuote->user->player_id, $message, null, ['type' => Notification::PROVIDER_SEND_QUOTE]);
            } catch (\Exception $e) { }
        }

        return response()->json([
            'success' => true
        ]);
    }
}
