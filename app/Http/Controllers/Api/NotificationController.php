<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\SendPushRequest;
use App\Domain\Utility\PushNotificationUtil;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\ForgotPasswordVerificationToken;
use Illuminate\Http\Request;
use NotificationChannels\Twilio\TwilioSmsMessage;

class NotificationController extends Controller
{
    public function send(SendPushRequest $request)
    {
        PushNotificationUtil::sendPush($request->get('player_id'), $request->get('message'), null, ['push_chat' => $request->get('payload')]);

        return response()->json([
            'success' => true
        ]);
    }

    public function getNotifications($userId)
    {
        $notifications = Notification::with('sender')->whereReceiverId($userId)->get();

        $notifications = array_map(function ($notification) {
            $isProviderNotification = $notification['type'] == Notification::PROVIDER_SEND_QUOTE || $notification['type'] == Notification::COMPLETE_JOB ? true : false;

            $data = $notification;
            unset($data['parent_id']);

            return array_merge($data,
                [
                    'provider_id' => $isProviderNotification ? $notification['sender_id'] : $notification['receiver_id'],
                    'user_id' => $isProviderNotification ? $notification['receiver_id'] : $notification['sender_id'],
                    'job_id' => $notification['parent_id']
                ]);
        }, $notifications->toArray());

        return response()->json([
            'success' => true,
            'notifications' => $notifications
        ]);
    }

    public function sendotp($id)
    {
        $user = User::find($id);
        $user->notify(new ForgotPasswordVerificationToken('12345'));
    }

    public function updateNumber($id, $number)
    {
        $user = User::findorfail($id);
        $user->fill([
            'mobile_no' => '+' . $number
        ])->save();

        dd('done');
    }
}
