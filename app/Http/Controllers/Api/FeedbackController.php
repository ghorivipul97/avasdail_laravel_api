<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\FeedbackRequest;
use App\Domain\Api\Request\ReportRequest;
use App\Http\Controllers\Controller;
use App\Models\FeedbackData;
use App\Models\ReportData;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FeedbackController extends Controller
{
    public function postSendFeedback(FeedbackRequest $request)
    {
        FeedbackData::create($request->persist());

        return response()->json([
            'success' => true
        ]);
    }

    public function postSendReport(ReportRequest $request)
    {
        $files = [];
        if ($request->hasFile('image1')) {
            $files[] = $this->uploadReportFile($request->file('image1'));
        }
        if ($request->hasFile('image2')) {
            $files[] = $this->uploadReportFile($request->file('image2'));
        }
        if ($request->hasFile('image3')) {
            $files[] = $this->uploadReportFile($request->file('image3'));
        }
        if ($request->hasFile('image4')) {
            $files[] = $this->uploadReportFile($request->file('image4'));
        }
        if ($request->hasFile('image5')) {
            $files[] = $this->uploadReportFile($request->file('image5'));
        }

        ReportData::create(array_merge(
            $request->persist(),
            [
                'files' => implode(',', $files)
            ]
        ));

        return response()->json([
            'success' => true
        ]);
    }

    private function uploadReportFile(UploadedFile $item)
    {
        $imageName = time() . '_' . $item->getClientOriginalName();
        $item->move(public_path('uploads/screenshots'), $imageName);

        return $imageName;
    }
}
