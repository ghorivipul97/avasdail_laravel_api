<?php namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\BusinessDetailsRequest;
use App\Helpers\Dropzone\DropzoneUploadTrait;
use App\Http\Requests;

use App\Business;
use App\Http\Requests\BusinessesRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessesController extends Controller {

    public static $pivot_fields = ['island'];
    public static $calculated_fields = ['logo', 'images'];

    use DropzoneUploadTrait;

    protected $dropzone_model = 'business';

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('scopes:read,write')->only('update');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Type $type
     * @param Request $request
     * @return Response
     */
	public function index(Request $request)
	{
        $default_fields = array_merge(\Schema::getColumnListing('businesses'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'orderby' => 'sometimes|required|in:id,name,slug,created_at,updated_at',
            'order' => 'sometimes|required|in:ASC,DESC',
            'fields' => 'sometimes|required|array_exists:'.implode(',',$default_fields),
            'per_page' => 'integer|min:1|max:50',
            'page' => 'integer|min:1',
            'city' => 'integer',
            'state' => 'integer',
            'category' => 'integer',
        ]);

        $per_page = $request->input('per_page', 10);
        $orderby = $request->input('orderby', 'name');
        $order = $request->input('order', 'ASC');
        $fields = to_array($request->input('fields', $default_fields));
        $search = $request->input('search');
        $with_city = in_array('city', $fields);

        $fields = array_merge($fields, ['id', 'user_id']);

        if ( $with_city ) {
            $fields = array_merge($fields, ['city_id']);
        }

        $businesses = Business::userVisible()
            ->select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ($search) {
            $businesses->search($search);
        }

        if ( $city = $request->input('city') ) {
            $businesses->whereCityId($city);
        }

        if ( $state = $request->input('state') ) {
            $businesses->hasState($state);
        }

        if ( $category = $request->input('category') ) {
            $businesses->belongsToCategory($category);
        }

        if ($with_city) {
            $businesses->with('city');
        }

        $businesses = $businesses->orderBy($orderby, $order)
            ->paginate($per_page);

        //fields to hide and show
        $to_hide = array_diff(self::$calculated_fields, $fields);
        $to_show = array_diff(self::$calculated_fields, $to_hide);

        //add calculated fields
        foreach ($businesses as $business) {
            $business->setAppends($to_show);
            $business->addHidden($to_hide);
        }

        return response()->json($businesses->appends($request->except(['page'])));
	}

    /**
     * Display a single resource.
     *
     * @param Request $request
     * @param Type $type
     * @return Response
     */
    public function show($business_id, Request $request)
    {
        $default_fields = array_merge(\Schema::getColumnListing('businesses'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'fields' => 'sometimes|required|array_exists:'.implode(',',$default_fields),
        ]);

        $fields = to_array($request->input('fields', $default_fields));
        $with_city = in_array('city', $fields);

        $fields = array_merge($fields, ['id', 'user_id']);

        if ( $with_city ) {
            $fields = array_merge($fields, ['city_id']);
        }

        $business = Business::userVisible()
            ->select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ($with_city) {
            $business = $business->with('city');
        }

        try {
            $business = $business->findOrFail($business_id);

            //fields to hide and show
            $to_hide = array_diff(self::$calculated_fields, $fields);
            $to_show = array_diff(self::$calculated_fields, $to_hide);

            //add calculated fields
            $business->setAppends($to_show);
            $business->addHidden($to_hide);

        } catch ( ModelNotFoundException $e ) {
            \App::abort(404);
        }

        return response()->json($business);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BusinessesRequest $request
     * @param  Business $business
     * @return \Illuminate\Http\Response
     */
    public function update(BusinessesRequest $request, Business $business)
    {
        $this->authorize('update', $business);

        $business->update($request->all());

        $status = $request->input('status');
        $approve = $request->has('approve');
        $user = $request->input('user');

        $business->city_id = $request->input('city', null);
        $business->updateUser($user);
        $business->updateStatus($status, $approve);

        if ( $request->user()->can('editOthers', Business::class) ) {
            if ( $slug = $request->input('slug') ) {
                $business->slug = $slug;
            }
        }

        //set categories
        $categories = $request->input('categories', []);
        $business->categories()->sync($categories);

        $business->updateSingleMedia('logo', $request);

        $business->save();

        return response()->json($business);
    }

    public function updateDetails(BusinessDetailsRequest $request)
    {
        $business = Business::whereUserId($request->get('user_id'))->first();

        if ($business) {
            $business->fill(
                array_merge(
                    $request->updatePersist(),
                    [$request->get('name') == $business->name ? [] : ['slug' => $business->getUniqueSlug($request->get('name'))]]
                )
            );

            return response()->json([
                'success' => true,
                'business' => $business
            ]);
        }

        $business = Business::create($request->createPersist());
        
        return response()->json([
            'success' => true,
            'business' => $business
        ]);
    }
}
