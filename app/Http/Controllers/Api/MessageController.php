<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\MessageRequest;
use App\Domain\Api\Request\MessagesRequest;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function addMessage(MessageRequest $request)
    {
        $message = Message::create($request->persist());

        return response()->json([
            'success' => true,
            'message' => $message
        ]);
    }

    public function getMessageUsers($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $senderUsers = Message::where('receiver_id', $user->id)
            ->get()->pluck('sender_id', 'id')->toArray();

        $receiverUsers = Message::where('sender_id', $user->id)
            ->get()->pluck('receiver_id', 'id')->toArray();

        $users = array_merge($senderUsers, $receiverUsers);
        $users = array_unique($users);
        ksort($users);

        $messages = Message::with(['sender', 'receiver'])->whereIn('id', array_keys($users))->get();

        $messages = array_map(function ($message) use ($user) {
            $m['user'] = $message['sender']['id'] == $user->id ? $message['receiver'] : $message['sender'];
            $m['message'] = $message['message'];
            $m['created_at'] = $message['created_at'];

            return $m;
        }, $messages->toArray());

        return response()->json([
            'success' => true,
            'messages' => $messages
        ]);
    }

    public function getMessages(MessagesRequest $request)
    {
        $messages = Message::with(['sender', 'receiver'])
            ->where(function($q) use ($request) {
                $q->where('sender_id', $request->get('user_id'))
                    ->where('receiver_id', $request->get('message_user_id'));
            })
            ->orWhere(function($q) use ($request) {
                $q->where('receiver_id', $request->get('user_id'))
                    ->where('sender_id', $request->get('message_user_id'));
            })
            ->get();

        return response()->json([
            'success' => true,
            'messages' => $messages
        ]);
    }
}
