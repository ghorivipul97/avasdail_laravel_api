<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\ResetPasswordRequest;
use App\Domain\Api\Request\SendOtpRequest;
use App\Domain\Api\Request\UserRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\ForgotPasswordVerificationToken;

class ForgotPasswordController extends Controller
{
    public function sendOtp(SendOtpRequest $request)
    {
        $user = User::where('mobile_no', 'like', '+'.$request->get('mobile_number'))->first();

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Mobile number does not match to our records.'
            ]);
        }

        $otp = rand(100000, 999999);
        $user->fill([
            'forgot_password_otp' => $otp
        ])->save();
        $user->notify(new ForgotPasswordVerificationToken($otp));

        return response()->json([
            'success' => true,
            'otp' => $otp,
            'user_id' => $user->id
        ]);
    }

    public function resendOtp(UserRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $otp = rand(100000, 999999);
        $user->fill([
            'forgot_password_otp' => $otp
        ])->save();
        $user->notify(new ForgotPasswordVerificationToken($otp));

        return response()->json([
            'success' => true,
            'otp' => $otp,
            'user_id' => $user->id
        ]);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill([
            'password' => bcrypt($request->get('password'))
        ])->save();

        return response()->json([
            'success' => true
        ]);
    }
}
