<?php

namespace App\Http\Controllers\Api\ServiceProviders;

use App\Domain\Api\ServiceProviders\Request\LoginRequest;
use App\Domain\Api\ServiceProviders\Request\SignupRequest;
use App\Http\Controllers\Controller;
use App\Models\ServiceProvider;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function signup(SignupRequest $request)
    {
        $user = ServiceProvider::create($request->persist());

        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    public function login(LoginRequest $request)
    {
        if (auth('serviceProviders')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = auth('serviceProviders')->user();
            auth('serviceProviders')->logout();

            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid Credentials.'
        ]);
    }
}
