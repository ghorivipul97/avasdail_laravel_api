<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\City;

class CitiesController extends Controller
{
    public function states()
    {
        $states = State::all();

        return response()->json([
            'success' => true,
            'states' => $states
        ]);
    }

    public function cities($stateId)
    {
        $cities = City::whereStateId($stateId)->get();

        return response()->json([
            'success' => true,
            'cities' => $cities
        ]);
    }
}
