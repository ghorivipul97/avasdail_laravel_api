<?php

namespace App\Http\Controllers\Api\User;

use App\Domain\Api\User\Request\LoginRequest;
use App\Domain\Api\User\Request\SignupRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function signup(SignupRequest $request)
    {
        $user = User::create($request->persist());

        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    public function login(LoginRequest $request)
    {
        if (auth('web')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = auth('web')->user();
            auth('web')->logout();

            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid Credentials.'
        ]);
    }
}
