<?php namespace App\Http\Controllers\Api;

use App\Exceptions\MobileNumberException;

use App\MobileNumber;
use App\Notifications\MobileNumberVerificationToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class MobileNumbersController extends Controller {

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('scopes:read,write')->only('update');
    }

    /**
     * Store the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //first validate
        $this->validate($request, [
            'phone' => [
                'required',
                //'mobile',
                Rule::unique('mobile_numbers', 'number')->where(function ($query) {
                    $query->whereNotNull('user_id');
                }),
            ],
            'country_code' => 'in:'.implode(',', MobileNumber::ALLOWED_COUNTRY_CODES),
        ]);

        //create the number
        $phone = MobileNumber::firstOrCreate([
            'country_code' => $request->input('country_code', MobileNumber::DEFAULT_COUNTRY_CODE),
            'number' => $request->input('phone'),
        ]);

        //check if locked
        if ( !$phone->can_request_code ) {
            throw new MobileNumberException('Too many verification attempts. '.
                'Request for a new verification code in '. $phone->attempts_expiry . ' minutes.');
        }

        //generate the token
        $token = $phone->generateToken();

        //send the token to the user
        $phone->notify( new MobileNumberVerificationToken($token) );

        return response()->json($phone);
    }

    /**
     * Update the mobile number of the user
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //first validate
        $this->validate($request, [
            'phone_id' => [
                'required',
                Rule::exists('mobile_numbers', 'id')->where(function ($query) {
                    $query->whereNull('user_id');
                }),
            ],
            'code' => ['required'],
        ]);

        //find the number
        $phone_id = $request->phone_id;
        $phone = MobileNumber::whereId($phone_id)
            ->whereNull('user_id')
            ->firstOrFail();

        //check if locked
        if ( $phone->is_locked ) {
            throw new MobileNumberException('Too many verification attempts. '.
                'Request for a new verification code in '. $phone->attempts_expiry . ' minutes.');
        }

        //check if the token is expired
        if ( $phone->is_token_expired ) {
            throw new MobileNumberException('The verification code for this number is expired.');
        }

        //verify the code
        if ( $phone->verifyToken( $request->code ) ) {
            //assign the phone to the user
            $user = $request->user();
            $user->updatePhone($phone);
        } else {
            throw new MobileNumberException('The code is invalid.');
        }

        return response()->json($phone);
    }

}
