<?php namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\AddServiceRequest;
use App\Domain\Api\Request\EditJobStatusRequest;
use App\Domain\Api\Request\HoldedJobRequest;
use App\Domain\Api\Request\HoldJobRequest;
use App\Helpers\Dropzone\DropzoneUploadTrait;
use App\Helpers\Enums\ServiceRequestStatuses;
use App\Http\Requests;

use App\Models\HoldedJob;
use App\ServiceRequest;
use App\Http\Requests\ServiceRequestsRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceRequestsController extends Controller {

    public static $pivot_fields = [];
    public static $calculated_fields = [];

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('scopes:read,write');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Type $type
     * @param Request $request
     * @return Response
     */
	public function index(Request $request)
	{
        $default_fields = array_merge(\Schema::getColumnListing('service_requests'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'orderby' => 'sometimes|required|in:id,name,slug,created_at,updated_at',
            'order' => 'sometimes|required|in:ASC,DESC',
            'fields' => 'sometimes|required|array_exists:'.implode(',',$default_fields),
            'per_page' => 'integer|min:1|max:50',
            'page' => 'integer|min:1',
            'city' => 'integer',
            'state' => 'integer',
            'category' => 'integer',
        ]);

        $per_page = $request->input('per_page', 10);
        $orderby = $request->input('orderby', 'name');
        $order = $request->input('order', 'ASC');
        $fields = to_array($request->input('fields', $default_fields));
        $search = $request->input('search');
        $with_city = in_array('city', $fields);

        $fields = array_merge($fields, ['id', 'user_id']);

        if ( $with_city ) {
            $fields = array_merge($fields, ['city_id']);
        }

        $service_requests = ServiceRequest::userVisible()
            ->select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ($search) {
            $service_requests->search($search);
        }

        if ( $city = $request->input('city') ) {
            $service_requests->whereCityId($city);
        }

        if ( $state = $request->input('state') ) {
            $service_requests->hasState($state);
        }

        if ( $category = $request->input('category') ) {
            $service_requests->belongsToCategory($category);
        }

        if ($with_city) {
            $service_requests->with('city');
        }

        $service_requests = $service_requests->orderBy($orderby, $order)
            ->paginate($per_page);

        //fields to hide and show
        $to_hide = array_diff(self::$calculated_fields, $fields);
        $to_show = array_diff(self::$calculated_fields, $to_hide);

        //add calculated fields
        foreach ($service_requests as $service_request) {
            $service_request->setAppends($to_show);
            $service_request->addHidden($to_hide);
        }

        return response()->json($service_requests->appends($request->except(['page'])));
	}

    /**
     * Display a single resource.
     *
     * @param Request $request
     * @param Type $type
     * @return Response
     */
    public function show($service_request_id, Request $request)
    {
        $default_fields = array_merge(\Schema::getColumnListing('service_requests'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'fields' => 'sometimes|required|array_exists:'.implode(',',$default_fields),
        ]);

        $fields = to_array($request->input('fields', $default_fields));
        $with_city = in_array('city', $fields);

        $fields = array_merge($fields, ['id', 'user_id']);

        if ( $with_city ) {
            $fields = array_merge($fields, ['city_id']);
        }

        $service_request = ServiceRequest::userVisible()
            ->select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ($with_city) {
            $service_request = $service_request->with('city');
        }

        try {
            $service_request = $service_request->findOrFail($service_request_id);

            //fields to hide and show
            $to_hide = array_diff(self::$calculated_fields, $fields);
            $to_show = array_diff(self::$calculated_fields, $to_hide);

            //add calculated fields
            $service_request->setAppends($to_show);
            $service_request->addHidden($to_hide);

        } catch ( ModelNotFoundException $e ) {
            \App::abort(404);
        }

        return response()->json($service_request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequestsRequest $request
     * @param  ServiceRequest $service_request
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequestsRequest $request, ServiceRequest $service_request)
    {
        $this->authorize('update', $service_request);

        $service_request->update($request->all());

        $status = $request->input('status');
        $approve = $request->has('approve');
        $user = $request->input('user');

        $service_request->city_id = $request->input('city', null);
        $service_request->updateUser($user);
        $service_request->updateStatus($status, $approve);

        if ( $request->user()->can('editOthers', ServiceRequest::class) ) {
            if ( $slug = $request->input('slug') ) {
                $service_request->slug = $slug;
            }
        }

        //set categories
        $categories = $request->input('categories', []);
        $service_request->categories()->sync($categories);

        $service_request->updateSingleMedia('logo', $request);

        $service_request->save();

        return response()->json($service_request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequestsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequestsRequest $request)
    {
        $this->authorize('create', ServiceRequest::class);

        $service_request = new ServiceRequest($request->all());

        $user = $request->input('user');
        $category = $request->input('category');

        $service_request->updateUser($user);
        $service_request->category()->associate($category);
        $service_request->skip_questions = $request->has('skip_questions');

        $service_request->save();

        //save all the answers
        if ( !$service_request->skip_questions ) {
            $questions = $service_request->questions;
            foreach ( $questions as $question ) {
                //first set the question, then the content
                $answer = new Answer();
                $answer->question()->associate($question);

                $content = $request->input('answers.'.$question->id, '');
                $answer->content = $content;

                $service_request->answers()->save($answer);

                //save file
                if ( $question->is_file ) {
                    $answer->updateSingleMedia('default', $request, 'answers.'.$question->id);
                }
            }
        }

        // sync the businesses
        if ( $businesses = $request->input('businesses', []) ) {
            $service_request->businesses()->sync($businesses);
        }

        return response()->json($service_request);
    }

    public function addServiceRequest(AddServiceRequest $request)
    {
        $service = ServiceRequest::create($request->persist());

        return response()->json([
            'success' => true,
            'service_request' => $service
        ]);
    }

    public function getServiceRequests($userId)
    {
        $services = ServiceRequest::whereUserId($userId)->get();

        return response()->json([
            'success' => true,
            'service_requests' => $services
        ]);
    }

    public function postHoldJob(HoldJobRequest $request)
    {
        $holdedJob = HoldedJob::whereUserId($request->get('user_id'))
            ->whereServiceRequestId($request->get('service_request_id'))
            ->first();

        if (!$holdedJob) {
            $holdedJob = HoldedJob::create($request->persist());
        }

        return response()->json([
            'success' => true,
            'holded_job' => $holdedJob
        ]);
    }

    public function getHoldedJobs($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $jobs = HoldedJob::whereUserId($userId)->get();

        return response()->json([
            'success' => true,
            'jobs' => $jobs
        ]);
    }

    public function postDeleteHoldedJob(HoldedJobRequest $request)
    {
        HoldedJob::find($request->get('holded_job_id'))->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function postEditJobStatus(EditJobStatusRequest $request)
    {
        $job = ServiceRequest::find($request->get('service_request_id'));
        $job->fill([
            'status' => array_search($request->get('status'), ServiceRequestStatuses::$slugs)
        ])->save();

        return response()->json([
            'success' => true,
            'job' => $job
        ]);
    }
}
