<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\CompleteJobRequest;
use App\Domain\Api\Request\JobDetailRequest;
use App\Domain\Api\Service\NotificationService;
use App\Domain\Utility\PushNotificationUtil;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\QuoteAnswer;
use App\Models\QuoteServiceProvider;
use App\Models\ServiceProviderAnswer;
use App\Models\User;
use App\Models\UserQuote;
use App\Models\UserReview;
use Carbon\Carbon;

class JobsController extends Controller
{
    public function userJobs($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $jobs = UserQuote::with(['subCategory', 'allProviders'])
            ->whereUserId($user->id)
            ->where('status', '!=', UserQuote::EXPIRED)
            ->orderBy('id', 'DESC')
            ->get();

        $jobsData = [];
        foreach ($jobs as $job) {
            $data = $job->toArray();
            $data['sub_category'] = $job->subCategory->name;
            $data['category'] = $job->subCategory->category ? $job->subCategory->category->name : "";
            if ($job->status == UserQuote::OPEN) {
                $data['remaining_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $job->expired_at) > Carbon::now() ? Carbon::createFromFormat('Y-m-d H:i:s', $job->expired_at)->diffInHours(Carbon::now()) : 0;
                $data['no_of_quotes'] = $job->allProviders()->whereStatus(QuoteServiceProvider::QUOTED)->count();
            }
            if ($job->status == UserQuote::COMPLETED) {
                $provider = $job->allProviders()->whereStatus(QuoteServiceProvider::COMPLETED)->first();
                $data['service_provider_id'] =$provider ? $provider->service_provider_id : '';
                if ($provider) {
                    $review = UserReview::whereUserId($provider->id)->whereReviewerId($user->id)->first();

                    $data['is_reviewed'] = $review ? true : false;
                    $data['reviews'] = $review ? $review->reviews : "";
                    $data['ratings'] = $review ? $review->ratings : "";
                } else {
                    $data['is_reviewed'] = false;
                    $data['reviews'] = "";
                    $data['ratings'] = "";
                }
            }
            $data['status'] = UserQuote::$labels[$job->status];

            unset($data['all_providers']);
            $jobsData[] = $data;
        }

        return response()->json([
            'success' => true,
            'jobs' => $jobsData
        ]);
    }

    public function serviceProviderJobs($id)
    {
        $serviceProvider = User::find($id);

        if (!$serviceProvider) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $userJobQuotesIds = QuoteServiceProvider::select('user_quotes.id')
            ->join('user_quotes', 'user_quotes.id', 'quote_service_providers.user_quote_id')
            ->where('quote_service_providers.status', '!=', QuoteServiceProvider::AWARDED)
            ->where('quote_service_providers.service_provider_id', $serviceProvider->id)
            ->where('user_quotes.is_all_providers', false)
            ->whereNotIn('user_quotes.status', [UserQuote::EXPIRED, UserQuote::AWARDED, UserQuote::COMPLETED])
            ->get()->pluck('id')->toArray();

        $allProviderJobsIds = UserQuote::where('is_all_providers', true)
            ->whereNotIn('status', [UserQuote::EXPIRED, UserQuote::AWARDED, UserQuote::COMPLETED])
            ->get()->pluck('id')->toArray();

        $jobs = UserQuote::with('allProviders')
            ->whereIn('id', array_unique(array_merge($userJobQuotesIds, $allProviderJobsIds)))
            ->orderBy('id', 'DESC')
            ->get();

        $jobsData = [];
        foreach ($jobs as $job) {
            $data = $job->toArray();
            $data['sub_category'] = $job->subCategory->name;
            $data['category'] = $job->subCategory->category ? $job->subCategory->category->name : "";
            if ($job->status == UserQuote::OPEN) {
                $data['remaining_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $job->expired_at) > Carbon::now() ? Carbon::createFromFormat('Y-m-d H:i:s', $job->expired_at)->diffInHours(Carbon::now()) : 0;
            }

            $provider = $job->allProviders()->whereServiceProviderId($serviceProvider->id)->first();
            $data['status'] = $provider ? QuoteServiceProvider::$labels[$provider->status] : QuoteServiceProvider::NOT_QUOTED;
//            $data['status'] = UserQuote::$labels[$job->status];
            unset($data['all_providers']);
            $jobsData[] = $data;
        }

        return response()->json([
            'success' => true,
            'jobs' => $jobsData
        ]);
    }

    public function jobDetail($id)
    {
        $userQuote = UserQuote::with(['serviceProvider', 'user', 'answers'])->find($id);

        if (!$userQuote) {
            return response()->json([
                'success' => false,
                'message' => 'Job not found.'
            ]);
        }

        $answers = $userQuote->answers()->with('question')->get()->toArray();

        $answers = array_map(function ($answer) {
            return [
                'question' => $answer['question']['question'],
                'answer' => $answer['answer']
            ];
        }, $answers);

        $data = $userQuote->toArray();
        $data['answers'] = $answers;

        return response()->json([
            'success' => true,
            'job' => $data
        ]);
    }

    public function acceptJob(JobDetailRequest $request, NotificationService $notificationService)
    {
        $quoteProvider = QuoteServiceProvider::with(['jobWithUser', 'serviceProviderAllDetail'])
            ->whereUserQuoteId($request->get('job_id'))
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->first();

        if (!$quoteProvider) {
            return response()->json([
                'success' => false,
                'message' => 'Job not found.'
            ]);
        }

        $quoteProvider->fill([
            'status' => QuoteServiceProvider::AWARDED
        ])->save();

        $job = $quoteProvider->jobWithUser;
        $job->fill([
            'status' => UserQuote::AWARDED
        ])->save();

        $user = $quoteProvider->jobWithUser->user;
        $message = 'Congratulations! User- ' . $user->name . ' has awarded you for job - ' . $quoteProvider->job->name . '.';
        $notificationService->addNotification(
            $user->id,
            $quoteProvider->service_provider_id,
            $message,
            Notification::ACCEPT_QUOTE,
            $quoteProvider->job->id
        );

        if ($quoteProvider->serviceProviderAllDetail->is_login) {
            try {
                PushNotificationUtil::sendPush($quoteProvider->serviceProviderAllDetail->player_id, $message, null, ['type' => Notification::ACCEPT_QUOTE]);
            } catch (\Exception $e) {
            }
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function serviceProviderJobDetail(JobDetailRequest $request)
    {
        $userQuote = UserQuote::find($request->get('job_id'));

        $quoteProvider = QuoteServiceProvider::with('job')
            ->whereUserQuoteId($userQuote->id)
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->first();

        if (!$quoteProvider) {
            if (!$userQuote->is_all_providers) {
                return response()->json([
                    'success' => false,
                    'message' => 'Job not found.'
                ]);
            }
            $quoteProvider = QuoteServiceProvider::create([
                'user_quote_id' => $request->get('job_id'),
                'service_provider_id' => $request->get('service_provider_id'),
                'status' => QuoteServiceProvider::NOT_QUOTED
            ]);
        }

        $answers = QuoteAnswer::with('question')
            ->where('user_quote_id', $quoteProvider->user_quote_id)
            ->get()
            ->toArray();

        $answers = array_map(function ($answer) {
            return [
                'question' => $answer['question']['question'],
                'answer' => $answer['answer']
            ];
        }, $answers);

        $user = $quoteProvider->job->user;

        $jobData = $quoteProvider->job->toArray();
        unset($jobData['sub_category']);
        unset($jobData['user']);
        $jobData['status'] = QuoteServiceProvider::$labels[$quoteProvider->status];
        $jobData['username'] = $user->name;
        $jobData['profile'] = $user->profile_url;

        return response()->json([
            'success' => true,
            'job' => array_merge(
                $jobData,
                ['answers' => $answers]
            )
        ]);
    }

    public function getJobProviders($jobId)
    {
        $job = UserQuote::find($jobId);

        if (!$job) {
            return response()->json([
                'success' => false,
                'message' => 'Job not found.'
            ]);
        }

        $providers = QuoteServiceProvider::with('serviceProviderWithReviews')
            ->whereUserQuoteId($job->id)
            ->whereStatus(QuoteServiceProvider::QUOTED)
            ->get();

        $providersData = [];
        foreach ($providers as $provider) {
            $user = $provider->serviceProviderWithReviews;
            $providersData[] = [
                'service_provider_id' => $user->id,
                'name' => $user->name,
                'image' => $user->profile_url,
                'amount' => $provider->amount,
                'total_rate' => $user->reviews()->count(),
                'avg_rate' => $user->reviews()->count() ? $user->reviews()->sum('ratings') / $user->reviews()->count() : 0
            ];
        }

        return response()->json([
            'success' => true,
            'job' => array_merge(
                $job->toArray(),
                ['providers' => $providersData]
            )
        ]);
    }

    public function providerAnswers(JobDetailRequest $request)
    {
        $quoteProvider = QuoteServiceProvider::with('job')
            ->whereUserQuoteId($request->get('job_id'))
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->first();

        if (!$quoteProvider) {
            return response()->json([
                'success' => false,
                'message' => 'Job not found.'
            ]);
        }

        $answers = ServiceProviderAnswer::with('question')
            ->where('quote_service_provider_id', $quoteProvider->id)
            ->get()
            ->toArray();

        $answers = array_map(function ($answer) {
            return [
                'question' => $answer['question']['question'],
                'answer' => $answer['answer']
            ];
        }, $answers);

        $jobData = $quoteProvider->job->toArray();
        $jobData['status'] = UserQuote::$labels[$quoteProvider->job->status];
        unset($jobData['sub_category']);

        return response()->json([
            'success' => true,
            'job' => array_merge(
                $jobData,
                ['answers' => $answers]
            )
        ]);
    }

    public function providerAcceptedJobs($providerId)
    {
        $provider = User::find($providerId);

        if (!$provider) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $jobs = QuoteServiceProvider::with('jobWithUser')
            ->whereServiceProviderId($provider->id)
            ->whereIn('status', [QuoteServiceProvider::AWARDED, QuoteServiceProvider::COMPLETED])
            ->orderBy('id', 'DESC')
            ->get();

        $jobs = array_map(function ($job) {
            $data = $job['job_with_user'];
            $data['category'] = $data['sub_category']['category'] ? $data['sub_category']['category']['name'] : '';
            $data['sub_category'] = $data['sub_category']['name'];
            $data['status'] = QuoteServiceProvider::$labels[$job['status']];
            $data['user_id'] = $data['user']['id'];
            $data['user_name'] = $data['user']['name'];
            unset($data['user']);

            return array_merge(
                $data,
                [
                    'amount' => $job['amount']
                ]
            );
        }, $jobs->toArray());

        return response()->json([
            'success' => true,
            'jobs' => $jobs
        ]);
    }

    public function completeJob(CompleteJobRequest $request, NotificationService $notificationService)
    {
        $quoteProvider = QuoteServiceProvider::with(['job', 'serviceProviderAllDetail'])
            ->whereUserQuoteId($request->get('job_id'))
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->first();

        if (!$quoteProvider) {
            return response()->json([
                'success' => false,
                'message' => 'Job not found.'
            ]);
        }

        $quoteProvider->fill([
            'status' => QuoteServiceProvider::COMPLETED
        ])->save();

        $job = $quoteProvider->job;
        $job->fill([
            'status' => UserQuote::COMPLETED
        ])->save();

        $jobs = QuoteServiceProvider::with('jobWithUser')
            ->whereServiceProviderId($request->get('service_provider_id'))
            ->whereIn('status', [QuoteServiceProvider::AWARDED, QuoteServiceProvider::COMPLETED])
            ->orderBy('id', 'DESC')
            ->get();

        $jobs = array_map(function ($job) {
            $data = $job['job_with_user'];
            $data['category'] = $data['sub_category']['category'] ? $data['sub_category']['category']['name'] : '';
            $data['sub_category'] = $data['sub_category']['name'];
            $data['status'] = QuoteServiceProvider::$labels[$job['status']];
            $data['user_id'] = $data['user']['id'];
            $data['user_name'] = $data['user']['name'];
            unset($data['user']);

            return array_merge(
                $data,
                [
                    'amount' => $job['amount']
                ]
            );
        }, $jobs->toArray());

        $user = $quoteProvider->jobWithUser->user;
        $message = 'Provider- ' . $quoteProvider->serviceProvider->name . ' has been completed job - ' . $quoteProvider->job->name . '.';
        $notificationService->addNotification(
            $quoteProvider->service_provider_id,
            $user->id,
            $message,
            Notification::COMPLETE_JOB,
            $quoteProvider->jobWithUser->id
        );

        if ($user->is_login) {
            try {
                PushNotificationUtil::sendPush($user->player_id, $message, null, ['type' => Notification::COMPLETE_JOB]);
            } catch (\Exception $e) {
            }
        }

        return response()->json([
            'success' => true,
            'jobs' => $jobs
        ]);
    }
}
