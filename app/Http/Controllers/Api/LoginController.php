<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\LoginRequest;
use App\Domain\Api\Request\ServiceProviderSignupRequest;
use App\Domain\Api\Request\SignupRequest;
use App\Domain\Api\Request\UserRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\FbLoginRequest;
use App\Http\Requests\GoogleLoginRequest;
use App\Http\Requests\TwitterLoginRequest;
use App\Models\Role;
use App\Models\UserCategory;
use App\Models\UserSignupAnswer;
use App\Models\User;
use App\Models\UserTag;
use App\Notifications\UserVerificationToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function serviceProviderSignup(ServiceProviderSignupRequest $request)
    {
        if (User::where('mobile_no', '+'.$request->get('phone_number'))->first()) {
            return response()->json([
                'success' => false,
                'message' => 'Mobile number already registered.'
            ]);
        }
        $user = User::create($request->persist());

        $role = Role::whereName(Role::SERVICE_PROVIDER)->firstorfail();
        $user->attachRole($role);
        $user->notify(new UserVerificationToken($user->verification_otp));

        $questionsData = [];
        foreach (json_decode($request->get('questions'), true) as $question) {
            $questionsData[] = [
                'user_id' => $user->id,
                'signup_question_id' => $question['question_id'],
                'answer' => $question['answer']
            ];
        }
        UserSignupAnswer::insert($questionsData);

        $categories = [];
        foreach (json_decode($request->get('categories')) as $categoryId) {
            $categories[] = [
                'user_id' => $user->id,
                'category_id' => $categoryId
            ];
        }
        UserCategory::insert($categories);
        
        Mail::send('emails.signup', ['username' => $user->username], function ($message) use($user) {
            $message->to($user->email);
            $message->subject('Thank you for singup to AVAS Dial');
        });

        $userData = $user->toArray();
        $userData['city'] = $user->city->name;
        $userData['state'] = $user->state->name;
        $userData['image1'] = $user->image1 ? asset('images/users/' . $user->image1) : '';
        $userData['image2'] = $user->image2 ? asset('images/users/' . $user->image2) : '';
        $userData['image3'] = $user->image3 ? asset('images/users/' . $user->image3) : '';
        $userData['image4'] = $user->image4 ? asset('images/users/' . $user->image4) : '';
        $userData['image5'] = $user->image5 ? asset('images/users/' . $user->image5) : '';

        return response()->json([
            'success' => true,
            'user' => array_merge(
                $userData,
                [
                    'user_role' => $role->name
                ]
            )
        ]);
    }

    public function userSignup(SignupRequest $request)
    {
        if (User::where('mobile_no', '+'.$request->get('phone_number'))->first()) {
            return response()->json([
                'success' => false,
                'message' => 'Mobile number already registered.'
            ]);
        }
        $user = User::create($request->persist());

        $role = Role::whereName(Role::USER)->firstorfail();
        $user->attachRole($role);
        $user->notify(new UserVerificationToken($user->verification_otp));

        return response()->json([
            'success' => true,
            'user' => array_merge(
                $user->toArray(),
                ['user_role' => $role->name]
            )
        ]);
    }

    public function login(LoginRequest $request)
    {
        if (auth('web')->attempt(['username' => $request->get('username'), 'password' => $request->get('password')])) {
            $user = auth('web')->user();
            $user->fill([
                'player_id' => $request->get('player_id'),
                'is_login' => true
            ])->save();

            auth('web')->logout();
            $role = User::with(['city', 'state', 'categories'])
                ->select('roles.name')
                ->join('user_role', 'users.id', '=', 'user_role.user_id')
                ->join('roles', 'roles.id', '=', 'user_role.role_id')
                ->where('users.id', $user->id)
                ->first();

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = $role ? $role->name : '';
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            if ($role->name == Role::SERVICE_PROVIDER) {
                $userData['categories'] = $user->categories()->select('categories.id', 'categories.name')->get();
                $userData['city'] = $user->city->name;
                $userData['state'] = $user->state->name;
                $userData['image1'] = $user->image1 ? asset('images/users/' . $user->image1) : '';
                $userData['image2'] = $user->image2 ? asset('images/users/' . $user->image2) : '';
                $userData['image3'] = $user->image3 ? asset('images/users/' . $user->image3) : '';
                $userData['image4'] = $user->image4 ? asset('images/users/' . $user->image4) : '';
                $userData['image5'] = $user->image5 ? asset('images/users/' . $user->image5) : '';
            }

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid Credentials.'
        ]);
    }

    public function fbLogin(FbLoginRequest $request)
    {
        $user = User::whereEmail($request->get('email'))->first();

        if (!$user) {
            $user = User::create($request->persist());
            $role = Role::whereName(Role::USER)->firstorfail();
            $user->attachRole($role);

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        if ($user->facebook_id && $user->facebook_id == $request->get('facebook_id')) {
            $data = $request->persist();
            unset($data['username']);

            $user->fill($data)->save();

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Email has already been taken.'
        ]);
    }

    public function googleLogin(GoogleLoginRequest $request)
    {
        $user = User::whereEmail($request->get('email'))->first();

        if (!$user) {
            $user = User::create($request->persist());
            $role = Role::whereName(Role::USER)->firstorfail();
            $user->attachRole($role);

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        if ($user->google_id && $user->google_id == $request->get('google_id')) {
            $data = $request->persist();
            unset($data['username']);

            $user->fill($data)->save();

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Email has already been taken.'
        ]);
    }

    public function twitterLogin(TwitterLoginRequest $request)
    {
        $user = User::whereEmail($request->get('email'))->first();

        if (!$user) {
            $user = User::create($request->persist());
            $role = Role::whereName(Role::USER)->firstorfail();
            $user->attachRole($role);

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        if ($user->twitter_id && $user->twitter_id == $request->get('twitter_id')) {
            $data = $request->persist();
            unset($data['username']);

            $user->fill($data)->save();

            $userData = $user->toArray();
            $userData['is_fb_user'] = $user->is_fb_user ? true : false;
            $userData['is_google_user'] = $user->is_google_user ? true : false;
            $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
            $userData['user_role'] = Role::USER;
            $userData['is_verified'] = $user->is_verified ? 1 : 0;

            return response()->json([
                'success' => true,
                'user' => $userData
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Email has already been taken.'
        ]);
    }

    public function verifyUser(UserRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill([
            'verification_otp' => null,
            'is_verified' => true
        ])->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function resendVerifyOtp(UserRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill([
            'verification_otp' => rand(100000, 999999)
        ])->save();
        $user->notify(new UserVerificationToken($user->verification_otp));

        return response()->json([
            'success' => true,
            'otp' => $user->verification_otp
        ]);
    }
}
