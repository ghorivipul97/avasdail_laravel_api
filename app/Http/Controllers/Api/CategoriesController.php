<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\CategoryPageImage;

class CategoriesController extends Controller
{
    public function categories()
    {
        $categories = Category::whereParentId(null)->get();

        return response()->json([
            'success' => true,
            'categories' => $categories,
            'slider-images' => CategoryPageImage::all()->pluck('image_url')->toArray()
        ]);
    }

    public function subCategories($categoryId)
    {
        $subCategories = Category::whereParentId($categoryId)->get();

        return response()->json([
            'success' => true,
            'sub_categories' => $subCategories
        ]);
    }
}
