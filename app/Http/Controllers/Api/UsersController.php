<?php
namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\ChangeLocationRequest;
use App\Domain\Api\Request\ChangePasswordRequest;
use App\Domain\Api\Request\GetProvidersRequest;
use App\Domain\Api\Request\ProviderDetailRequest;
use App\Domain\Api\Request\UpdateMobileNumberRequest;
use App\Domain\Api\Request\UserRequest;
use App\Models\Category;
use App\Domain\Api\Request\FavouriteServiceProviderRequest;
use App\Domain\Api\Request\ProfileRequest;
use App\Models\Notification;
use App\Models\Role;
use App\Models\ServiceProviderFavourite;
use App\Models\ServiceRequest;
use App\Models\SignupQuestion;
use App\Models\State;
use App\Models\User;
use App\Models\UserQuote;
use App\Notifications\UpdateNumberVerificationToken;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function serviceProviders(GetProvidersRequest $request)
    {
        $user = User::find($request->get('user_id'));

        if ($user->latitude && $user->longitude) {
            $users = User::with('reviews')
                ->select(DB::raw('users.*,
            ( 3959 * acos( cos( radians(' . $user->latitude . ') ) * cos( radians( latitude ) ) *
            cos( radians( longitude ) - radians(' . $user->longitude . ') ) + sin( radians(' . $user->latitude . ') ) *
            sin( radians( latitude ) ) ) ) AS distance'))
                ->having('distance', '<', 10)
                ->join('user_role', 'users.id', '=', 'user_role.user_id')
                ->join('roles', 'user_role.role_id', '=', 'roles.id')
                ->join('user_categories', 'user_categories.user_id', '=', 'users.id')
                ->where('roles.name', \App\Models\Role::SERVICE_PROVIDER)
                ->where('user_categories.category_id', $request->get('sub_category_id'))
                ->get();
        } else {
            $users = User::with('reviews')
                ->join('user_role', 'users.id', '=', 'user_role.user_id')
                ->join('roles', 'user_role.role_id', '=', 'roles.id')
                ->join('user_categories', 'user_categories.user_id', '=', 'users.id')
                ->where('roles.name', \App\Models\Role::SERVICE_PROVIDER)
                ->where('user_categories.category_id', $request->get('sub_category_id'))
                ->get();
        }

        $usersData = [];
        foreach ($users as $user) {
            $data = $user->toArray();
            unset($data['reviews']);

            $usersData[] = array_merge(
                $data,
                [
                    'reviewsCount' => $user->reviews()->count(),
                    'reviewsAverage' => $user->reviews()->count() == 0 ? 0 : $user->reviews()->sum('ratings') / $user->reviews()->count()
                ]
            );
        }

        return response()->json([
            'success' => true,
            'service_providers' => $usersData
        ]);
    }

    public function getServiceProviderDetail(ProviderDetailRequest $request, $userId)
    {
        $user = User::with(['reviews', 'signupAnswers', 'categories', 'city', 'state'])->find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $userData = $user->toArray();

        $hoursArray = json_decode($user['opening_hours'], true);
        $currentHour = Carbon::now($request->get('timezone'));
        $currentDay = $this->getDay();
        if (count($hoursArray)) {
            if (isset($hoursArray[$currentDay])) {
                $status = null;
                foreach ($hoursArray[$currentDay] as $times) {
                    $from = Carbon::createFromFormat('g:i a', $times['from']);
                    $to = Carbon::createFromFormat('g:i a', $times['to']);

                    if ($currentHour->between($from, $to)) {
                        if ($currentHour->diffInMinutes($to) <= 20) {
                            $status = 'Closing Soon';
                            break;
                        }
                        $status = 'Open';
                    } else {
                        if ($currentHour->diffInMinutes($from) <= 20) {
                            $status = 'Opening Soon';
                            break;
                        }
                    }
                }

                $userData['user_status'] = $status ? $status : 'Close';
            } else {
                $userData['user_status'] = 'Close';
            }
        } else {
            $userData['user_status'] = 'Close';
        }

        $userData['reviewsCount'] = $user->reviews()->count();
        $userData['city'] = $user->city ? $user->city->name : "";
        $userData['state'] = $user->state ? $user->state->name : "";
        $userData['reviewsAverage'] = $user->reviews()->count() == 0 ? 0 : $user->reviews()->sum('ratings') / $user->reviews()->count();
        if ($request->get('user_id')) {
            $review = $user->reviews()->where('reviewer_id', $request->get('user_id'))->first();
            $userData['is_reviewed'] = $review ? true : false;
            $userData['review'] = $review ? $review->reviews : '';
            $userData['ratings'] = $review ? $review->ratings : '';
            $userData['is_favourite'] = ServiceProviderFavourite::where('service_provider_id', $user->id)->where('user_id', $request->get('user_id'))->first() ? true : false;
        }
        $userData['image1'] = $user->image1 ? asset('images/users/' . $user->image1) : '';
        $userData['image2'] = $user->image2 ? asset('images/users/' . $user->image2) : '';
        $userData['image3'] = $user->image3 ? asset('images/users/' . $user->image3) : '';
        $userData['image4'] = $user->image4 ? asset('images/users/' . $user->image4) : '';
        $userData['image5'] = $user->image5 ? asset('images/users/' . $user->image5) : '';

        $signupAnswers = array_map(function($answer) {
            return [
                'question_id' => $answer['question']['id'],
                'question' => $answer['question']['question'],
                'answer' => $answer['answer']
            ];
        }, $user->signupAnswers->toArray());

        $categories = array_map(function($category) {
            return [
                'id' => $category['id'],
                'name' => $category['name']
            ];
        }, $user->categories()->select('categories.id', 'categories.name')->get()->toArray());

        $userData['signup_answers'] = $signupAnswers;
        $userData['categories'] = $categories;
        unset($userData['reviews']);

        return response()->json([
            'success' => true,
            'user' => $userData
        ]);
    }

    public function getDay()
    {
        $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        $dow_numeric = Carbon::now()->dayOfWeek;
        return $dowMap[$dow_numeric];
    }

    public function postProfile(ProfileRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill($request->persist())->save();

        $role = User::with(['city', 'state', 'categories'])
            ->select('roles.name')
            ->join('user_role', 'users.id', '=', 'user_role.user_id')
            ->join('roles', 'roles.id', '=', 'user_role.role_id')
            ->where('users.id', $user->id)
            ->first();

        $userData = $user->toArray();
        $userData['is_fb_user'] = $user->is_fb_user ? true : false;
        $userData['is_google_user'] = $user->is_google_user ? true : false;
        $userData['is_twitter_user'] = $user->is_twitter_user ? true : false;
        $userData['user_role'] = $role ? $role->name : '';

        if ($role->name == Role::SERVICE_PROVIDER) {
            $userData['categories'] = $user->categories()->select('categories.id', 'categories.name')->get();
            $userData['city'] = $user->city->name;
            $userData['state'] = $user->state->name;
            $userData['image1'] = $user->image1 ? asset('images/users/' . $user->image1) : '';
            $userData['image2'] = $user->image2 ? asset('images/users/' . $user->image2) : '';
            $userData['image3'] = $user->image3 ? asset('images/users/' . $user->image3) : '';
            $userData['image4'] = $user->image4 ? asset('images/users/' . $user->image4) : '';
            $userData['image5'] = $user->image5 ? asset('images/users/' . $user->image5) : '';
        }

        return response()->json([
            'success' => true,
            'user' => $userData
        ]);
    }

    public function getServiceProvidersBySubCategory($subCategoryId)
    {
        $subCategory = Category::where('parent_id', '!=', null)->find($subCategoryId);

        if (!$subCategory) {
            return response()->json([
                'success' => false,
                'message' => 'Sub category not found.'
            ]);
        }

        $userIdArray = ServiceRequest::whereCategoryId($subCategory->id)->groupBy('user_id')->select('user_id')->pluck('user_id')->toArray();

        $users = User::whereIn('id', $userIdArray)->get();

        return response()->json([
            'success' => true,
            'service_providers' => $users
        ]);
    }

    public function postFavouriteServiceProvider(FavouriteServiceProviderRequest $request)
    {
        $favourite = ServiceProviderFavourite::whereServiceProviderId($request->get('service_provider_id'))
            ->whereUserId($request->get('user_id'))
            ->first();

        if (!$favourite) {
            ServiceProviderFavourite::create($request->persist());

            return response()->json([
                'success' => true,
                'favourite' => true
            ]);
        }

        $favourite->delete();

        return response()->json([
            'success' => true,
            'favourite' => false
        ]);
    }

    public function getServiceProviderFavourites($userId)
    {
        $user = User::with(['favourites'])->find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $favouritesData = [];
        foreach ($user->favourites as $favourite) {
            $data = $favourite->toArray();
            unset($data['reviews_without_reviewer']);

            $favouritesData[] = array_merge(
                $data,
                [
                    'reviewsCount' => $favourite->reviewsWithoutReviewer()->count(),
                    'reviewsAverage' => $favourite->reviewsWithoutReviewer()->count() == 0 ? 0 : $favourite->reviewsWithoutReviewer()->sum('ratings') / $favourite->reviewsWithoutReviewer()->count()
                ]
            );
        }

        return response()->json([
            'success' => true,
            'favourites' => $favouritesData
        ]);
    }

    public function getServiceProviderProfile($userId)
    {
        $user = User::with('favourites', 'reviews')->find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        $userArray = $user->toArray();
        $userArray['favourites'] = $user->favourites()->count();
        $userArray['reviews'] = $user->reviews()->count();

        return response()->json([
            'success' => true,
            'service_provider' => $userArray
        ]);
    }

    public function signupData()
    {
        return response()->json([
            'success' => true,
            'categories' => Category::with('subcategories')->get(),
            'questions' => SignupQuestion::all(),
            'states' => State::with('cities')->get()
        ]);
    }

    public function postChangeLocation(ChangeLocationRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill($request->persist())->save();

        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    public function allUsers()
    {
        return response()->json([
            'success' => true,
            'users' => User::all()
        ]);
    }

    public function resetUsers()
    {
        User::where('id', '!=', null)->delete();

        dd('done');
    }

    public function allJobs()
    {
        return response()->json([
            'jobs' => UserQuote::with('allProviders')->get()
        ]);
    }

    public function deleteJobs()
    {
        UserQuote::where('id', '!=', null)->delete();

        dd('done');
    }

    public function deleteNotifications()
    {
        Notification::where('id', '!=', null)->delete();

        dd('done');
    }

    public function logout(UserRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill([
            'is_login' => false
        ])->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = User::find($request->get('user_id'));

        if (!auth()->attempt(['email' => $user->email, 'password' => $request->get('old_password')])) {
            return response()->json([
                'success' => false,
                'message' => 'Old password did not match.'
            ]);
        }
        auth()->logout();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function expireJobs()
    {
        $jobs = UserQuote::with('quotedProviders')
            ->where('expired_at', '<=', Carbon::now())
            ->whereStatus(UserQuote::OPEN)
            ->get();

        foreach ($jobs as $job) {
            if (!$job->quotedProviders()->count()) {
                $job->status = UserQuote::EXPIRED;
                $job->save();
            }
        }
    }

    public function updateNumber(UpdateMobileNumberRequest $request)
    {
        $existsNumber = User::where('mobile_no', '+'.$request->get('mobile_number'))
            ->where('id', '!=', $request->get('user_id'))
            ->first();
        if ($existsNumber) {
            return response()->json([
                'success' => false,
                'message' => 'Mobile number used by another user.'
            ]);
        }
        $user = User::find($request->get('user_id'));
        $user->fill([
            'mobile_no' => '+'.$request->get('mobile_number')
        ])->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function sendUpdateNumberOtp(UserRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->fill([
            'update_number_otp' => rand(100000, 999999)
        ])->save();
        $user->notify(new UpdateNumberVerificationToken($user->update_number_otp));

        return response()->json([
            'success' => true,
            'otp' => $user->update_number_otp
        ]);
    }
}
