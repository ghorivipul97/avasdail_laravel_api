<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\AddQuoteRequest;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\RequestQuote;
use App\Models\ServiceRequest;
use Illuminate\Http\Request;

class QuoteController extends Controller
{
    public function addQuote(AddQuoteRequest $request)
    {
        $serviceRequest = ServiceRequest::with('user')->find($request->get('service_request_id'));
        $quote = RequestQuote::create($request->persist());

        Notification::create([
            'sender_id' => $request->get('user_id'),
            'receiver_id' => $serviceRequest->user->id,
            'notification' => $quote->user->username . ' has requested quote for work ' . $serviceRequest->note,
            'parent_id' => $quote->id
        ]);

        return response()->json([
            'success' => true,
            'quote' => $quote
        ]);
    }
    
    public function getQuotes($serviceRequestId)
    {
        $service = ServiceRequest::find($serviceRequestId);

        if (!$service) {
            return response()->json([
                'success' => false,
                'message' => 'Request not found.'
            ]);
        }

        $quotes = RequestQuote::whereServiceRequestId($service->id)->get();

        return response()->json([
            'success' => true,
            'quotes' => $quotes
        ]);
    }
}
