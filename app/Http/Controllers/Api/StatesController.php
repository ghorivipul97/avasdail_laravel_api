<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;

use App\State;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatesController extends Controller {

    public static $pivot_fields = ['cities'];
    public static $calculated_fields = ['formatted_name'];

    /**
     * Display a listing of the resource.
     *
     * @param Type $type
     * @param Request $request
     * @return Response
     */
	public function index(Request $request)
	{
        $default_fields = array_merge(\Schema::getColumnListing('states'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'orderby' => 'sometimes|required|in:id,name,created_at,updated_at',
            'order' => 'sometimes|required|in:ASC,DESC',
            'fields' => 'sometimes|required|array_exists:'.implode(',', $default_fields),
            'per_page' => 'integer|min:1|max:50',
            'page' => 'integer|min:1',
            'search' => 'string',
            'type' => 'integer',
        ]);

        $per_page = $request->input('per_page', 10);
        $orderby = $request->input('orderby', 'name');
        $order = $request->input('order', 'ASC');
        $fields = to_array($request->input('fields', $default_fields));
        $search = $request->input('search');
        $type = $request->input('type');
        $with_cities = in_array('cities', $fields);

        if ( !in_array('id', $fields) ) {
            $fields = array_merge($fields, ['id']);
        }

        if ( in_array('formatted_name', $fields) &&
            ( !in_array('name', $fields) || !in_array('type', $fields) || !in_array('abbr', $fields) ) ) {
            $fields = array_merge($fields, ['type', 'abbr', 'name']);
        }

        $states = State::select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ($search) {
            $states = $states->search($search);
        }

        if ($type) {
            $states = $states->whereType($type);
        }

        if ( $with_cities ) {
            $states = $states->with('cities');
        }

        $states = $states->orderBy($orderby, $order)
            ->paginate($per_page);

        //fields to hide and show
        $to_hide = array_diff(self::$calculated_fields, $fields);
        $to_show = array_diff(self::$calculated_fields, $to_hide);

        //add calculated fields
        foreach ($states as $state) {
            $state->setAppends($to_show);
            $state->addHidden($to_hide);
        }
        
        return response()->json($states->appends($request->except(['page'])));
	}

    /**
     * Display a single resource.
     *
     * @param Request $request
     * @param Type $type
     * @return Response
     */
    public function show($state_id, Request $request)
    {
        $default_fields = array_merge(\Schema::getColumnListing('states'), self::$pivot_fields, self::$calculated_fields);

        $this->validate($request, [
            'fields' => 'sometimes|required|array_exists:'.implode(',',$default_fields),
        ]);

        $fields = to_array($request->input('fields', $default_fields));
        $with_cities = in_array('cities', $fields);

        if ( !in_array('id', $fields) ) {
            $fields = array_merge($fields, ['id']);
        }

        $state = State::select(array_diff($fields, self::$pivot_fields, self::$calculated_fields));

        if ( $with_cities ) {
            $state = $state->with('cities');
        }

        try {
            $state = $state->findOrFail($state_id);

            //fields to hide and show
            $to_hide = array_diff(self::$calculated_fields, $fields);
            $to_show = array_diff(self::$calculated_fields, $to_hide);

            $state->setAppends($to_show);
            $state->addHidden($to_hide);

        } catch ( ModelNotFoundException $e ) {
            \App::abort(404);
        }

        return response()->json($state);
    }

}
