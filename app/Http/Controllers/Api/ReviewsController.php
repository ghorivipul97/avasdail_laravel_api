<?php

namespace App\Http\Controllers\Api;

use App\Domain\Api\Request\AddReviewRequest;
use App\Http\Controllers\Controller;
use App\Models\UserReview;
use App\Models\User;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    public function addReview(AddReviewRequest $request)
    {
        $userReview = UserReview::whereUserId($request->get('user_id'))
            ->whereReviewerId($request->get('reviewer_id'))
            ->first();

        if (!$userReview) {
            $userReview = UserReview::create($request->persist());
        } else {
            $userReview->fill([
                'ratings' => $request->get('ratings'),
                'reviews' => $request->get('reviews')
            ])->save();
        }

        $providerReviews = $userReview->provider->reviews();
        $userReview['reviewsCount'] = $providerReviews->count();
        $userReview['reviewsAverage'] = $providerReviews->count() == 0 ? 0 : $providerReviews->sum('ratings') / $providerReviews->count();

        return response()->json([
            'success' => true,
            'reviews' => $userReview
        ]);
    }

    public function getReviews($userId)
    {
        $user = User::with('reviews')->find($userId);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        }

        return response()->json([
            'success' => true,
            'reviews' => $user->reviews
        ]);
    }
}
