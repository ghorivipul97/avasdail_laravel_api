<?php
namespace App\Http\Controllers\Api;

use App\Models\QuoteQuestion;
use App\Http\Controllers\Controller;
use App\Models\ServiceProviderQuestion;

class QuestionsController extends Controller
{
    public function getQuoteQuestions()
    {
        $questions = QuoteQuestion::all();

        return response()->json([
            'success' => true,
            'questions' => array_merge(
                $questions->map(function($question) {
                    return [
                        'id' => $question->id,
                        'question' => $question->is_required ? $question->question . '*' : $question->question,
                        'is_required' => (bool) $question->is_required
                    ];
                })->toArray(),
                [
                    [
                        'id' => 0,
                        'question' => 'Enter Job Description*',
                        'is_required' => true
                    ]
                ]
            )
        ]);
    }

    public function getServiceProviderQuestions()
    {
        $questions = ServiceProviderQuestion::all();

        return response()->json([
            'success' => true,
            'questions' => array_merge(
                $questions->map(function($question) {
                    return [
                        'id' => $question->id,
                        'question' => $question->is_required ? $question->question . '*' : $question->question,
                        'is_required' => (bool) $question->is_required
                    ];
                })->toArray(),
                [
                    [
                        'id' => 0,
                        'question' => 'Enter Amount*',
                        'is_required' => true
                    ]
                ]
            )
        ]);
    }
}
