<?php

namespace App;

use App\Helpers\SlugTrait;
use App\Helpers\UpdateMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Category extends Model implements HasMediaConversions
{
    use HasMediaTrait;
    use UpdateMediaTrait;
    use NodeTrait;
    use SlugTrait;

    protected $morphClass = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * Checks if the category has a parent
     */
    public function getHasParentAttribute() {
        return ! is_null($this->parent_id);
    }

    /**
     * Checks if the category have any children
     */
    public function getHasChildrenAttribute() {
        return $this->children()->exists();
    }

    /**
     * Get depth name
     */
    public function getDepthNameAttribute() {
        return str_repeat('― ', $this->depth) . $this->name;
    }

    /**
     * Returns a flattened list of categories
     * @param null $skip_id id to not return
     * @return array
     */
    public static function categoryList($skip_id = null) {
        $categories = Category::withDepth()
            ->defaultOrder()
            ->orderBy('name');

        if ( $skip_id ) {
            $categories = $categories
                ->where('id', '!=', $skip_id)
                ->whereNotDescendantOf($skip_id);
        }

        return $categories->get()->pluck('depth_name', 'id')->all();
    }

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('name', 'like', '%'.$search.'%')
            ->orWhere('slug', '=', $search);
    }

    /**
     * Defines image sizes
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(50)
            ->height(50)
            ->fit('crop', 50, 50)
            ->performOnCollections('category_image');

        $this->addMediaConversion('category-thumb')
            ->width(500)
            ->height(500)
            ->fit('crop', 500, 500)
            ->performOnCollections('category_image');
    }

    /**
     * Returns the category image
     */
    public function getCategoryImageAttribute()
    {
        return $this->getFirstMediaUrl('category_image');
    }

    /**
     * A category belongs to many businesses
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function businesses()
    {
        return $this->belongsToMany('App\Business');
    }

    /**
     * A category has many questions
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * Get inherited questions
     */
    public function getInheritedQuestionsAttribute()
    {
        $ancestors = Category::whereAncestorOf($this)
            ->pluck('id')
            ->all();

        $questions = Question::whereIn('category_id', $ancestors);

        if ( $ancestors ) {
            $questions = $questions->orderByRaw('FIELD(category_id, '.trim(str_repeat('?,', count($ancestors)), ',').')', $ancestors);
        }

        $questions = $questions->orderBy('menu_order', 'ASC')->get();

        return $questions;
    }

    /**
     * Get all questions
     */
    public function allQuestions()
    {
        $ancestors = Category::whereAncestorOf($this)
            ->pluck('id')
            ->all();

        //add the current id as last
        $ancestors[] = $this->id;

        $questions = Question::whereIn('category_id', $ancestors);

        if ( $ancestors ) {
            // not supported by sqlite
            if ( !App::environment('testing') ) {
                $questions->orderByRaw('FIELD(category_id, ' . trim(str_repeat('?,', count($ancestors)), ',') . ')', $ancestors);
            }
        }

        return $questions->orderBy('menu_order', 'ASC');
    }
}
