<?php

namespace App;

use App\Helpers\Enums\CityTypes;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $morphClass = 'city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'type', 'name'];

    /**
     * A city belongs to a state
     */
    public function state() {
        return $this->belongsTo('App\State');
    }

    /**
     * Get formatted name
     */
    public function getFormattedNameAttribute() {
        return $this->type == CityTypes::ISLAND ?
            $this->state->abbr . '. ' . $this->name :
            $this->state->name . ', ' . $this->name;
    }

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('name', 'like', '%'.$search.'%')
            ->orWhere('code', '=', $search);
    }

    /**
     * Distance scope
     * @param $query
     * @param $lat
     * @param $lng
     * @param null $radius
     * @param string $unit
     * @return
     */
    public function scopeWithDistance($query, $lat, $lng, $radius = null, $unit = "km") {
        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $lat = (float) $lat;
        $lng = (float) $lng;
        $distance = '(? * ACOS(COS(RADIANS(?))
                        * COS(RADIANS(lat))
                        * COS(RADIANS(?) - RADIANS(lng))
                        + SIN(RADIANS(?))
                        * SIN(RADIANS(lat))))';
        $query = $query->selectRaw("$distance AS distance",
            [$unit, $lat, $lng, $lat]
        );

        if ( $radius ) {
            $query = $query->whereRaw("$distance <= ?", [$unit, $lat, $lng, $lat, $radius]);
        }

        return $query;
    }
}
