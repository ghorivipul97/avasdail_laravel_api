<?php

namespace App;

use App\Helpers\ApprovedTrait;
use App\Helpers\Enums\ReviewStatuses;
use App\Helpers\HasBusinessTrait;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use ApprovedTrait;
    use HasBusinessTrait;

    protected $morphClass = 'review';

    protected static $status_class = ReviewStatuses::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rating',
        'content',
    ];

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('content', 'like', '%'.$search.'%');
    }

    /**
     * Return the draft key
     */
    public function getDeactivatedKey() {
        $status_class = static::$status_class;
        return $status_class::DRAFT;
    }

    /**
     * Get rating stars html
     */
    public function getRatingStarsAttribute()
    {
        $html = '';

        //first get all the filled star
        $html .= str_repeat('<i class="zmdi zmdi-star"></i> ', $this->rating);

        //get remaining
        $html .= str_repeat('<i class="zmdi zmdi-star-outline"></i> ', 5 - $this->rating);

        return $html;
    }
}
