<?php

namespace App\Policies;

use App\User;
use App\Review;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the review.
     *
     * @param  App\User  $user
     * @param  App\Review  $review
     * @return mixed
     */
    public function view(User $user, Review $review)
    {
        return $review->is_approved || $this->update($user, $review);
    }

    /**
     * Determine whether the user can create reviews.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_reviews');
    }

    /**
     * Determine whether the user can update the review.
     *
     * @param  App\User  $user
     * @param  App\Review  $review
     * @return mixed
     */
    public function update(User $user, Review $review)
    {
        //edit_reviews permission required
        if ( $user->can('edit_reviews') ) {
            //edit_others_reviews permission required to edit others
            return $review->user_id == $user->id || $this->editOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the review.
     *
     * @param  App\User  $user
     * @param  App\Review  $review
     * @return mixed
     */
    public function delete(User $user, Review $review)
    {
        //delete_reviews permission required
        if ( $user->can('delete_reviews') ) {
            //delete_others_reviews permission required to delete others
            return $review->user_id == $user->id || $this->deleteOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can see all reviews
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('view_reviews');
    }

    /**
     * Determine whether the user can edit others reviews
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_others_reviews') && $user->can('edit_reviews');
    }

    /**
     * Determine whether the user can delete others reviews
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_others_reviews') && $user->can('delete_reviews');
    }

    /**
     * Determine whether the user can delete own reviews
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_reviews');
    }

    /**
     * Determine whether the user can approve reviews
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function approve(User $user)
    {
        return $user->can('approve_reviews');
    }
}
