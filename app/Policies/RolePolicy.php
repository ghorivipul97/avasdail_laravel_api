<?php

namespace App\Policies;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  App\User  $user
     * @param  App\Role  $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return $this->update($user, $role);
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_roles');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  App\User  $user
     * @param  App\Role  $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $user->can('edit_roles');
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  App\User  $user
     * @param  App\Role  $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return $user->can('delete_roles');
    }

    /**
     * Determine whether the user can see all roles
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('edit_roles');
    }
}
