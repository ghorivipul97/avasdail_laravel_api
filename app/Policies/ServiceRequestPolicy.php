<?php

namespace App\Policies;

use App\User;
use App\ServiceRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServiceRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the service_request.
     *
     * @param  App\User  $user
     * @param  App\ServiceRequest  $service_request
     * @return mixed
     */
    public function view(User $user, ServiceRequest $service_request)
    {
        return $this->update($user, $service_request);
    }

    /**
     * Determine whether the user can create service_requests.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_service_requests');
    }

    /**
     * Determine whether the user can update the service_request.
     *
     * @param  App\User  $user
     * @param  App\ServiceRequest  $service_request
     * @return mixed
     */
    public function update(User $user, ServiceRequest $service_request)
    {
        //edit_resources permission required
        if ( $user->can('edit_service_requests') ) {
            //edit_others_service_requests permission required to edit others
            return $service_request->user_id == $user->id || $this->editOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the service_request.
     *
     * @param  App\User  $user
     * @param  App\ServiceRequest  $service_request
     * @return mixed
     */
    public function delete(User $user, ServiceRequest $service_request)
    {
        //delete_resources permission required
        if ( $user->can('delete_service_requests') ) {
            //delete_others_service_requests permission required to delete others
            return $service_request->user_id == $user->id || $this->deleteOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can see all service_requests
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('view_service_requests');
    }

    /**
     * Determine whether the user can edit others service_requests
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_others_service_requests') && $user->can('edit_service_requests');
    }

    /**
     * Determine whether the user can delete others service_requests
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_others_service_requests') && $user->can('delete_service_requests');
    }

    /**
     * Determine whether the user can delete own service_requests
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_service_requests');
    }

}
