<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  App\User  $user
     * @param  App\User  $user_model
     * @return mixed
     */
    public function view(User $user, User $user_model)
    {
        return $user->can('edit_users') || $user->id == $user_model->id;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_users');
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  App\User  $user
     * @param  App\User  $user_model
     * @return mixed
     */
    public function update(User $user, User $user_model)
    {
        return $user->can('edit_users') || $user->id == $user_model->id;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  App\User  $user
     * @param  App\User  $user_model
     * @return mixed
     */
    public function delete(User $user, User $user_model)
    {
        return $user->can('delete_users');
    }

    /**
     * Determine whether the user can access the profile
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function profile(User $user)
    {
        return $user->can('edit_users');
    }

    /**
     * Determine whether the user can see all users
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('edit_users');
    }

    /**
     * Determine whether the user can delete other users
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_users');
    }

    /**
     * Determine whether the user can delete own user account
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_users');
    }

    /**
     * Determine whether the user can edit other users
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_users');
    }

    /**
     * Determine whether the user can assign phone numbers
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function assignMobileNumbers(User $user)
    {
        return $this->editOthers($user);
    }
}
