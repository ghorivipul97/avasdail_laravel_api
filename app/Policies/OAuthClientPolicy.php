<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Laravel\Passport\Client;

class OAuthClientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the oauth_client.
     *
     * @param  App\User  $user
     * @param  Laravel\Passport\Client  $oauth_client
     * @return mixed
     */
    public function view(User $user, Client $oauth_client)
    {
        return $this->update($user, $oauth_client);
    }

    /**
     * Determine whether the user can create oauth_clients.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_oauth_clients');
    }

    /**
     * Determine whether the user can update the oauth_client.
     *
     * @param  App\User  $user
     * @param  Laravel\Passport\Client  $oauth_client
     * @return mixed
     */
    public function update(User $user, Client $oauth_client)
    {
        //edit_oauth_clients permission required
        if ( $user->can('edit_oauth_clients') ) {
            //edit_others_oauth_clients permission required to edit others
            return $oauth_client->user_id == $user->id || $this->editOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the oauth_client.
     *
     * @param  App\User  $user
     * @param  Laravel\Passport\Client  $oauth_client
     * @return mixed
     */
    public function delete(User $user, Client $oauth_client)
    {
        //delete_oauth_clients permission required
        if ( $user->can('delete_oauth_clients') ) {
            //delete_others_oauth_clients permission required to delete others
            return $oauth_client->user_id == $user->id || $this->deleteOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can see all oauth_clients
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('view_oauth_clients');
    }

    /**
     * Determine whether the user can edit others oauth_clients
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_others_oauth_clients') && $user->can('edit_oauth_clients');
    }

    /**
     * Determine whether the user can edit own oauth_clients
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOwn(User $user)
    {
        return $user->can('edit_oauth_clients');
    }

    /**
     * Determine whether the user can delete others oauth_clients
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_others_oauth_clients') && $user->can('delete_oauth_clients');
    }

    /**
     * Determine whether the user can delete own oauth_clients
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_oauth_clients');
    }
}
