<?php

namespace App\Policies;

use App\User;
use App\Business;
use Illuminate\Auth\Access\HandlesAuthorization;

class BusinessPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the business.
     *
     * @param  App\User  $user
     * @param  App\Business  $business
     * @return mixed
     */
    public function view(User $user, Business $business)
    {
        return $business->is_approved || $this->update($user, $business);
    }

    /**
     * Determine whether the user can create businesses.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('edit_businesses');
    }

    /**
     * Determine whether the user can update the business.
     *
     * @param  App\User  $user
     * @param  App\Business  $business
     * @return mixed
     */
    public function update(User $user, Business $business)
    {
        //edit_businesses permission required
        if ( $user->can('edit_businesses') ) {
            //edit_others_businesses permission required to edit others
            return $business->user_id == $user->id || $this->editOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the business.
     *
     * @param  App\User  $user
     * @param  App\Business  $business
     * @return mixed
     */
    public function delete(User $user, Business $business)
    {
        //delete_businesses permission required
        if ( $user->can('delete_businesses') ) {
            //delete_others_businesses permission required to delete others
            return $business->user_id == $user->id || $this->deleteOthers($user);
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can see all businesses
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can('view_businesses');
    }

    /**
     * Determine whether the user can edit others businesses
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function editOthers(User $user)
    {
        return $user->can('edit_others_businesses') && $user->can('edit_businesses');
    }

    /**
     * Determine whether the user can delete others businesses
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOthers(User $user)
    {
        return $user->can('delete_others_businesses') && $user->can('delete_businesses');
    }

    /**
     * Determine whether the user can delete own businesses
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function deleteOwn(User $user)
    {
        return $user->can('delete_businesses');
    }

    /**
     * Determine whether the user can approve businesses
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function approve(User $user)
    {
        return $user->can('approve_businesses');
    }
}
