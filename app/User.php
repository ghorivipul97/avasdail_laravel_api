<?php

namespace App;

use App\Helpers\Enums\UserStatuses;
use App\Helpers\UpdateMediaTrait;
use App\Models\Role;
use App\Models\ServiceProviderFavourite;
use App\Models\UserReview;
use App\Models\UserRole;
use App\Notifications\AdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class User extends Authenticatable implements HasMediaConversions
{
    use HasApiTokens;
    use Notifiable;
    use HasRoles;
    use HasMediaTrait;
    use UpdateMediaTrait;

    protected $morphClass = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'mobile_no', 'profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['profile_url'];

    /**
     * Set abbr attribute.
     *
     * @param string $value
     * @return void
     */
    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = str_slug($value, '_');
    }

    /**
     * Hash the password before saving
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('name', 'like', '%'.$search.'%');
    }

    /**
     * An active users scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeActive($query) {
        return $query->where('status', UserStatuses::ACTIVE);
    }

    /**
     * Register image conversions
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('avatar')
            ->width(200)
            ->height(200)
            ->fit('crop', 200, 200)
            ->performOnCollections('avatar');
    }

    /**
     * Doesnt have image scope
     */
    public function scopeDoesntHaveImage($query, $collection) {
        return $query->whereDoesntHave('media', function ($query) use($collection) {
            $query->whereCollectionName($collection);
        });
    }

    /**
     * Has image scope
     */
    public function scopeHasImage($query, $collection) {
        return $query->whereHas('media', function ($query) use($collection) {
            $query->whereCollectionName($collection);
        });
    }

    /**
     * Get the avatar url
     */
    public function getAvatarAttribute()
    {
        $avatar = $this->getFirstMediaUrl('avatar', 'avatar');
        return $avatar ? $avatar : asset(get_setting('default_avatar'));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPassword($token));
    }

    /**
     * Get admin user url
     */
    public function getAdminAuthorUrlAttribute()
    {
        return Auth::check() && Auth::user()->can('update', $this) ?
            action('Admin\\UsersController@edit', $this) : '';
    }

    /**
     * Get admin user attribute
     */
    public function getAdminAuthorLinkAttribute()
    {
        $author_link = $this->admin_author_url;

        $html = $author_link ?  '<a href="' . $author_link . '" class="admin-author-link">' : '';
        $html .= $this->name;
        $html .= $author_link ? '</a>' : '';

        return $html;
    }

    /**
     * Custom username column for passport
     *
     * @param $username
     * @return mixed
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)
            ->whereStatus(UserStatuses::ACTIVE)
            ->first();
    }

    /**
     * Get the phone record associated with the customer.
     */
    public function phone()
    {
        return $this->hasOne('App\MobileNumber');
    }

    /**
     * Check if verified
     */
    public function getIsVerifiedAttribute()
    {
        //has a phone
        return !empty($this->phone);
    }

    /**
     * Verified scope
     */
    public function scopeVerified($query)
    {
        return $query->has('phone');
    }

    /**
     * Get mobile number attribute
     */
    public function getMobileNumberAttribute()
    {
        return $this->phone ? $this->phone->number : '';
    }

    /**
     * Check if active
     */
    public function getIsActiveAttribute()
    {
        return $this->status == UserStatuses::ACTIVE;
    }

    /**
     * Update phone
     *
     * @param MobileNumber $phone
     * @return static
     */
    public function updatePhone( MobileNumber $phone ) {

        //update if npt same
        if ( empty($this->phone) || !$this->phone->isSame($phone) ) {
            //first clear
            $this->clearPhone();
            $this->phone()->save($phone);
        }

        return $this;
    }

    /**
     * A phone search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeHasPhone($query, $search) {
        return $query->whereHas('phone', function ($query) use ($search) {
                $query->whereNumber($search);
            });
    }

    /**
     * Clear phone
     */
    public function clearPhone() {
        $this->phone()->update(['user_id' => null]);
    }

    /**
     * Get role attribute
     */
    public function getRoleAttribute()
    {
        return $this->roles->first();
    }

    /**
     * Get Role name
     */
    public function getRoleNameAttribute()
    {
        return $this->role ? $this->role->description : '';
    }

    /**
     * A user has one business
     */
    public function business()
    {
        return $this->hasOne('App\Business');
    }

    /**
     * Get the business id attribute
     */
    public function getBusinessIdAttribute()
    {
        return $this->business ? $this->business->id : null;
    }

    public function attachRole(Role $role)
    {
        if (!UserRole::where('user_id', $this->id)->where('role_id', $role->id)->first()) {
             UserRole::create([
                 'user_id' => $this->id,
                 'role_id' => $role->id
             ]);
        }
    }

    public function reviews()
    {
        return $this->hasMany(UserReview::class)->with('reviewer');
    }

    public function getProfileUrlAttribute()
    {
        return isset($this->attributes['profile']) ?
            asset('images/users/' . $this->attributes['profile']) :
            asset('img/user.png');
    }

    public function favourites()
    {
        return $this->hasMany(ServiceProviderFavourite::class, 'service_provider_id', 'id')->with('user');
    }
}
