<?php

namespace App;

use App\Helpers\ApprovedTrait;
use App\Helpers\Dropzone\DropzoneTrait;
use App\Helpers\Enums\BusinessStatuses;
use App\Helpers\SlugTrait;
use App\Helpers\UpdateMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Business extends Model implements HasMediaConversions
{
    use SlugTrait;
    use HasMediaTrait;
    use UpdateMediaTrait;
    use DropzoneTrait;
    use ApprovedTrait;

    protected $morphClass = 'business';

    protected static $status_class = BusinessStatuses::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'address',
        'website',
        'description',
        'email',
    ];

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('name', 'like', '%'.$search.'%');
    }

    /**
     * Defines image sizes
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(120)
            ->height(120)
            ->fit('crop', 120, 120)
            ->performOnCollections('images');

        $this->addMediaConversion('logo')
            ->width(500)
            ->height(500)
            ->fit('crop', 500, 500)
            ->performOnCollections('logo');
    }

    /**
     * Returns the category image
     */
    public function getLogoAttribute()
    {
        $logo = $this->getFirstMediaUrl('logo', 'logo');
        return $logo ? $logo : asset(get_setting('default_avatar'));
    }

    /**
     * Location scope
     * @param $query
     * @param null $state
     */
    public function scopeHasState($query, $state)
    {
        return $query->whereHas('city', function ($query) use ($state) {
            $query->where('state_id', '=', $state);
        });
    }

    /**
     * A business belongs to a city
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * Belongs to category
     * @param $query
     * @param Category | array $category_ids
     * @return
     */
    public function scopeBelongsToCategory( $query, $category_ids )
    {
        $categories = [];

        if ( $category_ids instanceof Category ) {
            // Get ids of descendants
            $categories = $category_ids->descendants()->pluck('id');
            // Include the id of category itself
            $categories[] = $category_ids->getKey();
        } else {
            if ( !is_array($category_ids) ) {
                $category_ids = [$category_ids];
            }

            $cats = Category::whereIn('id', $category_ids)->get();

            foreach ($cats as $category) {
                // Get ids of descendants
                $categories = array_merge($categories, $category->descendants()->pluck('id')->all());
                // Include the id of category itself
                $categories[] = $category->getKey();
            }
        }

        // Get goods
        return $query->whereHas('categories', function ($query) use ($categories) {
            return $query->whereIn('category_id', $categories);
        });
    }

    /**
     * A business belongs to many categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Set city attribute
     */
    public function setCityIdAttribute( $value )
    {
        $value = trim($value);
        $this->attributes['city_id'] = $value ? $value : null;
    }

    /**
     * Get images attribute
     */
    public function getImagesAttribute()
    {
        $urls = [];
        $media = $this->getMedia('images');

        foreach ( $media as $media_item ) {
            $urls[$media_item->id] = $media_item->getUrl();
        }

        return $urls;
    }

    /**
     * Get admin url
     */
    public function getAdminUrlAttribute()
    {
        if ( Auth::check() ) {
            if ( Auth::user()->can('update', $this) ) {
                return action('Admin\\BusinessesController@edit', $this);
            } elseif ( Auth::user()->can('view', $this) ) {
                return $this->business->permalink;
            }
        }

        return '';
    }

    /**
     * Get admin link attribute
     */
    public function getAdminLinkAttribute()
    {
        $business_link = $this->admin_url;

        return $business_link ?
            '<a href="' . $business_link . '" class="admin-business-link">' .
            $this->name .
            '</a>' : $this->name;
    }

    /**
     * Calculate rating
     * @return
     */
    public function scopeWithRating($query)
    {
        $table_name = $this->getTable();
        $columns = array_map(function($column) use ($table_name) {
            return $table_name.'.'.$column;
        }, $query->getQuery()->columns);

        return $query->select($columns)
            ->addSelect(DB::raw('AVG(reviews.rating) as rating'))
            ->leftJoin('reviews', function ($join) {
                $join->on('businesses.id', '=', 'reviews.business_id')
                    ->where('reviews.status', '=', ReviewStatuses::PUBLISHED);
            })
            ->groupBy('businesses.id');
    }
}
