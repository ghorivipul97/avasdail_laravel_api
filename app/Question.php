<?php

namespace App;

use App\Helpers\Enums\QuestionTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Question extends Model
{
    protected $morphClass = 'question';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
        'allow_other' => 'boolean',
        'required' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'required',
        'allow_other',
        'options',
        'menu_order',
        'other',
    ];

    /**
     * A question belongs to a category
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the type name
     */
    public function getTypeNameAttribute()
    {
        return QuestionTypes::getLabel($this->type);
    }

    /**
     * Get the type name
     */
    public function getTypeSlugAttribute()
    {
        return QuestionTypes::getSlug($this->type);
    }

    /**
     * Check if is mcq
     */
    public function getIsMcqAttribute()
    {
        return in_array($this->type, QuestionTypes::mcqTypes());
    }

    /**
     * Check if is file
     */
    public function getIsFileAttribute()
    {
        return in_array($this->type, QuestionTypes::fileQTypes());
    }

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('title', 'like', '%'.$search.'%');
    }

    /**
     * Returns the url
     * @param string $action
     * @param string $namespace
     * @return
     */
    public function url($action = 'show', $namespace = 'Admin\\') {
        $controller_action = $namespace.'QuestionsController@'.$action;
        $params = ['category' => $this->category_id];

        if ( !in_array($action, ['index', 'store', 'create'])) {
            $params['question'] = $this->id;
        }

        return URL::action($controller_action, $params);
    }

    /**
     * Get the validation rules
     */
    public function validationRules( $required = true )
    {
        $default_rules = QuestionTypes::getValidationRules($this->type);

        if ( $required && $this->required ) {
            $default_rules = array_merge($default_rules, ['required']);
        }

        if ( $this->is_mcq && !$this->allow_other ) {
            if ( $this->type == QuestionTypes::MULTICHECK ) {
               // $default_rules[] = 'array_exists:'.implode(','. $this->options);
            } elseif ( $this->type == QuestionTypes::RADIO ) {
                //$default_rules[] = 'in:'.implode(','. $this->options);
            }
        }

        return $default_rules;
    }

    /**
     * Get the label
     */
    public function getLabelAttribute()
    {
        return $this->required ? $this->title . ' *' : $this->title;
    }
    
}
