<?php

namespace App\Listeners;

use App\Exceptions\MobileNumberException;
use App\MobileNumber;
use App\Notifications\MobileNumberVerificationToken;
use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;

class HandleFailedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationFailed  $event
     * @return void
     */
    public function handle(NotificationFailed $event)
    {
        //failed to send verification token
        if ( $event->notifiable instanceof MobileNumber
            && $event->notification instanceof MobileNumberVerificationToken ) {

            throw new MobileNumberException('Failed to send verification code to the specified number.', 500);
        }
    }
}
