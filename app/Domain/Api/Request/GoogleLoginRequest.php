<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class GoogleLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email',
            'name' => 'required',
            'google_id' => 'required',
            'profile_url' => 'required',
            'player_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ];
    }

    public function persist()
    {
        $user = new User();

        return [
            'email' => $this->get('email'),
            'name' => $this->get('name'),
            'username' => $user->getUsername($this->get('name')),
            'google_id' => $this->get('google_id'),
            'is_google_user' => true,
            'profile' => $this->get('profile_url'),
            'player_id' => $this->get('player_id'),
            'latitude' => $this->get('latitude'),
            'longitude' => $this->get('longitude'),
            'is_verified' => false
        ];
    }
}
