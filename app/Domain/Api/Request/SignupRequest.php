<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'phone_number' => 'required',
            'password' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'player_id' => 'required'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('name', 'email', 'latitude', 'longitude', 'username', 'player_id'),
            [
                'password' => bcrypt($this->get('password')),
                'mobile_no' => '+'.$this->get('phone_number'),
                'verification_otp' => rand(100000, 999999),
                'is_verified' => false
            ],
            $this->hasFile('image') ? ['profile' => $this->uploadImage()] : []
        );
    }

    private function uploadImage()
    {
        $imageName = time() . '.' .$this->file('image')->getClientOriginalName();
        $this->file('image')->move(public_path('images/users'), $imageName);

        return $imageName;
    }
}
