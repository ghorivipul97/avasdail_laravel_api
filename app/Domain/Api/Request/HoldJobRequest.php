<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class HoldJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'service_request_id' => 'required|exists:service_requests,id'
        ];
    }

    public function persist()
    {
        return $this->only('user_id', 'service_request_id');
    }
}
