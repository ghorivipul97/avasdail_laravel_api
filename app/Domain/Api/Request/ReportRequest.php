<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'username' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'description' => 'required'
        ];
    }

    public function persist()
    {
        return $this->only('user_id', 'username', 'email', 'phone_number', 'description');
    }
}
