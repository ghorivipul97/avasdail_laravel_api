<?php

namespace App\Domain\Api\Request;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ServiceProviderSignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'image' => 'required|image',
            'phone_number' => 'required',
            'password' => 'required',
            'questions' => 'required|json',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'state_id' => 'required|exists:states,id',
            'city_id' => 'required|exists:cities,id',
            'player_id' => 'required',
            'opening_hours' => 'required|json',
            'categories' => 'required|json'
        ];
    }

    public function persist()
    {
        $user = new User();
        return array_merge(
            $this->only('name', 'email', 'address', 'latitude', 'longitude', 'state_id', 'city_id', 'player_id'),
            [
                'password' => bcrypt($this->get('password')),
                'mobile_no' => '+' . $this->get('phone_number'),
                'website' => $this->get('website'),
                'description' => $this->get('description'),
                'profile' => $this->uploadImage('image'),
                'username' => $user->getUsername($this->get('name')),
                'verification_otp' => rand(100000, 999999),
                'is_verified' => false
            ],
            $this->hasFile('image1') ? ['image1' => $this->uploadImage('image1')] : [],
            $this->hasFile('image2') ? ['image2' => $this->uploadImage('image2')] : [],
            $this->hasFile('image3') ? ['image3' => $this->uploadImage('image3')] : [],
            $this->hasFile('image4') ? ['image4' => $this->uploadImage('image4')] : [],
            $this->hasFile('image5') ? ['image5' => $this->uploadImage('image5')] : [],
            ['opening_hours' => $this->get('opening_hours')]
        );
    }

    private function uploadImage($name)
    {
        $imageName = time() . '-' .$this->file($name)->getClientOriginalName();
        $this->file($name)->move(public_path('images/users'), $imageName);

        return $imageName;
    }
}
