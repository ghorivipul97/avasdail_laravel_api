<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class TwitterLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email',
            'name' => 'required',
            'twitter_id' => 'required',
            'profile_url' => 'required',
            'player_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ];
    }

    public function persist()
    {
        $user = new User();

        return [
            'email' => $this->get('email'),
            'name' => $this->get('name'),
            'username' => $user->getUsername($this->get('name')),
            'twitter_id' => $this->get('twitter_id'),
            'is_twitter_user' => true,
            'profile' => $this->get('profile_url'),
            'player_id' => $this->get('player_id'),
            'latitude' => $this->get('latitude'),
            'longitude' => $this->get('longitude'),
            'is_verified' => false
        ];
    }
}
