<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class QuoteAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'service_provider_id' => 'required_without:is_all_providers|exists:users,id',
            'user_id' => 'required|exists:users,id'
        ];
    }
}
