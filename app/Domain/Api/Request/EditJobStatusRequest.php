<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class EditJobStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_request_id' => 'required|exists:service_requests,id',
            'status' => 'required|in:active,pending,accepted,cancelled'
        ];
    }
}
