<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $this->get('user_id'),
            'profile_picture' => 'image'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('name', 'email'),
            $this->hasFile('profile_picture') ? ['profile' => $this->uploadImage()] : []
        );
    }

    protected function uploadImage()
    {
        $imageName = time().'.'.$this->file('profile_picture')->getClientOriginalName();
        $this->file('profile_picture')->move(public_path('images/users'), $imageName);

        return $imageName;
    }
}
