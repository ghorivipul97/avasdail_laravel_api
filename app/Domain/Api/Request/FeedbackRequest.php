<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'description' => 'required'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('username', 'email', 'phone_number', 'description'),
            $this->hasFile('image1') ? ['image1' => $this->uploadImage('image1')] : [],
            $this->hasFile('image2') ? ['image2' => $this->uploadImage('image2')] : [],
            $this->hasFile('image3') ? ['image3' => $this->uploadImage('image3')] : [],
            $this->hasFile('image4') ? ['image4' => $this->uploadImage('image4')] : [],
            $this->hasFile('image5') ? ['image5' => $this->uploadImage('image5')] : []
        );
    }

    private function uploadImage($name)
    {
        $imageName = time() . '-' .$this->file($name)->getClientOriginalName();
        $this->file($name)->move(public_path('uploads/feedback-images'), $imageName);

        return $imageName;
    }
}
