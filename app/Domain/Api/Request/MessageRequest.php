<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_request_id' => 'required|exists:service_requests,id',
            'sender_id' => 'required|exists:users,id',
            'receiver_id' => 'required|exists:users,id',
            'message' => 'required|string'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('sender_id', 'receiver_id', 'message'),
            ['parent_id' => $this->get('service_request_id')]
        );
    }
}
