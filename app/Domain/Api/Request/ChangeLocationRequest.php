<?php

namespace App\Domain\Api\Request;

use Illuminate\Foundation\Http\FormRequest;

class ChangeLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ];
    }

    public function persist()
    {
        return $this->only('address', 'latitude', 'longitude');
    }
}
