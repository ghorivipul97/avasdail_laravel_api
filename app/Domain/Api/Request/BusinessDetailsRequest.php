<?php

namespace App\Domain\Api\Request;

use App\Business;
use Illuminate\Foundation\Http\FormRequest;

class BusinessDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required|exists:cities,id',
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'website' => 'required',
            'email' => 'required|email',
            'description' => 'required'
        ];
    }

    public function createPersist()
    {
        $business = new Business();

        return array_merge(
            $this->only('city_id', 'name', 'address', 'phone', 'website', 'email', 'description'),
            ['slug' => $business->getUniqueSlug($this->get('name'))]
        );
    }

    public function updatePersist()
    {
        return $this->only('city_id', 'name', 'address', 'phone', 'website', 'email', 'description');
    }
}
