<?php
namespace App\Domain\Api\Service;

use App\Models\Notification;

class NotificationService
{
    public function addNotification($senderId, $receiverId, $message, $type, $parentId)
    {
        Notification::create([
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'notification' => $message,
            'parent_id' => $parentId,
            'type' => $type
        ]);
    }
}
