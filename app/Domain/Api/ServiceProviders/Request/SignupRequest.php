<?php

namespace App\Domain\Api\ServiceProviders\Request;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:service_providers,email',
            'username' => 'required|unique:service_providers,username',
            'password' => 'required'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('name', 'email', 'username'),
            ['password' => bcrypt($this->get('password'))]
        );
    }
}
