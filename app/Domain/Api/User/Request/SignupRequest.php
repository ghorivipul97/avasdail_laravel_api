<?php

namespace App\Domain\Api\User\Request;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'password' => 'required'
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only('name', 'email', 'username'),
            ['password' => bcrypt($this->get('password'))]
        );
    }
}
