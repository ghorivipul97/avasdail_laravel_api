<?php

namespace App\Domain\Admin\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'icon' => 'mimes:jpeg,jpg,png,bmp',
            'image' => 'mimes:jpeg,jpg,png,bmp',
            'parent_id' => 'exists:categories,id',
        ];
    }

    public function persist()
    {
        $category = new Category();

        return [
            'name' => $this->get('name'),
            'slug' => $category->getSlug($this->get('name')),
            'parent_id' => $this->has('parent_id') ? $this->get('parent_id') : null,
            'image' => $this->hasFile('image') ? $this->uploadImage() : '',
            'icon' => $this->hasFile('icon') ? $this->uploadIcon() : ''
        ];
    }

    public function updatePersist($category)
    {
        $newCategory = new Category();

        return array_merge([
            'name' => $this->get('name'),
            'parent_id' => $this->has('parent_id') ? $this->get('parent_id') : null
        ],
        $category->name != $this->get('name') ? ['slug' => $newCategory->getSlug($this->get('name'))] : [],
        $this->hasFile('image') ? ['image' => $this->uploadImage()] : [],
        $this->hasFile('icon') ? ['icon' => $this->uploadIcon()] : []
        );
    }

    protected function uploadImage()
    {
        $imageName = time().'.'.$this->file('image')->getClientOriginalName();
        $this->file('image')->move(public_path('uploads/categories/images'), $imageName);

        return $imageName;
    }

    protected function uploadIcon()
    {
        $iconName = time().'.'.$this->file('icon')->getClientOriginalName();
        $this->file('icon')->move(public_path('uploads/categories/icons'), $iconName);

        return $iconName;
    }
}
