<?php

namespace App\Domain\Admin\Requests;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;

class CitiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state_id' => 'required|integer|exists:states,id',
            'name' => 'required|string|max:255',
            'code' => 'required|string'
        ];
    }

    public function persist()
    {
        return $this->only('name', 'code', 'state_id');
    }
}
