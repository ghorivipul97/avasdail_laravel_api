<?php

namespace App\Domain\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required'
        ];
    }

    public function uploadImage()
    {
        $imageName = time().'.'.$this->file('image')->getClientOriginalName();
        $this->file('image')->move(public_path('uploads/slider-images'), $imageName);

        return $imageName;
    }
}
