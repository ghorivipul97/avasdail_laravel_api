<?php
namespace App\Domain\Utility;

class PushNotificationUtil
{
    public static function sendPush($userId, $message, $url = null, $data = null)
    {
        \OneSignal::sendNotificationToUser(
            $message,
            $userId,
            $url,
            $data,
            $buttons = null,
            $schedule = null
        );
    }
}