<?php
/**
 * Custom Exception
 *
 * @author Arushad Ahmed (@dash8x)
 * @author_uri http://arushad.org
 */
namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppException extends HttpException
{
    private $name;

    /**
     * @param string $statusCode
     * @param string $name
     * @param null $message
     */
    public function __construct($statusCode, $name = 'AppError', $message = null)
    {
        $this->name = $name;

        parent::__construct($statusCode, $message);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
