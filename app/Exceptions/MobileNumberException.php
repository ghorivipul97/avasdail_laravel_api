<?php
/**
 * Custom Exception
 *
 * @author Arushad Ahmed (@dash8x)
 * @author_uri http://arushad.org
 */
namespace App\Exceptions;

class MobileNumberException extends AppException
{
    /**
     * Constructor
     *
     * @param null|string $message
     * @param int $code
     */
    public function __construct($message = '', $code = 422)
    {
        parent::__construct($code, 'MobileNumberException', $message);
    }

}
