<?php
/**
 * Custom Exception
 *
 * @author Arushad Ahmed (@dash8x)
 * @author_uri http://arushad.org
 */
namespace App\Exceptions;

class JsonOnlyException extends AppException
{
    /**
     * Constructor
     *
     * @param null|string $message
     */
    public function __construct($message = '')
    {
        parent::__construct(400, 'OnlyJsonAllowed', $message);
    }

}
