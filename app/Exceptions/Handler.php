<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $e = $this->prepareException($exception);

        if ( $request->is(env('API_ROUTE_PREFIX', 'api/v1').'/*') || $request->expectsJson()) {
            return $this->convertExceptionToJsonResponse($e, $request);
        }

        if ($e instanceof HttpResponseException) {
            return $e->getResponse();
        } elseif ($e instanceof AuthenticationException) {
            return $this->unauthenticated($request, $e);
        } elseif ($e instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($e, $request);
        }

        return parent::render($request, $exception);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param Exception|ValidationException $e
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function convertExceptionToJsonResponse(Exception $e, $request)
    {
        $json_error = [];
        $json_error['status'] = 500;
        $json_error['name'] = basename(str_replace('\\', '/', get_class($e)));
        $json_error['message'] = $e->getMessage();

        if ($e instanceof HttpResponseException || $this->isHttpException($e)) {
            $json_error['status'] = $e->getStatusCode();
        } elseif ($e instanceof AuthenticationException) {
            $json_error['status'] = 401;
        } elseif ($e instanceof ValidationException) {
            $message = "";
            $count = 0;
            
            if(count($e->validator->errors()->getMessages()) > 0) {
                foreach($e->validator->errors()->getMessages() as $key => $error) {
                    if(isset($error[0])) {
                        $count++;

                        $message .= $error[0];
                        if (count($e->validator->errors()->getMessages()) != $count) {
                            $message .= " \n";
                        }
                    }
                }
            }

            return new JsonResponse(['success' => false, 'message' => $message], 200);
        }

        return response()->json(['error' => $json_error], $json_error['status']);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('/admin/login');
    }
}
