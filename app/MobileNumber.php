<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class MobileNumber extends Model
{
    use Notifiable;

    const DEFAULT_COUNTRY_CODE = 960;
    const ALLOWED_COUNTRY_CODES = [
        '960',
        '60',
        '91',
    ];


    protected $morphClass = 'mobile_number';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'country_code'];

    /**
     * Dates
     *
     * @var array
     */
    protected $dates = ['token_created_at'];

    /**
     * Hidden
     *
     * @var array
     */
    protected $hidden = ['token'];

    /**
     * Appends
     *
     * @var array
     */
    protected $appends = ['formatted_number', 'token_expires_in'];

    /**
     * Default prefix
     */
    public static function defaultPrefix()
    {
        return '+'.self::DEFAULT_COUNTRY_CODE;
    }

    /**
     * Convert dates to Carbon
     */
    public function setTokenCreatedAtAttribute($date)
    {
        $this->attributes['token_created_at'] = empty($date) ? null : Carbon::parse($date);
    }

    /**
     * A mobile number belongs to a user
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Hash the token before saving
     */
    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = $value ? Hash::make($value) : null;
    }

    /**
     * Generate the token
     */
    public function randomToken() {
        return str_pad(rand(0, 999999), 6, 0, STR_PAD_LEFT);
    }

    /**
     * Generate the token and save
     */
    public function generateToken() {
        $token = $this->randomToken();

        $this->attempts = 0;
        $this->token = $token;
        $this->token_created_at = Carbon::now();
        $this->save();

        return $token;
    }

    /**
     * Check if token is expired
     */
    public function getIsTokenExpiredAttribute()
    {
        return !$this->token ||
            ( $this->token_created_at &&
                $this->token_created_at->diffInMinutes() >= $this->token_expires_in );
    }

    /**
     * Get token expires in attribute
     */
    public function getTokenExpiresInAttribute()
    {
        return get_setting('mobile_number_token_validity');
    }

    /**
     * Check if number is locked
     */
    public function getIsLockedAttribute() {
        return $this->attempts >= get_setting('mobile_number_max_attempts');
    }

    /**
     * Check if can request for new code
     */
    public function getCanRequestCodeAttribute()
    {
        return !$this->is_locked || $this->token_created_at->diffInMinutes() >= get_setting('mobile_number_attempt_expiry');
    }

    /**
     * Attempts expiry at
     */
    public function getAttemptsExpiryAtAttribute()
    {
        return !$this->is_locked ? Carbon::now() :
            $this->token_created_at->addMinutes(get_setting('mobile_number_attempt_expiry'));
    }

    /**
     * Get attempts expiry minutes
     */
    public function getAttemptsExpiryAttribute()
    {
        return  $this->can_request_code ? 0 : $this->attempts_expiry_at->diffInMinutes();
    }

    /**
     * Verify token
     */
    public function verifyToken($token) {
        if ( $token && $this->token && Hash::check($token, $this->token) ) {
            $this->attempts = 0;
            $this->token = null;
            $this->token_created_at = null;
            $this->save();

            return true;
        }

        $this->attempts++;
        $this->save();

        return false;
    }

    /**
     * Get the formatted number
     */
    public function getFormattedNumberAttribute() {
        return $this->country_code ? '+'.$this->country_code.$this->number : $this->number;
    }

    /*
    * The number to send SMSs to
    */
    public function routeNotificationForTwilio()
    {
        //return '+9607645530';
        return $this->formatted_number;
    }

    /**
     * Check if is same
     * @param MobileNumber $other
     * @return bool
     */
    public function isSame( MobileNumber $other )
    {
        return $this->country_code == $other->country_code
            && $this->number == $other->number;
    }
}
