<?php

namespace App;

use App\Helpers\Enums\QuestionTypes;
use App\Helpers\UpdateMediaTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Answer extends Model implements HasMedia
{

    use HasMediaTrait;
    use UpdateMediaTrait;

    protected $morphClass = 'answer';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'string',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['service_request_id', 'question_id', 'content'];

    /**
     * Dynamic casts
     */
    protected function getCastType($key) {
        if ($key == 'content' && !empty($this->cast_type)) {
            return $this->cast_type;
        } else {
            return parent::getCastType($key);
        }
    }

    /**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed  $value
     * @return \Carbon\Carbon
     */
    protected function asDateTime($value)
    {
        try {
            return parent::asDateTime($value);
        } catch (\InvalidArgumentException $e) {
            return Carbon::parse($value);
        }
    }

    public function getContentAttribute( $value )
    {
        return $this->type == QuestionTypes::IMAGE ? $this->getFirstMediaUrl() : $this->castAttribute('content', $value);
    }

    /**
     * Get the question type
     */
    public function getTypeAttribute()
    {
        return $this->question ? $this->question->type : QuestionTypes::TEXT;
    }

    /**
     * Get the cast type
     */
    public function getCastTypeAttribute()
    {
        return QuestionTypes::getCastType($this->type);
    }

    /**
     * Question
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * Service request
     */
    public function request()
    {
        return $this->belongsTo('App\ServiceRequest');
    }
}
