<?php

namespace App\Console\Commands;

use App\Models\UserQuote;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire jobs';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jobs = UserQuote::with('quotedProviders')
            ->where('expired_at', '<=', Carbon::now())
            ->whereStatus(UserQuote::OPEN)
            ->get();

        foreach ($jobs as $job) {
            if (!$job->quotedProviders()->count()) {
                $job->status = UserQuote::EXPIRED;
                $job->save();
            }
        }
    }
}
