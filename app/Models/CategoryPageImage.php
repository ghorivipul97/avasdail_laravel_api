<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPageImage extends Model
{
    protected $fillable = ['name'];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return asset('uploads/slider-images/' . $this->attributes['name']);
    }
}
