<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ACTIVE = 'active';
    const PENDING = 'pending';
    const BANNED = 'banned';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'mobile_no', 'profile', 'facebook_id', 'google_id',
        'twitter_id', 'is_fb_user', 'is_google_user', 'is_twitter_user', 'address', 'latitude', 'longitude',
        'state_id', 'city_id', 'player_id', 'image1', 'image2', 'image3', 'image4', 'image5', 'website',
        'description', 'opening_hours', 'is_login', 'verification_otp', 'is_verified', 'forgot_password_otp',
        'category_id', 'sub_category_id', 'update_number_otp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['profile_url'];

    public function getProfileUrlAttribute()
    {
        if ($this->is_fb_user || $this->is_google_user || $this->is_twitter_user) {
            return $this->profile;
        }

        return isset($this->attributes['profile']) && $this->attributes['profile'] ?
            asset('images/users/' . $this->attributes['profile']) :
            asset('img/user.png');
    }

    public function routeNotificationForTwilio()
    {
        return $this->attributes['mobile_no'];
    }

    public function getUsername($name)
    {
        $username = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "_", strtolower($name)));
        $count = $this::where('username', 'like', $username . '%')->count();
        return ($count > 0) ? ($username . '_' . $count) : $username;
    }

    public function attachRole(Role $role)
    {
        if (!UserRole::where('user_id', $this->id)->where('role_id', $role->id)->first()) {
             UserRole::create([
                 'user_id' => $this->id,
                 'role_id' => $role->id
             ]);
        }
    }

    public function reviews()
    {
        return $this->hasMany(UserReview::class)->with('reviewer');
    }

    public function reviewsWithoutReviewer()
    {
        return $this->hasMany(UserReview::class);
    }

    public function favourites()
    {
        return $this->belongsToMany(User::class, 'service_provider_favourites', 'user_id', 'service_provider_id')->with('reviewsWithoutReviewer');
    }

    public function signupAnswers()
    {
        return $this->hasMany(UserSignupAnswer::class)->with('question');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'user_categories', 'user_id', 'category_id');
    }
}
