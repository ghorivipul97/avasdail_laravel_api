<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceProvider extends Authenticatable
{
    const ACTIVE = 'active';
    const BANNED = 'banned';

    protected $fillable = ['name', 'email', 'username', 'password', 'status'];
}
