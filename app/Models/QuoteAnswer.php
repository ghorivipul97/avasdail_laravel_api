<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteAnswer extends Model
{
    protected $fillable = ['user_quote_id', 'quote_question_id', 'answer', 'is_urgent'];

    public function question()
    {
        return $this->belongsTo(QuoteQuestion::class, 'quote_question_id', 'id');
    }
}
