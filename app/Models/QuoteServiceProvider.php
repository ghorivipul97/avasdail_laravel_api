<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteServiceProvider extends Model
{
    const NOT_QUOTED = 'not_quoted';
    const QUOTED = 'quoted';
    const QUOTE_ACCEPTED = 'quote_accepted';
    const AWARDED = 'awarded';
    const QUOTE_WITHDRAWN = 'quote_withdrawn';
    const COMPLETED = 'completed';

    public static $statuses = [
        self::NOT_QUOTED,
        self::QUOTED,
        self::QUOTE_ACCEPTED,
        self::AWARDED,
        self::QUOTE_WITHDRAWN
    ];

    public static $labels = [
        self::NOT_QUOTED => 'NOT QUOTED',
        self::QUOTED => 'QUOTED',
        self::QUOTE_ACCEPTED => 'QUOTE ACCEPTED',
        self::AWARDED => 'AWARDED',
        self::QUOTE_WITHDRAWN => 'QUOTE WITHDRAWN',
        self::COMPLETED => 'COMPLETED'
    ];

    protected $fillable = ['user_quote_id', 'service_provider_id', 'amount', 'status'];

    public function job()
    {
        return $this->hasOne(UserQuote::class, 'id', 'user_quote_id')->with('subCategory');
    }

    public function jobWithUser()
    {
        return $this->hasOne(UserQuote::class, 'id', 'user_quote_id')->with(['subCategory', 'user']);
    }

    public function serviceProvider()
    {
        return $this->belongsTo(User::class, 'service_provider_id', 'id')->select('id', 'name', 'email', 'username', 'status', 'profile');
    }

    public function serviceProviderAllDetail()
    {
        return $this->belongsTo(User::class, 'service_provider_id', 'id');
    }

    public function serviceProviderWithReviews()
    {
        return $this->belongsTo(User::class, 'service_provider_id', 'id')->select('id', 'name', 'email', 'username', 'status', 'profile')->with('reviews');
    }
}
