<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderFavourite extends Model
{
    protected $fillable = ['user_id', 'service_provider_id'];
}
