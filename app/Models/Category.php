<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'icon', 'image', 'parent_id'];

    protected $appends = ['image_url', 'icon_url'];

    public function getSlug($name)
    {
        $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "_", strtolower($name)));
        $count = $this::where('slug', 'like', $slug . '%')->count();
        return ($count > 0) ? ($slug . '_' . $count) : $slug;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }
    /**
     * A category belongs to many businesses
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function businesses()
    {
        return $this->belongsToMany('App\Business');
    }

    /**
     * A category has many questions
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function getImageUrlAttribute()
    {
        // send test image temp. for now
        return asset('img/restaurant.jpg');
    }

    public function getIconUrlAttribute()
    {
        // send test image temp. for now
        return asset('img/restaurant_icon.png');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }
}
