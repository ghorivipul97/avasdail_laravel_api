<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const USER = 'user';
    const SERVICE_PROVIDER = 'service_provider';

    protected $fillable = ['name', 'description'];
}
