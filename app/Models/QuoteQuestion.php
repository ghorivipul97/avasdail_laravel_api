<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteQuestion extends Model
{
    protected $fillable = ['question', 'is_required'];
}
