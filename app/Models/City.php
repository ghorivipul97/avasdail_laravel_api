<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['state_id', 'code', 'name'];

    public function state() {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Distance scope
     * @param $query
     * @param $lat
     * @param $lng
     * @param null $radius
     * @param string $unit
     * @return
     */
    public function scopeWithDistance($query, $lat, $lng, $radius = null, $unit = "km") {
        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $lat = (float) $lat;
        $lng = (float) $lng;
        $distance = '(? * ACOS(COS(RADIANS(?))
                        * COS(RADIANS(lat))
                        * COS(RADIANS(?) - RADIANS(lng))
                        + SIN(RADIANS(?))
                        * SIN(RADIANS(lat))))';
        $query = $query->selectRaw("$distance AS distance",
            [$unit, $lat, $lng, $lat]
        );

        if ( $radius ) {
            $query = $query->whereRaw("$distance <= ?", [$unit, $lat, $lng, $lat, $radius]);
        }

        return $query;
    }
}
