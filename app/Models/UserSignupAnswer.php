<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSignupAnswer extends Model
{
    protected $fillable = ['user_id', 'signup_question_id', 'answer'];

    public function question()
    {
        return $this->hasOne(SignupQuestion::class, 'id', 'signup_question_id');
    }
}
