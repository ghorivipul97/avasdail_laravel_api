<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HoldedJob extends Model
{
    protected $fillable = ['user_id', 'service_request_id'];
}
