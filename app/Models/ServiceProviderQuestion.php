<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderQuestion extends Model
{
    protected $fillable = ['question', 'is_required'];
}
