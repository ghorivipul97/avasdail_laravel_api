<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const REQUEST_QUOTE = 'request_quote';
    const PROVIDER_SEND_QUOTE = 'provider_send_quote';
    const ACCEPT_QUOTE = 'accept_quote';
    const AWARD_JOB = 'award_job';
    const AWARD_OTHER_JOB = 'award_other_job';
    const COMPLETE_JOB = 'complete_job';

    protected $fillable = ['sender_id', 'receiver_id', 'notification', 'parent_id', 'type'];

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id')->select('id', 'name', 'profile');
    }
}
