<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportData extends Model
{
    protected $table = 'report_data';

    protected $fillable = ['user_id', 'username', 'email', 'phone_number', 'description', 'files'];
}
