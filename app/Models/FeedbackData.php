<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackData extends Model
{
    protected $table = 'feedback_data';

    protected $fillable = ['username', 'email', 'phone_number', 'description', 'image1', 'image2', 'image3', 'image4', 'image5'];
}
