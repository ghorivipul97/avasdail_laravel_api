<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignupQuestion extends Model
{
    protected $fillable = ['question'];
}
