<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderAnswer extends Model
{
    protected $fillable = ['quote_service_provider_id', 'service_provider_question_id', 'answer'];

    public function question()
    {
        return $this->belongsTo(ServiceProviderQuestion::class, 'service_provider_question_id', 'id');
    }
}
