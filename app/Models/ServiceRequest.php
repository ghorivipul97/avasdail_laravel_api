<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    protected $fillable = ['user_id', 'business_id', 'category_id', 'note', 'skip_questions', 'status'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
