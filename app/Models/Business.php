<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Business extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'address',
        'website',
        'description',
        'email',
    ];

    /**
     * A business belongs to a city
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * A business belongs to many categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
