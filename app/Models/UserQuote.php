<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserQuote extends Model
{
    const OPEN = 'open';
    const COMPLETED = 'completed';
    const EXPIRED = 'expired';
    const AWARDED = 'awarded';

    public static $statuses = [
        self::OPEN,
        self::COMPLETED,
        self::EXPIRED
    ];

    public static $labels = [
        self::OPEN => 'OPEN',
        self::COMPLETED => 'COMPLETED',
        self::EXPIRED => 'EXPIRED',
        self::AWARDED => 'AWARDED'
    ];

    protected $fillable = [
        'user_id', 'category_id', 'name', 'description', 'expired_at', 'status', 'is_all_providers'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function subCategory()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')->with('category');
    }

    public function answers()
    {
        return $this->hasMany(QuoteAnswer::class, 'user_quote_id', 'id');
    }

    public function providers()
    {
        return $this->belongsToMany(User::class, 'quote_service_providers', 'user_quote_id', 'service_provider_id');
    }

    public function quotedProviders()
    {
        return $this->hasMany(QuoteServiceProvider::class)->where('status', QuoteServiceProvider::QUOTED);
    }

    public function allProviders()
    {
        return $this->hasMany(QuoteServiceProvider::class);
    }
}
