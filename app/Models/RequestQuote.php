<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RequestQuote extends Model
{
    protected $fillable = ['service_request_id', 'user_id', 'quote'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
