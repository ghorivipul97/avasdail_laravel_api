<?php

namespace App;

use App\Helpers\HasBusinessTrait;
use App\Helpers\HasUserTrait;
use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model
{

    use HasUserTrait;
    use HasBusinessTrait;

    protected $morphClass = 'service_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'business_id', 'category_id', 'note', 'skip_questions', 'status'];

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('id', $search);
    }

    /**
     * A service request belongs to a category
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * A service request has many answers
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * A service request has many businesses
     */
    public function businesses()
    {
        return $this->belongsToMany(Business::class);
    }

    /**
     * Get all questions
     */
    public function getQuestionsAttribute()
    {
        return $this->category ? $this->category->allQuestions()->get() : null;
    }

    /**
     * Get the answer
     */
    public function getAnswer( $question_id )
    {
        return $this->answers->filter(function ($answer, $id) use ($question_id) {
            return $answer->question_id == $question_id;
        })->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
