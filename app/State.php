<?php

namespace App;

use App\Helpers\Enums\StateTypes;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $morphClass = 'state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'type', 'abbr', 'name'];

    /**
     * A state has many cities
     */
    public function cities() {
        return $this->hasMany('App\City');
    }

    /**
     * Get formatted name
     */
    public function getFormattedNameAttribute() {
        return $this->type == StateTypes::ATOLL ?
            $this->name . ' (' . $this->abbr . ')' : $this->name;
    }

    /**
     * A search scope
     * @param $query
     * @param $search
     * @return
     */
    public function scopeSearch($query, $search) {
        return $query->where('name', 'like', '%'.$search.'%')
            ->orWhere('abbr', 'like', '%'.$search.'%')
            ->orWhere('code', '=', $search);
    }
}
