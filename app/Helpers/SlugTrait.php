<?php
/**
 * Simple trait to set slug field
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;

trait SlugTrait
{

    /**
     * Boot function from laravel.
     */
    protected static function bootSlugTrait()
    {
        static::saving(function ($model) {
            $model->slug = $model->getUniqueSlug( $model->slug );
        });
    }

    /**
     * Set slug attribute.
     *
     * @param string $value
     * @return void
     */
    /*public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = $this->getUniqueSlug($value);
    }*/

    /**
     * Find a unique slug
     *
     * @param string $value
     * @return string
     */
    public function getUniqueSlug( $value ) {

        if ( empty($value) ) {
            $value = $this->name;
        }

        //convert to slug
        $value = str_slug($value);
        $suffix = '';
        $count = 0;

        //find a unique slug
        while ( !$this->isUniqueSlug($value . $suffix) ) {
            $count++;
            $suffix = '-' . $count;
        }

        return $value . $suffix;
    }

	/**
	 * Check if slug is unique
	 *
	 * @param string $value
	 * @return bool
	 */
    public function isUniqueSlug( $value ) {
	    //check if slug exists
	    $count = static::whereSlug($value);
	    if ( $this->id ) {
		    $count = $count->where('id', '!=', $this->id);
	    }
	    return !$count->exists();
    }
}