<?php
/**
 * Simple trait to set controller orderbys
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;

trait AllowedOrdersTrait
{

    protected static $orderbys = [];
    protected static $orders = [];

    /**
     * Get order bys
     */
    public static function getOrderbys()
    {
        //first initialize
        if ( empty(static::$orderbys) ) {
            static::initOrderbys();
        }

        return static::$orderbys;
    }

    /**
     * Initialize orderbys
     */
    protected static function initOrderbys()
    {
        static::$orderbys = [
            'name' => __('Name'),
            'id' => __('ID'),
        ];
    }

    /**
     * Get orders
     */
    public static function getOrders()
    {
        //first initialize
        if ( empty(static::$orders) ) {
            static::initOrders();
        }

        return static::$orders;
    }

    /**
     * Initialize orders
     */
    protected static function initOrders()
    {
        static::$orders = [
            'ASC' => __('Ascending'),
            'DESC' => __('Descending'),
        ];
    }
}