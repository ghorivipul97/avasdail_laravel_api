<?php
/**
 * Simple trait for business posts
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

trait HasBusinessTrait
{

    /**
     * Belongs to a CeC
     *
     * @return BelongsTo
     */
    public function business()
    {
        return $this->belongsTo('App\Business');
    }

    /**
     * Update Business
     * @param $business mixed
     */
    public function updateBusiness( $business ) {
        $this->business()->associate($business);
    }

    /**
     * Get admin user url
     */
    public function getAdminBusinessUrlAttribute()
    {
        return $this->business ? $this->business->admin_url : '';
    }

    /**
     * Get admin user attribute
     */
    public function getAdminBusinessLinkAttribute()
    {
        return $this->business ? $this->business->admin_link : '';
    }

}