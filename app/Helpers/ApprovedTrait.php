<?php
/**
 * Simple trait for published models
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait ApprovedTrait
{

    use HasUserTrait;

    protected static $status_slugs = [];

	/**
	 * Checks if published
	 */
	public function getIsApprovedAttribute() {
		return $this->status == $this->getApprovedKey();
	}

    /**
     * Checks if pending
     */
    public function getIsPendingAttribute() {
        return $this->status == $this->getPendingKey();
    }

    /**
     * Checks if draft
     */
    public function getIsDeactivatedAttribute() {
        return $this->status == $this->getDeactivatedKey();
    }

    /**
     * Checks if rejected
     */
    public function getIsRejectedAttribute() {
        return $this->status == $this->getRejectedKey();
    }

	/**
	 * A published scope
	 */
	public function scopeApproved($query) {
		return $query->where($this->getTable().'.status', '=', $this->getApprovedKey());
	}

    /**
     * A draft scope
     */
    public function scopeDeactivated($query) {
        return $query->where($this->getTable().'.status', '=', $this->getDeactivatedKey());
    }

    /**
     * A pending scope
     */
    public function scopePending($query) {
        return $query->where($this->getTable().'.status', '=', $this->getPendingKey());
    }

    /**
     * A rejected scope
     */
    public function scopeRejected($query) {
        return $query->where($this->getTable().'.status', '=', $this->getRejectedKey());
    }

	/**
     * Return the published key
     */
	public function getApprovedKey() {
        $status_class = static::$status_class;
        return $status_class::APPROVED;
    }

    /**
     * Return the pending key
     */
    public function getPendingKey() {
        $status_class = static::$status_class;
        return $status_class::PENDING;
    }

    /**
     * Return the draft key
     */
    public function getDeactivatedKey() {
        $status_class = static::$status_class;
        return $status_class::DEACTIVATED;
    }

    /**
     * Return the rejected key
     */
    public function getRejectedKey() {
        $status_class = static::$status_class;
        return $status_class::REJECTED;
    }

    /**
     * Update the status
     *
     * @param $status | desired status
     * @param bool $approve | send for approving
     */
    public function updateStatus( $status, $approve = false )
    {
        $user = Auth::user();
        if ( !$user ) {
            return;
        }

        //first check if requesting for approveing
        if ( $approve || $status == $this->getApprovedKey() ) {
            $status = $user->can('approve', static::class) ? $this->getApprovedKey() : $this->getPendingKey();
        } elseif ( $status == $this->getRejectedKey() ) {
            if ( !$user->can('approve', static::class) ) {
                //only those allowed to approve can reject
                $status = $this->getDeactivatedKey();
            }
        } elseif ( !$user->can('approve', static::class) ) {
            //set to deactivated as default status for unapproveable users
            $status = $this->getDeactivatedKey();
        }

        $this->status = $status ? $status : $this->getDeactivatedKey();
    }

    /**
     * User visible
     */
    public function scopeUserVisible($query)
    {
        if ( Auth::check() ) {
            $user = Auth::user();

            if ( $user->can('editOthers', static::class) ) {
                //can view all
                return $query;
            } elseif ( $user->can('create', static::class) ) {
                //can view own and published
                return $query->approved()
                    ->orWhere('user_id', '=', $user->id);
            }
        }

        //everyone can view approved
        return $query->approved();
    }

    /**
     * Get the status label
     */
    public function getStatusLabel( $status )
    {
        $status_class = static::$status_class;
        return $status_class::getLabel($status);
    }
    
    /**
     * Get status name
     */
    public function getStatusNameAttribute()
    {
        return $this->getStatusLabel($this->status);
    }

    /**
     * Get the status label
     */
    public function getStatusSlug( $status )
    {
        $status_class = static::$status_class;
        return $status_class::getSlug($status);
    }

    /**
     * Get status name
     */
    public function getStatusSlugAttribute()
    {
        return $this->getStatusSlug($this->status);
    }

    /**
     * Get relative date
     * @param Carbon $date
     * @return
     */
    public function getRelativeDate( Carbon $date )
    {
        return $date->diffForHumans();
    }

    /**
     * Get Relative updated at
     */
    public function getUpdatedAtRelAttribute()
    {
        return $this->getRelativeDate($this->updated_at);
    }

    /**
     * Get Relative created at
     */
    public function getCreatedAtRelAttribute()
    {
        return $this->getRelativeDate($this->created_at);
    }

    /**
     * Get the excerpt attribute
     *
     * @param int $length
     *
     * @return string
     */
    public function getExcerpt($length = 200) {
        return str_limit(strip_tags($this->description), $length);
    }

    /**
     * Get the excerpt attribute
     *
     * @param int $length
     *
     * @return string
     */
    public function getExcerptAttribute() {
        return $this->getExcerpt();
    }

    /**
     * Latest for users scope
     */
    public function scopeLatestForUsers( $query )
    {
        return $query->userVisible()
            ->with('media', 'user');
    }
}