<?php
/**
 * Created by PhpStorm.
 * User: Arushad
 * Date: 18/09/2016
 * Time: 22:22
 */

namespace App\Helpers\Dropzone;


trait DropzoneTrait
{

    /**
     * Retreive all attached files
     *
     * @param $collection
     * @param string $preview
     * @return \Illuminate\Support\Collection
     */
    public function getFiles($collection = 'images', $preview = 'thumb') {
        $medias = $this->getMedia($collection);
        $files = [];

        foreach ($medias as $media) {
            $files[] = ['id' => $media->id, 'name' => $media->name, 'size' => $media->size, 'thumb' => $media->getUrl($preview)];
        }

        return collect($files);
    }

    /**
     * Deletes a file
     * @param $name
     * @param string $collection
     * @return int
     */
    public function deleteFile( $name, $collection = 'images' ) {
        $file = $this->media()
            ->whereCollectionName($collection)
            ->whereId($name)
            ->first();

        if( $file ) {
            return $file->delete();
        } else {
            return 0;
        }
    }
}