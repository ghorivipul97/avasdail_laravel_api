<?php
/**
 * Created by PhpStorm.
 * User: Arushad
 * Date: 18/09/2016
 * Time: 22:22
 */

namespace App\Helpers\Dropzone;


use Illuminate\Http\Request;
use Spatie\MediaLibrary\Media;

trait DropzoneUploadTrait
{

    /**
     * Uploads files to models
     *
     * @param Request $request
     * @param $model
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, $model_id)
    {
        $model = $this->getDropzoneModel($model_id);
        $this->authorize('update', $model);

        $this->validate($request, [
            'file' => 'required|mimes:jpeg,jpg,png|max:'.get_setting('max_image_file_size'),
        ]);

        //verify max files
        $max_files = get_setting('max_num_files');
        if ( $max_files && $model->getMedia($this->dropzoneCollection())->count() >= $max_files ) {
            return response()->json(['error' => 'Maximum number of files exceeded.'], 422);
        }

        //add file
        $media = null;
        if ( $file = ($request->file('file')) ) {
            $media = $model->addMedia($file)->usingFileName(str_slug(str_random(32)).'.'.$file->guessExtension())
                ->toMediaLibrary($this->dropzoneCollection());
        }

        if ( $media ) {
            return response()->json(['success' => true, 'id' => $media->id, 'location' => $media->getUrl()]);
        } else {
            return response()->json(['error' => 'File could not be saved.'], 500);
        }
    }

    /**
     * Uploads files to models
     *
     * @param Request $request
     * @param $model
     * @return \Illuminate\Http\Response
     */
    public function uploadDestroy(Request $request, $model_id)
    {
        $model = $this->getDropzoneModel($model_id);
        $this->authorize('update', $model);

        $this->validate($request, [
            'id' => 'required|integer',
        ]);

        if ( !($resp = $model->deleteFile($request->input('id'), $this->dropzoneCollection())) ) {
            $error_code = $resp == 0 ? 404 : 500;
            if ($request->expectsJson()) {
                return response()->json(false, $error_code);
            }
            \App::abort($error_code);
        }

        return response()->json(true);
    }

    /**
     * Get the dropzone model
     */
    public function getDropzoneModel( $model_id ) {
        $namespaced_model_name = $this->dropzoneNamespacedModelName();
        $id_field = with(new $namespaced_model_name)->getRouteKeyName();
        return $namespaced_model_name::where($id_field, '=', $model_id)->firstOrFail();
    }

    /**
     * Get dropzone collection name
     */
    public function dropzoneCollection() {
        return property_exists($this, 'dropzone_collection') ? $this->dropzone_collection : 'images';
    }

    /**
     * Get wc model name
     */
    public function dropzoneModelName() {
        return property_exists($this, 'dropzone_model') ? $this->dropzone_model : 'Model';
    }

    /**
     * Get wc model namespaced model name
     */
    public function dropzoneNamespacedModelName() {
        return Media::getActualClassNameForMorph( $this->dropzoneModelName() );
    }
}