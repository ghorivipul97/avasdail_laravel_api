<?php
/**
 * Fake service
 * User: dash-
 * Date: 10/06/2017
 * Time: 15:27
 */

namespace App\Helpers\Faker;


class Service extends \Faker\Provider\Base
{
    /**
     * Get a product category
     */
    public function category()
    {
        return static::randomElement(static::$categories);
    }

    /**
     * Get a product
     */
    public function product()
    {
        return static::randomElement(static::$products);
    }

    /**
     * Get a material
     */
    public function material()
    {
        return static::randomElement(static::$materials);
    }

    /**
     * Get an attribute
     */
    public function attribute()
    {
        return static::randomElement(static::$attributes);
    }

    /**
     * Get an adjective
     */
    public function adjective()
    {
        return static::randomElement(static::$adjectives);
    }

    /**
     * Get a brand
     */
    public function attributeValueSuffix()
    {
        return static::randomElement(static::$attribute_value_suffixes);
    }

    /**
     * Get a brand
     */
    public function attributeValue()
    {
        return static::randomElement(static::$attribute_values);
    }

    /**
     * Get a product category
     */
    public function attributeValueName()
    {
        $format = static::randomElement(static::$attribute_value_name_formats);
        return $this->generator->parse($format);
    }

    /**
     * Get a product category
     */
    public function productName()
    {
        $format = static::randomElement(static::$product_name_formats);
        return $this->generator->parse($format);
    }

    /**
     * Get an attribute name
     */
    public function attributeName()
    {
        $format = static::randomElement(static::$attribute_name_formats);
        return $this->generator->parse($format);
    }

    /**
     * Get an category name
     */
    public function categoryName()
    {
        $format = static::randomElement(static::$category_name_formats);
        return $this->generator->parse($format);
    }

    protected static $category_name_formats = [
        '{{adjective}} {{category}}',
        '{{material}} {{category}}',
        '{{category}}',
    ];
    
    protected static $product_name_formats = [
        '{{adjective}} {{material}} {{product}}',
        '{{adjective}} {{material}} {{category}}',
        '{{material}} {{category}} {{product}}',
        '{{material}} {{product}} {{product}}',
        '{{adjective}} {{material}} {{category}} {{product}}',
        '{{adjective}} {{material}} {{product}} {{product}}',
    ];

    protected static $attribute_name_formats = [
        '{{attribute}}',
        '{{product}} {{attribute}}',
        '{{material}} {{attribute}}',
        '{{adjective}} {{attribute}}',
    ];

    protected static $attribute_value_name_formats = [
        '{{randomDigitNotNull}} {{attributeValueSuffix}}',
        '{{randomDigitNotNull}}',
        '{{attributeValue}}',
        '{{material}}',
        '{{attributeValue}} {{material}}',
    ];

    protected static $adjectives = ['Small', 'Ergonomic', 'Rustic', 'Intelligent', 'Gorgeous', 'Incredible', 'Fantastic', 'Localized', 'Simple',
        'Practical', 'Sleek', 'Awesome', 'Enormous', 'Mediocre', 'Synergistic', 'Heavy Duty', 'Lightweight', 'Aerodynamic', 'Durable',
        'Long Sleeved', 'Short Sleeved', 'Waterproof', 'Lady\'s', 'Men\'s', 'Girl\'s', 'Boy\'s', 'Babie\'s', 'Soft', 'Hard', 'Bendable'];

    protected static $materials = ['Steel', 'Wooden', 'Concrete', 'Plastic', 'Cotton', 'Granite', 'Rubber', 'Leather', 'Silk', 'Wool', 'Linen',
        'Marble', 'Iron', 'Bronze', 'Copper', 'Aluminum', 'Paper', 'Carbon Fibre', 'Magnesium Alloy', 'Aluminium', 'Graphite', 'Alcantara',
        'Plush', 'Nylon', 'Woven', 'Silver', 'Gold', 'Platinum', 'Diamond', 'Fabric', 'Pyrex', 'Wool', 'Leather', 'Hemp', 'Bamboo', 'Canvas'];

    protected static $products = ['Chair', 'Car', 'Computer', 'Gloves', 'Pants', 'Shirt', 'Table', 'Shoes', 'Hat', 'Plate', 'Knife', 'Bottle', 'Coat', 'Lamp',
        'Keyboard', 'Bag', 'Bench', 'Clock', 'Watch', 'Wallet', 'Mouse', 'Phone', 'Tablet', 'Diapers', 'Hand Bag', 'Sachet', 'Dress', 'Shirt', 'T-Shirt',
        'Trousers', 'Camera', 'Laptop', 'Bra', 'Panties', 'Boxers', 'Shorts', 'Watch', 'Bracelet', 'Necklace', 'Ring', 'Bangle', 'Fork', 'Spoon', 'Pot',
        'Toy', 'Bicycle', 'Table', 'Wallet', 'Key', 'Card', 'Printer', 'Pen', 'Pencil', 'Fidget Spinner', 'Headphone', 'Box', 'Dispenser', 'Glasses'];

    protected static $attributes = ['Color', 'Size', 'Number of Sleeves', 'RAM', 'Processor', 'Storage', 'Camera', 'Resolution', 'Screen Size', 'OS',
        'Number of Pockets', 'Age', 'Style', 'Cut', 'Ports', 'Material', 'Capacity', 'Weight', 'Speed', 'Performance', 'Power Rating', 'Months', 'Carats',
        'Waterproofing', 'Shoe Length', 'Waist', 'Model', 'Touch Screen', 'Holes', 'Warranty', 'Support', 'Cut', 'Cup Size', 'Bust', 'Length', 'Batteries',
        'Video RAM', 'Torque', 'Fabric', 'Type', 'Neckline', 'Season', 'Number', 'Line', 'Gender', 'Sleeve', 'Speed', 'Depth', 'Diameter', 'Shape'];

    protected static $attribute_value_suffixes = ['GB', 'MB', 'GHz', 'megapixel', 'sleeves', 'cm', 'g', 'k', 'm', 'months', 'years', 'MHz', 'carats',
        'inches', 'feet', 'm/s', 'km/hr'];

    protected static $attribute_values = ['S', 'L', 'M', 'XL', 'XXL', '1080p', '4K', 'SD', 'iOS', 'Android', 'Windows', 'Linux', 'Slim-fit', 'Loose Fit', 'Baggy',
        'Winter', 'Summer', 'Male', 'Female', 'IP67', 'IP68'];

    protected static $categories = ['Electronics', 'Computer Components', 'RAM', 'Processors', 'Computer Power Supply', 'CPU Coolers', 'Gadgets',
        'Graphics Cards', 'Headphones & Speakers', 'Operator Headsets', 'Motherboard', 'Mouse & Keyboard', 'Mobile Phones & Tablets', 'Power Banks',
        'Headsets', 'Chargers', 'Tablets', 'Mobile Phone & Tablet Cover', 'Mobile phones', 'Mobile Phone Accessories', 'Screens & Screen Protectors',
        'POS Solutions', 'Reciept Printers', 'Scanners', 'Cash Counter', 'Consumer Electronics', 'Home Audio & Video',
        'Drives & Storage', 'Pen Drives', 'Memory Card', 'Hard Disk Drives', 'Appliance', 'Home Appliance', 'Telecom', 'IP Phones',
        'Operator Headsets', 'PABX', 'Phone', 'Telephone Networking', 'Computers, Laptops & Gaming', 'Laptop Accessories', 'Laptop Bags',
        'Laptop Adapters', 'Computer Systems', 'Desktop PC', 'Laptops', 'Printers', 'Receipt Printers', 'Network', 'Routers, Access Points & Switches',
        'Security', 'Time attendance system', 'Cameras', 'Access control System', 'Brackets', 'Other Brackets', 'TV & Monitors', 'Network Accessories',
        'Software', 'Beauty & Health Care', 'Supplement', 'Bone, Joint & Muscle', 'Children', 'Cough, Cold & Flu', 'Diabetes', 'General Health', 'Honey',
        'Men', 'Sleep & Stress Management', 'Vitamins & Minerals', 'Weight Loss', 'Women’s Suppliment', 'Skin Care', 'Face', 'Lip Care', 'Accessories',
        'Shaving & Hair Removal', 'Skin Care Herbal', 'Bath & body', 'Feet', 'Oils & Sprays', 'Lotions & Gels', 'Health & Wellness', 'Hair Care',
        'Accessories', 'Hair Care & Styling', 'Shampoos & Conditioners', 'Hair Care Herbal', 'Shaving & Hair Removal', 'Trimmers & Shavers',
        'Hair Treatment Products', 'Household Health Monitor', 'Temperature Monitors', 'Pressure Monitors', 'Glucose Monitors',
        'Massage & Relaxation', 'Essential Oil', 'Hygiene', 'Adult Hygiene', 'Feminine Hygiene', 'Oral Hygiene', 'Sexual Health', 'Health Accessories',
        'Make-up', 'Lip', 'Face', 'Eyes', 'Make-up Tools & Accessories', 'Make-up Sets', 'Nails', 'Perfume', 'Fragrance', 'Deodorants', 'Perfumes',
        'Kids and Infant', 'Infants / Baby', 'Activity & Gear', 'Bedding', 'Baby Care', 'Maternity Care', 'Diapers & Potty Training', 'Feeding & Nursing',
        'Baby Accessories', 'Baby Sets & Gift Sets', 'T-Shirts & Tops', 'Caps, Socks & Footwear', 'Frocks, Dresses & Skirts', 'Rompers & Body Suits',
        'Girls', 'Accessories', 'Tops, Tees & Shirts', 'Jeans, Trousers & Track Pants', 'Frocks & Dresses', 'Skirts, Leggings & Jeggings',
        'Nightwear', 'Innerwear', 'Footwear', 'Boys', 'Track Pants & Nightwear', 'Footwear', 'Top & Bottom Sets', 'T-Shirts & Shirts', 'Jeans, Pants & Shorts',
        'Accessories', 'Innerwear', 'Kid’s Watches', 'Kids Eyewear', 'Women', 'Women’s Clothing', 'Undergarments', 'Bottoms', 'Dress', 'Hijabs',
        'Lingerie & Sleepwear', 'Scarfs', 'Sportswear', 'Tops & T-Shirts', 'Accessories', 'Unisex Watches', 'Ladies Watches', 'Women’s Belt',
        'Wallets & Hand Bags', 'Ladies Eyewear', 'Unisex Eyewear', 'Women’s Footwear', 'Slipper & Flip Flops', 'Ladies Socks', 'Women’s Casual Shoes',
        'Flat & Heeled Slip-on', 'Women’s Sport Shoes', 'Ballerinas', 'Jewelry', 'Hair Jewelry', 'Necklaces & Pendants', 'Rings', 'Bracelets & Bangles',
        'Earrings', 'Brooches', 'Men', 'Accessories', 'Men’s Belt', 'Men’s Watches', 'Unisex Eyewear', 'Wallets & Accessories', 'Gents Eyewear',
        'Men’s Tie', 'Footwear', 'Men’s Sports Shoes', 'Casual Wear', 'Socks', 'Formal Shoes', 'Tops', 'Sportswear', 'Shirts', 'T-Shirts',
        'Suits', 'Bottoms', 'Pants & Trousers', 'Shorts & 3/4ths', 'Pajamas & Undergarments', 'Jeans', 'Home & Living', 'Toys, Games & Hobbies',
        'Shapes & Colors', 'Board Games', 'Game Consoles', 'Console Games', 'Electronic Toys', 'Boys Toys', 'Girls Toys', 'Soft Toys', 'Wooden Toys',
        'Other Toys', 'Home', 'Furniture', 'Fittings', 'Lights & Bulbs', 'Other Electrical', 'Bedding Set', 'Curtains', 'Home Storage & Organizers',
        'Cleaning', 'Kitchen Cleaning', 'Bathroom Cleaning', 'Home Cleaning', 'Party Supplies', 'Decoration', 'Cups & Plates', 'Balloons', 'Cleaning',
        'Kitchen Cleaning', 'Home Cleaning', 'Household Cleaning & Laundering', 'Kitchen', 'Kitchenware', 'Kitchen Appliances', 'Bathroom', 'Electrical Tools & Supplies',
        'Tools', 'Cable Cutters & Strippers', 'Knives & Cutters', 'Measuring Tools', 'Hex Keys & Screwdrivers', 'Test Instruments', 'Tool Kits & Cases',
        'Crimpers & Strippers', 'Power Supplies & Solar', 'AC / DC Adapters', 'CCTV & Doorlock Power Supply', 'Inverter', 'Switching Power Supply', 'UPS',
        'Battery', 'Button Cell Battery', 'Rechargeable Battery', 'Other Batteries',
        'Electric', 'Indicators, Switches & Warning', 'Connectors, Converters & Terminal', 'Cables & Wires', 'Active & Passive Components', 'Stationery & Books',
        'School Accessories', 'Bags', 'Desk Accessories & Organizers', 'Notebooks & Writing Pads', 'Paper', 'Writing & Drawing', 'Other Accessories', 'Office Equipment',
        'Speaker Systems', 'Monitors', 'TV', 'Projectors', 'Ink & Toner', 'Dhivehi Books', 'Food & Beverages', 'Bread & Bakery', 'Dairy', 'Nuts', 'Tea & Coffee',
        'Sports & Outdoor Wear', 'Sports', 'Water Sports', 'Outdoor Sports', 'Indoor Sports', 'Fitness Accessories', 'Fitness Equipment', 'Rain Gear',
        'Fishing & Agriculture', 'Agriculture', 'Fishing', 'Fishing Gear', 'Fishing Hooks & Lures', 'Fishing Rolls & Nets', 'Fishing Tools & Accessories',
    ];
}