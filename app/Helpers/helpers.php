<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

if (! function_exists('add_error_class')) {
    /**
     * Adds error class if has error
     *
     * @param bool $has_error
     * @param string $classes
     * @param string $error_class
     * @return string
     */
    function add_error_class($has_error, $classes = 'form-group', $error_class = 'has-error') {
        return $has_error ? $classes . ' ' . $error_class : $classes;
    }
}

if (! function_exists('to_array')) {
    /**
     * Converts a comma delimited string to an array
     * If an array is passed in, returns same array
     *
     * @param mixed
     * @return array
     */
    function to_array($array_str) {
        $array = $array_str;
        if ( !is_array($array_str) ) {
            $array = explode(',', $array_str);
        }
        return $array;
    }
}

if(! function_exists('safe_in_array')) {
    /**
     * Checks if value is in_array
     *
     * @param mixed $needle
     * @param mixed $haystack
     * @param bool $strict false
     * @return bool
     */
    function safe_in_array($needle, $haystack, $strict = false) {
        if ( is_array($haystack) ) {
           return in_array($needle, $haystack, $strict);
        } else {
            return $strict ? $needle === $haystack : $needle == $haystack;
        }
    }
}

if(! function_exists('default_setting')) {
    /**
     * Returns the default setting
     * If setting does not exist, return empty string
     *
     * @param string $key
     * @return string
     */
    function default_setting($key) {
        $default = Config::get('defaults.'.$key);
        return is_null($default) ? '' : $default;
    }
}

if(! function_exists('get_setting')) {
    /**
     * Shortcut for getting setting value
     *
     * @param string $key
     * @return string
     */
    function get_setting($key) {
        return Setting::get($key, default_setting($key));
    }
}

if(! function_exists('set_file_setting')) {
    /**
     * Saves a file settings
     *
     * @param string $key
     * @param string $path
     * @return bool
     */
    function set_file_setting($key, $path = 'img') {
        if ( Input::hasFile($key) && Input::file($key)->isValid() ) {
            $extension = Input::file($key)->guessExtension(); //get file extension
            $file_name = 'custom-'.$key.'.'.$extension; // renaming file
            Input::file($key)->move($path, $file_name); // uploading file to given path
            Setting::set($key, $path.'/'.$file_name);
            return true;
        }
        return false;
    }
}

if(! function_exists('reset_file_setting')) {
    /**
     * Resets a file settings
     *
     * @param string $key
     *
     */
    function reset_file_setting($key) {
        $current_file = get_setting($key);
        $default = default_setting($key);

        //delete the custom file if different from default
        if ( $current_file != $default ) {
            if ( Storage::disk('public')->has($current_file) ) {
                Storage::disk('public')->delete($current_file);
            }

            Setting::set($key, $default);
        }
    }
}

if(! function_exists('strip_protocol')) {
    /**
     * Remove protocol from url string
     *
     * @param string $url
     * @return string
     */
    function strip_protocol( $url )
    {
        return preg_replace('#^https?://#', '', $url);
    }
}

if(! function_exists('url_path')) {
    /**
     * Remove the host and query params from a url
     *
     * @param string $url
     * @return string
     */
    function url_path( $url )
    {
        $url_parts = parse_url($url);
        return isset($url_parts['path']) ? trim($url_parts['path'], '/ ') : '';
    }
}

if( !function_exists('add_query_args')) {
    /**
     * Add query args
     * @param $url
     * @param array $args
     * @return string
     */
    function add_query_args($url, $args = [])
    {
        if ( is_array($args) && !empty($args) ) {
            $params = '';
            foreach ($args as $key => $value) {
                $params .= $key . '=' . $value . '&';
            }

            $params = trim($params, '&');
            $url .= '?'.$params;
        }

        return $url;
    }
}

if (! function_exists('add_sort_class')) {
    /**
     * Adds sorting class
     *
     * @param string $field
     * @param string $classes
     * @return string
     */
    function add_sort_class($field, $classes = '') {
        $sorting_class = 'sorting';
        if ( Input::get('orderby') == $field ) {
            $sorting_class .= '_' . strtolower(Input::get('order', 'ASC'));
        }

        if ( $classes ) {
            $sorting_class .= ' ' . $classes;
        }

        return $sorting_class;
    }
}

if (! function_exists('random_id_or_generate')) {
    /**
     * Get a random id from the model or generate a new one using faker
     * @return mixed
     */
    function random_id_or_generate($model_class, $key = 'id')
    {
        $id = $model_class::inRandomOrder()->value($key);
        if ( !$id ) {
            $id = factory($model_class)
                ->create()
                ->{$key};
        }

        return $id;
    }
}