<?php
/**
 * Created by PhpStorm.
 * User: Arushad
 * Date: 15/09/2016
 * Time: 22:54
 */

namespace App\Helpers;

use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{
    /*
     * Get the path for the given media, relative to the root storage path.
     */
    public function getPath(Media $media) : string
    {
        return $media->token.'/';
    }

    /*
     * Get the path for conversions of the given media, relative to the root storage path.

     * @return string
     */
    public function getPathForConversions(Media $media) : string
    {
        return $media->token.'/c/';
    }
}