<?php
/**
 * Simple trait to update a single media collection
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;


use Illuminate\Http\Request;

trait UpdateMediaTrait
{

    /**
     * Updates the media collection with given file from request
     *
     * @param $collection
     * @param Request $request
     * @param string $key
     * @return mixed
     */
    public function updateSingleMedia( $collection, Request $request, $key = '' )
    {
        if (!$key) {
            $key = $collection;
        }

        if ( $file = $request->file($key) ) {
            //update the file
            $this->clearMediaCollection($collection);
            return $this->addMedia($file)
                ->usingFileName(str_slug(str_random(8)).'.'.$file->guessExtension())
                ->toMediaCollection($collection);
        } elseif ( $request->exists($key) ) {
            //remove file if empty
            $this->clearMediaCollection($collection);
            return 0;
        }

        return false;
    }

}