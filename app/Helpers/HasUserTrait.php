<?php
/**
 * Simple trait for user posts
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

trait HasUserTrait
{

    /**
     * Belongs to a user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Update the user
     *
     * @param $new_user | user object or id
     */
    public function updateUser( $new_user = null )
    {
        $user = Auth::user();
        if ( !$user ) {
            return;
        }

        //if no user specified and already assigned then skip
        if ( empty($new_user) && $this->user_id ) {
            return;
        }

        if ( empty($new_user) || !$user->can('editOthers', static::class) ) {
            //default to current user
            $new_user = $user;
        }

        $this->user()->associate($new_user);
    }

    /**
     * Get admin user url
     */
    public function getAdminAuthorUrlAttribute()
    {
        return $this->user->admin_author_url;
    }

    /**
     * Get admin user attribute
     */
    public function getAdminAuthorLinkAttribute()
    {
        return $this->user->admin_author_link;
    }

}