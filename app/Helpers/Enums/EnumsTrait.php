<?php
/**
 * Simple trait for enums
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

trait EnumsTrait
{

    protected static $labels = [];

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [];
    }

    /**
     * Get label for key
     *
     * @param $key
     * @return string
     */
    public static function getLabel($key)
    {
        return isset(static::getLabels()[$key]) ? trans(static::getLabels()[$key]) : '';
    }

    /**
     * Get type labels
     */
    public static function getLabels()
    {
        //first initialize
        if ( empty(static::$labels) ) {
            static::initLabels();
        }

        return static::$labels;
    }

    /**
     * Get keys
     *
     * @return array
     */
    public static function getKeys() {
        return array_keys(static::getLabels());
    }

}