<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class UserStatuses
{
    use EnumsTrait;

    const ACTIVE = 1;
    const PENDING = 2;
    const BANNED = 3;

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::ACTIVE    => __('Active'),
            static::PENDING   => __('Pending'),
            static::BANNED    => __('Banned'),
        ];
    }

}