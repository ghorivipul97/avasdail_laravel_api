<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class ReviewStatuses
{
    use EnumsTrait;

    const APPROVED = 1;
    const PENDING = 2;
    const REJECTED = 3;
    const DRAFT = 4;

    /**
     * Slugs
     */
    protected static $slugs = [
        self::APPROVED => 'approved',
        self::PENDING => 'pending',
        self::REJECTED => 'rejected',
        self::DRAFT => 'draft',
    ];

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::APPROVED       => __('Approved'),
            static::PENDING        => __('Pending'),
            static::REJECTED       => __('Rejected'),
            static::DRAFT    => __('Draft'),
        ];
    }

    /**
     * Get label for key
     *
     * @param $key
     * @return string
     */
    public static function getSlug($key)
    {
        return isset(static::$slugs[$key]) ? static::$slugs[$key] : '';
    }

}