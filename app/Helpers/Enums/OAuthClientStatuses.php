<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class OAuthClientStatuses
{
    use EnumsTrait;

    const ACTIVE = 0;
    const REVOKED = 1;

    /**
     * Slugs
     */
    protected static $slugs = [
        self::ACTIVE => 'active',
        self::REVOKED => 'revoked',
    ];

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::ACTIVE       => __('Active'),
            static::REVOKED      => __('Revoked'),
        ];
    }

    /**
     * Get label for key
     *
     * @param $key
     * @return string
     */
    public static function getSlug($key)
    {
        return isset(static::$slugs[$key]) ? static::$slugs[$key] : '';
    }

}