<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class ServiceRequestStatuses
{
    use EnumsTrait;

    const ACTIVE = 1;
    const CANCELLED = 2;
    const ACCEPTED = 3;
    const PENDING = 4;

    /**
     * Slugs
     */
    public static $slugs = [
        self::CANCELLED => 'cancelled',
        self::ACTIVE => 'active',
        self::PENDING => 'pending',
        self::ACCEPTED => 'accepted',
    ];

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::ACTIVE       => __('Active'),
            static::CANCELLED       => __('Cancelled'),
            static::PENDING        => __('Pending'),
            static::ACCEPTED       => __('Accepted'),
        ];
    }

    /**
     * Get label for key
     *
     * @param $key
     * @return string
     */
    public static function getSlug($key)
    {
        return isset(static::$slugs[$key]) ? static::$slugs[$key] : '';
    }

}