<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class QuestionTypes
{
    use EnumsTrait;

    const TEXT = 1;
    const TEXTAREA = 2;
    const MULTICHECK = 3;
    const RADIO = 4;
    const DATE = 5;
    const DATETIME = 6;
    const INTEGER = 7;
    const DOUBLE = 8;
    const IMAGE = 9;

    /**
     * Validation rules
     */
    protected static $validation_rules = [];

    /**
     * Cast types
     */
    protected static $cast_types = [
        self::TEXT => 'string',
        self::TEXTAREA => 'string',
        self::MULTICHECK => 'array',
        self::RADIO => 'string',
        self::DATE => 'date',
        self::DATETIME => 'datetime',
        self::INTEGER => 'integer',
        self::DOUBLE => 'double',
    ];

    /**
     * Slugs
     */
    protected static $slugs = [
        self::TEXT => 'text',
        self::TEXTAREA => 'textarea',
        self::MULTICHECK => 'multicheck',
        self::RADIO => 'radio',
        self::DATE => 'date',
        self::DATETIME => 'datetime',
        self::INTEGER => 'integer',
        self::DOUBLE => 'double',
        self::IMAGE => 'image',
    ];

    /**
     * MCQ question types
     *
     * @var array
     */
    protected static $mcq_types = [
        self::MULTICHECK,
        self::RADIO,
    ];

    /**
     * FILE question types
     *
     * @var array
     */
    protected static $file_q_types = [
        self::IMAGE,
    ];

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::TEXT        => __('Text'),
            static::TEXTAREA    => __('Text Area'),
            static::MULTICHECK  => __('Multi Checkbox'),
            static::RADIO       => __('Radio'),
            static::DATE        => __('Date'),
            static::DATETIME    => __('Datetime'),
            static::INTEGER     => __('Integer'),
            static::DOUBLE      => __('Double'),
            static::IMAGE       => __('Image File'),
        ];
    }

    /**
     * Get MCQ types
     */
    public static function mcqTypes()
    {
        return static::$mcq_types;
    }

    /**
     * Get File question types
     */
    public static function fileQTypes()
    {
        return static::$file_q_types;
    }

    /**
     * Get slug for key
     *
     * @param $key
     * @return string
     */
    public static function getSlug($key)
    {
        return isset(static::$slugs[$key]) ? static::$slugs[$key] : '';
    }

    /**
     * Get cast type for key
     *
     * @param $key
     * @return string
     */
    public static function getCastType($key)
    {
        return isset(static::$cast_types[$key]) ? static::$cast_types[$key] : 'string';
    }

    /**
     * Get label for key
     *
     * @param $key
     * @return array
     */
    public static function getValidationRules($key)
    {
        //first initialize
        if ( empty(static::$validation_rules) ) {
            static::initValidationRules();
        }

        return isset(static::$validation_rules[$key]) ? static::$validation_rules[$key] : [];
    }

    /**
     * Get type labels
     */
    protected static function initValidationRules()
    {
        static::$validation_rules = [
            self::TEXT => ['string'],
            self::TEXTAREA => ['string'],
            self::MULTICHECK => ['array'],
            self::RADIO => ['string'],
            self::DATE => ['date'],
            self::DATETIME => ['date'],
            self::INTEGER => ['integer'],
            self::DOUBLE => ['numeric'],
            self::IMAGE => [
                'mimes:jpeg,jpg,png',
                'max:'.get_setting('max_upload_file_size'),
            ],
        ];
    }

}