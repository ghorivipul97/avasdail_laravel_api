<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class CityTypes
{
    use EnumsTrait;

    const CITY = 1;
    const ISLAND = 2;
    const WARD = 3;

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::CITY   => __('City'),
            static::ISLAND   => __('Island'),
            static::WARD    => __('Ward'),
        ];
    }

}