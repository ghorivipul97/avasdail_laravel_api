<?php
/**
 * DisallowedSlugs
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class DisallowedSlugs
{

    use EnumsTrait;

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            'login',
            'logout',
            'register',
            'password',
            'profile',
            'resources',
            'comments',
            'users',
            'resources',
            'cecs',
            'news',
            'events',
            'jobs',
            'search',
            'ads',
            'categories',
        ];
    }

}