<?php
/**
 * User Statuses
 *
 * User: Arushad
 * Date: 06/10/2016
 * Time: 16:28
 */

namespace App\Helpers\Enums;

abstract class StateTypes
{
    use EnumsTrait;

    const STATE = 1;
    const ATOLL = 2;
    const CITY = 3;

    /**
     * Initialize labels
     */
    protected static function initLabels()
    {
        static::$labels = [
            static::STATE   => __('State'),
            static::ATOLL   => __('Atoll'),
            static::CITY    => __('City'),
        ];
    }

}