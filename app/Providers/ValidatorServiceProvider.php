<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Rule is to be defined like this:
         *
         * 'passcheck:users,password,id,1' - Means password is taken from users table, user is searched by field id equal to 1
         */
        Validator::extend('passcheck', function ($attribute, $value, $parameters) {
            $users_table = $parameters[0];
            $id_field = $parameters[2];
            $user_id = $parameters[3];
            $pass_field = $parameters[1];

            $user = DB::table($users_table)->where($id_field, $user_id)->first([$pass_field]);
            if (Hash::check($value, $user->{$pass_field})) {
                return true;
            } else {
                return false;
            }
        });

        /**
         * Rule is to be defined like this:
         *
         * 'slug:-' - Means the value must be a slug with special char -
         */
        Validator::extend('slug', function ($attribute, $value, $parameters) {
            $special_char = isset($parameters[0]) ? $parameters[0] : '-';
            return preg_match('/^([a-z0-9]+['.$special_char.'][a-z0-9]+|[a-z0-9])*$/', $value);
        });

        /**
         * Define error message for slug
         */
        Validator::replacer('slug', function($message, $attribute, $rule, $parameters) {
            $special_char = isset($parameters[0]) ? $parameters[0] : '-';
            return str_replace(':special_char', $special_char, $message);
        });

        /**
         * Rule is to be defined like this:
         *
         * 'ids_exist:table,[field],[where_field],[where_value]' - Means field is taken from given table
         */
        Validator::extend('ids_exist', function ($attribute, $value, $parameters) {
            $values = to_array($value);
            $id_field = isset($parameters[1]) ? $parameters[1] : 'id';
            if ( is_array($values) ) {
                //get specified ids
                $ids = DB::table($parameters[0])
                    ->whereIn($id_field, $values)
                    ->select([$id_field]);

                if ( !empty($parameters[2]) && !empty($parameters[3]) ) {
                    $ids = $ids->where($parameters[2], $parameters[3])
                        ->select([$id_field, $parameters[2]]);
                }

                $ids = $ids->pluck($id_field);

                //check if all ids exists
                foreach($values as $value) {
                    if ( ! $ids->contains($value) ) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        });

        /**
         * Rule is to be defined like this:
         *
         * 'latitude' - Means the value must be a valid latitude
         */
        Validator::extend('latitude', function ($attribute, $value) {
            return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $value);
        });

        /**
         * Rule is to be defined like this:
         *
         * 'longitude' - Means the value must be a valid longitude
         */
        Validator::extend('longitude', function ($attribute, $value) {
            return preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $value);
        });

        /**
         * Rule is to be defined like this:
         *
         * 'mobile:7777777' - Means the value must be a maldivian mobile number
         */
        Validator::extend('mobile', function ($attribute, $value, $parameters) {
            return preg_match('/^((7|9)[0-9]{6})$/', $value);
        });

        /**
         * Rule is to be defined like this:
         *
         * 'array_exists:val1,val2,..' - Means $value is array, and all values exists in given array
         */
        Validator::extend('array_exists', function ($attribute, $values, $parameters) {
            $values = to_array($values);
            if ( is_array($values) ) {
                foreach($values as $value) {
                    if ( !in_array($value, $parameters)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
