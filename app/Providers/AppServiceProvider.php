<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'answer' => 'App\Answer',
            'service_request' => 'App\ServiceRequest',
            'user' => 'App\User',
            'category' => 'App\Category',
            'business' => 'App\Business',
            'question' => 'App\Question',
            'state' => 'App\State',
            'city' => 'App\City',
            'mobile_number' => 'App\MobileNumber',
            'review' => 'App\Review',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        if ($this->app->environment() == 'local') {
//            $this->app->register('Dash8x\Generators\GeneratorsServiceProvider');
//        }

        Passport::ignoreMigrations();
    }
}
