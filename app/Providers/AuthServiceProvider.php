<?php

namespace App\Providers;

use App\Policies\OAuthClientPolicy;
use App\Policies\ReviewPolicy;
use App\Policies\RolePolicy;
use App\Policies\UserPolicy;
use App\Review;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\ServiceRequestPolicy;
use App\ServiceRequest;
use App\Policies\BusinessPolicy;
use App\Business;
use App\Policies\CategoryPolicy;
use App\Category;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;
use Spatie\Permission\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        ServiceRequest::class => ServiceRequestPolicy::class,
        Business::class => BusinessPolicy::class,
        Category::class => CategoryPolicy::class,
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        Client::class => OAuthClientPolicy::class,
        Review::class => ReviewPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensCan([
            'read' => 'Read',
            'write' => 'Write',
        ]);

        Passport::routes(function ($router) {
            //$router->forAuthorization();
            $router->forAccessTokens();
            //$router->forTransientTokens();
        }, ['prefix' => env('API_ROUTE_PREFIX', 'api/v1').'/oauth']);
    }
}
