<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Spatie\MediaLibrary\Media;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Notifications\Events\NotificationFailed' => [
            'App\Listeners\HandleFailedNotification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Register Event Listeners
        Media::creating(function ($media) {
            //get unique value
            $unique_token = str_slug(str_random(32));
            while ( Media::whereToken($unique_token)->first() ) {
                $unique_token = str_slug(str_random(32));
            }
            $media->token = $unique_token;
        });
    }
}
