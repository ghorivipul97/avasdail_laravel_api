<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Notifications\ResetPassword;

class AdminResetPasswordNotification extends ResetPassword implements ShouldQueue
{
    use Queueable;

    /**
     * @param mixed $notifiable
     * @return $this
     */
    public function toMail($notifiable)
    {
        return parent::toMail($notifiable)
            ->action('Reset Password', route('admin.password.reset.token', $this->token))
            ->subject(config('app.name').': Reset Password');
    }
}
