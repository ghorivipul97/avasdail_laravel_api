@extends('layouts.auth')

@section('title', 'Reset Password')

@section('content')
    <!-- Register -->
    <div class="lc-block toggled" id="l-register">
        {!! Form::open(['url' => route('admin.register-post'), 'class' => 'lcb-form']) !!}

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <p class="text-left">Register a new account.</p>

        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    {!! Form::text('username', old('username'),
                    ['class' => 'form-control', 'placeholder' => 'Username', 'required' => 'required']) !!}
                </div>
            </div>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                 </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    {!! Form::email('email', old('email'),
                    ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
                </div>
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                <div class="fg-line">
                    {!! Form::password('password',
                    ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required']) !!}
                </div>
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                <div class="fg-line">
                    {!! Form::password('password_confirmation',
                    ['class' => 'form-control', 'placeholder' => 'Confirm New Password', 'required' => 'required']) !!}
                </div>
            </div>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>

        <button type="submit" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></button>
        {!! Form::close() !!}

        <div class="lcb-navigation">
            <a href="{{ route('admin.login') }}" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign In</span></a>
            <a href="{{ route('admin.forgot-password') }}" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
        </div>
    </div>
@endsection
