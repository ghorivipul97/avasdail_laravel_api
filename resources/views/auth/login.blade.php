@extends('layouts.auth')

@section('title', 'Login')

@section('content')
    <!-- Login -->
    <div class="lc-block toggled" id="l-login">
        {!! Form::open(['url' => route('admin.post.login'), 'class' => 'lcb-form']) !!}

            @include('admin.partials.logo-title')

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="fg-line">
                        {!! Form::text('login', old('login'),
                        ['class' => 'form-control', 'placeholder' => 'Username or Email', 'required' => 'required']) !!}
                    </div>
                </div>
                @if ($errors->has('login'))
                 <span class="help-block">
                    <strong>{{ $errors->first('login') }}</strong>
                 </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                    <div class="fg-line">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required']) !!}
                    </div>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                     </span>
                @endif
            </div>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('remember', 1, old('remember')) !!}
                    <i class="input-helper"></i>
                    Remember Me
                </label>
            </div>

            <button type="submit" class="btn btn-login btn-success btn-float bgm-orange"><i class="zmdi zmdi-arrow-forward"></i></button>
        {!! Form::close() !!}

        <div class="lcb-navigation">
            {{--<a href="{{ route('admin.register') }}" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>--}}
            <a href="{{ route('admin.password.request') }}" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
            <a href="{{ url('/') }}" data-ma-block="#l-register"><i class="zmdi zmdi-home"></i> <span>{{ __('Home') }}</span></a>
        </div>
    </div>
@endsection
