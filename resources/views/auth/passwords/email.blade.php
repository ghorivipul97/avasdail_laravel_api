@extends('layouts.auth')

@section('title', 'Reset Password')

@section('content')
    <!-- Forgot Password -->
    <div class="lc-block toggled" id="l-forget-password">
        {!! Form::open(['url' => route('admin.password.email'), 'class' => 'lcb-form']) !!}

            @include('admin.partials.logo-title')

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <p class="text-left">We'll send a password reset link to your registered email.</p>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="fg-line">
                        {!! Form::email('email', old('email'),
                        ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
                    </div>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                     </span>
                @endif
            </div>

            <button type="submit" class="btn btn-login btn-success btn-float bgm-orange"><i class="zmdi zmdi-check"></i></button>
        {!! Form::close() !!}

        <div class="lcb-navigation">
            <a href="{{ route('admin.login') }}" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign In</span></a>
            {{--<a href="{{ route('admin.register') }}" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>--}}
            <a href="{{ url('/') }}" data-ma-block="#l-register"><i class="zmdi zmdi-home"></i> <span>{{ __('Home') }}</span></a>
        </div>
    </div>
@endsection
