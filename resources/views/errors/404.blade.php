@extends('layouts.admin-error')

@section('title', 'Error 404')

@section('content')
    <h2>SEX!</h2>
    <small>Nah.. it's 404</small>

    <div class="fzb-links">
        @if( Request::fullUrl() != URL::previous() )
        <a href="{{ URL::previous() }}"><i class="zmdi zmdi-arrow-back"></i></a>
        @endif
        <a href="{{ url('/') }}"><i class="zmdi zmdi-home"></i></a>
    </div>
@endsection