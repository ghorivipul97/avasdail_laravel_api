@unless( empty($error) )
<ul class="help-block">
    @foreach($error as $error_text)
    <li>{{ $error_text }}</li>
    @endforeach
</ul>
@endunless