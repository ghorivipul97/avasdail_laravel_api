<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
@include('admin.partials.head', ['hide_header' => true])
@section('title', 'Admin')

<body>
    <div class="login-content bgm-blue">
        @section('content')
        @show
    </div>

    @include('admin.partials.footer', ['hide_footer' => true])

</body>
</html>
