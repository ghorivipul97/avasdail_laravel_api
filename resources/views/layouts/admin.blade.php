<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie9">
<![endif]-->
<html>
    @include('admin.partials.head')
    @yield('page-css')
    @section('title', 'Admin')

    <body>
        @include('admin.partials.header')

        <section id="main">
            @include('admin.partials.sidebar')
            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>@yield('page-title')</h2>
                        @yield('model-actions')
                    </div>

                    @section('content')
                    @show
                </div>
            </section>
        </section>

        @include('admin.partials.footer')
        @yield('page-script')
    </body>
</html>
