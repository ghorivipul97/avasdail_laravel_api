<!DOCTYPE html>
<html lang="en">
@include('web.partials.head')

<body id="page-top">
    @include('web.partials.header')

    @section('content')
    @show

    @include('web.partials.footer')
</body>
</html>
