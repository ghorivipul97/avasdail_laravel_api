@extends('layouts.admin')

@section('page-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
@endsection

@section('title', 'Cities')
@section('page-title', 'Cities')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>
                Cities
                <a class="btn btn-primary pull-right" href="{{ url('admin/cities/create') }}">Add New City</a>
            </h2>
        </div>

        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <table class="table table-striped dataTable" id="example1">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>State Name</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('page-script')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('admin/cities') }}'
                },
                columns: [
                    { data: 'DT_Row_Index', sortable: false, searchable: false},
                    { data: 'state_name', name: 'state_name'},
                    { data: 'code', name: 'code'},
                    { data: 'name', name: 'name'},
                    { data: 'actions', name: 'actions', searchable: false, sortable: false}
                ]
            });
        })
    </script>
@endsection
