@extends('layouts.admin')

@section('title', 'Cities')
@section('page-title', 'Cities')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit City - {{ $city->name }}</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($city, ['method' => 'PATCH', 'files' => true, 'url' =>  url('admin/cities/' . $city->getKey())]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('code', 'Code *') !!}
                            {!! Form::text('code', old('code'), ['class' => 'form-control jtk', 'placeholder' => 'Code', 'required' => 'required']) !!}
                            @include('errors._list', ['error' => $errors->get('code')])
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('name', 'Name *') !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => 'Name', 'required' => 'required']) !!}
                            @include('errors._list', ['error' => $errors->get('name')])
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('state_id', 'State') !!}
                            {!! Form::select('state_id', isset($states) ? $states : [], old('state_id'),
                            ['class' => 'form-control selectpicker', 'data-live-search' => true ]) !!}
                            @include('errors._list', ['error' => $errors->get('state_id')])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a class="btn btn-primary" href="{{ url('admin/cities') }}">Cancel</a>
                            {!! Form::button('Edit City', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection