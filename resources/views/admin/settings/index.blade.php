@extends('layouts.admin')

@section('title', 'Settings')
@section('page-title', 'Settings')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit Settings</h2>
        </div>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-body card-padding">
            {!! Form::open(['method' => 'PATCH', 'action' => ['Admin\\SettingsController@update'], 'class' => 'form-horizontal']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('terms_conditions', 'Terms & Conditions *', ['class' => 'control-label']) !!}
                            {!! Form::textarea('terms_conditions', old('terms_conditions', $setting->terms_conditions), ['class' => 'form-control', 'placeholder' => 'Terms & Conditions',
                                'required' => 'required', 'rows' => 6, 'cols' => 40]) !!}
                            <span class="text-danger">{{ $errors->first('terms_conditions') }}</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::button('Cancel', ['class' => 'btn btn-primary', 'type' => 'reset']) !!}
                    {!! Form::button('Update Setting', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection