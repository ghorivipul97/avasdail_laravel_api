<div class="{{ add_error_class($errors->has('app_name')) }}">
    {!! Form::label('app_name', 'App Name *', ['class' => 'control-label col-sm-3']) !!}
    <div class="col-md-6 col-sm-8">
        <div class="fg-line">
            {!! Form::text('app_name', get_setting('app_name'), ['class' => 'form-control', 'placeholder' => 'App Name',
            'required' => 'required']) !!}
        </div>
        @include('errors._list', ['error' => $errors->get('app_name')])
    </div>
</div>

<div class="{{ add_error_class($errors->has('max_upload_file_size')) }}">
    {!! Form::label('max_upload_file_size', 'Max. Upload File Size (KB) *', ['class' => 'control-label col-sm-3']) !!}
    <div class="col-md-6 col-sm-8">
        <div class="fg-line">
            {!! Form::number('max_upload_file_size', get_setting('max_upload_file_size'), ['class' => 'form-control', 'placeholder' => 'Max. Upload File Size (KB)',
            'min' => '1', 'step' => '1', 'required' => 'required']) !!}
        </div>
        @include('errors._list', ['error' => $errors->get('max_upload_file_size')])
    </div>
</div>

<div class="{{ add_error_class($errors->has('default_avatar')) }}">
    {!! Form::label('default_avatar', 'Avatar', ['class' => 'control-label col-sm-3']) !!}
    <div class="col-md-6 col-sm-8">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                @if( get_setting('default_avatar') )
                    <img src="{{ asset(get_setting('default_avatar')) }}">
                @endif
            </div>
            <div>
                <span class="btn btn-info btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    {!! Form::file('default_avatar', ['class' => 'form-control', 'accept' => 'image/png, image/jpeg']) !!}
                </span>
                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
        @include('errors._list', ['error' => $errors->get('default_avatar')])
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-md-6 col-sm-8">
        @can('edit_settings')
        {!! Form::button('Reset', ['class' => 'btn btn-primary reset-link', 'data-request-url' => action('Admin\\SettingsController@reset'), 'data-redirect-url' => action('Admin\\SettingsController@index')]) !!}
        {!! Form::button('Save', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
        @endcan
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        //reset setting
        $('.reset-link').click(function(e){
            e.preventDefault();
            var request_url = $(this).data('request-url');
            var redirect_url = $(this).data('redirect-url');
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to undo this reset operation!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, reset it!',
            }).then(function(){
                $.ajax({
                    url: request_url,
                    type: 'POST',
                    success: function (result) {
                        swal('Reset!', 'The settings has been reset.', 'success').then(
                                function() {
                                    window.location.replace(redirect_url);
                                }
                        );
                    },
                    error: function () {
                        swal('Error!', 'An error occurred while reseting.', 'error');
                    }
                });
            });
        });

    });
</script>
@endpush