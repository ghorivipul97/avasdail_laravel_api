@extends('layouts.admin')

@section('title', 'Feedback')
@section('page-title', 'Feedback')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Feedback from - {{ $feedback->username }}</h2>
        </div>

        <div class="card-body card-padding">
            <div class="row">
                <div class="col-md-8">
                    <div>
                        {!! Form::label('name', 'Username') !!}
                        <p>{{ $feedback->username }}</p>
                    </div>
                    <div>
                        {!! Form::label('email', 'Email') !!}
                        <p>{{ $feedback->email }}</p>
                    </div>
                    <div>
                        {!! Form::label('number', 'phone Number') !!}
                        <p>{{ $feedback->phone_number }}</p>
                    </div>
                    <div>
                        {!! Form::label('description', 'Description') !!}
                        <p>{{ $feedback->description }}</p>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    {!! Form::label('images', 'Images') !!}
                </div>
                @if(!$feedback->image1 && !$feedback->image2 && !$feedback->image3 && !$feedback->image4 && !$feedback->image5)
                    <div class="col-md-12">
                        <span class="help-text">No images uploaded.</span>
                    </div>
                @endif
                @if($feedback->image1)
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <img src="{{ asset('uploads/feedback-images/' . $feedback->image1) }}" style="max-width: 100%;height: 200px;">
                    </div>
                @endif
                @if($feedback->image2)
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <img src="{{ asset('uploads/feedback-images/' . $feedback->image2) }}" style="max-width: 100%;height: 200px;">
                    </div>
                @endif
                @if($feedback->image3)
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <img src="{{ asset('uploads/feedback-images/' . $feedback->image3) }}" style="max-width: 100%;height: 200px;">
                    </div>
                @endif
                @if($feedback->image4)
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <img src="{{ asset('uploads/feedback-images/' . $feedback->image4) }}" style="max-width: 100%;height: 200px;">
                    </div>
                @endif
                @if($feedback->image5)
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <img src="{{ asset('uploads/feedback-images/' . $feedback->image5) }}" style="max-width: 100%;height: 200px;">
                    </div>
                @endif
            </div>

            <div class="form-group">
                <a class="btn btn-primary" href="{{ url('admin/feedback') }}">Back</a>
            </div>
        </div>
    </div>
@endsection