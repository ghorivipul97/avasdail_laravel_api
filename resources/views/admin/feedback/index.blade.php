@extends('layouts.admin')

@section('page-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
@endsection

@section('title', 'Feedback')
@section('page-title', 'Feedback')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Feedback</h2>
        </div>

        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <table class="table table-striped dataTable" id="example1">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Show</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('page-script')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('admin/feedback') }}'
                },
                columns: [
                    { data: 'DT_Row_Index', sortable: false, searchable: false},
                    { data: 'username', name: 'username'},
                    { data: 'email', name: 'email'},
                    { data: 'phone_number', name: 'phone_number'},
                    { data: 'show', name: 'show', sortable: false, searchable: false}
                ]
            });
        })
    </script>
@endsection
