@extends('layouts.admin')

@section('title', 'States')
@section('page-title', 'States')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit State - {{ $state->name }}</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($state, ['method' => 'PATCH', 'url' => url('admin/states/' . $state->getKey())]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('code', 'Code *') !!}
                            {!! Form::text('code', old('code'), ['class' => 'form-control jtk', 'placeholder' => 'Code', 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('code') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('name', 'Name *') !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => 'Name', 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a class="btn btn-primary" href="{{ url('admin/states') }}">Cancel</a>
                            {!! Form::button('Edit State', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection