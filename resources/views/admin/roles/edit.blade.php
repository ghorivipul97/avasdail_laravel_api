@extends('admin.roles.roles')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>{{ __('Edit Role') }} <small>{{ $role->name }}</small></h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($role, ['method' => 'PATCH', 'action' => ['Admin\\RolesController@update', $role], 'class' => 'form-horizontal']) !!}
            @include('admin.roles._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection