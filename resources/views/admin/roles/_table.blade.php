<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\RolesController@bulk'], 'method' => 'PUT', 'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20 p-b-20">
        @include('admin.roles._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable">
            <thead>
            <tr>
                <th class="clearfix">
                    <div class="pull-left flip checkbox">
                        <label>
                            <input data-all="roles" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                    {{ __('Description') }}
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @include('admin.roles._list')
            </tbody>
        </table>
        @unless( isset($no_pagination) )
        {{  $roles->links('admin.partials.pagination') }}
        @endunless
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>