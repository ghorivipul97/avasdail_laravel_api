<div class="{{ add_error_class($errors->has('description')) }}">
    {!! Form::label('description', __('Description').' *', ['class' => 'control-label col-sm-3']) !!}
    <div class="col-md-6 col-sm-8">
        <div class="fg-line">
            {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => __('Description'),
            'required' => 'required']) !!}
        </div>
        @include('errors._list', ['error' => $errors->get('description')])
    </div>
</div>

<div class="{{ add_error_class($errors->has('name')) }}">
    {!! Form::label('name', __('Name').' *', ['class' => 'control-label col-sm-3']) !!}
    <div class="col-md-6 col-sm-8">
        <div class="fg-line">
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => __('Name'),
            'required' => 'required']) !!}
        </div>
        @include('errors._list', ['error' => $errors->get('name')])
    </div>
</div>

<div class="form-group">
    {!! Form::label('permissions', __('Permissions'), ['class' => 'control-label col-sm-3']) !!}
    <?php
        $permissions = $permissions->groupBy('model');
    ?>
    <div class="col-md-6 col-sm-8 p-t-10">
        @include('errors._list', ['error' => $errors->get('permissions')])
        @foreach($permissions as $model => $model_permissions)
            <p class="c-black f-500 m-b-20">{{ __(ucfirst($model)) }}</p>
            <div class="row">
                <?php $per_group = ceil($model_permissions->count() / 3); ?>
                @foreach( $model_permissions->chunk($per_group) as $chunk)
                    <div class="col-sm-4">
                        @foreach($chunk as $permission)
                            <?php $checked = (isset($role) && $role->hasPermissionTo($permission)) || safe_in_array($permission->id, old('permissions')) ? ' checked' : ''; ?>
                            <div class="checkbox m-b-15">
                                <label>
                                    <input name="permissions[]" value="{{ $permission->id }}" type="checkbox" {{ $checked }} />
                                    <i class="input-helper"></i>
                                    {{ $permission->description }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-md-6 col-sm-8">
        {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
        <a class="btn btn-primary" href="{{ action('Admin\\RolesController@index') }}">{{ __('Cancel') }}</a>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //slugify
            $("#name").keypress(function (e) {
                return restrictCharacters($(this), e, /[A-Za-z0-9_]/g);
            });

            @unless( isset($role) )
            $('#description').keyup(function () {
                var str = $(this).val();
                $('#name').val(slugify(str, '_'));
            });
            @endunless
        });
    </script>
@endpush