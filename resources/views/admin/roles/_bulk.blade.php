<?php
$actions = [];

    if ( Auth::user()->can('delete_roles') ) {
        $actions['delete'] = __('Delete');
    }
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'roles'])