@extends('layouts.admin')

@section('title', 'Roles')
@section('page-title', 'Roles')

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\RolesController@index'])
@endsection

@section('model-actions')
    @include('admin.roles._actions')
@endsection