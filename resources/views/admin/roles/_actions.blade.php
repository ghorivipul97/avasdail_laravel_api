<ul class="actions">
    @if ( isset($role) ) @can('delete', $role)
    <li><a href="#" data-request-url="{{ action('Admin\\RolesController@destroy', $role) }}" data-redirect-url="{{ action('Admin\\RolesController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endcan @endif

    @can('create', Spatie\Permission\Models\Role::class)
    <li><a href="{{ action('Admin\\RolesController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', Spatie\Permission\Models\Role::class)
    <li><a href="{{ action('Admin\\RolesController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>