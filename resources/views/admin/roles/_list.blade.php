@foreach($roles as $role)
    <tr>
        <td class="clearfix">
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="roles" name="roles[]" value="{{ $role->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
            {{ $role->description }}
        </td>
        <td>
            <div class="pull-right flip">
                <ul class="actions">
                    @can('update', $role)
                        <li>
                            <a href="{{ action('Admin\\RolesController@edit', $role) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan

                    @can('delete', $role)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\RolesController@destroy', $role) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach