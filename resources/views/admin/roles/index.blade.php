@extends('admin.roles.roles')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>{{ $title }}</h2>
        </div>

        {!! Form::open(['action' => ['Admin\\RolesController@index'], 'method' => 'GET', 'class' => 'p-l-30 p-r-30 p-t-20 p-b-20']) !!}
        @include('admin.roles._filter')
        {!! Form::close() !!}

        @include('admin.roles._table')
    </div>
@endsection