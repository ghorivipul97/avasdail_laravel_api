@extends('admin.roles.roles')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>{{ __('New Role') }}</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::open(['action' => 'Admin\\RolesController@store', 'class' => 'form-horizontal']) !!}
            @include('admin.roles._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection