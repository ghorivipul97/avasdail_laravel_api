<div class="media media-orig logo-title">
    <div class="media-left">
        <img class="img-responsive" src="{{ asset('img/logo.png') }}" />
    </div>
    <div class="media-body">
        <h1>{{ config('APP_NAME') }} <br class="hidden-xs" />{{ __('Admin Portal') }}</h1>
    </div>
</div>