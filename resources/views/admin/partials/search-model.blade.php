<!-- Top Search Content -->
<div class="h-search-wrap">
    <div class="hsw-inner">
        {!! Form::open(['action' => $search_action, 'method' => 'GET']) !!}
        <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
        {!! Form::text('search', isset($search) ? $search : old('search')) !!}
        {!! Form::close() !!}
    </div>
</div>