<?php
    $max_files = get_setting('max_num_files');
?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function() {

        var {{ camel_case($dropzone_id) }}_files = {!! $files->toJson() !!};

        var {{ camel_case($dropzone_id) }}_dropzone = new Dropzone('#{{ $dropzone_id }}', {
            url: '{{ $upload_url }}',
            dictResponseError: '{{ __('Error uploading file!') }}',
            dictDefaultMessage: '{{ __('Drop file here to upload') }}',
            dictFileTooBig: '{{ __('File is too big (\{\{filesize\}\} MB). Max filesize: \{\{maxFilesize\}\} MB') }}',
            dictInvalidFileType: '{{ __('You can\'t upload files of this type.') }}',
            maxFilesize: {{ floor(get_setting('max_image_file_size') / 1024) }},
            maxFiles: {{ $max_files ? $max_files : 'null' }},
            uploadMultiple: false,
            parallelUploads: 50,
            acceptedFiles: 'image/png, image/jpeg',
            error: function(file, response) {
                var message = ($.type(response) === "string") ? response
                    : '{{ __('Error uploading file!') }}';

                file.previewElement.classList.add('dz-error');
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]');
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                }
                return _results;
            },
            init:function() {
                this.on('maxfilesexceeded', function(file){
                    this.removeFile(file);
                    swal({
                        title: '{{ __('Warning!') }}',
                        text: '{{ __('Maximum number of allowed files exceeded.') }}',
                        confirmButtonText: '{{ __('Ok') }}',
                        type: 'warning',
                    });
                });

                this.on('success', function(file, response) {
                    file.id = response.id;
                });

                this.on('addedfile', function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement('<a class="dz-remove">{{ __('Remove file') }}</a>');
                    // Capture the Dropzone instance as closure.
                    var _this = this;
                    // Listen to the click event
                    removeButton.addEventListener('click', function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        if (typeof(file.id) !== 'undefined') {
                            swal({
                                title: '{{ __('Are you sure you want to remove this file?') }}',
                                text: '{{ __('You will not be able to undo this delete operation!') }}',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: '{{ __('Yes, Delete!') }}',
                                cancelButtonText: '{{ __('Cancel') }}',
                            }).then( function () {
                                //remove file extension
                                var id = file.id;
                                $.ajax({
                                    type: 'DELETE',
                                    url: '{{ $delete_url }}',
                                    data: {id: id},
                                    dataType: 'html',
                                    success: function (data) {
                                        _this.removeFile(file);
                                        notify(
                                            '{{ __('Success!') }} ',
                                            '{{ __('File') }} ' + file.name + ' {{ __('removed successfully.') }}',
                                            'success'
                                        );
                                    },
                                    error: function (jqXHR, textStatus, errorThrow) {
                                        if (jqXHR.status != 404) {
                                            notify(
                                                '{{ __('Error!') }} ',
                                                '{{ __('Error while removing file') }} ' + file.name,
                                                'error'
                                            );
                                        } else {
                                            _this.removeFile(file);
                                        }
                                    }
                                });
                            });
                        } else {
                            _this.removeFile(file);
                        }
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });

                //add files to dropzone
                for ( var i in {{ camel_case($dropzone_id) }}_files ) {
                    var file = {{ camel_case($dropzone_id) }}_files[i];
                    file.accepted = true;

                    this.files.push(file);
                    this.emit('addedfile', file);
                    this.emit('thumbnail', file, file.thumb);
                    this.emit('complete', file);
                }
            }
        });

    });
</script>