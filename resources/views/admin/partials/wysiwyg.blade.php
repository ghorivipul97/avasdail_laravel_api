<?php
    $input_id = empty($input_id) ? '#content' : $input_id;
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({
        selector: '{{ $input_id }}',
        menubar: false,
        plugins: [
            'advlist autolink link lists charmap hr anchor pagebreak',
            'visualchars code nonbreaking',
            'paste contextmenu directionality textcolor image imagetools'
        ],
        toolbar: [
            'bold italic | bullist numlist blockquote | alignleft aligncenter alignright alignjustify |' +
            ' link unlink | image | ltr rtl | outdent indent | undo redo | styleselect fontsizeselect forecolor | code'
        ],
        contextmenu: 'link image',
        body_class: 'tinymce-editor',
        directionality : 'ltr',
        //content_css: '{{ asset('css/dv.css') }}',
        image_dimensions: false,
        @if( $image_upload_url )
        imagetools_cors_hosts: ['{{ url('/') }}'],
        images_upload_url: '{{ $image_upload_url or '' }}',
        images_upload_credentials: true,
        file_picker_types: 'image',
        automatic_uploads: true,
        paste_data_images: true,
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            // Note: In modern browsers input[type="file"] is functional without
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];

                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);

                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
            };

            input.click();
        }
        @endif
        /*setup: function(ed) {
            ed.on('keypress', function (e) {
                thaanaKeyboard.value = '';
                thaanaKeyboard.handleKey(e);
                ed.insertContent(thaanaKeyboard.value);
            });
        }*/
    });
</script>