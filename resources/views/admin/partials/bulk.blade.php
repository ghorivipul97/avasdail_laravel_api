@unless( empty($actions) )
    <div class="row">
        <div class="col-md-3">
            <div class="{{ add_error_class($errors->has('action')) }}">
                {!! Form::label('action', __('Bulk Action')) !!}
                {!! Form::select('action', ['' => ''] + $actions, '', ['class' => 'selectpicker form-control', 'required' => 'required']) !!}
                @include('errors._list', ['error' => $errors->get('action')])
                @include('errors._list', ['error' => $errors->get($model)])
            </div>
        </div>
        <div class="col-md-3">
            <div class="submit-group p-t-20">
                <button class="btn btn-warning m-r-10" type="submit">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
@endunless