<?php
    $alerts = session('alerts', []);
    if ( isset($errors) && !$errors->isEmpty() ) {
        $alerts[] = [
            'title' => __('Validation Errors!'),
            'text' => __('There are some errors in your input.'),
            'type' => 'danger',
        ];
    }
?>
@if ( !empty($alerts) )
    <script type="text/javascript">
        $(document).ready(function() {
            var alerts = {!! json_encode($alerts) !!};
            for ( var i in alerts ) {
                var alert = alerts[i];
                notify(alert.title + ' ', alert.text, alert.type);
            }
        });
    </script>
@endif