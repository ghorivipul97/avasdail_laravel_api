<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                @yield('panel-title')
                @yield('panel-actions')
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @yield('panel-content')
            </div>
        </div>
    </div>
</div>