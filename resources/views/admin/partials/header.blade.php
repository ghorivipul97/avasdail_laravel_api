<header id="header" class="clearfix" data-ma-theme="blue">
    <ul class="h-inner">
        <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="hi-logo hidden-xs">
            <a href="{{ route('admin.home') }}">
                <img src="{{ asset('img/header-logo.png') }}" alt="{{ config('APP_NAME') }}" />
                {{ ' Admin Portal' }}
            </a>
        </li>

        <li class="pull-right">
            <ul class="hi-menu">
                <li class="dropdown">
                    <a data-toggle="dropdown" href=""><i class="him-icon zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li><a href="#" data-redirect-url="{{ url('admin/login') }}" data-post-url="{{ url('admin/logout') }}"><i class="zmdi zmdi-time-restore"></i> {{ __('Log Out') }}</a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>

    @yield('top-search')
</header>
