<?php
    $type = isset($type) ? $type : 'image';
    $accept = $type == 'image' ? 'image/png, image/jpeg' :
        'application/vnd.ms-excel '.
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet '.
        'application/vnd.openxmlformats-officedocument.spreadsheetml.template '.
        'application/vnd.ms-excel.sheet.macroEnabled.12 '.
        'application/vnd.ms-excel.template.macroEnabled.12 '.
        'application/vnd.ms-excel.addin.macroEnabled.12 '.
        'application/vnd.ms-excel.sheet.binary.macroEnabled.12 '.
        'text/csv '.
        'application/vnd.ms-powerpoint '.
        'application/vnd.openxmlformats-officedocument.presentationml.presentation '.
        'application/vnd.openxmlformats-officedocument.presentationml.template '.
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow '.
        'application/vnd.ms-powerpoint.addin.macroEnabled.12 '.
        'application/vnd.ms-powerpoint.presentation.macroEnabled.12 '.
        'application/vnd.ms-powerpoint.template.macroEnabled.12 '.
        'application/vnd.ms-powerpoint.slideshow.macroEnabled.12 '.
        'application/vnd.openxmlformats-officedocument.presentationml.presentation '.
        'application/msword '.
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document '.
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template '.
        'application/vnd.ms-word.document.macroEnabled.12 '.
        'application/vnd.ms-word.template.macroEnabled.12 '.
        'text/plain ';
    $attribs = ['class' => 'form-control', 'accept' => $accept];
    if ( !empty($required) ) {
        $attribs['required'] = 'required';
    }
?>
<div class="fileinput {{ !empty($image) ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 150px;">
        @if( $type == 'image' && !empty($image) )
            <img src="{{ $image }}">
        @else
            {{ $image }}
        @endif
    </div>
    <div>
        <span class="btn btn-info btn-file">
            <span class="fileinput-new">{{ $type == 'image' ? __('Select image') : __('Select file') }}</span>
            <span class="fileinput-exists">{{ __('Change') }}</span>
            {!! Form::file($file_input_id, $attribs) !!}
        </span>
        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">{{ __('Remove') }}</a>
    </div>
</div>