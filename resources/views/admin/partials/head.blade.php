<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Avasdial - @yield('title', 'Admin Portal')</title>

    <!-- Material Admin Vendors -->
    <link href="{{ asset('material-admin/vendors/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">

    @unless( isset($hide_header) )
    <link href="{{ asset('material-admin/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/chosen/chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('material-admin/vendors/ajax-bootstrap-select/css/ajax-bootstrap-select.min.css') }}" rel="stylesheet">
    @endunless

    <!-- Material Admin CSS -->
    <link href="{{ asset('material-admin/css/material-admin.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('admin-assets/css/admin.css') }}" rel="stylesheet">
    <style>
        #sidebar .s-profile > a {
            background-image: url('{{ asset('img/admin-menu-background.png') }}');
        }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @stack('styles')
</head>