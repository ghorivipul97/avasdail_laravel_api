<?php
    $marker_lat = empty($marker_lat) ? get_setting('default_lat') : $marker_lat;
    $marker_lng = empty($marker_lng) ? get_setting('default_lng') : $marker_lng;
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{ env('MAP_API_KEY') }}"></script>
<script type="text/javascript">
    var map;
    var marker;
    var geocoder;
    //setting up map
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var myLatlng = new google.maps.LatLng(<?php echo $marker_lat.', '.$marker_lng; ?>);
        var mapOptions = {
            zoom: 14,
            center: myLatlng,
            disableDefaultUI: true,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        };
        map = new google.maps.Map(document.getElementById('location-map'), mapOptions);
        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        var custom_marker = {
            url: '{{ asset(get_setting('map_marker')) }}',
            size: new google.maps.Size(32, 48),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(16, 48)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var custom_shape = {
            coords: [14,43, 20,33, 24,25, 28,17, 28,0, 0,0, 0,17, 4,25, 8,33],
            type: 'poly'
        };

        marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: myLatlng,
            draggable: true,
            icon: custom_marker,
            shape: custom_shape,
            map: map
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('location-input');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var searchBox = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */(input),
                {
                    bounds: map.getBounds()
                }
        );

        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'place_changed', function() {
            var place = searchBox.getPlace();

            if (place == null) {
                return;
            }

            // Get place name, and location.
            var bounds = new google.maps.LatLngBounds();

            // Move marker to place.
            marker.setPosition(place.geometry.location);
            bounds.extend(place.geometry.location);
            map.fitBounds(bounds);
            map.setZoom(16);
            //update coordinates
            updateCordinates(marker.getPosition());
        });

        // Bias the SearchBox results towards places that are within the bounds of the
        // current map's viewport.
        google.maps.event.addListener(map, 'bounds_changed', function() {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });

        google.maps.event.addListener(marker, 'dragend', function(evt){
            updateCordinates(evt.latLng);
            codeLatLng(evt.latLng);
        });
        document.getElementById('lat').addEventListener('input', moveMarker);
        document.getElementById('lng').addEventListener('input', moveMarker);
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    //move marker when user changes text
    //function to move the maker
    function moveMarker() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if ( check_lat_lon(lat, lng) ) {
            marker.setPosition( new google.maps.LatLng( lat, lng ) );
            map.panTo( new google.maps.LatLng( lat, lng ) );
            codeLatLng(new google.maps.LatLng(lat, lng));
        }
    }

    //update the address
    function codeLatLng(latlng) {
        var input = document.getElementById('location-input');
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    input.value = results[0].formatted_address;
                } else {
                    input.value = "";
                }
            } else {
                input.value = "";
            }
        });
    }

    //update the coordinates
    function updateCordinates(latlng) {
        document.getElementById('lat').value = latlng.lat().toFixed(6);
        document.getElementById('lng').value = latlng.lng().toFixed(6);
    }

    //clear the coordinates
    function clearCordinates(e) {
        e.preventDefault();
        document.getElementById('lat').value = '';
        document.getElementById('lng').value = '';
    }

    //clear event
    document.getElementById('clear-coords').addEventListener('click', clearCordinates);
</script>