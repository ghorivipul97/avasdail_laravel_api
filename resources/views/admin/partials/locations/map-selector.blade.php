<div class="card">
    <div class="card-header">
        <h2>{{ __('Map') }}</h2>
    </div>
    <div class="card-body card-padding">
        <div class="{{ add_error_class($errors->has('lat'), 'form-group fg-line') }}">
            <?php $marker_lat = isset($lat) ? $lat : get_setting('default_lat'); ?>
            {!! Form::number('lat', old('lat'), ['class' => 'form-control', 'id' => 'lat', 'placeholder' => __('Latitude'),
            'step' => '0.000001', 'min' => -90, 'max' => 90]) !!}
            @include('errors._list', ['error' => $errors->get('lat')])
        </div>
        <div class="{{ add_error_class($errors->has('lng'), 'form-group fg-line') }}">
            <?php $marker_lng = isset($lng) ? $lng : get_setting('default_lng'); ?>
            {!! Form::number('lng', old('lng'), ['class' => 'form-control', 'id' => 'lng', 'placeholder' => __('Longitude'),
            'step' => '0.000001', 'min' => -90, 'max' => 90]) !!}
            @include('errors._list', ['error' => $errors->get('lng')])
        </div>
        <div class="form-group">
            <a href="#" class="btn btn-default" id="clear-coords">{{ __('Clear') }}</a>
        </div>
        <div class="form-group">
            <input id="location-input" class="material-controls" type="text" placeholder="{{ __('Search..') }}">
            <div id="location-map"></div>
        </div>
    </div>
</div>
@push('scripts')
    @include('admin.partials.locations.map-select-script')
@endpush