<aside id="sidebar" class="sidebar c-overflow">
    <div class="s-profile">
        <a href="" data-ma-action="profile-menu-toggle">
            <div class="sp-pic">
                <img src="{{ asset('img/user.png') }}" alt="{{ Auth::user()->name }}">
            </div>

            <div class="sp-info">
                {{ Auth::user()->name }}
                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>

        <ul class="main-menu">
{{--            <li><a href="{{ route('admin.users.profile') }}"><i class="zmdi zmdi-account"></i> View Profile</a></li>--}}
            <li><a href="#" data-redirect-url="{{ url('admin/login') }}" data-post-url="{{ url('admin/logout') }}"><i class="zmdi zmdi-time-restore"></i> {{ __('Log Out') }}</a></li>
        </ul>
    </div>

    <ul class="main-menu">
        <?php
            $c_nmspc = 'App\\Http\\Controllers\\Admin\\';
            $active_class = ' class="active"';
            //menu items
            $menu_items = [
                    ['controller' => 'Categories', 'name' => 'Categories', 'icon' => 'zmdi-local-offer'],
                    ['controller' => 'States', 'name' => 'States', 'icon' => 'zmdi-map'],
                    ['controller' => 'Cities', 'name' => 'Cities', 'icon' => 'zmdi-city'],
                    ['controller' => 'SignupQuestions', 'name' => 'Signup Questions', 'icon' => 'zmdi-pin-help'],
                    ['controller' => 'QuoteQuestions', 'name' => 'Quote Questions', 'icon' => 'zmdi-pin-help'],
                    ['controller' => 'Tags', 'name' => 'Tags', 'icon' => 'zmdi-tag'],
                    ['controller' => 'Settings', 'name' => 'General Settings', 'icon' => 'zmdi-settings'],
                    ['controller' => 'Feedbacks', 'name' => 'Feedback Data', 'icon' => 'zmdi-star'],
                    ['controller' => 'Reports', 'name' => 'Report Data', 'icon' => 'zmdi-file'],
                    ['controller' => 'SliderImages', 'name' => 'Slider Images', 'icon' => 'zmdi-image'],
                    ['controller' => 'ProviderQuestions', 'name' => 'Service Provider Questions', 'icon' => 'zmdi-pin-help'],
            ];
        ?>

        <li{!! if_route(['admin.home']) ? $active_class : '' !!}>
            <a href="{{ route('admin.home') }}"><i class="zmdi zmdi-home"></i> Home</a>
        </li>
        @foreach($menu_items as $menu_item)
            <li{!! current_controller() == $c_nmspc . $menu_item['controller'] . 'Controller' ? $active_class : '' !!}>
                <a href="{{ action('Admin\\'.$menu_item['controller'].'Controller@index') }}">
                    <i class="zmdi {{ $menu_item['icon'] }}"></i> {{ $menu_item['name'] }}
                </a>
            </li>
        @endforeach
    </ul>
</aside>