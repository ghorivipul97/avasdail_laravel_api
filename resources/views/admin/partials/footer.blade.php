@unless( isset($hide_footer) )
<footer id="footer">
    {{ 'Copyright &copy; '. Carbon\Carbon::now()->year .' ' . config('APP_NAME') }}
    - Developed by <a href="http://javaabu.com">Javaabu</a>
</footer>

{{--<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-blue">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p>Please wait...</p>
    </div>
</div>--}}
@endunless

<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{ asset('material-admin/img/browsers/chrome.png') }}" alt="">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{ asset('material-admin/img/browsers/firefox.png') }}" alt="">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{ asset('material-admin/img/browsers/opera.png') }}" alt="">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{ asset('material-admin/img/browsers/safari.png') }}" alt="">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{ asset('material-admin/img/browsers/ie.png') }}" alt="">
                    <div>IE (New)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->

<script src="{{ asset('material-admin/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/Waves/dist/waves.min.js') }}"></script>

@unless( isset($hide_footer) )
<!-- Material Admin Javascript Libraries -->
<script src="{{ asset('material-admin/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bootstrap-growl/bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('material-admin/vendors/fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/bower_components/autosize/dist/autosize.min.js') }}"></script>
<script src="{{ asset('material-admin/vendors/ajax-bootstrap-select/js/ajax-bootstrap-select.min.js') }}"></script>
@endunless


<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="{{ asset('material-admin/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js') }}"></script>
<![endif]-->

<!-- Material Admin JS -->
<script src="{{ asset('admin-assets/js/material-admin.js') }}"></script>

<!-- Custom JS -->
<script src="{{ asset('admin-assets/js/admin.js') }}"></script>

@unless( isset($hide_footer) )
    @include('admin.partials.alerts')
    @stack('scripts')
@endunless