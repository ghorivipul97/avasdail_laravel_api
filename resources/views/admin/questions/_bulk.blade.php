<?php
    $actions = [];
    
    if ( Auth::user()->can('update', $category) ) {
        $actions['approve'] = __('Approve');
        $actions['reject'] = __('Reject Posts');
        $actions['delete'] = __('Delete');
    }
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'questions'])