<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group fg-line') }}">
    <?php
        $value = isset($answer) ? $answer->content : old('answers.'.$question->id, []);
    ?>
    {!! Form::label('answers['.$question->id.'][]', $question->label) !!}
    @foreach($question->options as $option)
    <div class="checkbox">
        <label>
            <input type="checkbox" name="{{ 'answers['.$question->id.'][]' }}" value="{{ $option }}"
                {{ in_array($option, $value) ? 'checked' : '' }} />
            <i class="input-helper"></i>
            {{ $option }}
        </label>
    </div>
    @endforeach
    @if( $question->allow_other )
        <?php
            $other_values = array_diff($value, $question->options);
            $other_value = isset($other_values[0]) ? $other_values[0] : '';
            $other_id = 'answers_'.$question->id.'_other';
        ?>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="{{ 'answers['.$question->id.'_other]' }}" value="1"
                data-toggle-checkbox-enable="{{ '#'.$other_id }}" {{ !empty($other_value) ? 'checked' : '' }} />
                <i class="input-helper"></i>
                {{ __('Other') }}
            </label>
        </div>
        <input type="text" name="{{ 'answers['.$question->id.'][]' }}" value="{{ $other_value }}"
        class="jtk form-control other-input" id="{{ $other_id }}" placeholder="{{ __('Other') }}" />
    @endif
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>