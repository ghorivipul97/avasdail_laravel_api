<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group fg-line') }}">
    <?php
    $value = isset($answer) ? $answer->content : old('answers.' . $question->id);
    ?>
    {!! Form::label('answers['.$question->id.']', $question->label) !!}<br/>
    @include('admin.partials.file-input', [
                            'file_input_id' => 'answers['.$question->id.']',
                            'image' => $value,
                            'required' => $question->required,
                        ])
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>