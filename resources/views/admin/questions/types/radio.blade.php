<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group fg-line') }}">
    <?php
        $value = isset($answer) ? $answer->content : old('answers.'.$question->id);
    ?>
    {!! Form::label('answers['.$question->id.']', $question->label) !!}
    <?php $other_id = 'answers_'.$question->id.'_other'; ?>
    <div class="radio-buttons" {!! $question->allow_other ? 'data-other-input="#'.$other_id.'"' : '' !!}>
        @foreach($question->options as $option)
        <div class="radio">
            <label>
                <input type="radio" name="{{ 'answers['.$question->id.']' }}" value="{{ $option }}"
                        {{ $option == $value ? 'checked' : '' }} />
                <i class="input-helper"></i>
                {{ $option }}
            </label>
        </div>
        @endforeach
        @if( $question->allow_other )
            <?php $other_value = !in_array($value, $question->options) ? $value : ''; ?>
            <div class="radio">
                <label>
                    <input type="radio" name="{{ 'answers['.$question->id.']' }}" value=""
                           data-other="true" {{ !empty($other_value) ? 'checked' : '' }} />
                    <i class="input-helper"></i>
                    {{ __('Other') }}
                </label>
            </div>
            <input type="text" name="{{ 'answers['.$question->id.']' }}" value="{{ $other_value }}"
                   class="jtk form-control other-input" id="{{ $other_id }}" placeholder="{{ __('Other') }}" />
        @endif
    </div>
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>