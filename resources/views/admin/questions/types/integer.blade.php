<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group fg-line') }}">
    <?php
        $attribs = ['class' => 'form-control', 'step' => 1, 'placeholder' => '0'];
        $value = isset($answer) ? $answer->content : old('answers.'.$question->id);
        if ( $question->required ) {
            $attribs['required'] = 'required';
        }
    ?>
    {!! Form::label('answers['.$question->id.']', $question->label) !!}
    {!! Form::number('answers['.$question->id.']', $value, $attribs) !!}
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>