<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group fg-line') }}">
    <?php
        $attribs = ['class' => 'form-control', 'step' => 0.01, 'placeholder' => '0.00'];
        $value = isset($answer) ? $answer->content : old('answers.'.$question->id);
        if ( $question->required ) {
            $attribs['required'] = 'required';
        }
    ?>
    {!! Form::label('answers['.$question->id.']', $question->label) !!}
    {!! Form::number('answers['.$question->id.']', $value, $attribs) !!}
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>