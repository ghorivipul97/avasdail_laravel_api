<div class="{{ add_error_class($errors->has('answers.'.$question->id), 'form-group') }}">
    <?php
        $attribs = [
            'type' => 'date',
            'class' => 'form-control en-text date-time-picker',
            'data-toggle' => 'dropdown',
            'data-date-format' => 'YYYY-MM-DD',
            'placeholder' => __('YYYY-MM-DD'),
            'autocomplete' => 'off'];
        $value = isset($answer) ? $answer->content : old('answers.'.$question->id);
        if ( $question->required ) {
            $attribs['required'] = 'required';
        }
    ?>
    {!! Form::label('answers['.$question->id.']', $question->label) !!}
    <div class="input-group">
        <div class="dtp-container dropdown fg-line">
            {!! Form::text('answers['.$question->id.']', $value, $attribs) !!}
        </div>
        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
    </div>
    @include('errors._list', ['error' => $errors->get('answers.'.$question->id)])
</div>