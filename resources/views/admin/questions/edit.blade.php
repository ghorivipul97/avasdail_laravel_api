@extends('admin.questions.questions')

@section('tab-title', __('Edit Category Question'))

@section('tab-content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::model($question, ['method' => 'PATCH', 'files' => true, 'url' => $question->url('update')]) !!}
            @include('admin.questions._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection