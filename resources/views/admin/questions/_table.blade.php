<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\QuestionsController@bulk', $category], 'method' => 'PUT', 'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20 p-b-20">
        @include('admin.questions._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable">
            <thead>
            <tr>
                <th>
                    <div class="checkbox pull-left flip">
                        <label>
                            <input data-all="questions" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>
                <th>{{ __('Question') }}</th>
                <th>{{ __('Type') }}</th>
            </tr>
            </thead>
            <tbody class="sortable" data-sort-url="{{ action('Admin\\QuestionsController@sort', $category) }}">
            @include('admin.questions._list')
            </tbody>
        </table>
        @if( empty($no_pagination) )
        {{  $questions->links('admin.partials.pagination') }}
        @endif
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>