<ul class="actions">
    @if ( isset($question) )
        <li><a href="#" data-request-url="{{ $question->url('destroy') }}" data-redirect-url="{{ $question->url('index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endif

    <li><a href="{{ action('Admin\\QuestionsController@create', $category) }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>

    <li><a href="{{ action('Admin\\QuestionsController@index', $category) }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
</ul>