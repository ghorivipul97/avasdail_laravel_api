@foreach($questions as $question)
    <tr id="questions-{{ $question->id }}">
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="questions" name="questions[]" value="{{ $question->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td>{{ $question->title }}</td>
        <td>{{ $question->type_name }}</td>
        <td>
            <div class="pull-right flip">
                <ul class="actions">
                    @can('update', $category)
                        <li>
                            <a href="{{ $question->url('edit') }}" title="Edit">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#" data-request-url="{{ $question->url('destroy') }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete">
                                <i class="zmdi zmdi-delete"></i>
                            </a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach