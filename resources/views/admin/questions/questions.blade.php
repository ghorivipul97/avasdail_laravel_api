@extends('admin.categories.categories')

@section('page-title', $category->name)

@section('content')
    @include('admin.categories._tabs')

    <div class="block-header">
        <h2>@yield('tab-title', __('Category Questions'))</h2>
        @include('admin.questions._actions')
    </div>

    @yield('tab-content')
@endsection