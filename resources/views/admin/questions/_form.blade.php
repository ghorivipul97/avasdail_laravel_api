<div class="row">
    <div class="col-md-7">
        <div class="{{ add_error_class($errors->has('title'), 'form-group fg-line') }}">
            {!! Form::label('title', __('Title').' *') !!}
            {!! Form::text('title', old('title'), ['class' => 'jtk form-control',
            'placeholder' => __('Title'), 'required' => 'required']) !!}
            @include('errors._list', ['error' => $errors->get('title')])
        </div>
    </div>
    <div class="col-md-3">
        <div class="{{ add_error_class($errors->has('type'), 'form-group fg-line') }}">
            {!! Form::label('type', __('Type')) !!}
            {!! Form::select('type', \App\Helpers\Enums\QuestionTypes::getLabels(), old('type'),
            ['class' => 'form-control selectpicker', 'data-live-search' => true, 'required' => 'required' ]) !!}
            @include('errors._list', ['error' => $errors->get('type')])
        </div>
    </div>

    <div class="col-md-2">
        <div class="checkbox m-t-20">
            <label>
                {!! Form::checkbox('required', 1, isset($question) ? $question->required : old('required'), ['id' => 'required-0']) !!}
                <i class="input-helper"></i>
                {{ __('Required') }}
            </label>
        </div>
    </div>
</div>

@include('admin.questions.options._form')

<div class="form-group m-b-0">
    {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
    <a class="btn btn-default"
       href="{{ action('Admin\\QuestionsController@index', $category) }}">{{ __('Cancel') }}</a>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            var options_div = $('.options');

            $('#type').on('change', function (e) {
                var type = parseInt($(this).val());
                if ( $.inArray(type, {{ json_encode(\App\Helpers\Enums\QuestionTypes::mcqTypes()) }}) > -1 ) {
                    options_div.find('input').prop('disabled', false);
                    options_div.slideDown();
                } else {
                    options_div.find('input').prop('disabled', true);
                    options_div.slideUp();
                }
            }).trigger('change');
        });
    </script>
@endpush