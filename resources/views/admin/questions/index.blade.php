@extends('admin.questions.questions')

@section('page-title', $title)

@section('tab-content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\QuestionsController@index', $category], 'method' => 'GET']) !!}
            @include('admin.questions._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.questions._table')
    </div>
@endsection