@extends('admin.questions.questions')

@section('tab-title', __('New Category Question'))

@section('tab-content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\QuestionsController@store', $category], 'files' => true]) !!}
            @include('admin.questions._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection