<?php
use App\Helpers\Enums\QuestionTypes;
use App\Http\Controllers\Admin\QuestionsController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'),
                ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('type', __('Type')) !!}
            <?php
            $selected_type = Input::get('type', old('type'));
            $types = ['' => ''] + QuestionTypes::getLabels();

            ?>
            {!! Form::select('type', $types, $selected_type, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('orderby', __('Order by')) !!}
            <?php
            $selected_orderby = Input::get('orderby', old('orderby'));
            $orderbys = ['' => ''] + QuestionsController::getOrderbys();

            ?>
            {!! Form::select('orderby', $orderbys, $selected_orderby, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('order', __('Order')) !!}
            <?php
            $selected_order = Input::get('order', old('order'));
            $orders = ['' => ''] + QuestionsController::getOrders();

            ?>
            {!! Form::select('order', $orders, $selected_order, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\QuestionsController@index', $category) }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>