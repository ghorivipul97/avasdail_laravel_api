<?php
    $options = isset($question) ? $question->options : old('options', []);
?>
<div class="options">
    <p class="c-black f-500 m-b-5">{{ __('Options') }}</p>
    <div class="checkbox m-t-20">
        <label>
            {!! Form::checkbox('allow_other', 1, isset($question) ? $question->allow_other : old('allow_other'), ['id' => 'allow_other-0']) !!}
            <i class="input-helper"></i>
            {{ __('Allow Other') }}
        </label>
    </div>
    <div class="{{ add_error_class($errors->has('options'), 'form-group') }}">
        <div class="input-group">
            <div class="fg-line">
                {!! Form::text('new_option', '', ['class' => 'jtk form-control', 'id' => 'new-option', 'placeholder' => __('New Option'),
                'title' => __('Enter a value'), 'data-toggle' => 'tooltip', 'data-trigger' => 'manual' ]) !!}
            </div>
            <div class="input-group-addon">
                <a id="add-option" class="btn btn-primary" href="#">
                    <i class="zmdi zmdi-plus"></i>
                </a>
            </div>
        </div>
        @include('errors._list', ['error' => $errors->get('options')])
    </div>
    <div class="options-list sortable" data-deletable=".option">
    @if( is_array($options) )
        @foreach($options as $option)
            <div class="{{ add_error_class($errors->has('options.'.$loop->index), 'option bgm-white form-group') }}">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="btn btn-default"><i class="zmdi zmdi-swap-vertical"></i></span>
                    </div>
                    <div class="fg-line">
                        <input type="text" name="options[]" value="{{ $option }}" class="jtk form-control"
                        placeholder="{{ __('Option') }}" required="required" />
                    </div>
                    <div class="input-group-addon">
                        <a data-delete=".option" class="btn btn-danger" href="#"><i class="zmdi zmdi-close"></i></a>
                    </div>
                </div>
                @include('errors._list', ['error' => $errors->get('options.'.$loop->index)])
            </div>
        @endforeach
    @endif
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        var new_option_el = $('#new-option');

        $('#add-option').click(function (e) {

            e.preventDefault();

            var new_option = new_option_el.val();

            if ( new_option ) {
                $('.options-list').append(
                    '<div class="option form-group bgm-white">' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">' +
                                '<span class="btn btn-default"><i class="zmdi zmdi-swap-vertical"></i></span>' +
                            '</div>' +
                            '<div class="fg-line">' +
                                '<input name="options[]" class="form-control" type="text" value="' + new_option + '" required="required" />' +
                            '</div>' +
                            '<div class="input-group-addon">' +
                                '<a data-delete=".option" class="btn btn-danger" href="#"><i class="zmdi zmdi-close"></i></a>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
                new_option_el.val('');
                new_option_el.tooltip('hide');
                $('.options.sortable').sortable('refresh');
            } else {
                new_option_el.tooltip('show');
            }
        });
    });
</script>
@endpush