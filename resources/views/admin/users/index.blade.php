@extends('admin.users.users')

@section('content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\UsersController@index'], 'id' => 'filter', 'method' => 'GET']) !!}
            @include('admin.users._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.users._table')
    </div>
@endsection