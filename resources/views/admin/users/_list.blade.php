@foreach($users as $user)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="users" name="users[]" value="{{ $user->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td class="avatar"><img class="lgi-img" src="{{ $user->avatar }}"></td>
        <td>
            {{ $user->name }}
            @unless( $user->is_active )
                <strong class="status {{ $user->status_slug }}">
                    {{ '― '.$user->status_name }}
                </strong>
            @endunless
        </td>
        <td>{{ $user->username }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role_name }}</td>
        <td>{{ $user->mobile_number ?: '―' }}</td>
        <td>
            <div class="pull-right flip">
                <ul class="actions">
                    @can('update', $user)
                        <li>
                            <a href="{{ action('Admin\\UsersController@edit', $user) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan

                    @can('delete', $user)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\UsersController@destroy', $user) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach