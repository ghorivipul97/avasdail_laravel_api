<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\UsersController@bulk'], 'method' => 'PUT', 'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20">
        @include('admin.users._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable" data-form-sortable="#filter" data-select-all="users" >
            <thead>
            <tr>
                <th>
                    <div class="checkbox">
                        <label>
                            <input data-all="users" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>
                <th class="avatar"></th>
                <th data-sort-field="name" class="{{ add_sort_class('name') }}">{{ __('Name') }}</th>
                <th data-sort-field="username" class="{{ add_sort_class('username') }}">{{ __('Username') }}</th>
                <th data-sort-field="email" class="{{ add_sort_class('email') }}">{{ __('Email') }}</th>
                <th>{{ __('Role') }}</th>
                <th>{{ __('Phone') }}</th>
            </tr>
            </thead>
            <tbody>
            @include('admin.users._list')
            </tbody>
        </table>
        @if( empty($no_pagination) )
        {{  $users->links('admin.partials.pagination') }}
        @endif
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>