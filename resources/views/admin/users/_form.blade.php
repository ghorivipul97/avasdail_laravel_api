<div class="row">
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                <h2>{{ __('Details') }}</h2>
            </div>
            <div class="card-body card-padding p-b-30 p-t-10">
                <div class="row m-b-15">
                    <div class="col-md-8">
                        <div class="{{ add_error_class($errors->has('name'), 'form-group fg-line') }}">
                            {!! Form::label('name', __('Name').' *') !!}
                            {!! Form::text('name', old('name'), ['class' => 'jtk form-control', 'placeholder' => __('Name'),
                            'required' => 'required']) !!}
                            @include('errors._list', ['error' => $errors->get('name')])
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="{{ add_error_class($errors->has('username'), 'form-group fg-line') }}">
                            {!! Form::label('username', __('Username').' *') !!}
                            <?php
                            $attribs = ['class' => 'en-text form-control', 'placeholder' => __('Username'),];
                            if ( empty($user) ) {
                                $attribs['required'] = 'required';
                            } else {
                                $attribs['disabled'] = 'disabled';
                            }
                            ?>
                            {!! Form::text('username', old('username'), $attribs) !!}
                            @include('errors._list', ['error' => $errors->get('username')])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="{{ add_error_class($errors->has('email'), 'form-group fg-line') }}">
                            {!! Form::label('email', __('Email').' *') !!}
                            {!! Form::email('email', old('email'), ['class' => 'en-text form-control',
                                    'placeholder' => __('Email'), 'required' => 'required']) !!}
                            @include('errors._list', ['error' => $errors->get('email')])
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="{{ add_error_class($errors->has('mobile_number'), 'form-group') }}">
                            {!! Form::label('mobile_number', __('Mobile Number')) !!}
                            <?php
                                $attribs = ['class' => 'en-text form-control', 'placeholder' => __('Mobile Number')];
                                if ( !Auth::user()->can('assignMobileNumbers', App\User::class) ) {
                                    $attribs['disabled'] = 'disabled';
                                }
                            ?>
                            <div class="input-group">
                                <div class="input-group-addon p-l-0">{{ App\MobileNumber::defaultPrefix() }}</div>
                                <div class="fg-line">
                                    {!! Form::text('mobile_number', old('mobile_number'), $attribs) !!}
                                </div>
                            </div>
                            @include('errors._list', ['error' => $errors->get('mobile_number')])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <h2>{{ __('Avatar') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('avatar'), 'form-group m-b-0') }}">
                    @include('admin.partials.file-input', [
                                                'file_input_id' => 'avatar',
                                                'image' => isset($user) ? $user->getFirstMediaUrl('avatar') : null
                                            ])
                    @include('errors._list', ['error' => $errors->get('avatar')])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                <h2>{{ isset($user) ? __('Change Password') : __('Password') }}</h2>
            </div>

            <div class="card-body card-padding">
                @if( isset($user) )
                    <div class="{{ add_error_class($errors->has('current_password'), 'form-group fg-float') }}">
                        <div class="fg-line">
                            {!! Form::label('current_password', __('Current Password').' *', ['class' => 'fg-label']) !!}
                            {!! Form::password('current_password', ['class' => 'en-text form-control fg-input']) !!}
                        </div>
                        @include('errors._list', ['error' => $errors->get('current_password')])
                    </div>
                @endif

                <div class="{{ add_error_class($errors->has('password'), 'form-group fg-float') }}">
                    <?php
                    $pass_title = __('Password');
                    $attribs = ['class' => 'en-text form-control fg-input'];
                    if ( isset($user) ) {
                        $pass_title = __('New Password');
                    } else {
                        $attribs['required'] = 'required';
                    }
                    ?>
                    <div class="fg-line">
                        {!! Form::label('password', $pass_title, ['class' => 'fg-label']) !!}
                        {!! Form::password('password', $attribs) !!}
                    </div>
                    @include('errors._list', ['error' => $errors->get('password')])
                </div>

                <div class="{{ add_error_class($errors->has('password_confirmation'), 'form-group fg-float') }}">
                    <?php
                    $confirm_pass_title = __('Confirm :title', ['title' => $pass_title]);
                    $attribs = ['class' => 'form-control fg-input'];
                    if ( empty($user) ) {
                        $attribs['required'] = 'required';
                    }
                    ?>
                    <div class="fg-line">
                        {!! Form::label('password_confirmation', $confirm_pass_title, ['class' => 'fg-label']) !!}
                        {!! Form::password('password_confirmation', $attribs) !!}
                    </div>
                    @include('errors._list', ['error' => $errors->get('password_confirmation')])
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('role'), 'form-group fg-line') }}">
                    {!! Form::label('role', __('Role')) !!}
                    <?php
                    $attribs = ['class' => 'form-control selectpicker'];
                    if ( !Auth::user()->can('create', App\User::class) ) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('role', $roles, $selected_role, $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('role')])
                </div>

                <div class="{{ add_error_class($errors->has('status')) }}">
                    {!! Form::label('status', __('Status')) !!}
                    <?php
                    $attribs = ['class' => 'form-control selectpicker'];
                    if ( !Auth::user()->can('create', App\User::class) ) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('status', $user_statuses, $selected_user_status, $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('status')])
                </div>

                <div class="form-group">
                    {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                    <a class="btn btn-default" href="{{ action('Admin\\UsersController@index') }}">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //slugify
            $("#username").keypress(function (e) {
                return restrictCharacters($(this), e, /[A-Za-z0-9_]/g);
            });
        });
    </script>
@endpush