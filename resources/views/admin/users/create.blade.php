@extends('admin.users.users')

@section('page-title', __('New User'))

@section('content')
    {!! Form::open(['action' => 'Admin\\UsersController@store', 'files' => true]) !!}
    @include('admin.users._form')
    {!! Form::close() !!}
@endsection