<?php
use App\Http\Controllers\Admin\UsersController;use Illuminate\Support\Facades\Input;use Spatie\Permission\Models\Role;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'), ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('phone', __('Phone')) !!}
            <div class="fg-line">
                {!! Form::text('phone', Input::get('phone', old('phone')), ['class' => 'form-control', 'placeholder' => __('Phone')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('status', __('Status')) !!}
            <?php
            $selected_status = Input::get('status', old('status'));
            $statuses = ['' => ''] + \App\Helpers\Enums\UserStatuses::getLabels();

            ?>
            {!! Form::select('status', $statuses, $selected_status, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            <?php
            $roles = Role::orderby('name')->pluck('description', 'id');
            $selected_role = Input::get('role', old('role'));
            ?>
            {!! Form::label('role', __('Role')) !!}
            {!! Form::select('role', $roles->prepend('', ''), $selected_role,
            ['class' => add_error_class($errors->has('role'), 'selectpicker')]) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\UsersController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
    {!! Form::hidden('orderby', Input::get('orderby', old('orderby'))) !!}
    {!! Form::hidden('order',  Input::get('order', old('order'))) !!}
</div>