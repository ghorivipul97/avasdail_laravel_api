<?php
$actions = [];

if ( Auth::user()->can('create', App\User::class) ) {
    $actions['activate'] = __('Activate');
    $actions['ban'] = __('Ban');
}

if ( Auth::user()->can('deleteOwn', App\User::class) ) {
    $actions['delete'] = __('Delete');
}
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'users'])