@extends('layouts.admin')

@section('title', 'Users')
@section('page-title', __('Users'))

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\UsersController@index'])
@endsection

@section('model-actions')
    @include('admin.users._actions')
@endsection