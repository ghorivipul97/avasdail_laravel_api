<ul class="actions">
    @if ( isset($user) )
        @can('delete', $user)
        <li><a href="#" data-request-url="{{ action('Admin\\UsersController@destroy', $user) }}" data-redirect-url="{{ action('Admin\\UsersController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
        @endcan
    @endif

    @can('create', App\User::class)
    <li><a href="{{ action('Admin\\UsersController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', App\User::class)
    <li><a href="{{ action('Admin\\UsersController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>