@extends('admin.users.users')

@section('page-title', __('Edit User'))

@section('content')
    {!! Form::model($user, ['method' => 'PATCH', 'action' => ['Admin\\UsersController@update', $user], 'files' => true]) !!}
    @include('admin.users._form')
    {!! Form::close() !!}
@endsection