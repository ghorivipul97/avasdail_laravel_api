@extends('layouts.admin')

@section('page-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
@endsection

@section('title', 'Service Provider Questions')
@section('page-title', 'Service Provider Questions')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Service Provider Questions
                <a class="btn btn-primary pull-right" href="{{ url('admin/provider-questions/create') }}">Add New Question</a>
            </h2>
        </div>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <table class="table table-striped dataTable" id="example1">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Question</th>
                        <th>Is Active?</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('admin/provider-questions') }}'
                },
                columns: [
                    { data: 'DT_Row_Index', sortable: false, searchable: false},
                    { data: 'question', name: 'question'},
                    { data: 'is_required', name: 'is_required'},
                    { data: 'actions', name: 'actions', searchable: false, sortable: false}
                ]
            });
        })
    </script>
@endsection
