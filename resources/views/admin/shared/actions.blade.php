<ul class="actions">
    @if (isset($editUrl))
        <li>
            <a class="btn btn-sm btn-success" href="{{ $editUrl }}" title="Edit">
                <i class="zmdi zmdi-edit"></i>
            </a>
        </li>
    @endif

    @if (isset($deleteUrl))
        <li>
            {!! Form::open(['method' => 'DELETE', 'url' => $deleteUrl]) !!}
                <button class="btn btn-danger btn-sm" title="Delete" onclick="return alert('Are you sure you want to delete this record?');">
                    <i class="zmdi zmdi-delete"></i>
                </button>
            {!! Form::close() !!}
        </li>
    @endif
</ul>
