<div class="row">
    <div class="col-md-8">
        <div class="{{ add_error_class($errors->has('name'), 'form-group fg-line') }}">
            {!! Form::label('name', __('Name').' *') !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => __('Name'), 'required' => 'required']) !!}
            @include('errors._list', ['error' => $errors->get('name')])
        </div>
        <div class="{{ add_error_class($errors->has('slug'), 'form-group fg-line') }}">
            {!! Form::label('slug', __('Slug').' *') !!}
            {!! Form::text('slug', old('slug'), ['class' => 'form-control en-text', 'placeholder' => __('Slug'), 'required' => 'required']) !!}
            @include('errors._list', ['error' => $errors->get('slug')])
        </div>
        <div class="{{ add_error_class($errors->has('parent_id'), 'form-group fg-line') }}">
            {!! Form::label('parent_id', __('Parent')) !!}
            {!! Form::select('parent_id', isset($roots) ? ['' => __('')] + $roots : [], old('parent_id'),
            ['class' => 'form-control selectpicker', 'data-live-search' => true ]) !!}
            @include('errors._list', ['error' => $errors->get('parent_id')])
        </div>
    </div>
    <div class="col-md-4">
        <div class="{{ add_error_class($errors->has('category_image'), 'form-group fg-line') }}">
            {!! Form::label('category_image', 'Image') !!}<br/>
            @include('admin.partials.file-input', [
                                    'file_input_id' => 'category_image',
                                    'image' => isset($category) ? $category->getFirstMediaUrl('category_image') : null
                                ])
            @include('errors._list', ['error' => $errors->get('category_image')])
        </div>
    </div>
</div>


<div class="form-group">
    <a class="btn btn-primary" href="{{ action('Admin\\CategoriesController@index') }}">Cancel</a>
    {!! Form::button('Save', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //slugify
            $('#slug').keypress(function (e) {
                return restrictCharacters($(this), e, /[-A-Za-z0-9]/g);
            });

            @unless( isset($category) )
            $('#name').keyup(function(){
                var str = $(this).val();
                $('#slug').val(slugify(str));
            });
            @endunless
        });
    </script>
@endpush