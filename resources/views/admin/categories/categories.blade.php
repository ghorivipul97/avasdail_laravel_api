@extends('layouts.admin')

@section('title', 'Categories')
@section('page-title', __('Categories'))

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\CategoriesController@index'])
@endsection

@section('model-actions')
    @include('admin.categories._actions')
@endsection