<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\CategoriesController@bulk'], 'method' => 'PUT',
        'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20 p-b-20">
        @include('admin.categories._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable">
            <thead>
            <tr>
                <th>
                    <div class="checkbox pull-left flip">
                        <label>
                            <input data-all="categories" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Slug') }}</th>
            </tr>
            </thead>
            <tbody>
            @include('admin.categories._list')
            </tbody>
        </table>
        @if( empty($no_pagination) )
            {{  $categories->links('admin.partials.pagination') }}
        @endif
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>