<ul class="actions">
    @if (isset($editUrl))
        <li>
            <a href="{{ $editUrl }}" title="Edit" class="btn btn-success">
                <i class="zmdi zmdi-edit"></i>
            </a>
        </li>
    @endif

    @if (isset($deleteUrl))
        <li>
            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.categories.destroy', $deleteUrl]]) !!}
                <button title="Delete" class="btn btn-danger">
                    <i class="zmdi zmdi-delete"></i>
                </button>
            {!! Form::close() !!}
        </li>
    @endif

    @if(isset($subcategoryUrl))
        @if($subcategoryUrl != 0)
            @if(isset($deleteUrl))
                <li>
                    <a class="btn btn-primary" href="{{ route('admin.subcategory', $deleteUrl) }}">
                        Sub Categories
                    </a>
                </li>
            @endif
        @endif
    @endif
</ul>
