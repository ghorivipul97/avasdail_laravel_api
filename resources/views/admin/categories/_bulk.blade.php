<?php
$actions = [];

if ( Auth::user()->can('delete_categories') ) {
    $actions['delete'] = __('Delete');
}

?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'categories'])