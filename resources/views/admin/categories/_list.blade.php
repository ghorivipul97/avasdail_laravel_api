@foreach($categories as $category)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="categories" name="categories[]" value="{{ $category->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td>{{ $category->depth_name }}</td>
        <td>{{ $category->slug }}</td>
        <td>
            <div class="pull-right flip">
                <ul class="actions">
                    @can('update', $category)
                        <li>
                            <a href="{{ action('Admin\\CategoriesController@edit', $category) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan

                    @can('delete', $category)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\CategoriesController@destroy', $category) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach