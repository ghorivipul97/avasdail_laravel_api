<?php
    $active_class = 'class="active"';
    $c_nmspc = 'App\\Http\\Controllers\\Admin\\';
?>
<div class="card">
    <div class="card-body">
        <ul class="tab-nav tn-justified">
            <li {!! active_class(if_controller($c_nmspc . 'CategoriesController'), $active_class) !!}>
                <a href="{{ action('Admin\\CategoriesController@edit', $category) }}">{{ __('Details') }}</a>
            </li>
            <li {!! active_class(if_controller($c_nmspc . 'QuestionsController'), $active_class) !!}>
                <a href="{{ action('Admin\\QuestionsController@index', $category) }}">{{ __('Questions') }}</a>
            </li>
        </ul>
    </div>
</div>