@extends('layouts.admin')

@section('title', 'Categories')
@section('page-title', 'Categories')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit Category - {{ $category->name }}</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($category, ['method' => 'PATCH', 'files' => true, 'url' => [url('admin/categories/' . $category->id)]]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('name', 'Name'.' *') !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => __('Name'), 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('parent_id', 'Parent') !!}
                            {!! Form::select('parent_id', isset($roots) ? [null =>'Select Parent Category'] + $roots->toArray() : null, old('parent_id'),
                            ['class' => 'form-control selectpicker']) !!}
                            <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('image', 'Image') !!}
                            {!! Form::file('image') !!}
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('icon', 'Icon') !!}
                            {!! Form::file('icon') !!}
                            <span class="text-danger">{{ $errors->first('icon') }}</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <a class="btn btn-primary" href="{{ url('admin/categories') }}">Cancel</a>
                    {!! Form::button('Edit Category', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection