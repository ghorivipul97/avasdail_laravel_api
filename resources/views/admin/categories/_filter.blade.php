<?php
use App\Http\Controllers\Admin\CategoriesController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'), ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\CategoriesController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>