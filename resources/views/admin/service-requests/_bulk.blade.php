<?php
    $actions = [];

    if ( Auth::user()->can('deleteOwn', App\ServiceRequest::class) ) {
        $actions['delete'] = __('Delete');
    }
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'service_requests'])