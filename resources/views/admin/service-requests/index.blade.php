@extends('admin.service-requests.service-requests')

@section('page-title', $title)

@section('content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\ServiceRequestsController@index'], 'method' => 'GET']) !!}
            @include('admin.service-requests._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.service-requests._table')
    </div>
@endsection