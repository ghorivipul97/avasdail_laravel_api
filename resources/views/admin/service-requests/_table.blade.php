<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\ServiceRequestsController@bulk'], 'method' => 'PUT', 'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20 p-b-20">
        @include('admin.service-requests._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable">
            <thead>
            <tr>
                <th>
                    <div class="checkbox pull-left flip">
                        <label>
                            <input data-all="service_requests" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>
                <th>{{ __('Name') }}</th>
            </tr>
            </thead>
            <tbody>
            @include('admin.service-requests._list')
            </tbody>
        </table>
        @if( empty($no_pagination) )
        {{  $service_requests->links('admin.partials.pagination') }}
        @endif
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>