<?php
use App\Http\Controllers\Admin\ServiceRequestsController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'),
                ['class' => 'form-control', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('orderby', __('Order by')) !!}
            <?php
            $selected_orderby = Input::get('orderby', old('orderby'));
            $orderbys = ['' => ''] + ServiceRequestsController::getOrderbys();

            ?>
            {!! Form::select('orderby', $orderbys, $selected_orderby, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('order', __('Order')) !!}
            <?php
            $selected_order = Input::get('order', old('order'));
            $orders = ['' => ''] + ServiceRequestsController::getOrders();

            ?>
            {!! Form::select('order', $orders, $selected_order, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\ServiceRequestsController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>