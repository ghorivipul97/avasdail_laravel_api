<ul class="actions">
    @if ( isset($service_request) ) @can('delete', $service_request)
    <li><a href="#" data-request-url="{{ action('Admin\\ServiceRequestsController@destroy', $service_request) }}" data-redirect-url="{{ action('Admin\\ServiceRequestsController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endcan @endif

    @can('create', App\ServiceRequest::class)
    <li><a href="{{ action('Admin\\ServiceRequestsController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', App\ServiceRequest::class)
    <li><a href="{{ action('Admin\\ServiceRequestsController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>