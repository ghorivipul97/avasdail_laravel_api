@extends('layouts.admin')

@section('title', 'ServiceRequests')
@section('page-title', 'ServiceRequests')

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\ServiceRequestsController@index'])
@endsection

@section('model-actions')
    @include('admin.service-requests._actions')
@endsection