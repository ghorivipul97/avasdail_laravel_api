<div class="row">
    <div class="col-md-9">
        <div class="card">
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('category'), 'form-group') }}">
                    <?php
                    $selected_category = $category->id;
                    $categories = App\Category::whereId($selected_category)->pluck('name', 'id')->all();
                    ?>
                    {!! Form::label('category', 'Category') !!}
                    {!! Form::select('category', $categories, $selected_category,
                    ['class' => 'selectpicker-ajax jtk-select form-control', 'required' => 'required', 'disabled' => 'disabled',
                    'data-name-field' => 'depth_name', 'data-url' => action('Api\\CategoriesController@index')]) !!}
                    {!! Form::hidden('category', $selected_category) !!}
                    @include('errors._list', ['error' => $errors->get('category')])
                </div>

                <div class="{{ add_error_class($errors->has('note'), 'form-group fg-line') }}">
                    {!! Form::label('note', 'Note') !!}
                    {!! Form::textarea('note', old('note'), ['class' => 'form-control auto-size', 'placeholder' => 'Note']) !!}
                    @include('errors._list', ['error' => $errors->get('note')])
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <h2>{{ __('Approve') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('business'), 'form-group') }}">
                    {!! Form::label('business', __('Business')) !!}
                    <?php
                    $selected_business = isset($service_request) ? $service_request->business_id : old('business');
                    $attribs = ['class' => 'selectpicker-ajax jtk-select form-control',
                        'data-url' => action('Api\\BusinessesController@index')];

                    if ( isset($service_request) ) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('business', App\Business::whereId($selected_business)->pluck('name', 'id'), $selected_business, $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('business')])
                </div>

                @can('editOthers', App\ServiceRequest::class)
                    <div class="{{ add_error_class($errors->has('user'), 'form-group') }}">
                        {!! Form::label('user', __('User')) !!}
                        <?php
                        $selected_user = isset($service_request) ? $service_request->user_id : old('user', Auth::user()->id);
                        $attribs = ['class' => 'selectpicker-ajax jtk-select form-control', 'data-url' => action('Api\\UsersController@index')];

                        if ( isset($service_request) ) {
                            $attribs['disabled'] = 'disabled';
                        }
                        ?>
                        {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user, $attribs) !!}
                        @include('errors._list', ['error' => $errors->get('user')])
                    </div>
                @endcan

                <div class="{{ add_error_class($errors->has('status'), 'form-group') }}">
                    {!! Form::label('status', __('Status')) !!}
                    <?php
                    $attribs = ['class' => 'form-control selectpicker'];
                    if (!Auth::user()->can('approve', App\ServiceRequest::class)) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('status', \App\Helpers\Enums\ServiceRequestStatuses::getLabels(), old('status'), $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('status')])
                </div>

                <div class="form-group m-b-0">
                    <?php
                    $approve_btn_text = Auth::user()->can('approve', App\ServiceRequest::class) ? __('Approve') : __('Send for Approval');
                    ?>
                    {!! Form::button($approve_btn_text, ['class' => 'btn btn-primary', 'type' => 'submit', 'name' => 'approve', 'value' => '1']) !!}
                    {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                    <a class="btn btn-default"
                       href="{{ action('Admin\\ServiceRequestsController@index') }}">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h2>{{ __('Businesses') }}</h2>
    </div>
    <div class="card-body card-padding">
        <div class="{{ add_error_class($errors->has('businesses'), 'form-group') }}">
            <?php
            $selected_businesses = isset($service_request) ? $service_request->businesses()->pluck('id')->all() : old('businesses', []);
            $businesses = App\Business::whereIn('id', $selected_businesses)->pluck('name', 'id')->all();
            ?>
            {!! Form::select('businesses[]', $businesses, $selected_businesses,
            ['class' => 'selectpicker-ajax jtk-select form-control', 'multiple' => 'multiple',
            'data-url' => action('Api\\BusinessesController@index')]) !!}
            @include('errors._list', ['error' => $errors->get('businesses')])
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h2>{{ __('Questions') }}</h2>
    </div>
    <div class="card-body card-padding">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('skip_questions', 1, old('skip_questions'), ['id' => 'skip_questions']) !!}
                    <i class="input-helper"></i>
                    {{ __('Skip Questions') }}
                </label>
            </div>
            @include('errors._list', ['error' => $errors->get('skip_questions')])
        </div>
        <div class="row" id="questions">
            @foreach($questions->chunk( ceil($questions->count() / 2) ) as $chuncks)
                <div class="col-md-6">
                    @foreach($chuncks as $question)
                        <?php
                            $answer = isset($service_request) ? $service_request->getAnswer($question->id) : null;
                        ?>
                        @include('admin.questions.types.'.$question->type_slug, compact('answer'))
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            var questions_div = $('#questions');

            //checkbox enable toggle
            $('#skip_questions').change(function(){
                if ( $(this).is(':checked') ) {
                    questions_div.slideUp();
                    questions_div.find('input:not(.other-input), textarea, select').prop('disabled', true);
                } else {
                    questions_div.slideDown();
                    questions_div.find('input:not(.other-input), textarea, select').prop('disabled', false);
                }
            }).trigger('change');
        });
    </script>
@endpush