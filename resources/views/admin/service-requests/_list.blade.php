@foreach($service_requests as $service_request)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="service_requests" name="service_requests[]" value="{{ $service_request->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td>{{ $service_request->name }}</td>
        <td>
            <div class="pull-right flip">
                <ul class="actions">
                    @can('view', $service_request)
                        <li>
                            <a href="{{ $service_request->permalink }}" target="_blank" title="View">
                                <i class="zmdi zmdi-open-in-new"></i>
                            </a>
                        </li>
                    @endcan

                    @can('update', $service_request)
                        <li>
                            <a href="{{ action('Admin\\ServiceRequestsController@edit', $service_request) }}" title="Edit">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </li>
                    @endcan

                    @can('delete', $service_request)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\ServiceRequestsController@destroy', $service_request) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete">
                                <i class="zmdi zmdi-delete"></i>
                            </a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach