@extends('admin.service-requests.service-requests')

@section('page-title', __('New Service Request'))

@section('content')
    @if( empty($category) )
        {!! Form::open(['action' => 'Admin\\ServiceRequestsController@create', 'method' => 'GET']) !!}
        @include('admin.service-requests._category')
        {!! Form::close() !!}
    @else
        {!! Form::open(['action' => 'Admin\\ServiceRequestsController@store', 'files' => true]) !!}
        @include('admin.service-requests._form')
        {!! Form::close() !!}
    @endif
@endsection