<div class="card">
    <div class="card-header">
        <h2>{{ __('Choose Category') }}</h2>
    </div>
    <div class="card-body card-padding">
        <div class="{{ add_error_class($errors->has('category'), 'form-group') }}">
            <?php
            $selected_category = old('category');
            $categories = App\Category::whereId($selected_category)->pluck('name', 'id')->all();
            ?>
            {!! Form::select('category', $categories, $selected_category,
            ['class' => 'selectpicker-ajax jtk-select form-control', 'data-name-field' => 'depth_name',
            'required' => 'required', 'data-url' => action('Api\\CategoriesController@index')]) !!}
            @include('errors._list', ['error' => $errors->get('category')])
        </div>

        <div class="form-group">
            <a class="btn btn-primary" href="{{ action('Admin\\ServiceRequestsController@index') }}">Cancel</a>
            {!! Form::button('Go', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
        </div>
    </div>
</div>