@extends('admin.service-requests.service-requests')

@section('page-title', __('Edit ServiceRequest'))

@section('content')
    {!! Form::model($service_request, ['method' => 'PATCH', 'files' => true,
    'action' => ['Admin\\ServiceRequestsController@update', $service_request]]) !!}
    @include('admin.service-requests._form')
    {!! Form::close() !!}
@endsection