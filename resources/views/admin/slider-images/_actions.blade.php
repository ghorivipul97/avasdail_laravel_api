<ul class="actions">
    @if (isset($deleteUrl))
        <li>
            {!! Form::open(['method' => 'DELETE', 'url' => $deleteUrl]) !!}
                <button title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?');">
                    <i class="zmdi zmdi-delete"></i>
                </button>
            {!! Form::close() !!}
        </li>
    @endif
</ul>
