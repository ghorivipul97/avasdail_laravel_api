@extends('layouts.admin')

@section('title', 'Add Image')
@section('page-title', 'Add Image')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>New Category</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::open(['url' => url('/admin/category-slider-images'), 'method' => 'post', 'files' => true]) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('image', 'Image') !!}
                        {!! Form::file('image') !!}
                        <span class="text-danger">{{ $errors->first('image') }}</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <a class="btn btn-primary" href="{{ url('admin/category-slider-images') }}">Cancel</a>
                {!! Form::button('Add Image', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
