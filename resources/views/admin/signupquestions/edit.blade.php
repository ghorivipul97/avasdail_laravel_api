@extends('layouts.admin')

@section('title', 'Signup Questions')
@section('page-title', 'Signup Questions')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit Signup Question</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($question, ['method' => 'PATCH', 'url' => url('admin/signup_questions/' . $question->getKey())]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('question', 'Question *') !!}
                            {!! Form::text('question', old('question'), ['class' => 'form-control jtk', 'placeholder' => 'Question', 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('question') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a class="btn btn-primary" href="{{ url('admin/singup_questions') }}">Cancel</a>
                            {!! Form::button('Edit Question', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection