@extends('admin.reviews.reviews')

@section('page-title', __('Edit Review'))

@section('content')
    {!! Form::model($review, ['method' => 'PATCH', 'files' => true, 'action' => ['Admin\\ReviewsController@update', $review]]) !!}
    @include('admin.reviews._form')
    {!! Form::close() !!}
@endsection