<?php
use App\Http\Controllers\Admin\ReviewsController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'),
                ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('status', __('Status')) !!}
            <?php
            $selected_status = Input::get('status', old('status'));
            $statuses = ['' => ''] + \App\Helpers\Enums\ReviewStatuses::getLabels();

            ?>
            {!! Form::select('status', $statuses, $selected_status, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('business', __('Business')) !!}
            <?php
            $selected_business = Input::get('business', old('business'));
            ?>
            {!! Form::select('business', App\Business::whereId($selected_business)->pluck('name', 'id'), $selected_business,
            ['class' => 'selectpicker-ajax form-control', 'data-url' => action('Api\\BusinessesController@index')]) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('user', __('User')) !!}
            <?php
            $selected_user = Input::get('user', old('user'));
            ?>
            {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user,
            ['class' => 'selectpicker-ajax form-control', 'data-url' => action('Api\\UsersController@index')]) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('orderby', __('Order by')) !!}
            <?php
            $selected_orderby = Input::get('orderby', old('orderby'));
            $orderbys = ['' => ''] + ReviewsController::getOrderbys();

            ?>
            {!! Form::select('orderby', $orderbys, $selected_orderby, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('order', __('Order')) !!}
            <?php
            $selected_order = Input::get('order', old('order'));
            $orders = ['' => ''] + ReviewsController::getOrders();

            ?>
            {!! Form::select('order', $orders, $selected_order, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\ReviewsController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>