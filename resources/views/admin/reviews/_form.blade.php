<div class="row">
    <div class="col-md-9">

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Details') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('rating')) }}">
                    {!! Form::label('rating', __('Rating')) !!}
                    <div class="m-t-5">
                    @for($i = 0; $i <= 5; $i++)
                        <label class="radio radio-inline m-r-20">
                            {!! Form::radio('rating', $i, old('rating')) !!}
                            <i class="input-helper"></i>
                            {{ $i }}
                        </label>
                    @endfor
                    </div>
                    @include('errors._list', ['error' => $errors->get('rating')])
                </div>
                <div class="{{ add_error_class($errors->has('content'), 'form-group fg-line') }}">
                    {!! Form::label('content', __('Content')) !!}
                    {!! Form::textarea('content', old('content'), ['class' => 'form-control',
                    'placeholder' => __('Content'), 'rows' => 8, 'id' => 'content-input']) !!}
                    @include('errors._list', ['error' => $errors->get('content')])
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-3">

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Approve') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('business'), 'form-group') }}">
                    {!! Form::label('business', __('Business')) !!}
                    <?php
                    $selected_business = isset($review) ? $review->business_id : old('business');
                    $attribs = ['class' => 'selectpicker-ajax jtk-select form-control', 'require' => 'required',
                        'data-url' => action('Api\\BusinessesController@index')];

                    if ( isset($review) ) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('business', App\Business::whereId($selected_business)->pluck('name', 'id'), $selected_business, $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('business')])
                </div>
                
                @can('editOthers', App\Review::class)
                    <div class="{{ add_error_class($errors->has('user'), 'form-group') }}">
                        {!! Form::label('user', __('User')) !!}
                        <?php
                        $selected_user = isset($review) ? $review->user_id : old('user', Auth::user()->id);
                        $attribs = ['class' => 'selectpicker-ajax jtk-select form-control', 'data-url' => action('Api\\UsersController@index')];

                        if ( isset($review) ) {
                            $attribs['disabled'] = 'disabled';
                        }
                        ?>
                        {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user, $attribs) !!}
                        @include('errors._list', ['error' => $errors->get('user')])
                    </div>
                @endcan

                <div class="{{ add_error_class($errors->has('status'), 'form-group') }}">
                    {!! Form::label('status', __('Status')) !!}
                    <?php
                    $attribs = ['class' => 'form-control selectpicker'];
                    if (!Auth::user()->can('approve', App\Review::class)) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('status', \App\Helpers\Enums\ReviewStatuses::getLabels(), old('status'), $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('status')])
                </div>

                <div class="form-group m-b-0">
                    <?php
                    $approve_btn_text = Auth::user()->can('approve', App\Review::class) ? __('Approve') : __('Send for Approval');
                    ?>
                    {!! Form::button($approve_btn_text, ['class' => 'btn btn-primary', 'type' => 'submit', 'name' => 'approve', 'value' => '1']) !!}
                    {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                    <a class="btn btn-default"
                       href="{{ action('Admin\\ReviewsController@index') }}">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endpush