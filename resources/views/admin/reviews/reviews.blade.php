@extends('layouts.admin')

@section('title', 'Reviews')
@section('page-title', 'Reviews')

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\ReviewsController@index'])
@endsection

@section('model-actions')
    @include('admin.reviews._actions')
@endsection