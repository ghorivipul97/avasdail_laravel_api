@extends('admin.reviews.reviews')

@section('page-title', $title)

@section('content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\ReviewsController@index'], 'method' => 'GET']) !!}
            @include('admin.reviews._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.reviews._table')
    </div>
@endsection