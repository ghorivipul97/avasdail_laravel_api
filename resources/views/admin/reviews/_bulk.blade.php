<?php
$actions = [];

if ( Auth::user()->can('approve', App\Review::class) ) {
    $actions['approve'] = __('Approve');
    $actions['reject'] = __('Reject');
} else {
    $actions['approve'] = __('Send for Approval');
    $actions['reject'] = __('Set as Draft');
}

if ( Auth::user()->can('deleteOwn', App\Review::class) ) {
    $actions['delete'] = __('Delete');
}
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'reviews'])
