@extends('admin.reviews.reviews')

@section('page-title', __('New Review'))

@section('content')
    {!! Form::open(['action' => 'Admin\\ReviewsController@store', 'files' => true]) !!}
    @include('admin.reviews._form')
    {!! Form::close() !!}
@endsection