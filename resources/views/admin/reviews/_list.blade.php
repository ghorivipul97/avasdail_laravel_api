@foreach($reviews as $review)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="reviews" name="reviews[]" value="{{ $review->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td class="bigger">{{ $review->rating }} {!! $review->rating_stars !!}</td>
        <td>
            @unless( $review->is_approved )
                <strong class="status {{ $review->status_slug }}">
                    {{ $review->status_name.' ―' }}
                </strong><br/>
            @endunless
            {!! nl2br(e($review->content)) !!}
        </td>
        <td>{!! $review->admin_author_link !!}</td>
        <td>{!! $review->admin_business_link !!}</td>
        <td>{!! $review->created_at->format('d/m/Y H:i') !!}</td>
        <td>
            <div class="pull-right">
                <ul class="actions">
                    @can('delete', $review)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\ReviewsController@destroy', $review) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan

                    @can('update', $review)
                        <li>
                            <a href="{{ action('Admin\\ReviewsController@edit', $review) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach