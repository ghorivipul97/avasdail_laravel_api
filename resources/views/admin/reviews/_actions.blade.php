<ul class="actions">
    @if ( isset($review) ) @can('delete', $review)
    <li><a href="#" data-request-url="{{ action('Admin\\ReviewsController@destroy', $review) }}" data-redirect-url="{{ action('Admin\\ReviewsController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endcan @endif

    @can('create', App\Review::class)
    <li><a href="{{ action('Admin\\ReviewsController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', App\Review::class)
    <li><a href="{{ action('Admin\\ReviewsController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>