<div class="card">
    <div class="card-header">
        <h2>{{ __('Meta') }}</h2>
    </div>
    <div class="card-body card-padding">
        <div class="{{ add_error_class($errors->has('address'), 'form-group fg-float') }}">
            <div class="fg-line">
                {!! Form::label('address', __('Address'), ['class' => 'fg-label']) !!}
                {!! Form::textarea('address', old('address'), ['rows' => 2, 'class' => 'form-control fg-input auto-size jtk']) !!}
            </div>
            @include('errors._list', ['error' => $errors->get('address')])
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="{{ add_error_class($errors->has('state'), 'form-group') }}">
                    {!! Form::label('state', __('Atoll')) !!}
                    <?php
                    $selected_state = isset($business) && $business->city ? $business->city->state_id : old('state');
                    $states = App\State::orderBy('name', 'ASC')->get()->pluck('formatted_name', 'id');
                    ?>
                    {!! Form::select('state', $states->prepend('', ''), $selected_state,
                    ['class' => 'selectpicker form-control', 'data-live-search' => 'true',
                    'data-child' => '#city', 'data-filter-field' => 'state'] ) !!}
                    @include('errors._list', ['error' => $errors->get('state')])
                </div>
            </div>
            <div class="col-md-6">
                <div class="{{ add_error_class($errors->has('city'), 'form-group') }}">
                    {!! Form::label('city', __('Island')) !!}
                    <?php
                    $selected_city = isset($business) ? $business->city_id : old('city');
                    $cities = App\City::whereStateId($selected_state)->get()->pluck('formatted_name', 'id');
                    ?>
                    {!! Form::select('city', $cities->prepend('', ''), $selected_city,
                    ['class' => 'selectpicker form-control', 'data-name-field' => 'formatted_name',
                    'data-live-search' => 'true', 'data-url' => action('Api\\CitiesController@index')] ) !!}
                    @include('errors._list', ['error' => $errors->get('city')])
                </div>
            </div>
        </div>
        <div class="{{ add_error_class($errors->has('phone'), 'form-group fg-float') }}">
            <div class="fg-line">
                {!! Form::label('phone', __('Phone'), ['class' => 'fg-label']) !!}
                {!! Form::text('phone', old('phone'), ['class' => 'form-control fg-input en-text']) !!}
            </div>
            @include('errors._list', ['error' => $errors->get('phone')])
        </div>
        <div class="{{ add_error_class($errors->has('email'), 'form-group fg-float') }}">
            <div class="fg-line">
                {!! Form::label('email', __('Email'), ['class' => 'fg-label']) !!}
                {!! Form::email('email', old('email'), ['class' => 'form-control sans-serif fg-input en-text']) !!}
            </div>
            @include('errors._list', ['error' => $errors->get('email')])
        </div>
        <div class="{{ add_error_class($errors->has('website'), 'form-group fg-float') }}">
            <div class="fg-line">
                {!! Form::label('website', __('Website'), ['class' => 'fg-label']) !!}
                {!! Form::url('website', old('website'), ['class' => 'form-control sans-serif fg-input en-text']) !!}
            </div>
            @include('errors._list', ['error' => $errors->get('website')])
        </div>
    </div>
</div>