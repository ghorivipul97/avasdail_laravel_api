@extends('admin.businesses.businesses')

@section('page-title', $title)

@section('content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\BusinessesController@index'], 'method' => 'GET']) !!}
            @include('admin.businesses._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.businesses._table')
    </div>
@endsection