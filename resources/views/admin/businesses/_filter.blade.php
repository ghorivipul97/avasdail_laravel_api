<?php
use App\Http\Controllers\Admin\BusinessesController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'),
                ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('status', __('Status')) !!}
            <?php
            $selected_status = Input::get('status', old('status'));
            $statuses = ['' => ''] + \App\Helpers\Enums\BusinessStatuses::getLabels();

            ?>
            {!! Form::select('status', $statuses, $selected_status, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('category', __('Category')) !!}
            <?php
            $selected_category = Input::get('category', old('category'));
            ?>
            {!! Form::select('category', App\Category::whereId($selected_category)->pluck('name', 'id'), $selected_category,
            ['class' => 'selectpicker-ajax form-control', 'data-name-field' => 'depth_name',
             'data-url' => action('Api\\CategoriesController@index')]) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('user', __('Owner')) !!}
            <?php
            $selected_user = Input::get('user', old('user'));
            ?>
            {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user,
            ['class' => 'selectpicker-ajax form-control', 'data-url' => action('Api\\UsersController@index')]) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('state', __('Atoll')) !!}
            <?php
            $selected_state = Input::get('state', old('state'));
            $states = App\State::orderBy('name', 'ASC')->get()->pluck('formatted_name', 'id');
            ?>
            {!! Form::select('state', $states->prepend('', ''), $selected_state,
            ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'data-child' => '#city', 'data-filter-field' => 'state'] ) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('city', __('Island')) !!}
            <?php
            $selected_city = Input::get('city', old('city'));
            $cities = App\City::whereStateId($selected_state ? $selected_state : 0)->get()->pluck('formatted_name', 'id');
            ?>
            {!! Form::select('city', $cities->prepend('', ''), $selected_city,
            ['class' => 'selectpicker form-control', 'data-name-field' => 'formatted_name',
            'data-live-search' => 'true', 'data-url' => action('Api\\CitiesController@index')] ) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('orderby', __('Order by')) !!}
            <?php
            $selected_orderby = Input::get('orderby', old('orderby'));
            $orderbys = ['' => ''] + BusinessesController::getOrderbys();

            ?>
            {!! Form::select('orderby', $orderbys, $selected_orderby, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('order', __('Order')) !!}
            <?php
            $selected_order = Input::get('order', old('order'));
            $orders = ['' => ''] + BusinessesController::getOrders();

            ?>
            {!! Form::select('order', $orders, $selected_order, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\BusinessesController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>