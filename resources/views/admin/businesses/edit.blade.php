@extends('admin.businesses.businesses')

@section('page-title', __('Edit Business'))

@section('content')
    {!! Form::model($business, ['method' => 'PATCH', 'files' => true, 'action' => ['Admin\\BusinessesController@update', $business]]) !!}
    @include('admin.businesses._form')
    {!! Form::close() !!}
@endsection