@foreach($businesses as $business)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="businesses" name="businesses[]" value="{{ $business->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td class="avatar"><img class="lgi-img" src="{{ $business->logo }}"></td>
        <td>
            {{ $business->name }}
            @unless( $business->is_approved )
                <strong class="status {{ $business->status_slug }}">
                    {{ '― '.$business->status_name }}
                </strong>
            @endunless
        </td>
        <td>{{ $business->slug }}</td>
        <td>{!! $business->admin_author_link !!}</td>
        <td>{{ $business->city ? $business->city->formatted_name : '-' }}</td>
        <td>
            <div class="pull-right">
                <ul class="actions">
                    @can('delete', $business)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\BusinessesController@destroy', $business) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan

                    @can('update', $business)
                        <li>
                            <a href="{{ action('Admin\\BusinessesController@edit', $business) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach