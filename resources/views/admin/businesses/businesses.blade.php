@extends('layouts.admin')

@section('title', 'Businesses')
@section('page-title', 'Businesses')

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\BusinessesController@index'])
@endsection

@section('model-actions')
    @include('admin.businesses._actions')
@endsection