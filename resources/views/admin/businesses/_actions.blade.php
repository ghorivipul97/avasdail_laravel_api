<ul class="actions">
    @if ( isset($business) ) @can('delete', $business)
    <li><a href="#" data-request-url="{{ action('Admin\\BusinessesController@destroy', $business) }}" data-redirect-url="{{ action('Admin\\BusinessesController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endcan @endif

    @can('create', App\Business::class)
    <li><a href="{{ action('Admin\\BusinessesController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', App\Business::class)
    <li><a href="{{ action('Admin\\BusinessesController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>