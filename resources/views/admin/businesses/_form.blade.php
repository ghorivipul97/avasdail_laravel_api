<div class="card">
    <div class="card-body card-padding form-horizontal">
        <div class="row">
            {!! Form::label('name', __('Name').' *', ['class' => 'control-label col-lg-1 col-md-2']) !!}
            <div class="col-lg-7 col-md-6">
                <div class="fg-line">
                    {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => __('Name'), 'required' => 'required']) !!}
                </div>
                @include('errors._list', ['error' => $errors->get('name')])
            </div>
            {!! Form::label('slug', __('Slug').' *', ['class' => 'control-label col-lg-1 col-md-2']) !!}
            <div class="col-lg-3 col-md-2">
                <div class="fg-line">
                    <?php
                        $attribs = ['class' => 'form-control jtk', 'placeholder' => __('Slug'), 'required' => 'required'];
                        if ( isset($business) && !Auth::user()->can('editOthers', App\Business::class) ) {
                            $attribs['disabled'] = 'disabled';
                        }
                    ?>
                    {!! Form::text('slug', old('slug'), $attribs) !!}
                </div>
                @include('errors._list', ['error' => $errors->get('slug')])
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9">

        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h2>{{ __('Description') }}</h2>
                    </div>
                    <div class="card-body card-padding">
                        <div class="{{ add_error_class($errors->has('description'), 'form-group fg-line') }}">
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control',
                            'placeholder' => __('Description'), 'rows' => 16, 'id' => 'description']) !!}
                            @include('errors._list', ['error' => $errors->get('description')])
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                @include('admin.businesses._meta')
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Service Categories') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('categories'), 'form-group') }}">
                    <?php
                    $selected_categories = isset($business) ? $business->categories()->pluck('id')->all() : old('categories', []);
                    $categories = App\Category::whereIn('id', $selected_categories)->pluck('name', 'id')->all();
                    ?>
                    {!! Form::select('categories[]', $categories, $selected_categories,
                    ['class' => 'selectpicker-ajax jtk-select form-control', 'multiple' => 'multiple', 'data-name-field' => 'depth_name',
                    'data-url' => action('Api\\CategoriesController@index')]) !!}
                    @include('errors._list', ['error' => $errors->get('categories')])
                </div>
            </div>
        </div>

        @if( isset($business) )
            <div class="card">
                <div class="card-header">
                    <h2>{{ __('Images') }} <small>{{ __('Drop images here to upload') }}</small></h2>
                </div>
                <div class="card-body card-padding">
                    <div id="image-drop" class="dropzone">
                    </div>
                </div>
            </div>
        @endif

    </div>

    <div class="col-md-3">

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Approve') }}</h2>
            </div>
            <div class="card-body card-padding">
                @can('editOthers', App\Business::class)
                    <div class="{{ add_error_class($errors->has('user'), 'form-group') }}">
                        {!! Form::label('user', __('User')) !!}
                        <?php
                        $selected_user = isset($business) ? $business->user_id : old('user', Auth::user()->id);
                        ?>
                        {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user,
                        ['class' => 'selectpicker-ajax jtk-select form-control', 'data-url' => action('Api\\UsersController@index')]) !!}
                        @include('errors._list', ['error' => $errors->get('user')])
                    </div>
                @endcan

                <div class="{{ add_error_class($errors->has('status'), 'form-group') }}">
                    {!! Form::label('status', __('Status')) !!}
                    <?php
                    $attribs = ['class' => 'form-control selectpicker'];
                    if (!Auth::user()->can('approve', App\Business::class)) {
                        $attribs['disabled'] = 'disabled';
                    }
                    ?>
                    {!! Form::select('status', \App\Helpers\Enums\BusinessStatuses::getLabels(), old('status'), $attribs) !!}
                    @include('errors._list', ['error' => $errors->get('status')])
                </div>

                <div class="form-group m-b-0">
                    <?php
                    $approve_btn_text = Auth::user()->can('approve', App\Business::class) ? __('Approve') : __('Send for Approval');
                    ?>
                    {!! Form::button($approve_btn_text, ['class' => 'btn btn-primary', 'type' => 'submit', 'name' => 'approve', 'value' => '1']) !!}
                    {!! Form::button(__('Save'), ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                    <a class="btn btn-default"
                       href="{{ action('Admin\\BusinessesController@index') }}">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Logo') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('logo')) }}">
                    @include('admin.partials.file-input', [
                        'file_input_id' => 'logo',
                        'image' => isset($business) ? $business->getFirstMediaUrl('logo') : null
                    ])
                    @include('errors._list', ['error' => $errors->get('logo')])
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //slugify
            $("#slug").keypress(function (e) {
                return restrictCharacters($(this), e, /[A-Za-z0-9-]/g);
            });

            @unless( isset($business) )
            $('#name').keyup(function(){
                var str = $(this).val();
                $('#slug').val(slugify(str));
            });
            @endunless
        });
    </script>

    @if( isset($business) )
        @include('admin.partials.dropzone', [
            'dropzone_id' => 'image-drop',
            'upload_url' => action('Api\\BusinessesController@upload', $business),
            'delete_url' => action('Api\\BusinessesController@uploadDestroy', $business),
            'files' => $business->getFiles('images'),
        ])
    @endif
@endpush