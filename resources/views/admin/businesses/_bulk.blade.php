<?php
$actions = [];

if ( Auth::user()->can('approve', App\Business::class) ) {
    $actions['approve'] = __('Approve');
    $actions['reject'] = __('Reject');
} else {
    $actions['approve'] = __('Send for Approval');
    $actions['reject'] = __('Deactivate');
}

if ( Auth::user()->can('deleteOwn', App\Business::class) ) {
    $actions['delete'] = __('Delete');
}
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'businesses'])
