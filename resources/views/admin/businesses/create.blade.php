@extends('admin.businesses.businesses')

@section('page-title', __('New Business'))

@section('content')
    {!! Form::open(['action' => 'Admin\\BusinessesController@store', 'files' => true]) !!}
    @include('admin.businesses._form')
    {!! Form::close() !!}
@endsection