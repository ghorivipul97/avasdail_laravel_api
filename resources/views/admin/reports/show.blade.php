@extends('layouts.admin')

@section('title', 'Report')
@section('page-title', 'Report')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Report from - {{ $report->username }}</h2>
        </div>

        <div class="card-body card-padding">
            <div class="row">
                <div class="col-md-8 form-group">
                    <div>
                        {!! Form::label('name', 'Username') !!}
                        <p>{{ $report->username }}</p>
                    </div>
                    <div>
                        {!! Form::label('email', 'Email') !!}
                        <p>{{ $report->email }}</p>
                    </div>
                    <div>
                        {!! Form::label('number', 'phone Number') !!}
                        <p>{{ $report->phone_number }}</p>
                    </div>
                    <div>
                        {!! Form::label('description', 'Description') !!}
                        <p>{{ $report->description }}</p>
                    </div>

                    @if($report->files)
                        <div>
                            {!! Form::label('files', 'Files') !!}<br>
                            @foreach(explode(',', $report->files) as $image)
                                <img src="{{ asset('uploads/screenshots/'.$image) }}" width="300px" height="300px">
                            @endforeach
                        </div>
                    @endif

                </div>
            </div>

            <div class="form-group">
                <a class="btn btn-primary" href="{{ url('admin/report') }}">Back</a>
            </div>
        </div>
    </div>
@endsection