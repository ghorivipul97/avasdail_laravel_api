@extends('layouts.admin')

@section('title', 'Quote Questions')
@section('page-title', 'Quote Questions')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Edit Quote Question <small>{{ $question->name }}</small></h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::model($question, ['method' => 'PATCH', 'url' => url('admin/quote_questions/' . $question->id)]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('question', 'Question *') !!}
                            {!! Form::text('question', old('question'), ['class' => 'form-control jtk', 'placeholder' => 'Question', 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('question') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::checkbox('is_required', 'on', old('is_required', $question->is_required) ? true : false, ['id' => 'is_required']) !!}
                            {!! Form::label('is_required', 'Is Required?') !!}
                            <span class="text-danger">{{ $errors->first('is_required') }}</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <a class="btn btn-primary" href="{{ url('admin/quote_questions') }}">Cancel</a>
                    {!! Form::button('Edit Question', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection