<ul class="actions">
    @if ( isset($oauth_client) ) @can('delete', $oauth_client)
    <li><a href="#" data-request-url="{{ action('Admin\\OAuthClientsController@destroy', $oauth_client) }}" data-redirect-url="{{ action('Admin\\OAuthClientsController@index') }}" class="delete-link" title="Delete"><i class="zmdi zmdi-delete"></i></a></li>
    @endcan @endif

    @can('create', Laravel\Passport\Client::class)
    <li><a href="{{ action('Admin\\OAuthClientsController@create') }}" title="Add New"><i class="zmdi zmdi-plus"></i></a></li>
    @endcan

    @can('index', Laravel\Passport\Client::class)
    <li><a href="{{ action('Admin\\OAuthClientsController@index') }}" title="List All"><i class="zmdi zmdi-view-list-alt"></i></a></li>
    @endcan
</ul>