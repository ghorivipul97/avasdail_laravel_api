@extends('admin.oauth-clients.oauth-clients')

@section('page-title', __('Edit OAuth2 Client'))

@section('content')
    {!! Form::model($oauth_client, ['method' => 'PATCH', 'files' => true, 'action' => ['Admin\\OAuthClientsController@update', $oauth_client]]) !!}
    @include('admin.oauth-clients._form')
    {!! Form::close() !!}
@endsection