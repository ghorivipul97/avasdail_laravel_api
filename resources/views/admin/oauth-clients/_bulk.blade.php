<?php
$actions = [];

if ( Auth::user()->can('editOwn', Laravel\Passport\Client::class) ) {
    $actions['approve'] = __('Approve');
    $actions['revoke'] = __('Revoke');
}

if ( Auth::user()->can('deleteOwn', Laravel\Passport\Client::class) ) {
    $actions['delete'] = __('Delete');
}
?>

@include('admin.partials.bulk', ['actions' => $actions, 'model' => 'oauth_clients'])
