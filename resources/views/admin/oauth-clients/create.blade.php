@extends('admin.oauth-clients.oauth-clients')

@section('page-title', __('New OAuth2 Client'))

@section('content')
    {!! Form::open(['action' => 'Admin\\OAuthClientsController@store', 'files' => true]) !!}
    @include('admin.oauth-clients._form')
    {!! Form::close() !!}
@endsection