<?php
use App\Http\Controllers\Admin\OAuthClientsController;
use Illuminate\Support\Facades\Input;
?>
<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('search', __('Search')) !!}
            <div class="fg-line">
                {!! Form::text('search', isset($search) ? $search : old('search'),
                ['class' => 'form-control jtk', 'placeholder' => __('Search..')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('status', __('Status')) !!}
            <?php
            $selected_status = Input::get('status', old('status'));
            $statuses = ['' => ''] + \App\Helpers\Enums\OAuthClientStatuses::getLabels();

            ?>
            {!! Form::select('status', $statuses, $selected_status, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('user', __('User')) !!}
            <?php
            $selected_user = Input::get('user', old('user'));
            ?>
            {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id'), $selected_user,
            ['class' => 'selectpicker-ajax form-control', 'data-url' => action('Api\\UsersController@index')]) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('personal_access', __('Personal Access')) !!}
            <?php
            $selected_val = Input::get('personal_access', old('personal_access'));
            $yes_no = [
                '' => '',
                '1' => __('Yes'),
                '0' => __('No'),
            ];

            ?>
            {!! Form::select('personal_access', $yes_no, $selected_val, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('password', __('Password')) !!}
            <?php
            $selected_val = Input::get('password', old('password'));
            ?>
            {!! Form::select('password', $yes_no, $selected_val, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('orderby', __('Order by')) !!}
            <?php
            $selected_orderby = Input::get('orderby', old('orderby'));
            $orderbys = ['' => ''] + OAuthClientsController::getOrderbys();

            ?>
            {!! Form::select('orderby', $orderbys, $selected_orderby, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3 col-lg-2">
        <div class="form-group">
            {!! Form::label('order', __('Order')) !!}
            <?php
            $selected_order = Input::get('order', old('order'));
            $orders = ['' => ''] + OAuthClientsController::getOrders();

            ?>
            {!! Form::select('order', $orders, $selected_order, ['class' => 'form-control selectpicker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="submit-group p-t-20">
            <button class="btn btn-success m-r-10" type="submit">{{ __('Filter') }}</button>
            <a href="{{ action('Admin\\OAuthClientsController@index') }}" class="btn btn-default">{{ __('Clear') }}</a>
        </div>
    </div>
</div>