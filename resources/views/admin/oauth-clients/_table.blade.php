<div class="table-responsive">
    @if( empty($no_bulk) )
    {!! Form::open(['action' => ['Admin\\OAuthClientsController@bulk'], 'method' => 'PUT',
        'class' => 'delete-form']) !!}
    <div class="p-l-30 p-r-30 p-t-20 p-b-20">
        @include('admin.oauth-clients._bulk')
    </div>
    @endif
    <div class="dataTables_wrapper">
        <table class="table table-striped dataTable">
            <thead>
            <tr>
                <th>
                    <div class="checkbox pull-left flip">
                        <label>
                            <input data-all="oauth_clients" value="1" type="checkbox" />
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('User') }}</th>
                <th>{{ __('Personal Access') }}</th>
                <th>{{ __('Password') }}</th>
            </tr>
            </thead>
            <tbody>
            @include('admin.oauth-clients._list')
            </tbody>
        </table>
        @if( empty($no_pagination) )
        {{  $oauth_clients->links('admin.partials.pagination') }}
        @endif
    </div>
    @if( empty($no_bulk) )
    {!! Form::close() !!}
    @endif
</div>