@extends('layouts.admin')

@section('title', 'OAuth2 Clients')
@section('page-title', 'OAuthClients')

@section('top-search')
    @include('admin.partials.search-model', ['search_action' => 'Admin\\OAuthClientsController@index'])
@endsection

@section('model-actions')
    @include('admin.oauth-clients._actions')
@endsection