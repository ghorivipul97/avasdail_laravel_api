<div class="row">
    <div class="col-md-8">

        <div class="card">
            <div class="card-header">
                <h2>{{ __('Details') }}</h2>
            </div>
            <div class="card-body card-padding">
                <div class="{{ add_error_class($errors->has('name'), 'form-group fg-float') }}">
                    <div class="fg-line">
                        {!! Form::label('name', __('Name') . ' *', ['class' => 'fg-label']) !!}
                        {!! Form::text('name', old('name'), ['required' => 'required', 'class' => 'form-control fg-input']) !!}
                    </div>
                    @include('errors._list', ['error' => $errors->get('name')])
                </div>
                <div class="{{ add_error_class($errors->has('redirect'), 'form-group fg-float') }}">
                    <div class="fg-line">
                        {!! Form::label('redirect', __('Redirect'), ['class' => 'fg-label']) !!}
                        {!! Form::text('redirect', old('redirect'), ['class' => 'form-control fg-input']) !!}
                    </div>
                    @include('errors._list', ['error' => $errors->get('redirect')])
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body card-padding">
                @if( isset($oauth_client) )
                    <div class="form-horizontal">
                        <div class="form-group">
                            {!! Form::label('client_id', __('Client ID'), ['class' => 'control-label col-lg-2 col-md-3']) !!}
                            <div class="col-lg-10 col-md-9">
                                <div class="fg-line">
                                    <span class="form-control fg-input">{{ $oauth_client->id }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('client_secret', __('Client Secret'), ['class' => 'control-label col-lg-2 col-md-3']) !!}
                            <div class="col-lg-10 col-md-9">
                                <div class="fg-line">
                                    <span class="form-control fg-input">{{ $oauth_client->secret }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-offset-2 col-md-offset-3 col-md-4">
                        <?php
                        $attribs = [];
                        $disabled = isset($oauth_client) ? 'disabled' : '';
                        if ( $disabled ) {
                            $attribs[$disabled] = $disabled;
                        }
                        ?>
                        <div class="checkbox {{ $disabled }}">
                            <label>
                                {!! Form::checkbox('personal_access_client', 1, old('personal_access_client'), $attribs) !!}
                                <i class="input-helper"></i>
                                {{ __('Personal Access Client') }}
                            </label>
                        </div>
                        @include('errors._list', ['error' => $errors->get('personal_access_client')])
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox {{ $disabled }}">
                            <label>
                                {!! Form::checkbox('password_client', 1, old('password_client'), $attribs) !!}
                                <i class="input-helper"></i>
                                {{ __('Password Client') }}
                            </label>
                        </div>
                        @include('errors._list', ['error' => $errors->get('password_client')])
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h2>{{ __('Approve') }}</h2>
            </div>
            <div class="card-body card-padding">
                @can('editOthers', Laravel\Passport\Client::class)
                    <div class="{{ add_error_class($errors->has('user'), 'form-group') }}">
                        {!! Form::label('user', __('User')) !!}
                        <?php
                        $selected_user = isset($oauth_client) ? $oauth_client->user_id : old('user');
                        $attribs = ['class' => 'selectpicker-ajax jtk-select form-control', 'data-url' => action('Api\\UsersController@index')];
                        if ( isset($oauth_client) ) {
                            $attribs['disabled'] = 'disabled';
                        }
                        ?>
                        {!! Form::select('user', App\User::whereId($selected_user)->pluck('name', 'id')->prepend('', ''), $selected_user, $attribs) !!}
                        @include('errors._list', ['error' => $errors->get('user')])
                    </div>
                @endcan

                <div class="form-group m-b-0">
                    @if( isset($oauth_client) )
                    <?php
                        $approve_btn_text = $oauth_client->revoked ? __('Approve') : __('Revoke');
                        $approve_btn_context = $oauth_client->revoked ? 'btn-primary' : 'btn-danger';
                    ?>
                    {!! Form::button($approve_btn_text, ['class' => 'm-b-5 btn '.$approve_btn_context, 'type' => 'submit',
                    'name' => 'revoke', 'value' => !$oauth_client->revoked]) !!}
                    {!! Form::button(__('Regenerate Client Secret'), ['class' => 'm-b-5 btn btn-warning', 'type' => 'submit',
                    'name' => 'regenerate', 'value' => '1']) !!}
                    @endif
                    {!! Form::button(__('Save'), ['class' => 'm-b-5 btn btn-success', 'type' => 'submit']) !!}
                    <a class="btn btn-default m-b-5"
                       href="{{ action('Admin\\OAuthClientsController@index') }}">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endpush