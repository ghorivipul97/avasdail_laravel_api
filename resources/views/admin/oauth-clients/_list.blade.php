@foreach($oauth_clients as $oauth_client)
    <tr>
        <td>
            <div class="checkbox pull-left flip">
                <label>
                    <input data-check="oauth_clients" name="oauth_clients[]" value="{{ $oauth_client->id }}" type="checkbox" />
                    <i class="input-helper"></i>
                </label>
            </div>
        </td>
        <td>
            {{ $oauth_client->name }}
            @if( $oauth_client->revoked )
                <strong class="status revoked">
                    {{ '― Revoked' }}
                </strong>
            @endif
        </td>
        <td>
            @if( $oauth_client->user_id )
                <?php $user = App\User::find($oauth_client->user_id); ?>
                @if( $user )
                {!! $user->admin_author_link !!}
                @endif
            @endif
        </td>
        <td><i class="zmdi zmdi-{{ $oauth_client->personal_access_client ? 'check' : 'close' }}"></i></td>
        <td><i class="zmdi zmdi-{{ $oauth_client->password_client ? 'check' : 'close' }}"></i></td>
        <td>
            <div class="pull-right">
                <ul class="actions">
                    @can('delete', $oauth_client)
                        <li>
                            <a href="#" data-request-url="{{ action('Admin\\OAuthClientsController@destroy', $oauth_client) }}"
                               data-redirect-url="{{ Request::fullUrl() }}" class="delete-link" title="Delete"><i
                                        class="zmdi zmdi-delete"></i></a>
                        </li>
                    @endcan

                    @can('update', $oauth_client)
                        <li>
                            <a href="{{ action('Admin\\OAuthClientsController@edit', $oauth_client) }}" title="Edit"><i
                                        class="zmdi zmdi-edit"></i></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </td>
    </tr>
@endforeach