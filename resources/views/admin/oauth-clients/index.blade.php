@extends('admin.oauth-clients.oauth-clients')

@section('page-title', $title)

@section('content')
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['action' => ['Admin\\OAuthClientsController@index'], 'method' => 'GET']) !!}
            @include('admin.oauth-clients._filter')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        @include('admin.oauth-clients._table')
    </div>
@endsection