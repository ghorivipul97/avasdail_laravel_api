@extends('layouts.admin')

@section('title', 'Tags')
@section('page-title', 'Tags')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>New Tag</h2>
        </div>

        <div class="card-body card-padding">
            {!! Form::open(['url' => url('admin/tags')]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('name', 'Name *') !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control jtk', 'placeholder' => 'Name', 'required' => 'required']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <a class="btn btn-primary" href="{{ url('admin/tags') }}">Cancel</a>
                    {!! Form::button('Add Tag', ['class' => 'btn btn-success', 'type' => 'submit']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
