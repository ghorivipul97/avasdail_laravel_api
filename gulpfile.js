const elixir = require('laravel-elixir');

//require('laravel-elixir-vue');
require('es6-promise').polyfill();

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    //Admin
    mix.sass('admin/admin.scss', 'public/admin-assets/css/admin.css');
    mix.scripts(['admin/admin.js'], 'public/admin-assets/js/admin.js');
    mix.scripts(['admin/material-admin.js'], 'public/admin-assets/js/material-admin.js');

    //copy images
    mix.copy('resources/assets/img', 'public/img');
});
