<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->regexify('[a-z0-9_]{4,6}'),
        'status' => $faker->randomElement(\App\Helpers\Enums\UserStatuses::getKeys()),
        'password' => $password ?: $password = '123456',
        'remember_token' => str_random(10),
    ];
});

$factory->define(Spatie\Permission\Models\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'description' => $faker->text(20),
    ];
});

$factory->define(\App\State::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->state,
        'code' => $faker->stateAbbr,
        'abbr' => $faker->stateAbbr,
        'type' => $faker->randomElement(\App\Helpers\Enums\StateTypes::getKeys()),
    ];
});

$factory->define(\App\City::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city,
        'code' => $faker->regexify('[A-Z]{1}[0-9]{1,2}'),
        'type' => $faker->randomElement(\App\Helpers\Enums\CityTypes::getKeys()),
        'state_id' => random_id_or_generate(\App\State::class),
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
    ];
});

$factory->define(\App\Category::class, function (Faker\Generator $faker) {
    $faker->addProvider(new \App\Helpers\Faker\Service($faker));
    return [
        'name' => $faker->unique()->categoryName,
    ];
});

$factory->define(App\Business::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'description' => $faker->text(500),
        'status' => $faker->randomElement(\App\Helpers\Enums\BusinessStatuses::getKeys()),
        'city_id' => random_id_or_generate(\App\City::class),
        'user_id' => random_id_or_generate(\App\User::class),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url,
        'email' => $faker->email,
    ];
});

$factory->define(App\ServiceRequest::class, function (Faker\Generator $faker) {
    return [
        'skip_questions' => $faker->boolean,
        'note' => $faker->text(200),
        'status' => $faker->randomElement(\App\Helpers\Enums\ServiceRequestStatuses::getKeys()),
        'user_id' => random_id_or_generate(\App\User::class),
        'category_id' => random_id_or_generate(\App\Category::class),
        'accepted_at' => null,
        'business_id' => null,
    ];
});
