<?php

use App\Models\QuoteQuestion;
use Illuminate\Database\Seeder;

class QuoteQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 10) as $item)
        {
            QuoteQuestion::create([
                'question' => $faker->sentence
            ]);
        }
    }
}
