<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $subCategories = \App\Category::where('parent_id', '!=', null)->get();

        foreach ($subCategories as $subCategory) {
            foreach (range(1, 5) as $index) {
                \App\Question::create([
                    'category_id' => $subCategory->id,
                    'title' => $faker->sentence(),
                    'options' => implode(',', [$faker->word, $faker->word, $faker->word, $faker->word])
                ]);
            }
        }
    }
}
