<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    protected $data = [
        Role::USER => 'User',
        Role::SERVICE_PROVIDER => 'Service Provider'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $name => $description) {
            $role = Role::firstOrCreate([
                'name' => $name,
                'description' => $description
            ]);

            $role->save();
        }
    }
}
