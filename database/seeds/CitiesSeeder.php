<?php

use App\City;
use App\Helpers\Enums\CityTypes;
use App\Helpers\Enums\StateTypes;
use App\State;
use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $state_data) {
            $state = State::whereCode($state_data['code'])->first();
            
            if ( $state ) {
                $state->update([
                    'name' => $state_data['name_en'],
                    'abbr' => $state_data['abbr_en'],
                    'type' => $state_data['type'],
                ]);
            } else {
                $state = new State([
                    'name' => $state_data['name_en'],
                    'abbr' => $state_data['abbr_en'],
                    'type' => $state_data['type'],
                ]);
                $state->code = $state_data['code'];
                $state->save();
            }

            foreach ($state_data['islands'] as $city_data) {
                $city = $state->cities()->whereCode($city_data['code'])
                        ->first();
                if ( $city ) {
                    $city->update([
                        'name' => $city_data['name_en'],
                        'type' => $city_data['type'],
                        'lat' => $city_data['lat'],
                        'lng' => $city_data['lng'],
                    ]);
                } else {
                    $city = new City([
                        'name' => $city_data['name_en'],
                        'type' => $city_data['type'],
                        'lat' => $city_data['lat'],
                        'lng' => $city_data['lng'],
                    ]);
                    $city->code = $city_data['code'];
                    $state->cities()->save($city);
                }
            }
        }
    }

    protected $data = [
        [
            'code' => 'A', 'name' => 'ތިލަދުންމަތި އުތުރުބުރި', 'name_en' => 'Haa Alif', 'abbr' => 'ހއ', 'abbr_en' => 'HA', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'A01', 'type' => CityTypes::ISLAND, 'name' => 'ތުރާކުނު', 'name_en' => 'Thuraakunu', 'lat' => '7.103806', 'lng' => '72.898979', ],
                [ 'code' => 'A02', 'type' => CityTypes::ISLAND, 'name' => 'އުލިގަން', 'name_en' => 'Uligan', 'lat' => '7.083835', 'lng' => '72.925095', ],
                [ 'code' => 'A05', 'type' => CityTypes::ISLAND, 'name' => 'މޮޅަދޫ', 'name_en' => 'Molhadhoo', 'lat' => '7.016098', 'lng' => '72.996468', ],
                [ 'code' => 'A06', 'type' => CityTypes::ISLAND, 'name' => 'ހޯރަފުށި', 'name_en' => 'Hoarafushi', 'lat' => '6.981879', 'lng' => '72.896652', ],
                [ 'code' => 'A07', 'type' => CityTypes::ISLAND, 'name' => 'އިހަވަންދޫ', 'name_en' => 'Ihavandhoo', 'lat' => '6.953145', 'lng' => '72.926102', ],
                [ 'code' => 'A08', 'type' => CityTypes::ISLAND, 'name' => 'ކެލާ', 'name_en' => 'Kelaa', 'lat' => '6.951845', 'lng' => '73.215462', ],
                [ 'code' => 'A09', 'type' => CityTypes::ISLAND, 'name' => 'ވަށަފަރު', 'name_en' => 'Vashafaru', 'lat' => '6.898663', 'lng' => '73.164146', ],
                [ 'code' => 'A10', 'type' => CityTypes::ISLAND, 'name' => 'ދިއްދޫ', 'name_en' => 'Dhidhdhoo', 'lat' => '6.887120', 'lng' => '73.114044', ],
                [ 'code' => 'A11', 'type' => CityTypes::ISLAND, 'name' => 'ފިއްލަދޫ', 'name_en' => 'Filladhoo', 'lat' => '6.897072', 'lng' => '73.235268', ],
                [ 'code' => 'A12', 'type' => CityTypes::ISLAND, 'name' => 'މާރަންދޫ', 'name_en' => 'Maarandhoo', 'lat' => '6.852400', 'lng' => '72.984406', ],
                [ 'code' => 'A13', 'type' => CityTypes::ISLAND, 'name' => 'ތަކަންދޫ', 'name_en' => 'Thakandhoo', 'lat' => '6.844469', 'lng' => '72.995583', ],
                [ 'code' => 'A14', 'type' => CityTypes::ISLAND, 'name' => 'އުތީމު', 'name_en' => 'Utheemu', 'lat' => '6.835027', 'lng' => '73.112267', ],
                [ 'code' => 'A15', 'type' => CityTypes::ISLAND, 'name' => 'މުރައިދޫ', 'name_en' => 'Muraidhoo', 'lat' => '6.837774', 'lng' => '73.167145', ],
                [ 'code' => 'A16', 'type' => CityTypes::ISLAND, 'name' => 'ބާރަށް', 'name_en' => 'Baarah', 'lat' => '6.816916', 'lng' => '73.213150', ],
            ],
        ],
        [
            'code' => 'B', 'name' => 'ތިލަދުންމަތި ދެކުނުބުރި', 'name_en' => 'Haa Dhaalu', 'abbr' => 'ހދ', 'abbr_en' => 'HDh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'B01', 'type' => CityTypes::ISLAND, 'name' => 'ފަރިދޫ', 'name_en' => 'Faridhoo', 'lat' => '6.784964', 'lng' => '73.053902', ],
                [ 'code' => 'B02', 'type' => CityTypes::ISLAND, 'name' => 'ހަނިމާދޫ', 'name_en' => 'Hanimaadhoo', 'lat' => '6.766965', 'lng' => '73.177795', ],
                [ 'code' => 'B03', 'type' => CityTypes::ISLAND, 'name' => 'ފިނޭ', 'name_en' => 'Finey', 'lat' => '6.746074', 'lng' => '73.053513', ],
                [ 'code' => 'B04', 'type' => CityTypes::ISLAND, 'name' => 'ނައިވާދޫ', 'name_en' => 'Naivaadhoo', 'lat' => '6.747121', 'lng' => '72.934250', ],
                [ 'code' => 'B05', 'type' => CityTypes::ISLAND, 'name' => 'ހިރިމަރަދޫ', 'name_en' => 'Hirimaradhoo', 'lat' => '6.724919', 'lng' => '73.024124', ],
                [ 'code' => 'B06', 'type' => CityTypes::ISLAND, 'name' => 'ނޮޅިވަރަންފަރު', 'name_en' => 'Nolhivaranfaru', 'lat' => '6.693313', 'lng' => '73.120621', ],
                [ 'code' => 'B07', 'type' => CityTypes::ISLAND, 'name' => 'ނެއްލައިދޫ', 'name_en' => 'Nellaidhoo', 'lat' => '6.714940', 'lng' => '72.946831', ],
                [ 'code' => 'B08', 'type' => CityTypes::ISLAND, 'name' => 'ނޮޅިވަރަމް', 'name_en' => 'Nolhivaram', 'lat' => '6.662525', 'lng' => '73.082413', ],
                [ 'code' => 'B09', 'type' => CityTypes::ISLAND, 'name' => 'ކުރިނބި', 'name_en' => 'Kurinbi', 'lat' => '6.665513', 'lng' => '72.996597', ],
                [ 'code' => 'B10', 'type' => CityTypes::ISLAND, 'name' => 'ކުނބުރުދޫ', 'name_en' => 'Kunburudhoo', 'lat' => '6.658286', 'lng' => '73.027382', ],
                [ 'code' => 'B11', 'type' => CityTypes::ISLAND, 'name' => 'ކުޅުދުއްފުށި', 'name_en' => 'Kulhudhuffushi', 'lat' => '6.623269', 'lng' => '73.069458', ],
                [ 'code' => 'B12', 'type' => CityTypes::ISLAND, 'name' => 'ކުމުންދޫ', 'name_en' => 'Kumundhoo', 'lat' => '6.571084', 'lng' => '73.051636', ],
                [ 'code' => 'B13', 'type' => CityTypes::ISLAND, 'name' => 'ނޭކުރެންދޫ', 'name_en' => 'Neykurendhoo', 'lat' => '6.542908', 'lng' => '72.979805', ],
                [ 'code' => 'B14', 'type' => CityTypes::ISLAND, 'name' => 'ވައިކަރަދޫ', 'name_en' => 'Vaikaradhoo', 'lat' => '6.549727', 'lng' => '72.952866', ],
                [ 'code' => 'B15', 'type' => CityTypes::ISLAND, 'name' => 'މާވައިދޫ', 'name_en' => 'Maavaidhoo', 'lat' => '6.515374', 'lng' => '73.053780', ],
                [ 'code' => 'B16', 'type' => CityTypes::ISLAND, 'name' => 'މަކުނުދޫ', 'name_en' => 'Makunudhoo', 'lat' => '6.409141', 'lng' => '72.705200', ],
            ],
        ],
        [
            'code' => 'C', 'name' => 'މިލަދުންމަޑުލު އުތުރުބުރި', 'name_en' => 'Shaviyani', 'abbr' => 'ށ', 'abbr_en' => 'Sh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'C01', 'type' => CityTypes::ISLAND, 'name' => 'ކަނޑިތީމު', 'name_en' => 'Kanditheemu', 'lat' => '6.440665', 'lng' => '72.915695', ],
                [ 'code' => 'C02', 'type' => CityTypes::ISLAND, 'name' => 'ނޫމަރާ', 'name_en' => 'Noomaraa', 'lat' => '6.434146', 'lng' => '73.069023', ],
                [ 'code' => 'C03', 'type' => CityTypes::ISLAND, 'name' => 'ގޮއިދޫ', 'name_en' => 'Goidhoo', 'lat' => '6.430076', 'lng' => '72.929581', ],
                [ 'code' => 'C04', 'type' => CityTypes::ISLAND, 'name' => 'ފޭދޫ', 'name_en' => 'Feydhoo', 'lat' => '6.360313', 'lng' => '73.048714', ],
                [ 'code' => 'C05', 'type' => CityTypes::ISLAND, 'name' => 'ފީވައް', 'name_en' => 'Feevah', 'lat' => '6.348893', 'lng' => '73.209656', ],
                [ 'code' => 'C06', 'type' => CityTypes::ISLAND, 'name' => 'ބިލެތްފަހި', 'name_en' => 'Bilehffahi', 'lat' => '6.338018', 'lng' => '72.973839', ],
                [ 'code' => 'C07', 'type' => CityTypes::ISLAND, 'name' => 'ފޯކައިދޫ', 'name_en' => 'Foakaidhoo', 'lat' => '6.326729', 'lng' => '73.149292', ],
                [ 'code' => 'C08', 'type' => CityTypes::ISLAND, 'name' => 'ނަރުދޫ', 'name_en' => 'Narudhoo', 'lat' => '6.264328', 'lng' => '73.218102', ],
                [ 'code' => 'C09', 'type' => CityTypes::ISLAND, 'name' => 'މާކަނޑޫދޫ', 'name_en' => 'Maakandoodhoo', 'lat' => '6.234999', 'lng' => '73.267471', ],
                [ 'code' => 'C10', 'type' => CityTypes::ISLAND, 'name' => 'މަރޮށި', 'name_en' => 'Maroshi', 'lat' => '6.209848', 'lng' => '73.061096', ],
                [ 'code' => 'C11', 'type' => CityTypes::ISLAND, 'name' => 'ޅައިމަގު', 'name_en' => 'Lhaimagu', 'lat' => '6.163837', 'lng' => '73.251373', ],
                [ 'code' => 'C12', 'type' => CityTypes::ISLAND, 'name' => 'ފިރުނބައިދޫ', 'name_en' => 'Firubaidhoo', 'lat' => '6.114326', 'lng' => '73.227501', ],
                [ 'code' => 'C13', 'type' => CityTypes::ISLAND, 'name' => 'ކޮމަންޑޫ', 'name_en' => 'Komandoo', 'lat' => '6.055146', 'lng' => '73.054527', ],
                [ 'code' => 'C14', 'type' => CityTypes::ISLAND, 'name' => 'މާއުނގޫދޫ', 'name_en' => 'Maaungoodhoo', 'lat' => '6.038224', 'lng' => '73.284599', ],
                [ 'code' => 'C15', 'type' => CityTypes::ISLAND, 'name' => 'ފުނަދޫ', 'name_en' => 'Funadhoo', 'lat' => '6.150369', 'lng' => '73.290382', ],
                [ 'code' => 'C16', 'type' => CityTypes::ISLAND, 'name' => 'މިލަންދޫ', 'name_en' => 'Milandhoo', 'lat' => '6.288255', 'lng' => '73.245499', ],
            ],
        ],
        [
            'code' => 'D', 'name' => 'މިލަދުންމަޑުލު ދެކުނުބުރި', 'name_en' => 'Noonu', 'abbr' => 'ނ', 'abbr_en' => 'N', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'D01', 'type' => CityTypes::ISLAND, 'name' => 'ހެނބަދޫ', 'name_en' => 'Henbandhoo', 'lat' => '5.967417', 'lng' => '73.392906', ],
                [ 'code' => 'D02', 'type' => CityTypes::ISLAND, 'name' => 'ކެނދިކޮޅުދޫ', 'name_en' => 'Kendhikolhudhoo', 'lat' => '5.952303', 'lng' => '73.415199', ],
                [ 'code' => 'D04', 'type' => CityTypes::ISLAND, 'name' => 'މާޅެންދޫ', 'name_en' => 'Maalhendhoo', 'lat' => '5.902501', 'lng' => '73.456039', ],
                [ 'code' => 'D05', 'type' => CityTypes::ISLAND, 'name' => 'ކުޑަފަރީ', 'name_en' => 'Kudafari', 'lat' => '5.882637', 'lng' => '73.400452', ],
                [ 'code' => 'D06', 'type' => CityTypes::ISLAND, 'name' => 'ލަންދޫ', 'name_en' => 'Landhoo', 'lat' => '5.881740', 'lng' => '73.466438', ],
                [ 'code' => 'D07', 'type' => CityTypes::ISLAND, 'name' => 'މާފަރު', 'name_en' => 'Maafaru', 'lat' => '5.824048', 'lng' => '73.476540', ],
                [ 'code' => 'D08', 'type' => CityTypes::ISLAND, 'name' => 'ޅޮހި', 'name_en' => 'Lhohi', 'lat' => '5.815795', 'lng' => '73.377396', ],
                [ 'code' => 'D09', 'type' => CityTypes::ISLAND, 'name' => 'މިލަދޫ', 'name_en' => 'Miladhoo', 'lat' => '5.790502', 'lng' => '73.362320', ],
                [ 'code' => 'D10', 'type' => CityTypes::ISLAND, 'name' => 'މަގޫދޫ', 'name_en' => 'Magoodhoo', 'lat' => '5.776299', 'lng' => '73.362183', ],
                [ 'code' => 'D11', 'type' => CityTypes::ISLAND, 'name' => 'މަނަދޫ', 'name_en' => 'Manadhoo', 'lat' => '5.766205', 'lng' => '73.410973', ],
                [ 'code' => 'D12', 'type' => CityTypes::ISLAND, 'name' => 'ހޮޅުދޫ', 'name_en' => 'Holhudhoo', 'lat' => '5.754955', 'lng' => '73.262794', ],
                [ 'code' => 'D13', 'type' => CityTypes::ISLAND, 'name' => 'ފޮއްދޫ', 'name_en' => 'Fodhdhoo', 'lat' => '5.743517', 'lng' => '73.215790', ],
                [ 'code' => 'D14', 'type' => CityTypes::ISLAND, 'name' => 'ވެލިދޫ', 'name_en' => 'Velidhoo', 'lat' => '5.663899', 'lng' => '73.274239', ],
            ],
        ],
        [
            'code' => 'E', 'name' => 'މާޅޮސްމަޑުލު އުތުރުބުރި', 'name_en' => 'Raa', 'abbr' => 'ރ', 'abbr_en' => 'R', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'E01', 'type' => CityTypes::ISLAND, 'name' => 'އަލިފުށި', 'name_en' => 'Alifushi', 'lat' => '5.967254', 'lng' => '72.953033', ],
                [ 'code' => 'E02', 'type' => CityTypes::ISLAND, 'name' => 'ވާދޫ', 'name_en' => 'Vaadhoo', 'lat' => '5.855380', 'lng' => '72.991074', ],
                [ 'code' => 'E03', 'type' => CityTypes::ISLAND, 'name' => 'ރަސްގެތީމު', 'name_en' => 'Rasgetheemu', 'lat' => '5.807783', 'lng' => '73.003197', ],
                [ 'code' => 'E04', 'type' => CityTypes::ISLAND, 'name' => 'އަނގޮޅިތީމު', 'name_en' => 'Angolhitheemu', 'lat' => '5.792765', 'lng' => '73.006622', ],
                [ 'code' => 'E06', 'type' => CityTypes::ISLAND, 'name' => 'އުނގޫފާރު', 'name_en' => 'Ungoofaaru', 'lat' => '5.668157', 'lng' => '73.030151', ],
                [ 'code' => 'E08', 'type' => CityTypes::ISLAND, 'name' => 'މާކުރަތު', 'name_en' => 'Maakurathu', 'lat' => '5.606037', 'lng' => '73.043152', ],
                [ 'code' => 'E09', 'type' => CityTypes::ISLAND, 'name' => 'ރަސްމާދޫ', 'name_en' => 'Rasmaadhoo', 'lat' => '5.562959', 'lng' => '73.043480', ],
                [ 'code' => 'E10', 'type' => CityTypes::ISLAND, 'name' => 'އިންނަމާދޫ', 'name_en' => 'Innamaadhoo', 'lat' => '5.549437', 'lng' => '73.043800', ],
                [ 'code' => 'E11', 'type' => CityTypes::ISLAND, 'name' => 'މަޑުއްވަރީ', 'name_en' => 'Maduvvari', 'lat' => '5.486265', 'lng' => '72.896004', ],
                [ 'code' => 'E12', 'type' => CityTypes::ISLAND, 'name' => 'އިނގުރައިދޫ', 'name_en' => 'Inguraidhoo', 'lat' => '5.476875', 'lng' => '73.036301', ],
                [ 'code' => 'E13', 'type' => CityTypes::ISLAND, 'name' => 'ފައިނު', 'name_en' => 'Fainu', 'lat' => '5.463281', 'lng' => '73.034447', ],
                [ 'code' => 'E14', 'type' => CityTypes::ISLAND, 'name' => 'މީދޫ', 'name_en' => 'Meedhoo', 'lat' => '5.458021', 'lng' => '72.955284', ],
                [ 'code' => 'E15', 'type' => CityTypes::ISLAND, 'name' => 'ކިނޮޅަސް', 'name_en' => 'Kinolhas', 'lat' => '5.448406', 'lng' => '73.030502', ],
                [ 'code' => 'E05', 'type' => CityTypes::ISLAND, 'name' => 'ހުޅުދުއްފާރު', 'name_en' => 'Hulhudhuffaaru', 'lat' => '5.766033', 'lng' => '73.012619', ],
                [ 'code' => 'E', 'type' => CityTypes::ISLAND, 'name' => 'ދުވާފަރު', 'name_en' => 'Dhuvaafaru', 'lat' => '5.629225', 'lng' => '73.041939', ],
            ],
        ],
        [
            'code' => 'F', 'name' => 'މާޅޮސްމަޑުލު ދެކުނުބުރި', 'name_en' => 'Baa', 'abbr' => 'ބ', 'abbr_en' => 'B', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'F01', 'type' => CityTypes::ISLAND, 'name' => 'ކުޑަރިކިލު', 'name_en' => 'Kudarikilu', 'lat' => '5.300536', 'lng' => '73.070770', ],
                [ 'code' => 'F02', 'type' => CityTypes::ISLAND, 'name' => 'ކަމަދޫ', 'name_en' => 'Kamadhoo', 'lat' => '5.281926', 'lng' => '73.137016', ],
                [ 'code' => 'F03', 'type' => CityTypes::ISLAND, 'name' => 'ކެންދޫ', 'name_en' => 'Kendhoo', 'lat' => '5.275378', 'lng' => '73.010147', ],
                [ 'code' => 'F04', 'type' => CityTypes::ISLAND, 'name' => 'ކިހާދޫ', 'name_en' => 'Kihaadhoo', 'lat' => '5.215138', 'lng' => '73.125175', ],
                [ 'code' => 'F05', 'type' => CityTypes::ISLAND, 'name' => 'ދޮންފަނު', 'name_en' => 'Dhonfanu', 'lat' => '5.187738', 'lng' => '73.123047', ],
                [ 'code' => 'F06', 'type' => CityTypes::ISLAND, 'name' => 'ދަރަވަންދޫ', 'name_en' => 'Dharavandhoo', 'lat' => '5.157685', 'lng' => '73.130173', ],
                [ 'code' => 'F07', 'type' => CityTypes::ISLAND, 'name' => 'މާޅޮސް', 'name_en' => 'Maalhos', 'lat' => '5.134814', 'lng' => '73.108147', ],
                [ 'code' => 'F08', 'type' => CityTypes::ISLAND, 'name' => 'އޭދަފުށި', 'name_en' => 'Eydhafushi', 'lat' => '5.103483', 'lng' => '73.070396', ],
                [ 'code' => 'F09', 'type' => CityTypes::ISLAND, 'name' => 'ތުޅާދޫ', 'name_en' => 'Thulhaadhoo', 'lat' => '5.022891', 'lng' => '72.839981', ],
                [ 'code' => 'F10', 'type' => CityTypes::ISLAND, 'name' => 'ހިތާދޫ', 'name_en' => 'Hithaadhoo', 'lat' => '5.007428', 'lng' => '72.922791', ],
                [ 'code' => 'F11', 'type' => CityTypes::ISLAND, 'name' => 'ފުޅަދޫ', 'name_en' => 'Fulhadhoo', 'lat' => '4.884705', 'lng' => '72.934967', ],
                [ 'code' => 'F12', 'type' => CityTypes::ISLAND, 'name' => 'ފެހެންދޫ', 'name_en' => 'Fehendhoo', 'lat' => '4.882001', 'lng' => '72.967133', ],
                [ 'code' => 'F13', 'type' => CityTypes::ISLAND, 'name' => 'ގޮއިދޫ', 'name_en' => 'Goidhoo', 'lat' => '4.870928', 'lng' => '73.000053', ],
            ],
        ],
        [
            'code' => 'G', 'name' => 'ފާދިއްޕޮޅު', 'name_en' => 'Lhaviyani', 'abbr' => 'ޅ', 'abbr_en' => 'Lh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'G01', 'type' => CityTypes::ISLAND, 'name' => 'ހިންނަވަރު', 'name_en' => 'Hinnavaru', 'lat' => '5.491961', 'lng' => '73.412155', ],
                [ 'code' => 'G02', 'type' => CityTypes::ISLAND, 'name' => 'ނައިފަރު', 'name_en' => 'Naifaru', 'lat' => '5.445225', 'lng' => '73.365166', ],
                [ 'code' => 'G03', 'type' => CityTypes::ISLAND, 'name' => 'ކުރެންދޫ', 'name_en' => 'Kurendhoo', 'lat' => '5.333784', 'lng' => '73.463615', ],
                [ 'code' => 'G04', 'type' => CityTypes::ISLAND, 'name' => 'އޮޅުވެލިފުށި', 'name_en' => 'Olhuvelifushi', 'lat' => '5.278309', 'lng' => '73.606384', ],
                [ 'code' => 'G05', 'type' => CityTypes::ISLAND, 'name' => 'މާފިލާފުށި', 'name_en' => 'Maafilaafushi', 'lat' => '5.362965', 'lng' => '73.415276', ],
                [ 'code' => 'G', 'type' => CityTypes::ISLAND, 'name' => 'ފެލިވަރު', 'name_en' => 'Felivaru', 'lat' => '5.477100', 'lng' => '73.386635', ],
            ],
        ],
        [
            'code' => 'H', 'name' => 'މާލެ އަތޮޅު', 'name_en' => 'Kaafu', 'abbr' => 'ކ', 'abbr_en' => 'K', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'H01', 'type' => CityTypes::ISLAND, 'name' => 'ކާށިދޫ', 'name_en' => 'Kaashidhoo', 'lat' => '4.956995', 'lng' => '73.459610', ],
                [ 'code' => 'H02', 'type' => CityTypes::ISLAND, 'name' => 'ގާފަރު', 'name_en' => 'Gaafaru', 'lat' => '4.736115', 'lng' => '73.499847', ],
                [ 'code' => 'H03', 'type' => CityTypes::ISLAND, 'name' => 'ދިއްފުށި', 'name_en' => 'Dhiffushi', 'lat' => '4.442153', 'lng' => '73.713753', ],
                [ 'code' => 'H04', 'type' => CityTypes::ISLAND, 'name' => 'ތުލުސްދޫ', 'name_en' => 'Thulusdhoo', 'lat' => '4.374358', 'lng' => '73.650436', ],
                [ 'code' => 'H05', 'type' => CityTypes::ISLAND, 'name' => 'ހުރާ', 'name_en' => 'Huraa', 'lat' => '4.334328', 'lng' => '73.600693', ],
                [ 'code' => 'H06', 'type' => CityTypes::ISLAND, 'name' => 'ހިންމަފުށި', 'name_en' => 'Himmafushi', 'lat' => '4.308904', 'lng' => '73.571861', ],
                [ 'code' => 'H07', 'type' => CityTypes::ISLAND, 'name' => 'ގުޅި', 'name_en' => 'Gulhi', 'lat' => '3.990986', 'lng' => '73.509117', ],
                [ 'code' => 'H09', 'type' => CityTypes::ISLAND, 'name' => 'މާފުށި', 'name_en' => 'Maafushi', 'lat' => '3.941216', 'lng' => '73.489479', ],
                [ 'code' => 'H08', 'type' => CityTypes::ISLAND, 'name' => 'ގުރައިދޫ', 'name_en' => 'Guraidhoo', 'lat' => '3.900871', 'lng' => '73.468193', ],
            ],
        ],
        [
            'code' => 'U', 'name' => 'އަރިއަތޮޅު އުތުރުބުރި', 'name_en' => 'Alif Alif', 'abbr' => 'އއ', 'abbr_en' => 'AA', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'U01', 'type' => CityTypes::ISLAND, 'name' => 'ތޮއްޑޫ', 'name_en' => 'Thoddoo', 'lat' => '4.437322', 'lng' => '72.945633', ],
                [ 'code' => 'U02', 'type' => CityTypes::ISLAND, 'name' => 'ރަސްދޫ', 'name_en' => 'Rasdhoo', 'lat' => '4.263105', 'lng' => '72.991974', ],
                [ 'code' => 'U03', 'type' => CityTypes::ISLAND, 'name' => 'އުކުޅަސް', 'name_en' => 'Ukulhas', 'lat' => '4.214655', 'lng' => '72.863731', ],
                [ 'code' => 'U04', 'type' => CityTypes::ISLAND, 'name' => 'މަތިވެރި', 'name_en' => 'Mathiveri', 'lat' => '4.191563', 'lng' => '72.745804', ],
                [ 'code' => 'U05', 'type' => CityTypes::ISLAND, 'name' => 'ބޮޑުފޮޅުދޫ', 'name_en' => 'Bodufolhudhoo', 'lat' => '4.185455', 'lng' => '72.773476', ],
                [ 'code' => 'U06', 'type' => CityTypes::ISLAND, 'name' => 'ފެރިދޫ', 'name_en' => 'Feridhoo', 'lat' => '4.051082', 'lng' => '72.725159', ],
                [ 'code' => 'U07', 'type' => CityTypes::ISLAND, 'name' => 'މާޅޮސް', 'name_en' => 'Maalhos', 'lat' => '3.986480', 'lng' => '72.719627', ],
                [ 'code' => 'U08', 'type' => CityTypes::ISLAND, 'name' => 'ހިމަންދޫ', 'name_en' => 'Himandhoo', 'lat' => '3.921612', 'lng' => '72.744499', ],
            ],
        ],
        [
            'code' => 'I', 'name' => 'އަރިއަތޮޅު ދެކުނުބުރި', 'name_en' => 'Alif Dhaalu', 'abbr' => 'އދ', 'abbr_en' => 'Adh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'I01', 'type' => CityTypes::ISLAND, 'name' => 'ހަންޏާމީދޫ', 'name_en' => 'Hangnameedhoo', 'lat' => '3.849337', 'lng' => '72.955391', ],
                [ 'code' => 'I02', 'type' => CityTypes::ISLAND, 'name' => 'އޮމަދޫ', 'name_en' => 'Omadhoo', 'lat' => '3.791600', 'lng' => '72.961128', ],
                [ 'code' => 'I03', 'type' => CityTypes::ISLAND, 'name' => 'ކުނބުރުދޫ', 'name_en' => 'Kuburudhoo', 'lat' => '3.775587', 'lng' => '72.923691', ],
                [ 'code' => 'I04', 'type' => CityTypes::ISLAND, 'name' => 'މަހިބަދޫ', 'name_en' => 'Mahibadhoo', 'lat' => '3.757220', 'lng' => '72.969063', ],
                [ 'code' => 'I05', 'type' => CityTypes::ISLAND, 'name' => 'މަންދޫ', 'name_en' => 'Mandhoo', 'lat' => '3.697902', 'lng' => '72.710083', ],
                [ 'code' => 'I06', 'type' => CityTypes::ISLAND, 'name' => 'ދަނގެތި', 'name_en' => 'Dhangethi', 'lat' => '3.606805', 'lng' => '72.955406', ],
                [ 'code' => 'I07', 'type' => CityTypes::ISLAND, 'name' => 'ދިގުރަށް', 'name_en' => 'Dhigurah', 'lat' => '3.526920', 'lng' => '72.924103', ],
                [ 'code' => 'I08', 'type' => CityTypes::ISLAND, 'name' => 'ފެންފުށި', 'name_en' => 'Fenfushi', 'lat' => '3.489213', 'lng' => '72.782913', ],
                [ 'code' => 'I09', 'type' => CityTypes::ISLAND, 'name' => 'ދިއްދޫ', 'name_en' => 'Dhidhdhoo', 'lat' => '3.484906', 'lng' => '72.879501', ],
                [ 'code' => 'I10', 'type' => CityTypes::ISLAND, 'name' => 'މާމިގިލި', 'name_en' => 'Maamigili', 'lat' => '3.476670', 'lng' => '72.836914', ],
            ],
        ],
        [
            'code' => 'J', 'name' => 'ފެލިދެ އަތޮޅު', 'name_en' => 'Vaavu', 'abbr' => 'ވ', 'abbr_en' => 'V', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'J01', 'type' => CityTypes::ISLAND, 'name' => 'ފުލިދޫ', 'name_en' => 'Fulidhoo', 'lat' => '3.680477', 'lng' => '73.415787', ],
                [ 'code' => 'J02', 'type' => CityTypes::ISLAND, 'name' => 'ތިނަދޫ', 'name_en' => 'Thinadhoo', 'lat' => '3.487673', 'lng' => '73.537857', ],
                [ 'code' => 'J03', 'type' => CityTypes::ISLAND, 'name' => 'ފެލިދޫ', 'name_en' => 'Felidhoo', 'lat' => '3.471751', 'lng' => '73.547043', ],
                [ 'code' => 'J04', 'type' => CityTypes::ISLAND, 'name' => 'ކެޔޮދޫ', 'name_en' => 'Keyodhoo', 'lat' => '3.462117', 'lng' => '73.550110', ],
                [ 'code' => 'J05', 'type' => CityTypes::ISLAND, 'name' => 'ރަކީދޫ', 'name_en' => 'Rakeedhoo', 'lat' => '3.314940', 'lng' => '73.469826', ],
            ],
        ],
        [
            'code' => 'K', 'name' => 'މުލަކު އަތޮޅު', 'name_en' => 'Meemu', 'abbr' => 'މ', 'abbr_en' => 'M', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'K01', 'type' => CityTypes::ISLAND, 'name' => 'ރަތްމަންދޫ', 'name_en' => 'Raimandhoo', 'lat' => '3.089010', 'lng' => '73.639091', ],
                [ 'code' => 'K03', 'type' => CityTypes::ISLAND, 'name' => 'ވޭވަށް', 'name_en' => 'Veyvah', 'lat' => '2.955840', 'lng' => '73.599762', ],
                [ 'code' => 'K04', 'type' => CityTypes::ISLAND, 'name' => 'މުލައް', 'name_en' => 'Mulah', 'lat' => '2.946387', 'lng' => '73.584259', ],
                [ 'code' => 'K05', 'type' => CityTypes::ISLAND, 'name' => 'މުލި', 'name_en' => 'Muli', 'lat' => '2.918991', 'lng' => '73.581367', ],
                [ 'code' => 'K06', 'type' => CityTypes::ISLAND, 'name' => 'ނާލާފުށި', 'name_en' => 'Naalaafushi', 'lat' => '2.895659', 'lng' => '73.577400', ],
                [ 'code' => 'K07', 'type' => CityTypes::ISLAND, 'name' => 'ކޮޅުފުށި', 'name_en' => 'Kolhufushi', 'lat' => '2.780810', 'lng' => '73.424438', ],
                [ 'code' => 'K08', 'type' => CityTypes::ISLAND, 'name' => 'ދިއްގަރު', 'name_en' => 'Dhiggaru', 'lat' => '3.112102', 'lng' => '73.565254', ],
                [ 'code' => 'K09', 'type' => CityTypes::ISLAND, 'name' => 'މަޑުއްވަރި', 'name_en' => 'Maduvvari', 'lat' => '3.104647', 'lng' => '73.572899', ],
            ],
        ],
        [
            'code' => 'L', 'name' => 'ނިލަންދެ އަތޮޅު އުތުރުބުރި', 'name_en' => 'Faafu', 'abbr' => 'ފ', 'abbr_en' => 'F', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'L01', 'type' => CityTypes::ISLAND, 'name' => 'ފީއަލި', 'name_en' => 'Feeali', 'lat' => '3.269859', 'lng' => '73.002167', ],
                [ 'code' => 'L02', 'type' => CityTypes::ISLAND, 'name' => 'ބިލެތްދޫ', 'name_en' => 'Biledhdhoo', 'lat' => '3.118307', 'lng' => '72.984093', ],
                [ 'code' => 'L03', 'type' => CityTypes::ISLAND, 'name' => 'މަގޫދޫ', 'name_en' => 'Magoodhoo', 'lat' => '3.118307', 'lng' => '72.984093', ],
                [ 'code' => 'L04', 'type' => CityTypes::ISLAND, 'name' => 'ދަރަނބޫދޫ', 'name_en' => 'Dharaboodhoo', 'lat' => '3.078585', 'lng' => '72.963387', ],
                [ 'code' => 'L05', 'type' => CityTypes::ISLAND, 'name' => 'ނިލަންދޫ', 'name_en' => 'Nilandhoo', 'lat' => '3.063024', 'lng' => '72.925491', ],
            ],
        ],
        [
            'code' => 'M', 'name' => 'ނިލަންދެ އަތޮޅު ދެކުނުބުރި', 'name_en' => 'Dhaalu', 'abbr' => 'ދ', 'abbr_en' => 'Dh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'M01', 'type' => CityTypes::ISLAND, 'name' => 'މީދޫ', 'name_en' => 'Meedhoo', 'lat' => '2.998282', 'lng' => '73.006371', ],
                [ 'code' => 'M02', 'type' => CityTypes::ISLAND, 'name' => 'ބަނޑިދޫ', 'name_en' => 'Bandidhoo', 'lat' => '2.936484', 'lng' => '72.990761', ],
                [ 'code' => 'M03', 'type' => CityTypes::ISLAND, 'name' => 'ރިނބުދޫ', 'name_en' => 'Rinbudhoo', 'lat' => '2.925362', 'lng' => '72.894051', ],
                [ 'code' => 'M04', 'type' => CityTypes::ISLAND, 'name' => 'ހުޅުދެލި', 'name_en' => 'Hulhudheli', 'lat' => '2.858436', 'lng' => '72.846359', ],
                [ 'code' => 'M05', 'type' => CityTypes::ISLAND, 'name' => 'ގެމެންދޫ', 'name_en' => 'Gemendhoo', 'lat' => '2.803711', 'lng' => '73.026268', ],
                [ 'code' => 'M06', 'type' => CityTypes::ISLAND, 'name' => 'ވާނީ', 'name_en' => 'Vaanee', 'lat' => '2.725510', 'lng' => '73.003220', ],
                [ 'code' => 'M07', 'type' => CityTypes::ISLAND, 'name' => 'މާއެނބޫދޫ', 'name_en' => 'Maaenboodhoo', 'lat' => '2.695566', 'lng' => '72.963333', ],
                [ 'code' => 'M08', 'type' => CityTypes::ISLAND, 'name' => 'ކުޑަހުވަދޫ', 'name_en' => 'Kudahuvadhoo', 'lat' => '2.671707', 'lng' => '72.893715', ],
            ],
        ],
        [
            'code' => 'N', 'name' => 'ކޮޅުމަޑުލު', 'name_en' => 'Thaa', 'abbr' => 'ތ', 'abbr_en' => 'Th', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'N01', 'type' => CityTypes::ISLAND, 'name' => 'ބުރުނީ', 'name_en' => 'Buruni', 'lat' => '2.558714', 'lng' => '73.105995', ],
                [ 'code' => 'N02', 'type' => CityTypes::ISLAND, 'name' => 'ވިލުފުށި', 'name_en' => 'Vilufushi', 'lat' => '2.502818', 'lng' => '73.308228', ],
                [ 'code' => 'N03', 'type' => CityTypes::ISLAND, 'name' => 'މަޑިފުށި', 'name_en' => 'Madifushi', 'lat' => '2.356963', 'lng' => '73.354820', ],
                [ 'code' => 'N04', 'type' => CityTypes::ISLAND, 'name' => 'ދިޔަމިގިލި', 'name_en' => 'Dhiyamigili', 'lat' => '2.340187', 'lng' => '73.336929', ],
                [ 'code' => 'N05', 'type' => CityTypes::ISLAND, 'name' => 'ގުރައިދޫ', 'name_en' => 'Guraidhoo', 'lat' => '2.324968', 'lng' => '73.319420', ],
                [ 'code' => 'N06', 'type' => CityTypes::ISLAND, 'name' => 'ކަނޑޫދޫ', 'name_en' => 'Kandoodhoo', 'lat' => '2.321204', 'lng' => '72.916611', ],
                [ 'code' => 'N07', 'type' => CityTypes::ISLAND, 'name' => 'ވަންދޫ', 'name_en' => 'Vandhoo', 'lat' => '2.291691', 'lng' => '72.941994', ],
                [ 'code' => 'N08', 'type' => CityTypes::ISLAND, 'name' => 'ހިރިލަންދޫ', 'name_en' => 'Hirilandhoo', 'lat' => '2.269611', 'lng' => '72.932533', ],
                [ 'code' => 'N09', 'type' => CityTypes::ISLAND, 'name' => 'ގާދިއްފުށި', 'name_en' => 'Gaadhiffushi', 'lat' => '2.253139', 'lng' => '73.212402', ],
                [ 'code' => 'N10', 'type' => CityTypes::ISLAND, 'name' => 'ތިމަރަފުށި', 'name_en' => 'Thimarafushi', 'lat' => '2.205529', 'lng' => '73.142937', ],
                [ 'code' => 'N11', 'type' => CityTypes::ISLAND, 'name' => 'ވޭމަންޑޫ', 'name_en' => 'Veymandoo', 'lat' => '2.186917', 'lng' => '73.094460', ],
                [ 'code' => 'N12', 'type' => CityTypes::ISLAND, 'name' => 'ކިނބިދޫ', 'name_en' => 'Kinbidhoo', 'lat' => '2.168105', 'lng' => '73.066002', ],
                [ 'code' => 'N13', 'type' => CityTypes::ISLAND, 'name' => 'އޮމަދޫ', 'name_en' => 'Omadhoo', 'lat' => '2.167358', 'lng' => '73.023552', ],
            ],
        ],
        [
            'code' => 'O', 'name' => 'ހައްދުންމަތި', 'name_en' => 'Laamu', 'abbr' => 'ލ', 'abbr_en' => 'L', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'O01', 'type' => CityTypes::ISLAND, 'name' => 'އިސްދޫ', 'name_en' => 'Isdhoo', 'lat' => '2.119007', 'lng' => '73.568443', ],
                [ 'code' => 'O02', 'type' => CityTypes::ISLAND, 'name' => 'ދަނބިދޫ', 'name_en' => 'Dhabidhoo', 'lat' => '2.093724', 'lng' => '73.544342', ],
                [ 'code' => 'O03', 'type' => CityTypes::ISLAND, 'name' => 'މާބައިދޫ', 'name_en' => 'Maabaidhoo', 'lat' => '2.027370', 'lng' => '73.532516', ],
                [ 'code' => 'O04', 'type' => CityTypes::ISLAND, 'name' => 'މުންޑޫ', 'name_en' => 'Mundoo', 'lat' => '2.011052', 'lng' => '73.533737', ],
                [ 'code' => 'O05', 'type' => CityTypes::ISLAND, 'name' => 'ކަޅައިދޫ', 'name_en' => 'Kalhaidhoo', 'lat' => '1.988794', 'lng' => '73.537529', ],
                [ 'code' => 'O06', 'type' => CityTypes::ISLAND, 'name' => 'ގަން', 'name_en' => 'Gan', 'lat' => '1.911136', 'lng' => '73.539757', ],
                [ 'code' => 'O07', 'type' => CityTypes::ISLAND, 'name' => 'މާވަށް', 'name_en' => 'Maavah', 'lat' => '1.885328', 'lng' => '73.242348', ],
                [ 'code' => 'O08', 'type' => CityTypes::ISLAND, 'name' => 'ފޮނަދޫ', 'name_en' => 'Fonadhoo', 'lat' => '1.836324', 'lng' => '73.504242', ],
                [ 'code' => 'O09', 'type' => CityTypes::ISLAND, 'name' => 'ގާދޫ', 'name_en' => 'Gaadhoo', 'lat' => '1.821457', 'lng' => '73.452087', ],
                [ 'code' => 'O10', 'type' => CityTypes::ISLAND, 'name' => 'މާމެންދޫ', 'name_en' => 'Maamendhoo', 'lat' => '1.818038', 'lng' => '73.389870', ],
                [ 'code' => 'O11', 'type' => CityTypes::ISLAND, 'name' => 'ހިތަދޫ', 'name_en' => 'Hithadhoo', 'lat' => '1.795957', 'lng' => '73.388046', ],
                [ 'code' => 'O12', 'type' => CityTypes::ISLAND, 'name' => 'ކުނަހަންދޫ', 'name_en' => 'Kunahandhoo', 'lat' => '1.783307', 'lng' => '73.368362', ],
            ],
        ],
        [
            'code' => 'P', 'name' => 'ހުވަދު އަތޮޅު އުތުރުބުރި', 'name_en' => 'Gaafu Alifu', 'abbr' => 'ގއ', 'abbr_en' => 'GA', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'P01', 'type' => CityTypes::ISLAND, 'name' => 'ކޮލަމާފުށި', 'name_en' => 'Kolamaafushi', 'lat' => '0.837154', 'lng' => '73.185013', ],
                [ 'code' => 'P02', 'type' => CityTypes::ISLAND, 'name' => 'ވިލިނގިލި', 'name_en' => 'Villingili', 'lat' => '0.755293', 'lng' => '73.434883', ],
                [ 'code' => 'P03', 'type' => CityTypes::ISLAND, 'name' => 'މާމެންދޫ', 'name_en' => 'Maamendhoo', 'lat' => '0.713464', 'lng' => '73.438889', ],
                [ 'code' => 'P04', 'type' => CityTypes::ISLAND, 'name' => 'ނިލަންދޫ', 'name_en' => 'Nilandhoo', 'lat' => '0.636416', 'lng' => '73.446205', ],
                [ 'code' => 'P05', 'type' => CityTypes::ISLAND, 'name' => 'ދާންދޫ', 'name_en' => 'Dhaandhoo', 'lat' => '0.621114', 'lng' => '73.462097', ],
                [ 'code' => 'P06', 'type' => CityTypes::ISLAND, 'name' => 'ދެއްވަދޫ', 'name_en' => 'Dhevvadhoo', 'lat' => '0.556858', 'lng' => '73.242233', ],
                [ 'code' => 'P07', 'type' => CityTypes::ISLAND, 'name' => 'ކޮނޑޭ', 'name_en' => 'Kondey', 'lat' => '0.499554', 'lng' => '73.549652', ],
                [ 'code' => 'P08', 'type' => CityTypes::ISLAND, 'name' => 'ދިޔަދޫ', 'name_en' => 'Dhiyadhoo', 'lat' => '0.476468', 'lng' => '73.558968', ],
                [ 'code' => 'P09', 'type' => CityTypes::ISLAND, 'name' => 'ގެމަނަފުށި', 'name_en' => 'Gemanafushi', 'lat' => '0.442017', 'lng' => '73.568520', ],
                [ 'code' => 'P10', 'type' => CityTypes::ISLAND, 'name' => 'ކަނޑުހުޅުދޫ', 'name_en' => 'Kanduhulhudhoo', 'lat' => '0.351344', 'lng' => '73.539993', ],
            ],
        ],
        [
            'code' => 'Q', 'name' => 'ހުވަދު އަތޮޅު ދެކުނުބުރި', 'name_en' => 'Gaafu Dhaalu', 'abbr' => 'ގދ', 'abbr_en' => 'GDh', 'type' => StateTypes::ATOLL, 'islands' =>
            [
                [ 'code' => 'Q01', 'type' => CityTypes::ISLAND, 'name' => 'މަޑަވެލި', 'name_en' => 'Madaveli', 'lat' => '0.459010', 'lng' => '72.999237', ],
                [ 'code' => 'Q02', 'type' => CityTypes::ISLAND, 'name' => 'ހޯޑެއްދޫ', 'name_en' => 'Hoadedhdhoo', 'lat' => '0.444125', 'lng' => '73.003250', ],
                [ 'code' => 'Q03', 'type' => CityTypes::ISLAND, 'name' => 'ނަޑެއްލާ', 'name_en' => 'Nadellaa', 'lat' => '0.293590', 'lng' => '73.040726', ],
                [ 'code' => 'Q04', 'type' => CityTypes::ISLAND, 'name' => 'ގައްދޫ', 'name_en' => 'Gadhdhoo', 'lat' => '0.289472', 'lng' => '73.456436', ],
                [ 'code' => 'Q05', 'type' => CityTypes::ISLAND, 'name' => 'ރަތަފަންދޫ', 'name_en' => 'Rathafandhoo', 'lat' => '0.253248', 'lng' => '73.104874', ],
                [ 'code' => 'Q06', 'type' => CityTypes::ISLAND, 'name' => 'ވާދޫ', 'name_en' => 'Vaadhoo', 'lat' => '0.227282', 'lng' => '73.273880', ],
                [ 'code' => 'Q07', 'type' => CityTypes::ISLAND, 'name' => 'ފިޔޯރީ', 'name_en' => 'Fiyoari', 'lat' => '0.221270', 'lng' => '73.136642', ],
                [ 'code' => 'Q10', 'type' => CityTypes::ISLAND, 'name' => 'ތިނަދޫ', 'name_en' => 'Thinadhoo', 'lat' => '0.530185', 'lng' => '72.997231', ],
                [ 'code' => 'Q', 'type' => CityTypes::ISLAND, 'name' => 'ފަރެސްމާތޮޑާ', 'name_en' => 'Fares-Maathodaa', 'lat' => '0.199020', 'lng' => '73.189812', ],
            ],
        ],
        [
            'code' => 'R', 'name' => 'ފުއައްމުލައް ސިޓީ', 'name_en' => 'Fuvahmulah City', 'abbr' => 'ޏ', 'abbr_en' => 'Gn', 'type' => StateTypes::CITY, 'islands' =>
            [
                [ 'code' => 'R01', 'type' => CityTypes::WARD, 'name' => 'ދަނޑިމަގު', 'name_en' => 'Dhadimagu', 'lat' => '-0.285767', 'lng' => '73.413091', ],
                [ 'code' => 'R01', 'type' => CityTypes::WARD, 'name' => 'ދިގުވާނޑު', 'name_en' => 'Dhiguvaandu', 'lat' => '-0.292984', 'lng' => '73.419880', ],
                [ 'code' => 'R02', 'type' => CityTypes::WARD, 'name' => 'ހޯދަނޑު', 'name_en' => 'Hoadhadu', 'lat' => '-0.285220', 'lng' => '73.419807', ],
                [ 'code' => 'R03', 'type' => CityTypes::WARD, 'name' => 'މާދަނޑު', 'name_en' => 'Maadhadu', 'lat' => '-0.295190', 'lng' => '73.421757', ],
                [ 'code' => 'R04', 'type' => CityTypes::WARD, 'name' => 'މާލެގަން', 'name_en' => 'Maalegan', 'lat' => '-0.297179', 'lng' => '73.430337', ],
                [ 'code' => 'R05', 'type' => CityTypes::WARD, 'name' => 'މިސްކިތްމަގު', 'name_en' => 'Miskiy Magu', 'lat' => '-0.295667', 'lng' => '73.424052', ],
                [ 'code' => 'R06', 'type' => CityTypes::WARD, 'name' => 'ފުނާޑު', 'name_en' => 'Funaadu', 'lat' => '-0.304691', 'lng' => '73.429213', ],
                [ 'code' => 'R07', 'type' => CityTypes::WARD, 'name' => 'ދޫނޑިގަން', 'name_en' => 'Dhoondigan', 'lat' => '-0.307015', 'lng' => '73.435745', ],
            ],
        ],
        [
            'code' => 'S', 'name' => 'އައްޑޫ ސިޓީ', 'name_en' => 'Addu City', 'abbr' => 'ސ', 'abbr_en' => 'S', 'type' => StateTypes::CITY, 'islands' =>
            [
                [ 'code' => 'S01', 'type' => CityTypes::WARD, 'name' => 'މީދޫ', 'name_en' => 'Meedhoo', 'lat' => '-0.582437', 'lng' => '73.232323', ],
                [ 'code' => 'S02', 'type' => CityTypes::WARD, 'name' => 'ހިތަދޫ', 'name_en' => 'Hithadhoo', 'lat' => '-0.615665', 'lng' => '73.093651', ],
                [ 'code' => 'S03', 'type' => CityTypes::WARD, 'name' => 'މަރަދޫ', 'name_en' => 'Maradhoo', 'lat' => '-0.665054', 'lng' => '73.119102', ],
                [ 'code' => 'S04', 'type' => CityTypes::WARD, 'name' => 'ފޭދޫ', 'name_en' => 'Feydhoo', 'lat' => '-0.681034', 'lng' => '73.136391', ],
                [ 'code' => 'S05', 'type' => CityTypes::WARD, 'name' => 'މަރަދޫފޭދޫ', 'name_en' => 'Maradhoo-Feydhoo', 'lat' => '-0.673394', 'lng' => '73.125885', ],
                [ 'code' => 'S06', 'type' => CityTypes::WARD, 'name' => 'ހުޅުދޫ', 'name_en' => 'Hulhudhoo', 'lat' => '-0.592650', 'lng' => '73.227859', ],
                [ 'code' => 'S',   'type' => CityTypes::WARD, 'name' => 'ހުޅުމީދޫ', 'name_en' => 'Hulhumeedhoo', 'lat' => '-0.586900', 'lng' => '73.232262', ],
            ],
        ],
        [
            'code' => 'T', 'name' => 'މާލެ ސިޓީ', 'name_en' => 'Male\' City', 'abbr' => 'މާލެ', 'abbr_en' => 'K', 'type' => StateTypes::CITY, 'islands' =>
            [
                [ 'code' => 'H',   'type' => CityTypes::WARD, 'abbr' => 'ހ',  'name' => 'ހެންވެއިރު',  'name_en' => 'Henveiru', 'lat' => '4.174621', 'lng' => '73.516021' ],
                [ 'code' => 'M',   'type' => CityTypes::WARD, 'abbr' => 'މ',  'name' => 'މާފަންނު',   'name_en' => 'Maafannu', 'lat' => '4.175781', 'lng' => '73.504125' ],
                [ 'code' => 'Ma',  'type' => CityTypes::WARD, 'abbr' => 'މއ', 'name' => 'މައްޗަންގޮޅި', 'name_en' => 'Mahchangolhi', 'lat' => '4.173320', 'lng' => '73.507955' ],
                [ 'code' => 'G',   'type' => CityTypes::WARD, 'abbr' => 'ގ',  'name' => 'ގަލޮޅު',    'name_en' => 'Galolhu', 'lat' => '4.173191', 'lng' => '73.511683' ],
                [ 'code' => 'V',   'type' => CityTypes::WARD, 'abbr' => 'ވ',  'name' => 'ވިލިނގިލި',  'name_en' => 'Villingili', 'lat' => '4.173352', 'lng' => '73.485054' ],
                [ 'code' => 'HLh', 'type' => CityTypes::WARD, 'abbr' => '',   'name' => 'ހުޅުމާލެ',   'name_en' => 'Hulhumale\'', 'lat' => '4.212238', 'lng' => '73.539879' ]
            ],
        ],
    ];
}
