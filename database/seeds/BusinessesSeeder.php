<?php

use App\Currency;
use Illuminate\Database\Seeder;

class BusinessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Business::class, DatabaseSeeder::$num_models['businesses'])
            ->make()
            ->each( function (\App\Business $business) {
                $user_id = App\User::doesntHave('business')
                    ->limit(1)
                    ->value('id');

                $business->user_id = $user_id;
                $business->save();

                //select categories
                $categories = App\Category::inRandomOrder()
                    ->limit(rand(0, 5))
                    ->pluck('id')
                    ->all();

                $business->categories()->sync($categories);
            });
    }

}
