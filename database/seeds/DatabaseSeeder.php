<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(DefaultUsersSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(QuoteQuestionsSeeder::class);
        $this->call(SignupQuestionsSeeder::class);
        $this->call(ServiceProviderQuestionsSeeder::class);
        $this->call(TagsSeeder::class);
        $this->call(StatesCitiesSeeder::class);
    }
}
