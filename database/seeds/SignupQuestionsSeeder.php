<?php

use Illuminate\Database\Seeder;
use App\Models\SignupQuestion;

class SignupQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 10) as $item)
        {
            SignupQuestion::create([
                'question' => $faker->sentence
            ]);
        }
    }
}
