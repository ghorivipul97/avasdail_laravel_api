<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $role = Role::whereName(Role::USER)->first();
        foreach (range(1, 10) as $index) {
            $name = $faker->name($index %2 == 0 ? 'male' : 'female');

            $user = User::create([
                'name' => $name,
                'email' => $faker->email,
                'username' => (new User)->getUsername($name),
                'password' => bcrypt($name),
                'mobile_no' => $faker->phoneNumber
            ]);

            $user->attachRole($role);
        }

        $role = Role::whereName(Role::SERVICE_PROVIDER)->first();
        foreach (range(1, 10) as $index) {
            $name = $faker->name($index %2 == 0 ? 'male' : 'female');

            $user = User::create([
                'name' => $name,
                'email' => $faker->email,
                'username' => (new User)->getUsername($name),
                'password' => bcrypt($name),
                'mobile_no' => $faker->phoneNumber
            ]);

            $user->attachRole($role);
        }
    }
}
