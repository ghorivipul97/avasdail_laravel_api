<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    protected $data = [
        /**
         * ServiceRequests permissions
         */
        'service_requests' => [
            'edit_service_requests' => 'Edit service_requests',
            'delete_service_requests' => 'Delete service_requests',
            'edit_others_service_requests' => 'Edit others service_requests',
            'delete_others_service_requests' => 'Delete others service_requests',
            'view_service_requests' => 'View service_requests',
        ],

        /**
         * Reviews permissions
         */
        'reviews' => [
            'view_reviews' => 'View reviews',
            'edit_reviews' => 'Edit reviews',
            'delete_reviews' => 'Delete reviews',
            'edit_others_reviews' => 'Edit others reviews',
            'delete_others_reviews' => 'Delete others reviews',
            'approve_reviews' => 'Approve reviews',
        ],
        
        /**
         * OAuth Clients permissions
         */
        'oauth_clients' => [
            'view_oauth_clients' => 'View oauth clients',
            'edit_oauth_clients' => 'Edit oauth clients',
            'delete_oauth_clients' => 'Delete oauth clients',
            'edit_others_oauth_clients' => 'Edit others oauth clients',
            'delete_others_oauth_clients' => 'Delete others oauth clients',
        ],
        
        /**
         * Businesses permissions
         */
        'businesses' => [
            'view_businesses' => 'View businesses',
            'edit_businesses' => 'Edit businesses',
            'delete_businesses' => 'Delete businesses',
            'edit_others_businesses' => 'Edit others businesses',
            'delete_others_businesses' => 'Delete others businesses',
            'approve_businesses' => 'Approve businesses',
        ],

        /**
         * Categories permissions
         */
        'categories' => [
            'edit_categories' => 'Edit categories',
            'delete_categories' => 'Delete categories',
        ],

        /**
         * Users permissions
         */
        'users' => [
            'edit_users' => 'Edit users',
            'delete_users' => 'Delete users',
        ],

        /**
         * Roles permissions
         */
        'roles' => [
            'edit_roles' => 'Edit roles',
            'delete_roles' => 'Delete roles',
        ],

        /**
         * Settings permissions
         */
        'settings' => [
            'edit_settings' => 'Edit settings',
        ],

        /**
         * Admin permissions
         */
        'admin' => [
            'access_admin' => 'Access admin dashboard',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $model => $permissions) {
            foreach ($permissions as $name => $desc) {
                $permission = Permission::firstOrCreate(['name' => $name]);
                $permission->update(['description' => $desc, 'model' => $model]);
                $permission->save();
            }
        }
    }
}
