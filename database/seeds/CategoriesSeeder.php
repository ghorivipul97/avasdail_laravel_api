<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // add main categories
        foreach (range(1, 10) as $item) {
            $name = $faker->word;

            Category::create([
                'name' => $name,
                'slug' => (new Category())->getSlug($name),
                'icon' => '',
                'image' => '',
                'parent_id' => null
            ]);
        }

        $categories = Category::where('parent_id', null)->pluck('id')->toArray();
        foreach (range(1, 10) as $item) {
            $name = $faker->word;

            Category::create([
                'name' => $name,
                'slug' => (new Category())->getSlug($name),
                'icon' => '',
                'image' => '',
                'parent_id' => array_random($categories)
            ]);
        }
    }
}
