<?php

use App\Models\QuoteServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompletedTypeToQuoteServiceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE quote_service_providers CHANGE COLUMN status status ENUM(
            '". QuoteServiceProvider::NOT_QUOTED ."',
            '". QuoteServiceProvider::QUOTED ."',
            '". QuoteServiceProvider::QUOTE_ACCEPTED ."',
            '". QuoteServiceProvider::AWARDED ."',
            '". QuoteServiceProvider::QUOTE_WITHDRAWN ."',
            '". QuoteServiceProvider::COMPLETED . "')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE quote_service_providers CHANGE COLUMN status status ENUM(
            '". QuoteServiceProvider::NOT_QUOTED ."',
            '". QuoteServiceProvider::QUOTED ."',
            '". QuoteServiceProvider::QUOTE_ACCEPTED ."',
            '". QuoteServiceProvider::AWARDED ."',
            '". QuoteServiceProvider::QUOTE_WITHDRAWN ."')"
        );
    }
}
