<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsRequiredFieldToQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_questions', function (Blueprint $table) {
            $table->boolean('is_required')->after('question');
        });
        Schema::table('service_provider_questions', function (Blueprint $table) {
            $table->boolean('is_required')->after('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_questions', function (Blueprint $table) {
            $table->dropColumn('is_required');
        });
        Schema::table('service_provider_questions', function (Blueprint $table) {
            $table->dropColumn('is_required');
        });
    }
}
