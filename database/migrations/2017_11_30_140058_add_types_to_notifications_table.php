<?php

use App\Models\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE notifications CHANGE COLUMN parent_type type ENUM(
            '". Notification::REQUEST_QUOTE ."',
            '". Notification::PROVIDER_SEND_QUOTE ."',
            '". Notification::ACCEPT_QUOTE ."',
            '". Notification::AWARD_JOB ."',
            '". Notification::AWARD_OTHER_JOB ."',
            '". Notification::COMPLETE_JOB . "')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE notifications CHANGE COLUMN type parent_type ENUM('quote')");
    }
}
