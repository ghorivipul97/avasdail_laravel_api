<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_quote_id');
            $table->foreign('user_quote_id')->references('id')->on('user_quotes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('quote_question_id');
            $table->foreign('quote_question_id')->references('id')->on('quote_questions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->text('answer');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_answers');
    }
}
