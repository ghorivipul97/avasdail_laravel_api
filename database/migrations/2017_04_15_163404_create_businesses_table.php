<?php

use App\Helpers\Enums\BusinessStatuses;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->unsignedInteger('status')->default(BusinessStatuses::APPROVED)->index();
            $table->string('name');
            $table->string('address')->default('');
            $table->string('phone')->index();
            $table->string('website')->default('');
            $table->string('email')->default('');
            $table->text('description')->default('');
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
