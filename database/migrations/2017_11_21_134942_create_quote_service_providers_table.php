<?php

use App\Models\QuoteServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteServiceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_service_providers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_quote_id');
            $table->foreign('user_quote_id')->references('id')->on('user_quotes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('service_provider_id');
            $table->foreign('service_provider_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->string('amount')->nullable();
            $table->enum('status', QuoteServiceProvider::$statuses)->default(QuoteServiceProvider::NOT_QUOTED);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_service_providers');
    }
}
