<?php

use App\Helpers\Enums\QuestionTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
//            $table->unsignedInteger('menu_order')->default(0)->index();
            $table->string('title');
//            $table->unsignedInteger('type')->default(QuestionTypes::TEXT)->index();
            $table->text('options');
//            $table->boolean('required')->default(false);
//            $table->boolean('allow_other')->default(false);
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
