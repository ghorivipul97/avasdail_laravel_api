<?php

use App\Models\UserQuote;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddCompletedStatusToUserQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE user_quotes CHANGE COLUMN status status ENUM(
            '". UserQuote::OPEN ."',
            '". UserQuote::COMPLETED ."',
            '". UserQuote::EXPIRED."',
            '". UserQuote::AWARDED . "')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE user_quotes CHANGE COLUMN status status ENUM(
            '". UserQuote::OPEN ."',
            '". UserQuote::COMPLETED ."',
            '". UserQuote::EXPIRED . "')"
        );
    }
}
