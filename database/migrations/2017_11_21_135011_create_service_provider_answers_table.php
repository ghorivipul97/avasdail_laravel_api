<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProviderAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('quote_service_provider_id');
            $table->foreign('quote_service_provider_id')->references('id')->on('quote_service_providers')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('service_provider_question_id');
            $table->foreign('service_provider_question_id')->references('id')->on('service_provider_questions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->text('answer');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_answers');
    }
}
