<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtpFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('verification_otp')->nullable()->after('is_login');
            $table->boolean('is_verified')->default(true)->after('verification_otp');
            $table->string('forgot_password_otp')->nullable()->after('is_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verification_otp');
            $table->dropColumn('is_verified');
            $table->dropColumn('forgot_password_otp');
        });
    }
}
