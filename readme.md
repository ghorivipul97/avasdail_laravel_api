# AvasDial

AvasDial API Server

## Installation

1. Copy all the files to server
2. Create a MySQL database
3. Add MySQL database SMTP mail server details and to .env file
4. Set the server document root to /public directory
5. Unzip resources/material-admin.zip to public/material-admin
6. Run the following commands inside the server directory

```
composer install
composer update
php artisan key:generate
php artisan migrate --seed
php artisan passport:install
```

## Credits

- [Arushad Ahmed (@dash8x)](http://arushad.org)

Based on [Laravel 5.4.x](http://laravel.com)

## About Javaabu

[Javaabu](http://javaabu.com) is a small software development firm based in Maldives.
