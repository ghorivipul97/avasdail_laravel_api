<?php
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function () {
    Route::middleware('guest:admin')->group(function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('post.login');

        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')
            ->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')
            ->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')
            ->name('password.reset.token');
        Route::post('password/reset', 'ResetPasswordController@reset')
            ->name('password.reset');
    });

    Route::middleware('auth:admin')->group(function () {
        Route::post('logout', 'LoginController@adminLogout')->name('logout');

        /**
         * Home route
         */
        Route::get('/home', function () {
            return view('layouts.admin');
        })->name('home');

        /**
         * Category route
         */
        Route::resource('categories', 'CategoriesController');

        /**
         * Subcategory route
         */
        Route::get('subcategories/{categoryId}', 'CategoriesController@subCategory')->name('subcategory');

        /**
         * State route
         */
        Route::resource('states', 'StatesController');

        /**
         * City route
         */
        Route::resource('cities', 'CitiesController');

        /**
         * SignupQuestion route
         */
        Route::resource('signup_questions', 'SignupQuestionsController');

        /**
         * QuoteQuestion route
         */
        Route::resource('quote_questions', 'QuoteQuestionsController');

        /**
         * Tag route
         */
        Route::resource('tags', 'TagsController');

        /**
         * Service Provider Question route
         */
        Route::resource('provider-questions', 'ProviderQuestionsController');

        /**
         * Setting route
         */
        Route::get('settings', 'SettingsController@index');
        Route::patch('settings', 'SettingsController@update');

        Route::get('feedback', 'FeedbacksController@index');
        Route::get('feedback/{feedbackId}/show', 'FeedbacksController@show');

        Route::get('report', 'ReportsController@index');
        Route::get('report/{feedbackId}/show', 'ReportsController@show');

        Route::resource('category-slider-images', 'SliderImagesController');
    });
});
