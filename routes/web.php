<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);

Route::group(['prefix' => 'api/v1', 'namespace' => 'Api'], function () {
    Route::get('signup-data', 'UsersController@signupData');

    Route::post('service-provider-signup', 'LoginController@serviceProviderSignup');
    Route::post('user-signup', 'LoginController@userSignup');
    Route::post('verify-user', 'LoginController@verifyUser');
    Route::post('resend-verify-otp', 'LoginController@resendVerifyOtp');
    Route::post('update-number', 'UsersController@updateNumber');
    Route::post('send-update-number-otp', 'UsersController@sendUpdateNumberOtp');

    Route::post('login', 'LoginController@login');
    Route::post('fb-login', 'LoginController@fbLogin');
    Route::post('google-login', 'LoginController@googleLogin');
    Route::post('twitter-login', 'LoginController@twitterLogin');

    Route::post('send-forgot-password-otp', 'ForgotPasswordController@sendOtp');
    Route::post('resend-forgot-password-otp', 'ForgotPasswordController@resendOtp');
    Route::post('reset-password', 'ForgotPasswordController@resetPassword');

    Route::get('categories', 'CategoriesController@categories');
    Route::get('sub-categories/{id}', 'CategoriesController@subCategories');

    Route::get('service-providers', 'UsersController@serviceProviders');
    Route::get('service-provider/{id}', 'UsersController@getServiceProviderDetail');

    Route::post('favourite-service-provider', 'UsersController@postFavouriteServiceProvider');
    Route::get('favourites/{serviceProviderId}', 'UsersController@getServiceProviderFavourites');

    Route::post('review', 'ReviewsController@addReview');
    Route::get('reviews/{userId}', 'ReviewsController@getReviews');

    Route::get('discover-service-providers', 'ServiceProviderController@discoverServiceProviders');

    Route::get('notifications/{userId}', 'NotificationController@getNotifications');

    Route::post('send-feedback', 'FeedbackController@postSendFeedback');
    Route::post('report', 'FeedbackController@postSendReport');

    Route::post('profile', 'UsersController@postProfile');
    Route::post('change-password', 'UsersController@changePassword');
    Route::post('change-location', 'UsersController@postChangeLocation');

    Route::get('quote-questions', 'QuestionsController@getQuoteQuestions');
    Route::post('quote-answer', 'AnswerController@postQuoteAnswer');

    Route::get('service-provider-jobs/{providerId}', 'JobsController@serviceProviderJobs');
    Route::get('provider-job-detail', 'JobsController@serviceProviderJobDetail');

    Route::get('service-provider-questions', 'QuestionsController@getServiceProviderQuestions');
    Route::post('service-provider-answers', 'AnswerController@postServiceProviderAnswers');

    Route::get('user-jobs/{userId}', 'JobsController@userJobs');
    Route::get('job-providers/{jobId}', 'JobsController@getJobProviders');
    Route::get('provider-answers', 'JobsController@providerAnswers');
    Route::post('accept-job', 'JobsController@acceptJob');
    Route::post('complete-job', 'JobsController@completeJob');

    Route::get('provider-accepted-jobs/{providerId}', 'JobsController@providerAcceptedJobs');
    Route::get('filter-providers', 'ServiceProviderController@filterProviders');

    Route::get('users', 'UsersController@allUsers');
    Route::get('reset-users', 'UsersController@resetUsers');
    Route::get('jobs', 'UsersController@allJobs');
    Route::get('delete-jobs', 'UsersController@deleteJobs');
    Route::get('delete-notifications', 'UsersController@deleteNotifications');
    Route::get('expire-job', 'UsersController@expireJobs');

    Route::post('update-provider-answers', 'ServiceProviderController@updateProviderAnswers');
    Route::post('logout', 'UsersController@logout');

    Route::post('send-notification', 'NotificationController@send');
    Route::get('send-notification-otp/{id}', 'NotificationController@sendotp');
    Route::get('update-number/{id}/{number}', 'NotificationController@updateNumber');
});