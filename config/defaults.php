<?php

return [

    /**
     * Defaults
     */
    'app_name' => env('APP_NAME', 'Laravel'),
    'default_avatar' => 'img/user.png',
    'admin_login_logo' => 'img/logo.png',
    'admin_header_logo' => 'img/header-logo.png',
    'admin_menu_background' => 'img/admin-menu-background.png',
    'max_upload_file_size' => 1024 * 2, //2MB
    'max_image_file_size' => 1024 * 2, //2MB
    'max_num_files' => 5,
    'mobile_number_token_validity' => 30, //in minutes
    'mobile_number_max_attempts' => 5,
    'mobile_number_attempt_expiry' => 30, //in minutes
];
